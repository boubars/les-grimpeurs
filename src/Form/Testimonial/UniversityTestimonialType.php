<?php

namespace App\Form\Testimonial;

use App\Entity\Testimonial\UniversityTestimonial;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UniversityTestimonialType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('published')
            ->add('content', TextareaType::class,[
                'attr' => [
                    'maxlength' => 280
                ]
            ])
            ->add('thumbnail', 
                FileType::class, 
                [
                    'mapped'   => false,
                    'required' => false,
                ]
            )
            ->add('name')
            ->add('country')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UniversityTestimonial::class,
        ]);
    }
}
