<?php
namespace App\Form\EventListener\Admin;


use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormBuilder;

class OrganizationTypeEventListener {
    
    public function __construct()
    {
     
    }

    public function init(FormBuilder &$builder){
      $this->handleFormPresetData($builder);
    }
    private function handleFormPresetData(&$builder){
      $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
        $postedData = $event->getData();
        $formData = $event->getForm()->getData();
        
        if(in_array(0, $postedData['countryAction'])){
          //$formData->setCountryAction(null);
          //dd($formData);
        }

        $event->setData($postedData);
          
      });

  }
}