<?php
namespace App\Form\EventListener\Admin;

use App\Entity\Common\Continent;
use App\Entity\Common\Country;
use App\Entity\Discipline\Discipline;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationType;
use App\Entity\User\User;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormBuilder;
use App\Service\Lang\Translation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ScholarshipTypeEventListener {
    /**
     * @var UserRepository 
     **/
    private $organizationManager;
    private $disciplineManager;
    private $countryManager;
    private $translations;
    private $institutionTypeManager;

    public function __construct(EntityManagerInterface $entityManager, Translation $translation)
    {
      $this->organizationManager    = $entityManager->getRepository(Organization::class);
      $this->institutionTypeManager = $entityManager->getRepository(OrganizationType::class);
      $this->countryManager = $entityManager->getRepository(Country::class);
      $this->disciplineManager = $entityManager->getRepository(Discipline::class);
      $this->translations = $translation->getResource();
    }

    public function init(FormBuilder &$builder){
      /*** add default relatedOrganizationField */
      $organizationType = $builder->getForm()->get("awardOrganizationType")->getData();
      $organizationType = $organizationType ?  $organizationType : $this->institutionTypeManager->findOneById(1);  
      $builder->add('relatedOrganization', EntityType::class,[
        'class' => Organization::class,
        'choices' => $this->organizationManager->findByInstitutionType($organizationType),
        'choice_label' => function($relatedOrganization){
            return $relatedOrganization->getName();
        }
      ]);

      /** on organization type change, refresh relatedOrganizationList */
      $builder->get('awardOrganizationType')->addEventListener(
          FormEvents::POST_SUBMIT,
          function (FormEvent $event) {
              $form = $event->getForm();
              $this->addRelatedOrganizationField($form->getParent(), $form->getData());
          }
      );
      
      /** refresh granted country list on granted continent change */
      $builder->get('grantedContinent')->addEventListener(
          FormEvents::POST_SUBMIT,
          function (FormEvent $event) {
              $form = $event->getForm();
              $this->refreshGrantedCountries($form->getParent(), $form->getData());
          }
      );

      /** refresh discipline list on category change */
      $builder->get('studyField')->addEventListener(
          FormEvents::POST_SUBMIT,
          function (FormEvent $event) {
              $form = $event->getForm();
              $this->refreshStudyFieldTags($form->getParent(), $form->getData());
          }
      );



    }
   
    /*** refresh list on change */
    private function addRelatedOrganizationField(Form $form, ?OrganizationType $type){
      $builder = $form->getConfig()->getFormFactory()->createNamedBuilder(
          'relatedOrganization',
          EntityType::class,
          null,
          [
              'class'           => Organization::class,
              'choices'         => $this->organizationManager->findByInstitutionType($type),
              'choice_label'    => 'name',
              'auto_initialize' => false,
              'required'        => true
          ]
      );
      $form->add($builder->getForm());
    }
  
    /*** refresh list on continent change */
    private function refreshGrantedCountries(Form $form, $continents){
      $continentIds = [];
      foreach (($continents ?? [] ) as $key => $continent) {
        $continentIds[] = $continent->getId();
      }

      $choices = empty($continentIds) ? $this->countryManager->findAll() : $this->countryManager->getByContinents($continentIds);
      $builder = $form->getConfig()->getFormFactory()->createNamedBuilder(
          'grantedCountries',
          EntityType::class,
          null,
          [
              'class'           => Country::class,
              'choices'         => $choices,
              'choice_label' => function($grantedCountries){
                return $this->translations['country'][$grantedCountries->getId()];
              },
              'auto_initialize' => false,
              'required'        => false,
              'multiple'        => true
          ]
      );
      $form->add($builder->getForm());
    }


    /*** refresh list on discipline change */
    private function refreshStudyFieldTags(Form $form, $categories){
      $categIds = [];
      foreach (($categories ?? []) as $key => $categ) {
        $categIds[] = $categ->getId();
      }

      $choices = empty($categIds) ? $this->disciplineManager->findAll() : $this->disciplineManager->getByCateIds($categIds);
      $builder = $form->getConfig()->getFormFactory()->createNamedBuilder(
          'studyFieldTags',
          EntityType::class,
          null,
          [
              'class'           => Discipline::class,
              'choices'         => $choices,
              'choice_label' => function($studyFieldTags){
                $name = $studyFieldTags->getName();
                $id   = $studyFieldTags->getId();
                $label = $this->translations['discipline'][$id] ?? $name;
                return ucfirst(strtolower($label));
              },
              'auto_initialize' => false,
              'required'        => false,
              'multiple'        => true
          ]
      );
      $form->add($builder->getForm());
    }
  }
  