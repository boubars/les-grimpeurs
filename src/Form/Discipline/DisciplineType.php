<?php

namespace App\Form\Discipline;

use App\Entity\Discipline\Category;
use App\Entity\Discipline\Discipline;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DisciplineType extends AbstractType
{
    private $locale;
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->locale = $options['locale'];
        $builder
            ->add('name')
            ->add('nameFr')
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => function($category){
                    $label = $this->locale == 'en' ? $category->getName() : $category->getNameFr();
                    return ucfirst(strtolower($label));
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Discipline::class,
        ]);
        $resolver->setRequired("locale");
    }
}
