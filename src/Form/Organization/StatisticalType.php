<?php

namespace App\Form\Organization;

use App\Entity\Organization\Statistical;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StatisticalType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('scholarshipAwarded')
            ->add('acceptanceRate')
            ->add('menCount')
            ->add('womenCount')
            ->add('scholarshipCount')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Statistical::class,
        ]);
    }
}
