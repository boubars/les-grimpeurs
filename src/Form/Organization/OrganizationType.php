<?php

namespace App\Form\Organization;

use App\Entity\Common\Country;
use App\Entity\Organization\Organization;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrganizationType extends AbstractType
{
   
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $context = $options['context'];

        switch($context){
            case 'register':
                $builder->add('email',EmailType::class,[
                        'required' => true
                ]) 
                ->add('roles')
                ->add('password');
            break;

            case 'adresse' : 
                $builder
                ->add('adresse')
                ->add('postalCode')
                ->add('contactName')
                ->add('avatar')
                ->add('phoneNumber')
                ->add('organizationEmail');
            break;

            case 'infos' : 
                $builder
                ->add('yearOfCreation')
                ->add('siretNumber')
                ->add('presidentName')
                ->add('avatar')
                ->add('website')
                ->add('accreditation')
                ->add('institutionType')
                ->add('description');
            break;
        }
            /*
            //->add('name')
            ->add('logo')
            ->add('')
            ->add('')
            ->add('')
            ->add('')
            ->add('')
            ->add('')
            ->add('')
           
            ->add('coverPicture')
            ->add('addedBy')
            
            ->add('statistical')
            ->add('socialNetwork')
            ->add('gallery')
            */
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Organization::class,
        ]);
        $resolver->setRequired("context");
    }

}
