<?php

namespace App\Form\University;

use App\Entity\Common\Country;
use App\Entity\University\University;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UniversityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            //->add('roles')
            ->add('password')
            ->add('enable')
            //->add('token')
            ->add('firstname')
            ->add('lastname')
            ->add('dateOfBirth', DateType::class, [
                'widget'   => 'single_text',
                'required' => false
            ])
            ->add('phone')
            ->add('country', EntityType::class,[
                'class' => Country::class,
                'choice_label'  => function($country){
                    return $country->getName();
                }
            ])
            ->add('adress')
            ->add('name')
            ->add('logo', FileType::class, ['mapped'=>false])
            ->add('yearOfCreation')
            ->add('siretNumber')
            ->add('presidentName')
            ->add('website')
            ->add('institutionType', ChoiceType::class, [
                'choices' => [
                    'Université' => 'University',
                    'Organization'  => 'Organization'
                ]
            ])
            ->add('description')
            //->add('favoriteScholarship')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => University::class,
        ]);
    }
}
