<?php

namespace App\Form\Admin;

use App\Entity\Common\Continent;
use App\Entity\Common\Country;
use App\Entity\Common\Currency;
use App\Entity\Common\Level;
use App\Entity\Common\Nationality;
use App\Entity\Discipline\Category;
use App\Entity\Discipline\Discipline;
use App\Entity\Organization\OrganizationType;
use App\Entity\Scholarship\Scholarship;
use App\Entity\User\User;
use App\Form\Common\SocialNetworkType;
use App\Form\EventListener\Admin\ScholarshipTypeEventListener as Listener;
use App\Service\Lang\Translation;
use DateTime;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ScholarshipType extends AbstractType
{

    private $translations, $eventListner;

    public function __construct(Translation $translator, Listener $listener){
        $this->translator = $translator;        
        $this->eventListner = $listener;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->translations = $this->translator->getResource($options['locale']);
        $this->initFields($builder);
        $this->handleFormPresetData($builder);
        /*** init event listner  */
        $this->eventListner->init($builder);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'        => Scholarship::class,
            'validation_groups' => false
        ]);
        $resolver->setRequired("locale");
    }

    private function initFields(&$builder){

        $builder->add('name')
                ->add('faculty')
                ->add('amount')
                ->add('count')
                ->add('duration')
                ->add('minAge')
                ->add('maxAge')
                ->add('otherInfos')
                ->add('story')
                ->add('contactName'   ,    TextType::class, ['required' => false])
                ->add('contactEmail'  ,    TextType::class, ['required' => false])
                ->add('contactAdresse',    TextType::class, ['required' => false])
                ->add('contactPhone'  ,    TextType::class, ['required' => false])
                ->add('cadidateOptionUrl', UrlType::class,  ['required' => false])
                ->add('logo',              FileType::class, [ 'mapped'  => false, 'required' => false ])
                ->add('contactAvatar',     FileType::class, [ 'mapped'  => false, 'required' => false ])
                ->add('candidatureLimit',  DateType::class, [ 'widget' => 'single_text', 'empty_data' => '', 'required' => false])
            
                ->add('durationUnit', ChoiceType::class, [
                    'choices' => $this->translations['durations']
                ])
            
                ->add('grantedSex', ChoiceType::class,[
                    'choices' => $this->translations['sexe']
                ])
            
            ->add('siteUrl', UrlType::class, ['required' => false])
            ->add('contactAdresseMap')
           
            ->add('grantedNationalities', EntityType::class, [
                'class' => Nationality::class,
                'choice_label' => function($grantedNationalities){
                    return $this->translations['nationality'][$grantedNationalities->getId()];
                },
                'multiple' => true,
                'required' => false,
                'empty_data' => []
            ])
            ->add('studyField', EntityType::class, [
                'class' => Category::class,
                'choice_label' => function($studyField){
                    return ucfirst(strtolower($this->translations['discipline']['category'][$studyField->getId()]));
                },
                'multiple' => true,
                'required' => false,
                'empty_data' => []
            ])
            ->add('studyFieldTags', EntityType::class, [
                'class' => Discipline::class,
                'choice_label' => function($studyFieldTags){
                    $name = $studyFieldTags->getName();
                    $id   = $studyFieldTags->getId();
                    $label = $this->translations['discipline'][$id] ?? $name;
                    return ucfirst(strtolower($label));
                },
                'choice_attr' => function (Discipline $discipline, $key, $index) {
                    return ['data-categ' => $discipline->getCategory()->getId() ];
                },
                'multiple' => true,
                'required' => false,
                'empty_data' => []
            ])
            ->add('grantedContinent', EntityType::class, [
                'class' => Continent::class,
                'choice_label' => function($grantedContinent){
                    return $this->translations['continent'][$grantedContinent->getId()];
                },
                'multiple' => true,
                'empty_data' => [],
                'required' => false,
            ])
            ->add('grantedCountries', EntityType::class, [
                'class' => Country::class,
                'choice_label' => function($grantedCountries){
                    return $this->translations['country'][$grantedCountries->getId()];
                },
                'multiple' => true,
                'empty_data' => [],
                'required' => false
            ])
            ->add('grantedLevel', EntityType::class, [
                'class' => Level::class,
                'choice_label' => function($grantedLevel){
                    return $this->translations['level'][$grantedLevel->getId()];
                },
                'multiple' => true,
                'required' => false,
                'empty_data' => []
            ])
            ->add('socialNetwork', SocialNetworkType::class, [
                'block_prefix' => "organization_social_network"
            ])
            ->add('currency', EntityType::class,[
                'class' => Currency::class,
                'choice_label' => function($currency){
                    return $currency->getCode();
                }
            ])
            ->add('originCountry', EntityType::class,[
                'class' => Country::class,
                'choice_label' => function($originCountry){
                    return $this->translations['country'][$originCountry->getId()];
                }
            ])
            ->add('awardOrganizationType', EntityType::class,[
                'class' => OrganizationType::class,
                'choice_label' => function($awardOrganizationType){
                    return $this->translations['institutionType'][$awardOrganizationType->getId()];
                }
            ]);
    }

    private function handleFormPresetData(&$builder){

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $scholarship = $event->getData();
        });

    }


}
