<?php

namespace App\Form\Admin;

use App\Entity\Common\Country;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationType as OrganizationOrganizationType;
use App\Form\Common\SocialNetworkType;
use App\Form\DataTransformer\CountryListTransoformer;
use App\Form\Organization\StatisticalType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Service\Lang\Translation;
use App\Form\EventListener\Admin\OrganizationTypeEventListener as Listener;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class OrganizationType extends AbstractType
{
    private $translator;
    private $translations;


    public function __construct(Translation $translator,  Listener $listener){
        $this->translator = $translator;      
        $this->eventListner = $listener;       
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->translations = $this->translator->getResource($options['locale']);  

        $this->initFields($builder);
        //$this->eventListner->init($builder);
    }
    
    private function initFields(&$builder){
        $transformer = new CountryListTransoformer();
        
        $builder
        ->add('email')
        ->add('password')
        ->add('enable')
        ->add('name')
        ->add('adresseMap')
        
        ->add('firstname',         TextType::class, ['required' => false])
        ->add('lastname',          TextType::class, ['required' => false])
        ->add('phone',             TextType::class, ['required' => false])
        ->add('adress',            TextType::class, ['required' => false])
        ->add('yearOfCreation',    TextType::class, ['required' => false])
        ->add('siretNumber',       TextType::class, ['required' => false])
        ->add('presidentName',     TextType::class, ['required' => false])
        ->add('website',           UrlType::class,  ['required' => false])
        ->add('accreditation',     TextType::class, ['required' => false])
        ->add('description',       TextType::class, ['required' => false])
        ->add('adresse',           TextType::class, ['required' => false])
        ->add('postalCode',        TextType::class, ['required' => false])
        ->add('contactName',       TextType::class, ['required' => false])
        ->add('contactLastName',   TextType::class, ['required' => false])
        ->add('contactPostalCode', TextType::class, ['required' => false])
        ->add('phoneNumber',       TextType::class, ['required' => false])
        ->add('organizationEmail', TextType::class, ['required' => false])
        
        ->add('logo',              FileType::class,['mapped'   => false, 'required' => false])
        ->add('avatar',            FileType::class,['mapped'   => false, 'required' => false])
        ->add('coverPicture',      FileType::class,['mapped'   => false, 'required' => false])
        
        ->add('dateOfBirth',       DateType::class,['widget'   => 'single_text', 'required' => false ])

        

        ->add('contactCountry',    EntityType::class, [
            'class' => Country::class,
            'choice_label' => function($contactCountry){
                return $this->translations['country'][$contactCountry->getId()];
            },
            'mapped' => false
        ])

        ->add('country',       EntityType::class, [
            'class'            => Country::class,
            'choice_label'     => function($country){ return $this->translations['country'][$country->getId()];},
            'required'         => false,
        ])

        ->add('countryAction', EntityType::class, [
            'class'            => Country::class,
            'multiple'         => true,
            'choice_label'     => function($countryAction){ return $this->translations['country'][$countryAction->getId()]; },
            'empty_data'       => [],
            'required'         => false,
        ])

        ->add('statistical',   StatisticalType::class, [
            'block_prefix'     => "organization_statistical"
        ])

        ->add('socialNetwork', SocialNetworkType::class, [
            'block_prefix'     => "organization_social_network"
        ])

        //->add('gallery')
        ->add('institutionType', EntityType::class, [
            'class'              => OrganizationOrganizationType::class,
            'choice_label'       => function($institutionType){
                return $this->translations['institutionType'][$institutionType->getId()];
            }
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Organization::class,
        ]);
        $resolver->setRequired("locale");
    }
}
