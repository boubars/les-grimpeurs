<?php

namespace App\Form\Admin\Grimpeurs;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ValeursType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('image',FileType::class, [
                'label' => false,
                'required' => false,
                'data_class' => null,
                "attr" => [ 'class' => 'value_images hidden']
            ])
            ->add('title', TextType::class, [
                'label' => false,
                'required' => true
            ])
            ->add('text', TextareaType::class, [
                'label' => false,
                'required' => false,
            ])
        ;
    }
}
