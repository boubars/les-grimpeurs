<?php

namespace App\Form\Admin\Grimpeurs;

use App\Entity\Grimpeurs\Concept;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConceptType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lang', ChoiceType::class, [
                'choices' =>  [
                    'Français' => 'fr',
                    'Anglais'  => 'en'
                ]
            ])
            ->add('title')
            ->add('image',FileType::class, array('required' => false, 'data_class' => null))
            ->add('content', null, ['required' => false])
            ->add('valeurs', CollectionType::class, [
                'label' => false,
                'entry_type' => ValeursType::class,
                'block_prefix' => 'valeurs_entry',
                'prototype' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Concept::class,
        ]);
    }
}
