<?php

namespace App\Form\Admin\Grimpeurs\FAQ;

use App\Entity\Grimpeurs\FAQ\Question;
use App\Entity\Grimpeurs\FAQ\Thematic;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('answer')
            ->add('lang')
            ->add('thematic', EntityType::class, [
                'class'            => Thematic::class,
                'multiple'         => true,
                'choice_label'     => function($thematic){ 
                    return $thematic->getName();
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Question::class,
        ]);
    }
}
