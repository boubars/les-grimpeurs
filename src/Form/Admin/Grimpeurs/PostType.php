<?php

namespace App\Form\Admin\Grimpeurs;

use App\Entity\Grimpeurs\Post;
use App\Service\Lang\Translation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{

    private $translations;

    public function __construct(Translation $translator){
        $this->translator = $translator;        
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->translations = $this->translator->getResource($options['locale']);  
        $postType = $options['type'] ?? false ;

       
        switch ($postType) {
            case 'PRESS': 
                $this->addPresseFields($builder);
            break;
            
            default:
                # code...
                break;
        }
        
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => $this->translations['post_type']
            ])
            ->add('name')
            ->add('title')
            ->add('lang', ChoiceType::class, [
                'choices' => [
                    'Francais' => 'fr',
                    'Anglais'  => 'en'
                ]
            ])
            ->add('content')
            ->add('slug')
        ;
    }

    private function addPresseFields(FormBuilderInterface &$builder){
        $builder
        //->add('files')
        ->add('date', DateType::class,[
            'widget' => 'single_text'
        ])
        ->add('magazine');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
        $resolver->setRequired("locale");
        $resolver->setDefined("type");
    }
}
