<?php

namespace App\Form\Student;

use App\Entity\Common\Country;
use App\Entity\Common\Language;
use App\Entity\Common\Level;
use App\Entity\Common\Nationality;
use App\Entity\Discipline\Category;
use App\Entity\Student\Student;
use App\Form\Common\SocialNetworkType;
use App\Service\Lang\Translation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentType extends AbstractType
{
    private $translations;
    private  $translator;
    public function __construct(Translation $translator){
        $this->translator = $translator;        
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $lang = $options['locale'];
        $this->translations = $this->translator->getResource($lang);
       

        $builder
            /*** basic infos */
            ->add('firstname'   , TextType::class, ['required' => false])
            ->add('avatar'      , FileType::class,['mapped'   => false, 'required' => false])
            ->add('lastname'    , TextType::class, ['required' => false])
            ->add('dateOfBirth' , DateType::class,['widget'   => 'single_text', 'required' => false ])
            ->add('phone'       , TextType::class, ['required' => false])
            ->add('adress'      , TextType::class, ['required' => false])
            ->add('sexe'        , ChoiceType::class,[
                'choices' => $this->translations['sexe']
            ])
            
            /*** profil infos */
            ->add('siteUrl',    UrlType::class, ['required' => false])
            ->add('nationalities', EntityType::class, [
                'class' => Nationality::class,
                'choice_label' => function($nationalities){
                    return $this->translations['nationality'][$nationalities->getId()];
                },
                'multiple' => true,
                'required' => false,
                'empty_data' => []
            ])
            
            ->add('disciplines', EntityType::class, [
                'class' => Category::class,
                'choice_label' => function($disciplines){
                    return ucfirst(strtolower($this->translations['discipline']['category'][$disciplines->getId()]));
                },
                'multiple' => true,
                'required' => false,
                'empty_data' => []
            ])
            /*
            ->add('languages', EntityType::class, [
                'class' => Language::class,
                'choice_label' => function($languages){
                    return ucfirst(strtolower($this->translations['language'][$languages->getId()]['name']));
                },
                'multiple' => true,
                'required' => false,
                'empty_data' => []
            ])
            */
            ->add('country',  EntityType::class, [
                'class' => Country::class,
                'choice_label' => function($country){
                    return ucfirst(strtolower($this->translations['country'][$country->getId()]));
                },
                'required' => false,
                'empty_data' => []
            ])
            ->add('currentLevel', EntityType::class, [
                'class' => Level::class,
                'choice_label' => function($currentLevel){
                    return ucfirst(strtolower($this->translations['level'][$currentLevel->getId()]));
                },
                'required' => false,
                'empty_data' => []
            ])
            
            ->add('desiredStudyLevel', EntityType::class, [
                'class' => Level::class,
                'choice_label' => function($desiredStudyLevel){
                    return ucfirst(strtolower($this->translations['level'][$desiredStudyLevel->getId()]));
                },
                'required' => false,
                'empty_data' => []
            ])
            ->add('desiredDisciplines', EntityType::class, [
                'class' => Category::class,
                'choice_label' => function($desiredDisciplines){
                    return ucfirst(strtolower($this->translations['discipline']['category'][$desiredDisciplines->getId()]));
                },
                'multiple' => true,
                'required' => false,
                'empty_data' => []
            ])
            ->add('desiredCountries', EntityType::class, [
                'class' => Country::class,
                'choice_label' => function($desiredCountries){
                    return ucfirst(strtolower($this->translations['country'][$desiredCountries->getId()]));
                },
                'multiple' => true,
                'required' => false,
                'empty_data' => []
            ])
            

            ->add('socialNetwork', SocialNetworkType::class, [
                'block_prefix'     => "organization_social_network"
            ])
            ->add('email')
            ->add('password')
            ->add('enable')
            ->add('codeActivate');

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Student::class,
        ]);
        $resolver->setRequired("locale");
        
    }
}
