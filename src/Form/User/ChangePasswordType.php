<?php

namespace App\Form\User;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ChangePasswordType
{
    public function buildForm(FormBuilderInterface $builder)
    {
        $builder
            ->add('oldPassword', PasswordType::class, [
                'label' => 'Mot de passe actuelle',
                'attr' => ['class' => 'form-control mb-2'],
            ])
            ->add('newPassword', RepeatedType::class, [
                'label' => 'Nouveau mot de passe',
                'type' => PasswordType::class,
                'invalid_message' =>'Les deux mots de passe doivent être identiques',
                'options' => ['attr' => ['class' => 'form-control mb-2', 'placeholder' => '', 'pattern' => "(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"]],
                'required' => true,
                'first_options'  => ['label' => 'Nouveau mot de passe'],
                'second_options' => ['label' => 'Confirmer votre mot de passe'],
                'help' => 'Doit comporter 8 caractères ou plus, contenir au moins un chiffre, une lettre majuscule, une lettre minuscule et un caractère spécial.',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez entrer un mot de passe',
                    ]),
                    new Length([
                        'min' => 8,
                        'minMessage' => "Votre mot de passe doit contenir au moins '{{ limit }}' caractères",
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('logout', CheckboxType::class, ['required' => false])
        ;
        return  $builder->getForm();
    }
}
