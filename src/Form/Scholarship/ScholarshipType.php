<?php

namespace App\Form\Scholarship;

use App\Entity\Scholarship\Scholarship;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ScholarshipType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('logo')
            ->add('cadidateOption')
            ->add('cadidateOptionUrl')
            ->add('contactName')
            //->add('contactAvatar')
            ->add('contactEmail')
            ->add('contactAdresse')
            ->add('contactPhone')
            //->add('candidatureLimit')
            ->add('infosEmail')
            ->add('faculty')
            ->add('amount')
            ->add('count')
            ->add('duration')
            ->add('durationUnit')
            ->add('totalOffered')
            ->add('grantedSex')
            ->add('minAge')
            ->add('maxAge')
            ->add('otherInfos')
            //->add('grantedContinent')
            //->add('grantedNationality')
            ->add('story')
            ->add('siteUrl')
            //->add('attachments')
            //->add('studyFieldTags')
            //->add('selectionCriteria')
            //->add('advantages')
            //->add('studyField')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Scholarship::class,
        ]);
    }
}
