<?php
namespace App\EventSubscriber\User;

use App\Entity\Organization\Contact;
use App\Entity\Organization\Organization;
use App\Entity\Student\Student;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use App\Service\Mailer\Mailer;


class UserEventSubscriber implements EventSubscriber
{
     /**@var Mailer service*/
   private $mailer;

   private $entityManager;
   
   private $codeOld;

  public function __construct(Mailer $mailer)
  {
    $this->mailer = $mailer;
  }
  public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
            //Events::postUpdate,
            //Events::prePersist,
            //Events::postPersist
        ];
    }
    
    public function prePersist(LifecycleEventArgs $args)
    {
       


    }

    public function postPersist(LifecycleEventArgs $args){
        $user = $args->getObject();  
        if ($user instanceof Contact) {
            if (!$user->getEnable()){
                $this->mailer->sendUserActivationLink($user);
            }
        }elseif($user instanceof Student){
            if (!$user->getEnable()){
                $this->mailer->sendUserActivationCode($user);
            }
        }
    }

    /*
    public function postUpdate(LifecycleEventArgs $args)
    {
        $user = $args->getObject();  
        if(!$user->getEnable()){
            $this->mailer->sendUserActivationLink($user);
        }
        
    } */

            
}