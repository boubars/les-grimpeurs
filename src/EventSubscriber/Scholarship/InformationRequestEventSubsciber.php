<?php
namespace App\EventSubscriber\Scholarship;

use App\Entity\Organization\Organization;
use App\Entity\Scholarship\InformationRequest;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use App\Service\Mailer\Mailer;


class InformationRequestEventSubsciber implements EventSubscriber
{
  /**@var Mailer service*/
  private $mailer;

  public function __construct(Mailer $mailer)
  {
    $this->mailer = $mailer;
  }
  public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
            Events::prePersist,
        ];
    }
    public function prePersist(LifecycleEventArgs $args){
      $message = $args->getObject();  
        if ($message instanceof InformationRequest) {
          $message->setSentAt(new \DateTime());
        }
    }
    public function postPersist(LifecycleEventArgs $args){
        $message = $args->getObject();  
        if ($message instanceof InformationRequest) {
          $this->mailer->sendScholarshipRequestInfos($message);
        }
    }
}