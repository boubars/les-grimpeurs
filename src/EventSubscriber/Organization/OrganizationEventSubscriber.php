<?php 
namespace App\EventSubscriber\Organization;

use App\Entity\Organization\Organization;
use App\Repository\Organization\OrganizationRepository;
use DateTime;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\String\Slugger\SluggerInterface;

class OrganizationEventSubscriber implements EventSubscriber
{
  private $sluger;
  private $organizationManager;

  public function __construct(SluggerInterface $sluger, OrganizationRepository $organizationManager)
  {
    $this->sluger = $sluger;
    $this->organizationManager = $organizationManager;
  }
  public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate
            //Events::postPersist
        ];
    }

    public function prePersist(LifecycleEventArgs $args){
        $this->index($args);
    }
    public function preUpdate(LifecycleEventArgs $args){
        $this->index($args);
    }
    public function index(LifecycleEventArgs $args){
        $organization = $args->getObject();  
        if ($organization instanceof Organization) {
            /*** set role */
            /*** generate an unik slug */
            $this->generateSlug($organization);
        }
    }

    
    private function generateSlug(&$organization){
        $name = $organization->getName();
        $exist = $this->organizationManager->findOneByName($name);
        $slug  = null ; 
        $name  = mb_strtolower($name);
        if (function_exists('iconv')) {
            $name = iconv('utf-8', 'us-ascii//TRANSLIT', $name);
        }
        if($exist){
            if($organization->getId() == $exist->getId()){
                $slug = $this->sluger->slug($name);
            }else{
                $slug = $this->sluger->slug(
                    sprintf("%s-%s", $name, (new DateTime())->format("ymd"))
                );
            }
        }else{
            $slug = $this->sluger->slug($name);
        }
        $organization->setSlug($slug);
    }

            
}