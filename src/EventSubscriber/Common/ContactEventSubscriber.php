<?php
namespace App\EventSubscriber\Common;

use App\Entity\Common\Contact;
use App\Service\Mailer\Mailer;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\String\Slugger\SluggerInterface;


class ContactEventSubscriber implements EventSubscriber
{
    /**@var Mailer service*/
    private $mailer;
    private $slugger;

    public function __construct(Mailer $mailer, SluggerInterface $slugger)
    {
        $this->mailer = $mailer;
        $this->slugger = $slugger;
    }
    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
        ];
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function index(LifecycleEventArgs $args)
    {
        $page = $args->getObject();
        if ($page instanceof Contact) {
            $this->mailer->sendMessageContact($page);
        }
        if($page instanceof Article) {
            $page->computeSlug($this->slugger);
        }
    }
}