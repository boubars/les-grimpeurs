<?php

namespace App\Repository\Discipline;

use App\Entity\Discipline\Discipline ;
use App\Entity\Student\Student;
use App\Repository\EntityRepositoryTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Discipline |null find($id, $lockMode = null, $lockVersion = null)
 * @method Discipline |null findOneBy(array $criteria, array $orderBy = null)
 * @method Discipline []    findAll()
 * @method Discipline []    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DisciplineRepository extends ServiceEntityRepository
{
    use EntityRepositoryTrait;

    private $em;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        parent::__construct($registry, Discipline ::class);

    }

    public function findByStudent(Student $student){
        return $this->createQueryBuilder('d')
            ->join('d.desiredStudents', 's')
            ->andWhere()
            ->getQuery()->getResult();
    }

    // /**
    //  * @return Discipline [] Returns an array of Discipline  objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Discipline 
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function getByCateIds($categIds){
        return $qb = $this->createQueryBuilder('d')
            ->leftJoin("d.category","dc")
            ->where("dc.id IN (:ids)")
            ->setParameter('ids', $categIds)
            ->orderBy('d.name', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
