<?php

namespace App\Repository\User;

use App\Entity\User\FavoriteScholarship;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FavoriteScholarship|null find($id, $lockMode = null, $lockVersion = null)
 * @method FavoriteScholarship|null findOneBy(array $criteria, array $orderBy = null)
 * @method FavoriteScholarship[]    findAll()
 * @method FavoriteScholarship[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FavoriteScholarshipRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FavoriteScholarship::class);
    }

    // /**
    //  * @return FavoriteScholarship[] Returns an array of FavoriteScholarship objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FavoriteScholarship
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
