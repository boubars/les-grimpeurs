<?php

namespace App\Repository\User;

use App\Entity\User\User\BlacklistScholarship;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BlacklistScholarship|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlacklistScholarship|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlacklistScholarship[]    findAll()
 * @method BlacklistScholarship[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlacklistScholarshipRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BlacklistScholarship::class);
    }

    // /**
    //  * @return BlacklistScholarship[] Returns an array of BlacklistScholarship objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BlacklistScholarship
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
