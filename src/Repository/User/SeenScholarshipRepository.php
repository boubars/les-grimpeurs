<?php

namespace App\Repository\User;

use App\Entity\User\SeenScholarship;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SeenScholarship|null find($id, $lockMode = null, $lockVersion = null)
 * @method SeenScholarship|null findOneBy(array $criteria, array $orderBy = null)
 * @method SeenScholarship[]    findAll()
 * @method SeenScholarship[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SeenScholarshipRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SeenScholarship::class);
    }

    // /**
    //  * @return SeenScholarship[] Returns an array of SeenScholarship objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function countSeenByOrganization($organization_id)
    {
        return $this->createQueryBuilder('ss')
            ->select('count(DISTINCT ss.id) as nb')
            ->innerJoin('ss.scholarship', 's')
            ->leftJoin('s.relatedOrganization', 'o')
            ->andWhere('o.id = :oid')
            ->setParameter('oid', $organization_id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

}
