<?php

namespace App\Repository\Grimpeurs;

use App\Entity\Grimpeurs\ConceptValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConceptValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConceptValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConceptValue[]    findAll()
 * @method ConceptValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConceptValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConceptValue::class);
    }

    // /**
    //  * @return ConceptValue[] Returns an array of ConceptValue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConceptValue
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
