<?php

namespace App\Repository\Grimpeurs;

use App\Entity\Grimpeurs\Post;
use App\Repository\EntityRepositoryTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Page|null find($id, $lockMode = null, $lockVersion = null)
 * @method Page|null findOneBy(array $criteria, array $orderBy = null)
 * @method Page[]    findAll()
 * @method Page[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    use EntityRepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    /**
      * @return Article[] Returns an array of Post objects
      */
 
      public function getByCriteria($params)
      {
        $qb = $this->createQueryBuilder('p');
         
        $qbParams = [];
        
        foreach ($params as $key => $value) {

            if($key == "year" && $value && $value != "false"){
                $qb->andWhere("p.date like :year");
                $qbParams['year'] = "%$value%";
                continue;
            }

            if($this->getClassMetadata()->hasField($key)){
                $qb->andWhere("p.$key = :$key");
                $qbParams[$key] = $value;
            }
        }
        if(!empty($qbParams)){
            $qb->setParameters($qbParams);  
        }

        return $qb->getQuery()
              ->getResult()
          ;
      }
    

    /*
    public function findOneBySomeField($value): ?Page
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
