<?php

namespace App\Repository\Grimpeurs;

use App\Entity\Grimpeurs\SitePreference;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SitePreference|null find($id, $lockMode = null, $lockVersion = null)
 * @method SitePreference|null findOneBy(array $criteria, array $orderBy = null)
 * @method SitePreference[]    findAll()
 * @method SitePreference[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SitePreferenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SitePreference::class);
    }

    // /**
    //  * @return SitePreference[] Returns an array of SitePreference objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SitePreference
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
