<?php

namespace App\Repository\Testimonial;

use App\Entity\Testimonial\BaseTestimonial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BaseTestimonial|null find($id, $lockMode = null, $lockVersion = null)
 * @method BaseTestimonial|null findOneBy(array $criteria, array $orderBy = null)
 * @method BaseTestimonial[]    findAll()
 * @method BaseTestimonial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BaseTestimonialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BaseTestimonial::class);
    }

    // /**
    //  * @return BaseTestimonial[] Returns an array of BaseTestimonial objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BaseTestimonial
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
