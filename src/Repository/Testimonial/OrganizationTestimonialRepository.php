<?php

namespace App\Repository\Testimonial;

use App\Entity\Testimonial\OrganizationTestimonial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrganizationTestimonial|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrganizationTestimonial|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrganizationTestimonial[]    findAll()
 * @method OrganizationTestimonial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganizationTestimonialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrganizationTestimonial::class);
    }

    // /**
    //  * @return OrganizationTestimonial[] Returns an array of OrganizationTestimonial objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrganizationTestimonial
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
