<?php

namespace App\Repository\Testimonial;

use App\Entity\Testimonial\StudentTestimonial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StudentTestimonial|null find($id, $lockMode = null, $lockVersion = null)
 * @method StudentTestimonial|null findOneBy(array $criteria, array $orderBy = null)
 * @method StudentTestimonial[]    findAll()
 * @method StudentTestimonial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudentTestimonialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StudentTestimonial::class);
    }

    // /**
    //  * @return StudentTestimonial[] Returns an array of StudentTestimonial objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StudentTestimonial
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
