<?php

namespace App\Repository\Testimonial;

use App\Entity\Testimonial\UniversityTestimonial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UniversityTestimonial|null find($id, $lockMode = null, $lockVersion = null)
 * @method UniversityTestimonial|null findOneBy(array $criteria, array $orderBy = null)
 * @method UniversityTestimonial[]    findAll()
 * @method UniversityTestimonial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UniversityTestimonialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UniversityTestimonial::class);
    }

    // /**
    //  * @return UniversityTestimonial[] Returns an array of UniversityTestimonial objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UniversityTestimonial
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
