<?php

namespace App\Repository;
use Doctrine\ORM\Tools\Pagination\Paginator;

trait EntityRepositoryTrait
{
    public function findAllData($params , $limit = 15)
    {
        $page = $params['page'] ?? 1 ;
        $qb       = $this->createQueryBuilder('e'); 
        $qbParams = [];


        foreach ($params as $key => $value) {
            if($value == null){
                continue;
            }
            
            if ($key !== 'page' && 'q' != $key){
                if (is_string($value)) {
                    $qb->andWhere("e.$key like :$key");
                    $qbParams[$key] = '%'.$value.'%';
                } else {
                    $qb->andWhere("e.$key = :$key");
                    $qbParams[$key] = $value;
                }
            } elseif ('q' == $key) {
                $aFields = $this->getClassMetadata()->fieldNames;
                $orX = $qb->expr()->orX();
                foreach ($aFields as $field) {
                    if (!$this->getClassMetadata()->hasAssociation($field)) {
                        $orX->add("e.$field like :$key");
                    }

                }
                $qb->andWhere($orX);
                $qbParams[$key] = '%'.$value.'%';
            }
        }
        
        if(!empty($qbParams)){
            $qb->setParameters($qbParams);  
        }

        $query  = $qb->orderBy("e.id",'DESC')->getQuery();
        

        $paginator = new Paginator($query);
        $paginator->getQuery()
            ->setParameters($qbParams)
            ->setFirstResult($limit * ($page - 1)) 
            ->setMaxResults($limit);

        return $paginator;
        
    }

    public function paginate($query, $page, $limit = 15 , $walker = true){

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers($walker);
        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1)) 
            ->setMaxResults($limit);
        return $paginator;
    }
}