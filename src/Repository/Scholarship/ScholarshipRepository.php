<?php

namespace App\Repository\Scholarship;

use App\Entity\Common\Continent;
use App\Entity\Common\Country;
use App\Entity\Common\Level;
use App\Entity\Discipline\Category;
use App\Entity\Discipline\Discipline;
use App\Entity\Scholarship\Scholarship;
use App\Entity\Student\Student;
use App\Entity\University\University;
use App\Entity\User\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\EntityRepositoryTrait;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method Scholarship|null find($id, $lockMode = null, $lockVersion = null)
 * @method Scholarship|null findOneBy(array $criteria, array $orderBy = null)
 * @method Scholarship[]    findAll()
 * @method Scholarship[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScholarshipRepository extends ServiceEntityRepository
{
    use EntityRepositoryTrait;
    use ScholarshipStatTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Scholarship::class);
    }

    /**
     * @override 
     *
     * @param boolean|User $user
     * @return void
     */
    public function findAll($user =  false){
        $qb = $this->createQueryBuilder('s');

        /*** exclude backlist scholarship */
        $blacklistScholarships  =  $user ? $user->getBlacklistScholarship() : [];
        $blacklistScholarshipsIds = [];
        foreach ($blacklistScholarships as $scholarship){
            if(!in_array($scholarship->getId(), $blacklistScholarshipsIds)){
                $blacklistScholarshipsIds[] = $scholarship->getId();
            }
        }
        
        if(!empty($blacklistScholarshipsIds)){
            $qb->andWhere('s.id NOT IN (:blacklistIds)');
            $qb->setParameter("blacklistIds", $blacklistScholarshipsIds);
        }

        return $qb->getQuery()->getResult();

 
    }

    /**
     * @param array $countries
     * @param array $levels
     * @param array $disciplines
     * @param bool $institution
     * @param User|null $user
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findTotalAndCount($countries = [], $levels = [], $disciplines = [], $institution = false,User $user = null, $onlyFavorited = false): array
    {
        $qb = $this->createQueryBuilder('s')
            ->leftJoin('s.currency', 'cu')
            ->select('SUM(DISTINCT s.count) as count')
            ->addSelect('SUM(DISTINCT s.totalOffered * cu.usdRate) as total')
            ->addSelect('SUM(DISTINCT s.amount * cu.usdRate) as totalUnit')
            ->addSelect('count(DISTINCT s.id ) as nb');
        $parameters = [];
        if(!empty($countries)){
            $searchContinentIds = [];
            /** @var Country[] $countriesObj */
            $countriesObj = $this->getEntityManager()->getRepository(Country::class)->findBy(['id' => $countries]);
            foreach ($countriesObj as $countryObj){
                if($countryObj->getContinent() instanceof Continent){
                    $searchContinentIds[] = $countryObj->getContinent()->getId();
                }
            }
            $qb
                ->leftJoin('s.grantedCountries', 'c')
                ->leftJoin('s.grantedContinent', 'o')
                ->leftJoin('o.countries', 'oc')
                ->andWhere(
                    $qb->expr()->orX( $qb->expr()->orX(
                        $qb->expr()->orX(
                            $qb->expr()->in('c.id', ':searchCountries'),
                            $qb->expr()->in('oc.id', ':searchCountries')
                        ),
                        $qb->expr()->andX(
                            $qb->expr()->isNull('c'),
                            $qb->expr()->isNull('oc'),
                            $qb->expr()->in('o.id', ':searchContinentIds')
                        ),
                        $qb->expr()->andX(
                            $qb->expr()->isNull('c'),
                            $qb->expr()->isNull('oc'),
                            $qb->expr()->isNull('o')
                        )
                    )
                    )
                );
            $parameters['searchCountries'] = $countries;
            $parameters['searchContinentIds'] = $searchContinentIds;
        }
        if(!empty($levels)){
            $qb
                ->leftJoin('s.grantedLevel', 'l')
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->in('l.id', ':grantedLevel'),
                        $qb->expr()->isNull('l')
                    )
                );
            $parameters['grantedLevel'] = $levels;
        }
        if(!empty($disciplines)){
            $searchCategoryIds = [];
            /** @var Discipline[] $disciplinesObj */
            $disciplinesObj = $this->getEntityManager()->getRepository(Discipline::class)->findBy(['id' => $disciplines]);
            foreach ( $disciplinesObj as  $disciplineObj){
                if($disciplineObj->getCategory() instanceof Category){
                    $searchCategoryIds[] = $disciplineObj->getCategory()->getId();
                }
            }

            $qb
                ->leftJoin('s.studyFieldTags', 'd') //discipline
                ->leftJoin('s.studyField', 't')// category
                ->leftJoin('t.disciplines', 'i')
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->in('d.id',':studyFieldTags'),
                        $qb->expr()->andX(
                            $qb->expr()->isNull('d'),
                            $qb->expr()->orX(
                                $qb->expr()->in('t.id', ':searchCategoryIds'),
                                $qb->expr()->isNull('t'),
                            ),
                        )
                    )
                );

            $parameters['studyFieldTags']=  $disciplines;
            $parameters['searchCategoryIds'] = $searchCategoryIds;

        }

        if($institution){
            $qb->leftJoin("s.awardOrganizationType",'so')
            ->andWhere("so.name = :institution");
            $parameters['institution'] = $institution; 
        }
        if($onlyFavorited){
            $favoriteScholarships = $user->getFavoriteScholarships();
            $favoriteScholarshipIds = [];

            foreach ($favoriteScholarships as $favorite){
                $scholarship = $favorite->getScholarship();
                if(!in_array($scholarship->getId(), $favoriteScholarshipIds)){
                    $favoriteScholarshipIds[] = $scholarship->getId();
                }
            }
            if(!empty($favoriteScholarshipIds)){
                $qb->andWhere('s.id IN (:scholarshipIds)');
                $parameters['scholarshipIds'] = $favoriteScholarshipIds;
            }
        }

        /*** exclude backlist scholarship */
        $blacklistScholarships  =  $user ? $user->getBlacklistScholarship() : [];
        $blacklistScholarshipsIds = [];
        foreach ($blacklistScholarships as $scholarship){
            if(!in_array($scholarship->getId(), $blacklistScholarshipsIds)){
                $blacklistScholarshipsIds[] = $scholarship->getId();
            }
        }
        if(!empty($blacklistScholarshipsIds)){
            $qb->andWhere('s.id NOT IN (:blacklistIds)');
            $parameters['blacklistIds'] = $blacklistScholarshipsIds;
        }
        
        $qb->setParameters($parameters);

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @param integer[] $countries array of country Ids
     * @param integer[] $levels
     * @param integer[] $disciplines
     * @param string $searchSort
     * @param integer $searchLimit
     * @param integer $searchOffset
     * @param string|null $searchOrder
     * @param User|null $currentUser
     * @return mixed
     */
    public function findByCriteria( $countries, $levels, $disciplines, $searchSort, $searchLimit, $searchOffset, $searchOrder = null, User $currentUser = null, $onlyFavorited = false){

        $qb = $this->createQueryBuilder('s');
        $parameters = [];
        if(!empty($countries)){
            $searchContinentIds = [];
            /** @var Country[] $countriesObj */
            $countriesObj = $this->getEntityManager()->getRepository(Country::class)->findBy(['id' => $countries]);
            foreach ($countriesObj as $countryObj){
                $continent = $countryObj->getContinent();
                if($continent instanceof Continent && !in_array($continent->getId(), $searchContinentIds)){
                    $searchContinentIds[] = $countryObj->getContinent()->getId();
                }
            }
            $qb
                ->leftJoin('s.grantedCountries', 'c')
                ->leftJoin('s.grantedContinent', 'o')
                ->leftJoin('o.countries', 'oc')
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->orX(
                            $qb->expr()->in('c.id', ':searchCountries'),
                            $qb->expr()->in('oc.id', ':searchCountries')
                        ),
                        $qb->expr()->andX(
                            $qb->expr()->isNull('c'),
                            $qb->expr()->isNull('oc'),
                            $qb->expr()->in('o.id', ':searchContinentIds')
                        ),
                        $qb->expr()->andX(
                            $qb->expr()->isNull('c'),
                            $qb->expr()->isNull('oc'),
                            $qb->expr()->isNull('o')
                        )
                    )
                );
                $parameters['searchCountries'] = $countries;
                $parameters['searchContinentIds'] = $searchContinentIds;
        }
        if(!empty($levels)){
            $qb
                ->leftJoin('s.grantedLevel', 'l')
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->in('l.id', ':grantedLevel'),
                        $qb->expr()->isNull('l')
                        )
                    );
            $parameters['grantedLevel'] = $levels;
        }
        if(!empty($disciplines)){
                $searchCategoryIds = [];
                /** @var Discipline[] $disciplinesObj */
                $disciplinesObj = $this->getEntityManager()->getRepository(Discipline::class)->findBy(['id' => $disciplines]);
                foreach ( $disciplinesObj as  $disciplineObj){
                    if($disciplineObj->getCategory() instanceof Category){
                        $searchCategoryIds[] = $disciplineObj->getCategory()->getId();
                    }
                }

                $qb
                    ->leftJoin('s.studyFieldTags', 'd') //discipline
                    ->leftJoin('s.studyField', 't')// category
                    ->leftJoin('t.disciplines', 'i')
                    ->andWhere(
                        $qb->expr()->orX(
                            $qb->expr()->in('d.id',':studyFieldTags'),
                            $qb->expr()->andX(
                                $qb->expr()->isNull('d'),
                                $qb->expr()->orX(
                                    $qb->expr()->in('t.id', ':searchCategoryIds'),
                                    $qb->expr()->isNull('t'),
                                ),
                            )
                        )
                    );

                    $parameters['studyFieldTags']=  $disciplines;
                    $parameters['searchCategoryIds'] = $searchCategoryIds;

        }

        if($onlyFavorited){
            $favoriteScholarships = $currentUser->getFavoriteScholarships();
            $favoriteScholarshipIds = [];

            foreach ($favoriteScholarships as $favorite){
                $scholarship = $favorite->getScholarship();
                if(!in_array($scholarship->getId(), $favoriteScholarshipIds)){
                    $favoriteScholarshipIds[] = $scholarship->getId();
                }
            }
            if(empty($favoriteScholarshipIds)){
                return []; //no favorites, no scholarship
            }
            $qb->andWhere('s.id IN (:scholarshipIds)');
            $parameters['scholarshipIds'] = $favoriteScholarshipIds;
            
        }
        
        /*** exclude backlist scholarship */
        $blacklistScholarships  =  $currentUser ? $currentUser->getBlacklistScholarship() : [];
        $blacklistScholarshipsIds = [];
        foreach ($blacklistScholarships as $scholarship){
            if(!in_array($scholarship->getId(), $blacklistScholarshipsIds)){
                $blacklistScholarshipsIds[] = $scholarship->getId();
            }
        }
        if(!empty($blacklistScholarshipsIds)){
            $qb->andWhere('s.id NOT IN (:blacklistIds)');
            $parameters['blacklistIds'] = $blacklistScholarshipsIds;
        }

        $qb->setParameters($parameters);
        if(!empty($searchSort)){
            if($searchSort == 'amount'){
                $qb ->leftJoin('s.currency', 'u')
                    ->addSelect('s.amount * u.usdRate as HIDDEN usdAmount')
                    ->addOrderBy('usdAmount', $searchOrder);
            }
            else {
                $qb->addOrderBy("s." . $searchSort, $searchOrder);
            }
        }

        if(!empty($searchLimit)){
            $qb->setMaxResults( $searchLimit );
        }
        if(!empty($searchOffset)){
            $qb->setFirstResult( $searchOffset );
        }
        $qb->addGroupBy('s.id');
        return $qb->getQuery()
            ->getResult();

    }

    /**
     * @param $countries
     * @param $levels
     * @param $disciplines
     * @return mixed
     */
    public function getSuggestion($currentUser){
        
        /*** prepare country ids criteria */
        $desiredCountries = $currentUser->getDesiredCountries();
        $countries = [] ;
        foreach ($desiredCountries as $key => $country) {
        $countries[] = $country->getId();
        }
    
        /** prepare level criteria */
        $level    = $currentUser->getDesiredStudyLevel(); 
        $levels   = $level ? [$level->getId()] : [];
        
        /** prepare disciplines criteria */
        $disciplinesData   = $currentUser->getDesiredDisciplines(); 
        $disciplines       = [];
        foreach ($disciplinesData as $key => $discipline) {
            $disciplines[] = $discipline->getId();
        }

        $qb = $this->createQueryBuilder('s');
        $params = [];
        if(!empty($countries)){
            $searchContinentIds = [];
            /** @var Country[] $countriesObj */
            $countriesObj = $this->getEntityManager()->getRepository(Country::class)->findBy(['id' => $countries]);
            foreach ($countriesObj as $countryObj){
                if($countryObj->getContinent() instanceof Continent){
                    $searchContinentIds[] = $countryObj->getContinent()->getId();
                }
            }
            $qb->leftJoin('s.grantedCountries', 'c')
                ->leftJoin('s.grantedContinent', 'o')
                ->leftJoin('o.countries', 'oc')
                ->orWhere(
                    $qb->expr()->orX(
                        $qb->expr()->in('c.id',':searchCountries'),
                        $qb->expr()->andX(
                            $qb->expr()->isNull('c'),
                            $qb->expr()->orX(
                                $qb->expr()->in('o.id', ':searchContinentIds'),
                                $qb->expr()->isNull('t'),
                            ),
                        )
                    )
                );

            $params['searchCountries'] = $countries;
            $params['searchContinentIds'] = $searchContinentIds;
        }
        
        if(!empty($levels)){
            $qb
                ->leftJoin('s.grantedLevel', 'l')
                ->orWhere(
                    $qb->expr()->orX(
                        $qb->expr()->isNull('l'),
                        $qb->expr()->in('l.id', ':grantedLevel')  
                    )
                );

            $params['grantedLevel'] = $levels;
        }

        if(!empty($disciplines)){
            $searchCategoryIds = [];
            /** @var Discipline[] $disciplinesObj */
            $disciplinesObj = $this->getEntityManager()->getRepository(Discipline::class)->findBy(['id' => $disciplines]);
            foreach ( $disciplinesObj as  $disciplineObj){
                if($disciplineObj->getCategory() instanceof Category){
                    $searchCategoryIds[] = $disciplineObj->getCategory()->getId();       
                }
            }

            $qb
            ->leftJoin('s.studyFieldTags', 'd') //discipline
            ->leftJoin('s.studyField', 't')// category
            ->leftJoin('t.disciplines', 'i')
            ->orWhere(
                $qb->expr()->orX(
                    $qb->expr()->in('d.id',':studyFieldTags'),
                    $qb->expr()->andX(
                        $qb->expr()->isNull('d'),
                        $qb->expr()->orX(
                            $qb->expr()->in('t.id', ':searchCategoryIds'),
                            $qb->expr()->isNull('t'),
                        )
                    )
                )
            );
            $params['studyFieldTags'] = $disciplines;
            $params['searchCategoryIds'] = $searchCategoryIds;
        }

        /*** exclude backlist scholarship */
        $blacklistScholarships  =  $currentUser ? $currentUser->getBlacklistScholarship() : [];
        $blacklistScholarshipsIds = [];
        foreach ($blacklistScholarships as $scholarship){
            if(!in_array($scholarship->getId(), $blacklistScholarshipsIds)){
                $blacklistScholarshipsIds[] = $scholarship->getId();
            }
        }
 
        if(!empty($blacklistScholarshipsIds)){
            $qb->andWhere('s.id NOT IN (:blacklistIds)');
            $params['blacklistIds'] = $blacklistScholarshipsIds;
        }


        /** return null if any params is defined */
        if(empty($params))
            return [];
       
        $paginator = new Paginator($qb);
        $paginator->getQuery()
            ->setParameters($params)
            ->setFirstResult(0)
            ->setMaxResults(20);

        return $paginator->getIterator();
    }

    public function getStudentStat(Student $student){
        $stat['byUniversity'] = $this->getStatByUniversity();
        $stat['byCountry']    = $this->getStatByFavoriteCountry($student);
        $stat['byDiscipline'] = $this->getStatByDiscipline($student);
        $stat['byLevel']      = $this->getStatByLevel($student);
        return $stat; 
    }

    public function getStatByUniversity(){
        $qb = $this->createQueryBuilder('s');
        return $qb->select('count(s.id) as count')
        ->leftJoin("s.awardOrganizationType","so")
        ->andWhere("so.name = 'university'")
        ->getQuery()
        ->getSingleResult();
    }

    public function getStatByFavoriteCountry($student){
        $qb = $this->createQueryBuilder('s');
        $countries = [];
        $params   = [];
        foreach ($student->getDesiredCountries() as $key => $country) {
            $countries[] = $country->getId();
        }
        if(empty($countries)){
            return 0;
        }

        $qb->leftJoin('s.originCountry','so')
            ->where('so.id IN (:ids)');
        $params['ids'] = $countries;

        /*** exclude backlist scholarship */
        $blacklistScholarships  =  $student ? $student->getBlacklistScholarship() : [];
        $blacklistScholarshipsIds = [];
        foreach ($blacklistScholarships as $scholarship){
            if(!in_array($scholarship->getId(), $blacklistScholarshipsIds)){
                $blacklistScholarshipsIds[] = $scholarship->getId();
            }
        }
 
        if(!empty($blacklistScholarshipsIds)){
            $qb->andWhere('s.id NOT IN (:blacklistIds)');
            $params['blacklistIds'] = $blacklistScholarshipsIds;
        }
        return 
            $qb->select('count(s.id) as count')
            ->setParameters($params)
            ->getQuery()
            ->getSingleResult();
    }

    public function getStatByDiscipline($student){
        $qb = $this->createQueryBuilder('s');
        $disciplines = [];
        $parameters = [];
        foreach ($student->getDesiredDisciplines() as $key => $discipline) {
            $disciplines[] = $discipline->getId();
        }
        if(!empty($disciplines)){
            $searchCategoryIds = [];
            /** @var Discipline[] $disciplinesObj */
            $disciplinesObj = $this->getEntityManager()->getRepository(Discipline::class)->findBy(['id' => $disciplines]);
            foreach ( $disciplinesObj as  $disciplineObj){
                if($disciplineObj->getCategory() instanceof Category){
                    $searchCategoryIds[] = $disciplineObj->getCategory()->getId();
                }
            }
            $qb->leftJoin('s.studyFieldTags', 'd') //discipline
                ->leftJoin('s.studyField', 't')// category
                ->leftJoin('t.disciplines', 'i')
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->in('d.id',':studyFieldTags'),
                        $qb->expr()->andX(
                            $qb->expr()->isNull('d'),
                            $qb->expr()->orX(
                                $qb->expr()->in('t.id', ':searchCategoryIds'),
                                $qb->expr()->isNull('t'),
                            ),
                        )
                    )
                );
                $parameters['studyFieldTags']=  $disciplines;
                $parameters['searchCategoryIds'] = $searchCategoryIds;

        }

        /*** exclude backlist scholarship */
        $blacklistScholarships  =  $student ? $student->getBlacklistScholarship() : [];
        $blacklistScholarshipsIds = [];
        foreach ($blacklistScholarships as $scholarship){
            if(!in_array($scholarship->getId(), $blacklistScholarshipsIds)){
                $blacklistScholarshipsIds[] = $scholarship->getId();
            }
        }

        if(!empty($blacklistScholarshipsIds)){
            $qb->andWhere('s.id NOT IN (:blacklistIds)');
            $parameters['blacklistIds'] = $blacklistScholarshipsIds;
        }
        $qb->setParameters($parameters);
        return $qb->select('count(s.id) as count')
        ->getQuery()
        ->getSingleResult();
    }

    public function getStatByLevel($student){
        $qb = $this->createQueryBuilder('s');
        $level    = $student->getDesiredStudyLevel(); 
        $level    = $level ? $level->getId() : false;
        $params   = [];
        if(!empty($level)){
            $qb
                ->leftJoin('s.grantedLevel', 'l')
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->eq('l.id',':grantedLevel'),
                        $qb->expr()->andX(
                            $qb->expr()->isNull('l')
                        )
                    )
                );
                $params['grantedLevel'] = $level;
        }


        /*** exclude backlist scholarship */
        $blacklistScholarships  =  $student ? $student->getBlacklistScholarship() : [];
        $blacklistScholarshipsIds = [];
        foreach ($blacklistScholarships as $scholarship){
            if(!in_array($scholarship->getId(), $blacklistScholarshipsIds)){
                $blacklistScholarshipsIds[] = $scholarship->getId();
            }
        }
         
        if(!empty($blacklistScholarshipsIds)){
            $qb->andWhere('s.id NOT IN (:blacklistIds)');
            $params['blacklistIds'] = $blacklistScholarshipsIds;
        }
        $qb->setParameters($params);


        return $qb->select('count(s.id) as count')
        ->getQuery()
        ->getSingleResult();
    }

    public function findByUserAndCountry($country, $searchSort, $searchLimit, $searchOffset, $searchOrder, $favoriteBy, $onlyFavorited = false , $university = false)
    {
        $parameters = [];
        $qb = $this->createQueryBuilder('s')
            ->select('s')
            ->addSelect('f')
            ->leftJoin('s.owner','f')
            //->where(":country MEMBER OF s.grantedCountries")
            ->leftJoin('s.grantedCountries','sc');
            $qb->where(
                $qb->expr()->orX(
                    $qb->expr()->eq('sc.id',':country'),
                    $qb->expr()->andX(
                        $qb->expr()->isNull('sc')
                    )
                )
            );

        $parameters['country'] = $country;

        if($university && isset($university['id']) && $university['id'] > 0){
            $qb->leftJoin('s.relatedOrganization','so')
            ->andWhere('so.name = :university');
            $parameters['university'] = $university['name'];  
        }

        //dd($university);

        if($onlyFavorited && $favoriteBy instanceof User){
            $favoriteScholarships = $favoriteBy->getFavoriteScholarships();
            $favoriteScholarshipIds = [];

            foreach ($favoriteScholarships as $favorite){
                $scholarship = $favorite->getScholarship();
                if(!in_array($scholarship->getId(), $favoriteScholarshipIds)){
                    $favoriteScholarshipIds[] = $scholarship->getId();
                }
            }

            if(empty($favoriteScholarshipIds)){
                return []; // no favorites, no scholarship found
            }
            $qb->andWhere('s.id IN (:scholarshipIds)');
            $parameters['scholarshipIds'] = $favoriteScholarshipIds;
        }
        $qb->setParameters($parameters);
        if(!empty($searchSort)){
            if($searchSort == 'amount'){
                $qb ->leftJoin('s.currency', 'u')
                    ->addSelect('s.amount * u.usdRate as HIDDEN usdAmount')
                    ->addOrderBy('usdAmount', $searchOrder);
            }
            else {
                $qb->addOrderBy("s." . $searchSort, $searchOrder);
            }
        }
        if(!empty($searchLimit)){
            $qb->setMaxResults( $searchLimit );
        }
        if(!empty($searchOffset)){
            $qb->setFirstResult( $searchOffset );
        }
        return $qb->getQuery()
            ->getResult();
    }

    public function findTotalAndCountByUserAndCountry($country, $favoriteBy, $onlyFavorited){
        $qb = $this->createQueryBuilder('s')
            ->leftJoin('s.currency', 'cu')
            ->select('SUM(s.count) as count')
            ->addSelect('SUM(s.totalOffered * cu.usdRate) as total')
            ->addSelect('count(s.id ) as nb')
            ->leftJoin('s.grantedCountries','sc');

         $qb->where(
                $qb->expr()->orX(
                    $qb->expr()->eq('sc.id',':country'),
                    $qb->expr()->andX(
                        $qb->expr()->isNull('sc')
                    )
                )
            );

        $parameters = [];
        $parameters['country'] = $country;
        if($onlyFavorited && $favoriteBy instanceof User){
            $favoriteScholarships = $favoriteBy->getFavoriteScholarships();
            $favoriteScholarshipIds = [];

            foreach ($favoriteScholarships as $favorite){
                $scholarship = $favorite->getScholarship();
                if(!in_array($scholarship->getId(), $favoriteScholarshipIds)){
                    $favoriteScholarshipIds[] = $scholarship->getId();
                }
            }
            if(!empty($favoriteScholarshipIds)){
                $qb->andWhere('s.id IN (:scholarshipIds)');
                $parameters['scholarshipIds'] = $favoriteScholarshipIds;
            }
        }
        $qb->setParameters($parameters);
        return $qb->getQuery()
            ->getSingleResult();
    }

    /**
     * Select all scholarship fovited by user $user
     *
     * @param User $user
     * @return void
     */
    public function getFoviteByUser(User $user){
        return $this->createQueryBuilder('s')
        ->leftJoin('s.interestedUsers', 'si')
        ->leftJoin('si.user', 'su')
        ->andWhere('su.id = :user')
        ->setParameter('user', $user->getId())
        ->getQuery()
        ->getResult();
    }
}
