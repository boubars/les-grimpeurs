<?php

namespace App\Repository\Scholarship;

use App\Entity\Scholarship\ScholarshipAlert;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ScholarshipAlert|null find($id, $lockMode = null, $lockVersion = null)
 * @method ScholarshipAlert|null findOneBy(array $criteria, array $orderBy = null)
 * @method ScholarshipAlert[]    findAll()
 * @method ScholarshipAlert[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScholarshipAlertRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ScholarshipAlert::class);
    }

    // /**
    //  * @return ScholarshipAlert[] Returns an array of ScholarshipAlert objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ScholarshipAlert
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @param array $aLevels
     * @param array $aDisciplines
     * @param array $aCountries
     * @return array
     */
    public function findWithCriteria($aLevels = array(), $aDisciplines = array(), $aCountries = array()):array
    {
        $qb = $this->createQueryBuilder('a');
        if (count($aLevels) > 0){
            $qb->innerJoin('a.levels', 'l')
                ->andWhere('l.id in (:_levels)')
            ->setParameter('_levels', $aLevels);
        }
        if (count($aDisciplines) > 0) {
            $qb->innerJoin('a.disciplines', 'd')
                ->andWhere('d.id in (:_disciplines)')
            ->setParameter('_disciplines', $aDisciplines);
        }
        if (count($aCountries) > 0) {
            $qb->innerJoin('a.countries', 'c')
                ->andWhere('c.id in (:_countries)')
            ->setParameter('_countries', $aCountries);
        }

        return $qb->getQuery()->getResult();
    }
}
