<?php

namespace App\Repository\Scholarship;

use App\Entity\Scholarship\Scholarship;
use App\Entity\User\FavoriteScholarship;
use App\Entity\User\SeenScholarship;
use App\Entity\User\User;
use DateTime;
use Doctrine\ORM\Query\ResultSetMapping;

trait ScholarshipStatTrait
{
    public function getEvolutionStat($scholarships, $serie, $date){
        
        /*** 
        * if $serie == views or undefined
        * */
        $statRepo = $this->getEntityManager()->getRepository(SeenScholarship::class);

        if($serie == 'interested'){
            $statRepo = $this->getEntityManager()->getRepository(FavoriteScholarship::class);
        }

        $params = [
            'scholarships' => $scholarships
        ];

        $qb = $statRepo->createQueryBuilder('q')
            ->leftJoin('q.scholarship', 'qs')
            ->leftJoin('q.user', 'qu')
            ->select('count(distinct(qu.id)) as count')
            ->andWhere('qs.id IN (:scholarships)');

        /*** result key name [''] */

        switch ($date) {
            case 'years':
                    $qb->addSelect("DATE_FORMAT(q.createdAt, '%m' ) AS month")->groupBy('month');
                break;

            case 'months':
                    $qb->addSelect("DATE_FORMAT(q.createdAt, '%d') AS day")
                       ->andWhere("DATE_FORMAT(q.createdAt, '%Y') = :currentYear")
                       ->andWhere("DATE_FORMAT(q.createdAt, '%m') = :currentMonth")
                       ->groupBy('day');
                    $params['currentYear']  = (new DateTime())->format("Y");
                    $params['currentMonth'] = (new DateTime())->format("m");

                break;
            

            case 'weeks':
                    $day = date('w');
                    $weekStart = date('Y-m-d', strtotime('-'.($day).' days'));
                    $weekEnd = date('Y-m-d', strtotime('+'.(8-$day).' days'));

                    $qb->addSelect("DATE_FORMAT(q.createdAt, '%d') AS day")
                    ->andWhere("DATE_FORMAT(q.createdAt, '%Y-%m-%d') > :startDate")
                    ->andWhere("DATE_FORMAT(q.createdAt, '%Y-%m-%d') < :endDate")
                    ->groupBy('day');
                    $params['startDate']  = $weekStart;
                    $params['endDate']    = $weekEnd;

                break;

            case 'days':
                 $qb->addSelect("DATE_FORMAT(q.createdAt, '%H') AS hour")
                    ->andWhere("DATE_FORMAT(q.createdAt, '%Y-%m-%d') = :today")
                    ->groupBy('hour');
                    $params['today'] = (new DateTime())->format("Y-m-d");
                break;
            
            case 'today':
            case 'now':
                    $qb->andWhere("DATE_FORMAT(q.createdAt, '%Y-%m-%d') = :today");
                    $params['today'] = (new DateTime())->format("Y-m-d");
                break;

            case 'thisyear':
                    $qb->andWhere("DATE_FORMAT(q.createdAt, '%Y') = :thisyear");
                    $params['thisyear'] = (new DateTime())->format("Y");
                break;

            default:
                # code...
                break;
        }

        $result = $qb->setParameters($params)
                ->getQuery()
                ->getResult();

        return $this->getSerieData($result, $date);
    }

    /**
     * Method Used by  getEvolutionStat
     *
     * @param  Array $data
     * @param  String $date
     * @return Array
     */
    private function getSerieData($data, $date){
        
        switch($date){
            case 'years':
                        $resultArray = array_column($data,'count','month');
                        $thisMonth   = (new DateTime())->format("m");
                        $result = [];
                        for($i=1; $i <= intval($thisMonth) ; $i++) { 
                            $key = $i < 10 ? "0$i" : $i;
                            $result[$i] = $resultArray["$key"] ?? 0;
                        }
                    return $result;
                break;

            case 'months':
                $resultArray = array_column($data,'count','day');
                        $thisDay   = (new DateTime())->format("d");
                        $result = [];
                        for($i=1; $i <= intval($thisDay) ; $i++) { 
                            $key = $i < 10 ? "0$i" : $i;
                            $result[$i] = $resultArray[$key] ?? 0;
                        }
                    return $result;
                break;

            case 'weeks' :
                        $resultArray = array_column($data,'count','day');
                        $day = date('w');
                        $weekStart = date('d', strtotime('-'.($day - 1).' days'));

                        $thisDay   = (new DateTime())->format("d");
                        $result = [];
                        for($i=intval($weekStart); $i <= intval($thisDay) ; $i++) { 
                            $key = $i < 10 ? "0$i" : $i;
                            $result[$i] = $resultArray[$key] ?? 0;
                        }
                    return $result;
                break;
                
            case 'days':
                    $resultArray = array_column($data,'count','hour');
                    $thisH   = (new DateTime())->format("H");
                    $result = [];
                    for($i=1; $i <= intval($thisH) ; $i++) { 
                        $key = $i < 10 ? "0$i" : $i;
                        $result[$i] = $resultArray[$key] ?? 0;
                    }
                    return $result;
                    break;
                    
                    default:
                    return $data;
                break;

        }


        $key = $date == 'years' ? 'month' : ($date == 'months' ? 'day' : ($date == 'weeks' ? 'day' : false));
        return $key ?  : $data;


    }

    /**
     * Method to get  user level range
     *
     * @param Scholarship|Integer $scholarship
     * @return Array
     */
    public function getRangeLevelStat($scholarships, $period = false){
        $qb["fields"]      = " COUNT(DISTINCT(student.id)) as count, level.id as level";
        $qb["tables"]      = " user_student student, user_seen_scholarship useen, level , scholarship_scholarship scholarship";
        $qb["conditions"]  = " student.id = useen.user_id AND student.current_level_id = level.id AND scholarship.id = useen.scholarship_id AND scholarship.id IN (?) ";
        
        if($period){
            
            
        }
        switch ($period) {
            case 'years':
                    $year = (new DateTime())->format("Y");
                    $qb["conditions"] .= " AND DATE_FORMAT(useen.created_at, '%Y') = $year ";
                break;

            case 'months':
                    $month = (new DateTime())->format("Y-m");
                    $qb["conditions"] .= " AND DATE_FORMAT(useen.created_at, '%Y-%m') = $month "; 

                break;

            case 'days':
                    $day = (new DateTime())->format("Y-m-d");
                    $qb["conditions"] .= " AND DATE_FORMAT(useen.created_at, '%Y-%m-%d') = $day "; 
                break;
        }


        $qb["group"]       = " level";

        $rsm = new ResultSetMapping();
        $rsm->addScalarResult("count", "count");
        $rsm->addScalarResult("level", "level");
        
        $sql = sprintf("SELECT %s FROM %s WHERE %s GROUP BY %s", $qb['fields'], $qb['tables'], $qb['conditions'] ,$qb['group']);
        
        return $this->_em->createNativeQuery($sql, $rsm)
                    ->setParameter(1, $scholarships)
                    ->getArrayResult(); 
    }

    /**
     * Method to get  user discipline range
     *
     * @param Scholarship|Integer $scholarship
     * @return Array
     */
    public function getRangeDisciplineStat($scholarships, $period = false){
        
        $qb["fields"]      = " COUNT(DISTINCT(student.id)) as count, discipline.id as discipline";
        $qb["tables"]     =  " user_student student, user_seen_scholarship useen, discipline_category discipline, student_discipline_category student_discipline, scholarship_scholarship scholarship";
        $qb["conditions"] =  " student.id = useen.user_id AND student_discipline.student_id = student.id AND  student_discipline.category_id = discipline.id AND scholarship.id = useen.scholarship_id AND scholarship.id IN (?) ";
        
        switch ($period) {
            case 'years':
                    $year = (new DateTime())->format("Y");
                    $qb["conditions"] .= " AND DATE_FORMAT(useen.created_at, '%Y') = $year ";
                break;

            case 'months':
                    $month = (new DateTime())->format("Y-m");
                    $qb["conditions"] .= " AND DATE_FORMAT(useen.created_at, '%Y-%m') = $month "; 

                break;

            case 'days':
                    $day = (new DateTime())->format("Y-m-d");
                    $qb["conditions"] .= " AND DATE_FORMAT(useen.created_at, '%Y-%m-%d') = $day "; 
                break;
        }
        
        $qb["group"]      =  " discipline";

        $rsm = new ResultSetMapping();
        $rsm->addScalarResult("count", "count");
        $rsm->addScalarResult("discipline", "discipline");
        
        $sql = sprintf("SELECT %s FROM %s WHERE %s GROUP BY %s", $qb['fields'], $qb['tables'], $qb['conditions'] ,$qb['group']);
        
        return $this->_em->createNativeQuery($sql, $rsm)
        ->setParameter(1, $scholarships)
        ->getArrayResult();  
    }

    /**
     * Method to get  user sexe range
     *
     * @param Scholarship|Integer $scholarship
     * @return Array
     */
    public function getRangeSexeStat($scholarships, $period = false){
        
        $qb["fields"]      = " COUNT(DISTINCT(student.id)) as count, student.sexe as sexe";
        $qb["tables"]     = " user_student student, user_seen_scholarship useen, scholarship_scholarship scholarship";
        $qb["conditions"] = " student.id = useen.user_id AND scholarship.id = useen.scholarship_id AND scholarship.id IN (?) ";
        
        switch ($period) {
            case 'years':
                    $year = (new DateTime())->format("Y");
                    $qb["conditions"] .= " AND DATE_FORMAT(useen.created_at, '%Y') = $year ";
                break;

            case 'months':
                    $month = (new DateTime())->format("Y-m");
                    $qb["conditions"] .= " AND DATE_FORMAT(useen.created_at, '%Y-%m') = $month "; 

                break;

            case 'days':
                    $day = (new DateTime())->format("Y-m-d");
                    $qb["conditions"] .= " AND DATE_FORMAT(useen.created_at, '%Y-%m-%d') = $day "; 
                break;
        }
        
        $qb["group"]      =  " sexe";

        $rsm = new ResultSetMapping();
        $rsm->addScalarResult("count", "count");
        $rsm->addScalarResult("sexe", "sexe");
        
        $sql = sprintf("SELECT %s FROM %s WHERE %s GROUP BY %s", $qb['fields'], $qb['tables'], $qb['conditions'] ,$qb['group']);
        
        return $this->_em->createNativeQuery($sql, $rsm)
                    ->setParameter(1, $scholarships)
                    ->getArrayResult(); 
        
    }

    /**
     * Method to get  user level range
     *
     * @param Scholarship|Integer $scholarship
     * @return Array
     */
    public function getRangeAgeStat($scholarships, $period = false){

        $statRepo = $this->getEntityManager()->getRepository(SeenScholarship::class);

        $qb = $statRepo->createQueryBuilder('q')
        ->leftJoin('q.scholarship', 'qs')
        ->leftJoin('q.user', 'u')
        ->select('count(q.id) as count')
        ->select("DATE_FORMAT(u.dateOfBirth, '%Y') as yearBirth")
        ->andWhere("qs.id IN (:scholarships)");
        switch ($period) {
            case 'years':
                    $year = (new DateTime())->format("Y");
                    $qb = $qb->andWhere("DATE_FORMAT(q.createdAt, '%Y') = $year");
                break;

            case 'months':
                    $month = (new DateTime())->format("Y-m");
                    $qb = $qb->andWhere("DATE_FORMAT(q.createdAt, '%Y-%m') = $month");

                break;

            case 'days':
                    $day = (new DateTime())->format("Y-m-d");
                    $qb = $qb->andWhere("DATE_FORMAT(q.createdAt, '%Y-%m-%d') = $day");
                break;
        }

        $qb = $qb->setParameter("scholarships", $scholarships)
        ->getQuery()
        ->getResult();

        $rs = [];

        $thisyear = (new DateTime())->format("Y");

        foreach ($qb as $key => $value) {
           
            if(!$value['yearBirth']){
                continue;
           }
           
           $age = $thisyear - $value['yearBirth'];
           $key = $this->getIntervale($age);
           $rs["$key"] = isset($rs[$key]) ? $rs[$key]++ : 1;
           
        }


        ksort($rs);
        return $rs;
    }
    private function getIntervale($value){
        /** $ranges = [15,20, 25, 30, 35] */
        for ( $i=15 ; $i < 35 ; $i+=5 ){
            if($i <= $value && $value < ($i + 5)){
                return $i."-".($i+5);
            }    
        }
    }

    public function getRangeCountryStat($scholarships, $period = false){

        $qb["fields"]     = " country.id as id, country.code as code, COUNT(DISTINCT(student.id)) as students";
        $qb["tables"]     = " user_student student, user_seen_scholarship useen, country , scholarship_scholarship scholarship";
        $qb["conditions"] = " student.id = useen.user_id AND student.country_id = country.id AND scholarship.id = useen.scholarship_id AND scholarship.id IN (?) ";
        $qb["group"]      = " id";

        switch ($period) {
            case 'years':
                    $year = (new DateTime())->format("Y");
                    $qb["conditions"] .= " AND DATE_FORMAT(useen.created_at, '%Y') = $year ";
                break;

            case 'months':
                    $month = (new DateTime())->format("Y-m");
                    $qb["conditions"] .= " AND DATE_FORMAT(useen.created_at, '%Y-%m') = $month "; 

                break;

            case 'days':
                    $day = (new DateTime())->format("Y-m-d");
                    $qb["conditions"] .= " AND DATE_FORMAT(useen.created_at, '%Y-%m-%d') = $day "; 
                break;
        }

        $qb["order"]      = " students DESC";

        $rsm = new ResultSetMapping();
        $rsm->addScalarResult("students", "students");
        $rsm->addScalarResult("id", "id");
        $rsm->addScalarResult("code", "code");
        
        $sql = sprintf("SELECT %s FROM %s WHERE %s GROUP BY %s ORDER BY %s", $qb['fields'], $qb['tables'], $qb['conditions'] ,$qb['group'], $qb["order"]);
        $rs  = $this->_em->createNativeQuery($sql, $rsm)
        ->setParameter(1, $scholarships)
        ->getArrayResult(); 
        
        $sum = array_sum(array_column($rs,"students"));
        
        for ($i=0; $i < count($rs) ; $i++) { 
            $rs[$i]['evol'] = (int) round(($rs[$i]['students'] / $sum ) * 100);
        }
        
        return $rs;
    }

    public function getUserStory($scholarship = null, $page = 1, $limit = 8){
       

        $seens = $this->_em->createQueryBuilder()
        ->addSelect("f.createdAt as date")
        ->addSelect("(CASE WHEN f.user = f.user   THEN 'seen'    ELSE 'x' END) as action")
        ->addSelect("fu.id  as  uid")
        ->addSelect("fu.firstname as firstname")
        ->addSelect("fu.lastname  as lastname")
        ->addSelect("fu.avatar    as avatar")
        
        ->from(Scholarship::class, "s")

        ->join("s.seenUsers", "f")
        ->join("f.user", "fu")

        ->andWhere("f.scholarship   = :scholarship")
        ->orderBy("date", "DESC")
        ->distinct()
        ->setParameter('scholarship', $scholarship)
        
            //->setFirstResult($limit * ($page - 1)) 
            //->setMaxResults($limit)
            ->getQuery()
            ->getResult();
        
            
        $favs = $this->_em->createQueryBuilder()
        ->addSelect("f.createdAt as date")
        ->addSelect("(CASE WHEN f.user = f.user   THEN 'favorite'    ELSE 'x' END) as action")
        ->addSelect("fu.id  as  uid")
        ->addSelect("fu.firstname as firstname")
        ->addSelect("fu.lastname  as lastname")
        ->addSelect("fu.avatar    as avatar")
        
        ->from(Scholarship::class, "s")

        ->join("s.interestedUsers", "f")
        ->join("f.user", "fu")

        ->andWhere("f.scholarship   = :scholarship")
        
        ->distinct()
        ->setParameter('scholarship', $scholarship)
        ->getQuery()
        ->getResult();


        

        $data = array_merge($favs, $seens);

        usort($data, function($a, $b){
           
            if ($a == $b) {
                return 0;
            }
            return ($a < $b) ? 1 : -1;
        });

        return $data;
    }
}