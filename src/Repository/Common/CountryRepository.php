<?php

namespace App\Repository\Common;

use App\Entity\Common\Country;
use App\Entity\Scholarship\Scholarship;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\EntityRepositoryTrait;

/**
 * @method Country|null find($id, $lockMode = null, $lockVersion = null)
 * @method Country|null findOneBy(array $criteria, array $orderBy = null)
 * @method Country[]    findAll()
 * @method Country[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CountryRepository extends ServiceEntityRepository
{
    use EntityRepositoryTrait;
    
    private $entityManager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        $this->entityManager =  $entityManager;
        parent::__construct($registry, Country::class);
    }

    // /**
    //  * @return Country[] Returns an array of Country objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Country
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function getByContinents(array $contientIds){
        return $this->createQueryBuilder('c')
        ->innerJoin('c.continent','cc')
        ->andWhere("cc.id IN (:array) OR c.name = 'All'")
        ->setParameter('array', $contientIds)
        ->orderBy('c.name', 'ASC')
        ->getQuery()
        ->getResult()
    ;
    }
    public function getTotalAndCountSchoalrship($country): array
    {
        /**
         * @var ScholarshipRepository
         */
        $scholarshipRepos = $this->entityManager->getRepository(Scholarship::class);
        $qb = $scholarshipRepos->createQueryBuilder('s')
            ->leftJoin('s.currency', 'cu')
            ->select('SUM(s.count) as count')
            ->addSelect('SUM(s.totalOffered * cu.usdRate) as total')
            ->addSelect('SUM(s.amount * cu.usdRate) as totalUnit')
            ->addSelect('count(s.id ) as nb')
            ->leftJoin('s.originCountry','sc')
            ->where('sc.id =:countryId')
            ->setParameter('countryId',$country->getId());
            
        
        return $qb->getQuery()->getSingleResult();

    }
}
