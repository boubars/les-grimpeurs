<?php

namespace App\Repository\Common;

use App\Entity\Common\Country;
use App\Entity\Common\Statistical;
use App\Entity\Organization\Organization;
use App\Entity\Application\Application;
use App\Entity\Scholarship\Scholarship;
use App\Entity\Student\Formation;
use App\Entity\Student\School;
use App\Entity\Student\Student;
use App\Entity\University\University;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\Scholarship\ScholarshipRepository;

/**
 * @method Statistical|null find($id, $lockMode = null, $lockVersion = null)
 * @method Statistical|null findOneBy(array $criteria, array $orderBy = null)
 * @method Statistical[]    findAll()
 * @method Statistical[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatisticalRepository extends ServiceEntityRepository
{
    
    private $em;
    private $repositorySholarship;

    public function __construct(EntityManagerInterface $manager ,  ScholarshipRepository $repositorySholarship)
    {
   
        $this->em =  $manager;
        $this->repositorySholarship = $repositorySholarship;
    }

    public function countScholarship() {
        return $this->repositorySholarship->findTotalAndCount()['count'] ;
    }

    public function countFoundation() {
        $repoOrganization = $this->em->getRepository(Organization::class);
        return $repoOrganization->createQueryBuilder('f')
            ->select('count(f.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countTotalScholarship() {
        return $this->repositorySholarship->findTotalAndCount()['total'] ;
    }

    public function countCandidate() {
        $applicationRepo =  $this->em->getRepository(Application::class);
        return $applicationRepo->createQueryBuilder('c')
            ->select('count(c.id)')
            ->getQuery()
            ->getSingleScalarResult();
     }

     public function countCountry(){
       $scholarshipRepos =  $this->em->getRepository(Scholarship::class);
       return $scholarshipRepos->createQueryBuilder('s')
       ->leftJoin('s.originCountry','so')
       ->select('count(distinct(so.id)) as count')
       ->getQuery()
       ->getSingleScalarResult();
     }

    
    public function countRegister() {
        $repoStudent = $this->em->getRepository(Student::class);
        return $repoStudent->createQueryBuilder('s')
            ->select('count(s.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countEtablishement() {
        $repoScholarship = $this->em->getRepository(University::class);
        return $repoScholarship->createQueryBuilder('u')
        ->select('count(u.id)')
        ->getQuery()
        ->getSingleScalarResult();
    }
    public function countFormation() {
        //$repoScholarship = $this->em->getRepository(Formation::class);
        return 85;
     }
}
