<?php

namespace App\Repository\Todo;

use App\Entity\Todo\Todo;
use DateInterval;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Todo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Todo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Todo[]    findAll()
 * @method Todo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TodoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Todo::class);
    }

    // /**
    //  * @return Todo[] Returns an array of Todo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Todo
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function getScheduledTodo(){

     
        $date = new DateTime();
        $today = clone $date;
        $hour = $date->format("H:i");
        
        
        $tmp1 = clone $date;
        $tmp2 = clone $date;
        $startDate = $tmp1->sub(new DateInterval('P0Y0M0DT0H3M0S'));
        $endDate = $tmp2->add(new DateInterval('P0Y0M0DT0H3M0S'));

        $params = [
            'today' => $date,
            'startDate' => $startDate,
            'endDate' => $endDate
        ];
        //dd($params);
        
        return $this->createQueryBuilder('t')
        ->andWhere('t.alertOn = 1')
        ->andWhere('t.sent = 0 OR t.sent is null')
        ->andWhere('t.alertDate = :today')
        ->andWhere("DATE_FORMAT(CONCAT(t.alertDate,':',t.full_hour),'%Y-%m-%d %H:%i') > :startDate")
        ->andWhere("DATE_FORMAT(CONCAT(t.alertDate,':',t.full_hour),'%Y-%m-%d %H:%i') < :endDate")
        ->setParameters([
            'today' => $today->format("Y-m-d"),
            'startDate' => $startDate,
            'endDate' => $endDate
        ])
        ->orderBy('t.id', 'ASC')
        ->getQuery()
        ->getResult();


    }
}
