<?php

namespace App\Repository\Student\Activity;

use App\Entity\Student\Activity\ActivityCause;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ActivityCause|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActivityCause|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActivityCause[]    findAll()
 * @method ActivityCause[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActivityCauseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ActivityCause::class);
    }

    // /**
    //  * @return ActivityCause[] Returns an array of ActivityCause objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ActivityCause
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
