<?php

namespace App\Repository\Student;

use App\Entity\Student\Disctinction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Disctinction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Disctinction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Disctinction[]    findAll()
 * @method Disctinction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DisctinctionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Disctinction::class);
    }

    // /**
    //  * @return Disctinction[] Returns an array of Disctinction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Disctinction
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
