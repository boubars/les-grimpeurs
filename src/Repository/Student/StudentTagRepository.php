<?php

namespace App\Repository\Student;

use App\Entity\Student\StudentTag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StudentTag|null find($id, $lockMode = null, $lockVersion = null)
 * @method StudentTag|null findOneBy(array $criteria, array $orderBy = null)
 * @method StudentTag[]    findAll()
 * @method StudentTag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudentTagRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StudentTag::class);
    }

    // /**
    //  * @return StudentTag[] Returns an array of StudentTag objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StudentTag
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


    /**
     * @param $student
     * @param $organization
     * @param int $favorites
     * @return StudentTag|bool|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setFavorites($student, $organization, $favorites = 1)
    {
        $oStugentTag = $this->findOneBy(['student' => $student, 'organization'=> $organization]);
        if (0 == $favorites && false == $oStugentTag instanceof StudentTag) {
            return true;
        } elseif (false == $oStugentTag instanceof StudentTag) {
            $oStugentTag = new StudentTag($student, $organization);
        }
        $dateFavs = 1 == $favorites ? new \DateTime() : null;
        $oStugentTag->setDateFavorite($dateFavs);
        if (1 == $favorites) {
            $oStugentTag->setDateRemove(null);
        }
        $this->getEntityManager()->persist($oStugentTag);
        $this->getEntityManager()->flush();

        return $oStugentTag;
    }

    /**
     * @param $student
     * @param $organization
     * @param int $removed
     * @return StudentTag|bool|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setRemoved($student, $organization, $removed = 1)
    {
        $oStugentTag = $this->findOneBy(['student' => $student, 'organization'=> $organization]);
        if (0 == $removed && false == $oStugentTag instanceof StudentTag) {
            return true;
        } elseif (false == $oStugentTag instanceof StudentTag) {
            $oStugentTag = new StudentTag($student, $organization);
        }
        $dateRem = 1 == $removed ? new \DateTime() : null;
        $oStugentTag->setDateFavorite(null);
        $oStugentTag->setDateRemove($dateRem);
        $this->getEntityManager()->persist($oStugentTag);
        $this->getEntityManager()->flush();

        return $oStugentTag;
    }
}
