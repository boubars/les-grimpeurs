<?php

namespace App\Repository\Student;

use App\Entity\Application\Application;
use App\Entity\Common\Continent;
use App\Entity\Common\Country;
use App\Entity\Discipline\Category;
use App\Entity\Discipline\Discipline;
use App\Entity\Organization\Organization;
use App\Entity\Student\Student;
use App\Entity\User\User;
use App\Repository\EntityRepositoryTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Student|null find($id, $lockMode = null, $lockVersion = null)
 * @method Student|null findOneBy(array $criteria, array $orderBy = null)
 * @method Student[]    findAll()
 * @method Student[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudentRepository extends ServiceEntityRepository
{
    use EntityRepositoryTrait;
    
    private $em;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        parent::__construct($registry, Student::class);

    }

    // /**
    //  * @return Student[] Returns an array of Student objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Student
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function countByNationalities($nationality){
        return $this->createQueryBuilder('s')
        ->select('COUNT(s.id) as count')
        ->leftJoin('s.nationalities','sn')
        ->andWhere('sn = :nid')
        ->setParameter('nid', $nationality)
        ->getQuery()
        ->getSingleResult()
    ;
    }
    /**
     * @param $countrieDesired
     * @param integer[] $countries array of country Ids
     * @param integer[] $levels
     * @param integer[] $disciplines
     * @param string $searchSort
     * @param integer $searchLimit
     * @param integer $searchOffset
     * @param string|null $searchOrder
     * @param Organization $organization
     * @return mixed
     */
    public function findByCriteria($countrieDesired, $countries, $levels, $disciplines, $searchSort, $searchLimit,
                                   $searchOffset, $searchOrder = null, $organization, $tag = ''){

        $qb = $this->createQueryBuilder('s')
            ->leftJoin('s.tag', 'tag', Join::WITH, 'tag.organization = :_organization');
        $parameters = [];
        if(!empty($countries)){
            $searchContinentIds = [];
            /** @var Country[] $countriesObj */
            $countriesObj = $this->getEntityManager()->getRepository(Country::class)->findBy(['id' => $countries]);
            foreach ($countriesObj as $countryObj){
                $continent = $countryObj->getContinent();
                if($continent instanceof Continent && !in_array($continent->getId(), $searchContinentIds)){
                    $searchContinentIds[] = $countryObj->getContinent()->getId();
                }
            }

            $qb->leftJoin('s.country', 'c')
                ->leftJoin('c.continent', 'o')
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->in('c.id', ':searchCountries'),
                        $qb->expr()->andX(
                            $qb->expr()->isNull('c'),
                            $qb->expr()->in('o.id', ':searchContinentIds')
                        ),$qb->expr()->andX(
                            $qb->expr()->isNull('c'),
                            $qb->expr()->isNull('o')
                        )
                    )
                );
            $parameters['searchCountries'] = $countries;
            $parameters['searchContinentIds'] = $searchContinentIds;
        }
        if(!empty($countrieDesired)){
            $searchContinentIds = [];
            /** @var Country[] $countriesObj */
            $countriesObj = $this->getEntityManager()->getRepository(Country::class)->findBy(['id' => $countrieDesired]);
            foreach ($countriesObj as $countryObj){
                $continent = $countryObj->getContinent();
                if($continent instanceof Continent && !in_array($continent->getId(), $searchContinentIds)){
                    $searchContinentIds[] = $countryObj->getContinent()->getId();
                }
            }

            $qb->leftJoin('s.desiredCountries', 'dc')
                ->leftJoin('dc.continent', 'do')
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->in('dc.id', ':searchCountries'),
                        $qb->expr()->andX(
                            $qb->expr()->isNull('dc'),
                            $qb->expr()->in('do.id', ':searchContinentIds')
                        ),$qb->expr()->andX(
                            $qb->expr()->isNull('dc'),
                            $qb->expr()->isNull('do')
                        )
                    )
                );
            $parameters['searchCountries'] = $countries;
            $parameters['searchContinentIds'] = $searchContinentIds;
        }
        if(!empty($levels)){
            $qb
                ->leftJoin('s.currentLevel', 'l')
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->in('l.id', ':grantedLevel'),
                        $qb->expr()->isNull('l')
                    )
                );
            $parameters['grantedLevel'] = $levels;
        }
        if(!empty($disciplines)){

            $qb
                ->leftJoin('s.disciplines', 'd')
                ->andWhere(
                    $qb->expr()->orX(
                            $qb->expr()->isNull('d'),
                            $qb->expr()->in('d.id',':searchCategoryIds')
                    )
                );

            $parameters['searchCategoryIds'] = $disciplines;

        }

       $qb->andWhere(
                $qb->expr()->isNull('tag.date_remove')
            );
        if ('' != $tag) {
            switch ($tag) {
                case 'favorite':
                    $qb->andWhere(
                            $qb->expr()->isNotNull('tag.date_favorite')
                        );
                    break;
                case 'candidate':
                    $qb->innerJoin('s.applications', 'sa')
                        ->join('sa.scholarship', 'sc')
                        ->andWhere('sc.relatedOrganization = :_organization');
                    break;
                case 'refused':
                    $qb->innerJoin('s.applications', 'sa', Join::WITH, 'sa.status = :refused')
                        ->join('sa.scholarship', 'sc')
                        ->andWhere('sc.relatedOrganization = :_organization');
                    $parameters['refused'] = Application::REFUSED;
                    break;
                case 'interested':
                    $qb->innerJoin('s.favoriteScholarships', 'fs')
                        ->join('fs.scholarship', 'sc')
                        ->andWhere('sc.relatedOrganization = :_organization');
                    break;
            }
        }

        $parameters['_organization'] = $organization;
        if ($parameters) {
            $qb->setParameters($parameters);
        }

        if (!empty($searchSort)) {
            $qb->addOrderBy("s." . $searchSort, $searchOrder);
        }
        if(!empty($searchLimit)){
            $qb->setMaxResults( $searchLimit );
        }
        if(!empty($searchOffset)){
            $qb->setFirstResult( $searchOffset );
        }
        $qb->addGroupBy('s.id');

        $paginator = new Paginator($qb, $fetchJoinCollection = true);

        $c = count($paginator);

        $result = $qb->getQuery()->getResult();
        return array($result, $c);

    }

}
