<?php
namespace App\Repository\Organization;

use App\Entity\Company\Company;
use App\Entity\Organization\Organization;
use App\Entity\University\University;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\EntityRepositoryTrait;


/**
 * @method Organization|null find($id, $lockMode = null, $lockVersion = null)
 * @method Organization|null findOneBy(array $criteria, array $orderBy = null)
 * @method Organization[]    findAll()
 * @method Organization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganizationRepository extends ServiceEntityRepository
{
    use EntityRepositoryTrait;
    
    private $em;
    
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        parent::__construct($registry, Organization::class);
    }

    // /**
    //  * @return Organization[] Returns an array of Organization objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Organization
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function getList($type = 'none')
    {
        $qb = $this->createQueryBuilder('o');
        return $qb
            ->leftJoin('o.institutionType', 'oi')
            ->andWhere($qb->expr()->isNotNull("o.name"))
            ->andWhere('oi.id = :typeId')
            ->setParameter('typeId', $type['id'])
            ->orderBy('o.name', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
    
}
