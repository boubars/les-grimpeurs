<?php

namespace App\Repository\Messaging\Bot;

use App\Entity\Messaging\Bot\Conversation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Conversation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Conversation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Conversation[]    findAll()
 * @method Conversation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConversationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Conversation::class);
    }

    public function findAll(){
        return $this->createQueryBuilder('c')
        ->innerJoin('c.messages', 'cm')
        ->orderBy('cm.id', 'DESC')
        ->getQuery()
        ->getResult();
    }

    public function findByUid($uid)
    {
        return $this->createQueryBuilder('c')
            ->leftJoin('c.participant', 'cp')
            ->andWhere('cp.uid = :val')
            ->setParameter('val', $uid)
            ->getQuery()
            ->getResult()
        ;
    }
}
