<?php

namespace App\Repository\Application;

use App\Entity\Application\Application;
use App\Entity\Organization\Organization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Null_;

/**
 * @method Application|null find($id, $lockMode = null, $lockVersion = null)
 * @method Application|null findOneBy(array $criteria, array $orderBy = null)
 * @method Application[]    findAll()
 * @method Application[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApplicationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Application::class);
    }

    public function getByOrganization($aParams, $aOrder, $isAll = false)
    {
        $qb = $this->createQueryBuilder('a')
            ->innerJoin('a.scholarship', 's')
            ->leftJoin('a.student', 'u');

        $params['oid'] = $aParams['organization']->getId();

        if(false == $isAll) {
            if ($aParams['status']) {
                if(is_numeric($aParams['status'])) {
                    $qb->andwhere("a.status = :status");
                    $params['status'] = $aParams['status'];
                } else {
                    $qb->innerJoin('s.interestedUsers', 'iu', Join::WITH, 'iu.user = a.student');
                }
            }

            if ($aParams['scholarship']) {
                $qb->andwhere("s.id = :scholarship");
                $params['scholarship'] = $aParams['scholarship'];
            }
        }
        $form = $aParams['form'] ?? false; 
        if($form){
            $qb->leftJoin('s.form', 'sf')
            ->andWhere('sf.slug = :fslug');
            $params['fslug'] = $aParams['form'];
        }

        $qb->leftJoin('s.relatedOrganization', 'o')
            ->andWhere('a.status IS NOT NULL')
            ->andWhere('o.id = :oid')
            ->setParameters($params);

        if ($aParams['max']) {
            $qb->setMaxResults($aParams['max']);
        }
        if (isset($aOrder['order'])){
            $aSort = explode('.',$aOrder['order']);
            if(isset($aSort[0]) && $aSort[0] == 'student'){
                $qb->addOrderBy('u.'.$aSort[1], $aOrder['direction']);
            }else{
                $qb->addOrderBy('a.'.$aOrder['order'], $aOrder['direction']);
            }

        }

       


        $paginator = new Paginator($qb, $fetchJoinCollection = true);

        $c = count($paginator);
        if ($isAll) {
            return $c;
        }
        $result = $qb->getQuery()->getResult();

        return array($result, $c);
    }


    public function countApplicationAndType($organization_id)
    {
        $qb = $this->createQueryBuilder('a')
            ->select('count(a.id) as total, SUM( CASE WHEN (a.status = 3) THEN 1 ELSE 0 END) AS sub_total ')
            ->innerJoin('a.scholarship', 's')
            ->leftJoin('s.relatedOrganization', 'o')
            ->andWhere('o.id = :oid')
            ->setParameter('oid', $organization_id);

        return $qb->getQuery()
            ->getOneOrNullResult();
    }

    public function findByCriteria($student, $organization = false)
    { 
        $qb     = $this->createQueryBuilder('a')
        ->leftJoin('a.student', 'st')
        ->andWhere('st.id = :student')
        ->setParameter('student', $student->getId())
        ;

        if($organization){
            $qb->leftJoin('a.scholarship', 'sc')
            ->leftJoin('sc.relatedOrganization', 'so')
            ->andWhere('so.id = :organizaiton')
            ->setParameter('organization', ($organization instanceof Organization) ? $organization->getId(): $organization)
            ;
        }

        return
            $qb->getQuery()
            ->getResult()
        ;
    }
}
