<?php

namespace App\Repository\Application;

use App\Entity\Application\ApplicationSheet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ApplicationSheet|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApplicationSheet|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApplicationSheet[]    findAll()
 * @method ApplicationSheet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApplicationSheetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ApplicationSheet::class);
    }

    // /**
    //  * @return ApplicationSheet[] Returns an array of ApplicationSheet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ApplicationSheet
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
