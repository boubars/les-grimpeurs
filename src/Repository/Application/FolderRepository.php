<?php

namespace App\Repository\Application;

use App\Entity\Application\Folder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Folder|null find($id, $lockMode = null, $lockVersion = null)
 * @method Folder|null findOneBy(array $criteria, array $orderBy = null)
 * @method Folder[]    findAll()
 * @method Folder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FolderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Folder::class);
    }

    // /**
    //  * @return Folder[] Returns an array of Folder objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findByQuery($criteria =[])
    {
        $qb = $this->createQueryBuilder('f');
        if (!empty($criteria['q'])) {
            $qb->andWhere('f.name like :q or f.slug like :q')
                ->setParameter('q', '%' .$criteria['q'] .'%');
        }
        $offset = $criteria['offset'] ?? 0;
        if (!empty($criteria['max'])) {
            $qb->setFirstResult($offset)->setMaxResults($criteria['max']);
        }

        return $qb ->getQuery()
            ->getResult();
    }

}
