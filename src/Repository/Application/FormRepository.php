<?php

namespace App\Repository\Application;

use App\Entity\Application\Form;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Form|null find($id, $lockMode = null, $lockVersion = null)
 * @method Form|null findOneBy(array $criteria, array $orderBy = null)
 * @method Form[]    findAll()
 * @method Form[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Form::class);
    }

    // /**
    //  * @return Form[] Returns an array of Form objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findByQuery($criteria =[])
    {
        $qb = $this->createQueryBuilder('f');
        if (!empty($criteria['q'])) {
            $qb->andWhere('f.name like :q or f.slug like :q')
                ->setParameter('q', '%' .$criteria['q'] .'%');
        }
        $offset = $criteria['offset'] ?? 0;
        if (!empty($criteria['max'])) {
            $qb->setFirstResult($offset)->setMaxResults($criteria['max']);
        }

        return $qb ->getQuery()
            ->getResult();
    }

    public function deleteByIds($ids =[], $isExclude = false)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->delete(Form::class, 'f');
        if ($isExclude) {
            $qb->andWhere('f.id not in (:_id)');
        } else {
            $qb->andWhere('f.id in (:_id)');
        }
        $qb->setParameter('_id', $ids);

        return $qb->getQuery()->execute();
    }
}
