<?php
namespace App\Command\Root;

use App\Entity\Application\ApplicationSheet;
use App\Entity\Application\Form;
use App\Entity\Application\Sheet;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


class InitDefaultFormCommand extends Command
{
    protected static $defaultName = 'grimp:import:default-form';
    
    private $em;
    private $params;

    public function __construct(EntityManagerInterface $entityManager,ParameterBagInterface $params)
    {
        $this->em    = $entityManager;
        $this->params    = $params;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Create default form application.')
            ->setHelp('This command create the default form application and sheet');
        
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $rootDir = $this->params->get('project_dir');
        $files   = $this->getDirContents(realpath($rootDir .'/res/formdata/').DIRECTORY_SEPARATOR);
        $slug    = $this->params->get('default_application_form');
        $oForm   = $this->em->getRepository(Form::class)->findOneBy(['slug' => $slug]);
        $newInsert = false;
        if ( false === $oForm instanceof Form) {
            $output->writeln(date_format(new \DateTime(), 'Y-m-d H:i:s') . " Command add default form starting");
            $oForm = new Form();
            $oForm->setName('Default form');
            $oForm->setDateCreation(new \DateTime());
            $oForm->setSlug($slug);
            $this->em->persist($oForm);
            $newInsert = true;
        } else {
            $output->writeln(date_format(new \DateTime(), 'Y-m-d H:i:s') . " Updating the default form sheet");
            //Reset all form built
            $this->em->getRepository(Form::class)->deleteByIds([$oForm->getId()], true);
            //remove orphan application
            $qb = $this->em->createQueryBuilder();
            $qb->delete(ApplicationSheet::class, 'app');
            $qb->where('app.Sheet is null')->getQuery()->execute();
        }
        //updating all sheet
        foreach ($files as $file) {
            $content = file_get_contents($file);
            $basename = basename($file, '.json');
            $order = str_replace('step', '',$basename);
            $sheet = null;
            if (false == $newInsert) {
                $sheet = $this->em->getRepository(Sheet::class)->findOneBy(['order' => $order, 'form'=> $oForm]);
            }
            if (false == $sheet instanceof Sheet) {
                $sheet = new Sheet();
            }

            $sheetName = $this->getSheetTitle($content, $order);
            $sheet->setName($sheetName);
            $sheet->setDateCreation(new \DateTime());
            $sheet->setContent(json_decode($content));
            $sheet->setOrder($order);
            $sheet->setForm($oForm);
            $this->em->persist($sheet);
        }

        $this->em->flush();

        return count($files);

    }


    /**
     * Get file list in directory $dir
     *
     * @param $dir
     * @param array $results
     * @return array
     */
    private function getDirContents($dir, &$results = array()) {
        $files = scandir($dir);

        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            } else if ($value != "." && $value != "..") {
                $this->getDirContents($path, $results);
                $results[] = $path;
            }
        }

        return $results;
    }

    private function getSheetTitle($array, $order = 0){
        $array = json_decode($array, true);
        $title = $array['list'][0] ?? false;
        
        $type = $title['type'] ?? false ;
        
        if($type == 'title'){
            return $title['options']['defaultValue'];
        }
        return "steep $order";
    }

}