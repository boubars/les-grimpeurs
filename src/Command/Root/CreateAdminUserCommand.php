<?php
namespace App\Command\Root;

use App\Entity\Admin\Admin;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\Utilis\TokenGenerator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Question\Question;

class CreateAdminUserCommand extends Command
{

  protected static $defaultName = 'grimp:create:admin-user';

  private $passwordEncoder;
  private $entityManager;


  public function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $entityManager)
  {
      $this->passwordEncoder = $encoder;
      $this->entityManager = $entityManager;
      parent::__construct();
  }

  protected function configure()
  {
      $this->setDescription('Creates a new grimpeurs admin user.')
          ->setHelp('This command allows you to create a new admin user for grimpeurs platforme');
      
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
   
    $helper = $this->getHelper('question');

    $question1 = new Question('User email :', 'admin@admin.com');
    $question2 = new Question('password   :', '4v3ry5tr0ngp4ssword');

    $email = $helper->ask($input, $output, $question1);
    $password = $helper->ask($input, $output, $question2);
   
    $this->createUser($email, $password);
    $output->writeln('User successfully generated!');

    return Command::SUCCESS;
  }

  private function createUser($email, $password){
    $user = new Admin();
    $user->setRoles(["ROLE_ADMIN"]);
    $user->setEnable(1);
    $user->setEmail($email);
    $user->setToken(TokenGenerator::generate(15));
    $user->setFirstname("Admin");
    $user->setLastName("Grimpeur");
    $user->setPhone("123456");
    $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
    $this->entityManager->persist($user);
    $this->entityManager->flush();
  }
}