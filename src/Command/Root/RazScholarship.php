<?php
namespace App\Command\Root;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


class RazScholarship extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'grimp:raz:scholarship';

    private $entityManager;
    private $connection;
    private $platform;


   
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->connection    = $entityManager->getConnection();
        $this->platform      = $this->connection->getDatabasePlatform();

        parent::__construct();
    }

    protected function configure()
    {
        
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

      return Command::SUCCESS; //just to avoid command excecution in each deploiement

      /** config truncate method */  
      $this->connection->executeQuery('SET FOREIGN_KEY_CHECKS = 0;');

      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('scholarship_category'));
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('scholarship_continent'));
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('scholarship_country'));
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('scholarship_discipline'));
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('scholarship_level'));
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('scholarship_nationality'));
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('user_favorite_scholarship'));
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('scholarship_information_request'));
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('scholarship_scholarship'));
      
      /** update schema force */
      $command = $this->getApplication()->find('doctrine:schema:update');
      $arguments = [
        '--force'  => true,
        '--complete'  => true,
      ];
      $greetInput = new ArrayInput($arguments);
      $command->run($greetInput, $output);


      return Command::SUCCESS;
    }
   


}