<?php
namespace App\Command\Root;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;


class MJMLCompilerCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'grimp:compile:emails';

    private $rootFolder;
   
    public function __construct(ParameterBagInterface $params)
    {
        $this->rootFolder = $params->get('project_dir');

        parent::__construct();
    }

    protected function configure()
    {
        
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

      /*** copile emails  */
      $srcdir    = sprintf('%s/%s', $this->rootFolder, "templates/mjml/emails");
      $outputdir = sprintf('%s/%s', $this->rootFolder, "templates/emails");

      /** start compilatation */
      $this->compileMJML($srcdir, $outputdir);
      
      return Command::SUCCESS;
    }
   

    private function exec($script){
      $process = new Process($script);
      $process->run();

      if (!$process->isSuccessful()) {
          throw new ProcessFailedException($process);
      }
      echo $process->getOutput();
    }

    private function compileMJML($srcdir, $outputdir){

       /*** clean output dir */
       $filesystem = new Filesystem();
       $filesystem->remove([$outputdir]);

      $aFiles = $this->getDirContents($srcdir);
      /**** create emails directory structure if according to mjml/src structure */
      foreach($aFiles as $file){
          $dirname  = dirname($file);
          $outputdir = str_replace(['\mjml', '/mjml'], '',$dirname);
          if (!file_exists($outputdir)) {
            $filesystem->mkdir($outputdir, 0777, true);
          }

        /*** copile the template in each directory*/
        $script = [];
          $script[] = "yarn"; 
          $script[] = "mjml";
          $script[] = $file;
          $script[] = "-o";
          $script[] = $outputdir . DIRECTORY_SEPARATOR . str_replace('.mjml', '', basename($file));
        $this->exec($script);
        
      }


    }

    /**
     * Get file list in directory $dir
     *
     * @param $dir
     * @param array $results
     * @return array
     */
    private function getDirContents($dir, &$results =[]) {
        $files = scandir($dir);
        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            } else if ($value != "." && $value != "..") {
                $this->getDirContents($path,$results);
            }
        }

        return $results;
    }

    private function deleteOldTemplate(){
      
    }
}