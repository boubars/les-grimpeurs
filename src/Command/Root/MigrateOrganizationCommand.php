<?php
namespace App\Command\Root;

use App\Entity\Todo\Todo;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


class MigrateOrganizationCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'grimp:migrate:student:organization';

    private $entityManager;
    private $connection;
    private $platform;


   
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->connection    = $entityManager->getConnection();
        $this->platform      = $this->connection->getDatabasePlatform();

        parent::__construct();
    }

    protected function configure()
    {
        
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {


      /** config truncate method */  
      $this->connection->executeQuery('SET FOREIGN_KEY_CHECKS = 0;');

      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('student_association'));
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('student_activity'));
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('student_organization'));
   
      /** update schema force */
      $command = $this->getApplication()->find('doctrine:schema:update');
      $arguments = [
        '--force'  => true,
        '--complete'  => true,
      ];
      $greetInput = new ArrayInput($arguments);
      $command->run($greetInput, $output);

      return Command::SUCCESS;
    }
   


}