<?php
namespace App\Command\Root;

use App\Entity\Todo\Todo;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


class MigrateTodoCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'grimp:migrate:todo';

    private $entityManager;
    private $connection;
    private $platform;


   
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    protected function configure()
    {
        
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

      $todos = $this->entityManager->getRepository(Todo::class)->findAll();

      foreach ($todos as $key => $todo) {
        $todo->setFullDate($todo->getFullDate());
        $this->entityManager->persist($todo);
      }

      $this->entityManager->flush();

      return Command::SUCCESS;
    }
   


}