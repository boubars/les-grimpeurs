<?php
namespace App\Command\Root;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


class InitBaseCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'grimp:init:database';
    
    private $connection;
    private $platform;
   
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->connection    = $entityManager->getConnection();
        $this->platform      = $this->connection->getDatabasePlatform();

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Reset grimpeurs database.')
            ->setHelp('This command will delete all data in the database');
        
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

      /** drop database */
      $command = $this->getApplication()->find('doctrine:database:drop');
      $arguments = [
        '--force'  => true
      ];
      $greetInput = new ArrayInput($arguments);
      $command->run($greetInput, $output);

      /** create new database */
      $command = $this->getApplication()->find('doctrine:database:create');
    
      $greetInput = new ArrayInput([]);
      $command->run($greetInput, $output);

      /** update schema force */
      $command = $this->getApplication()->find('doctrine:schema:update');
      $arguments = [
        '--force'  => true,
        '--complete'  => true,
      ];
      $greetInput = new ArrayInput($arguments);
      $command->run($greetInput, $output);


      /** raz and load all data */
      $command = $this->getApplication()->find('grimp:raz:data');
      $greetInput = new ArrayInput([]);
      $command->run($greetInput, $output);

      $output->writeln('Database reinit successfully');
 
      return Command::SUCCESS;
    }



}