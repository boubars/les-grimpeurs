<?php
namespace App\Command\Root;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;


class LoadFontsCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'grimp:load:fonts';

    private $fontSrcDir;
   

   
    public function __construct(ParameterBagInterface $params)
    {
        $this->fontSrcDir = $params->get('project_dir')."/assets/vue/assets/css/fonts";

        parent::__construct();
    }

    protected function configure()
    {
        
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

      /* command Native
        php load_font.php NunitoSans-Bold assets/vue/assets/css/fonts/NunitoSans-Bold.ttf 
        php load_font.php NunitoSans-ExtraBold assets/vue/assets/css/fonts/NunitoSans-ExtraBold.ttf 
        php load_font.php NunitoSans-SemiBold assets/vue/assets/css/fonts/NunitoSans-SemiBold.ttf 
        php load_font.php NunitoSans-Regular assets/vue/assets/css/fonts/NunitoSans-Regular.ttf
      */
      
      $fonts = ["NunitoSans-Bold", "NunitoSans-ExtraBold", "NunitoSans-SemiBold", "NunitoSans-Regular"];
      
      foreach ($fonts as $key => $font) {
        $this->load($font);
      }
      
      return Command::SUCCESS;
    }
   

  
    private function load($name){

        $fontSrc = sprintf("%s%s%s%s", $this->fontSrcDir, "/", $name, ".ttf");
  
        $script[] = "php"; 
        $script[] = "load_font.php"; 
        $script[] =  $name;  
        $script[] =  $fontSrc; 
        $this->exec($script);
        
      
    }

    private function exec($script){
      $process = new Process($script);
      $process->run();

      if (!$process->isSuccessful()) {
          throw new ProcessFailedException($process);
      }
      
      echo $process->getOutput();
    }

}