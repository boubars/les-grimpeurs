<?php
namespace App\Command\Root;

use App\Entity\Common\Continent;
use App\Entity\Common\Country;
use App\Entity\Common\Currency;
use App\Entity\Common\Grade;
use App\Entity\Common\Language;
use App\Entity\Common\Level;
use App\Entity\Common\Nationality;
use App\Entity\Discipline\Category;
use App\Entity\Discipline\Discipline;
use App\Entity\Organization\OrganizationType;
use App\Entity\Scholarship\Scholarship;
use App\Entity\Student\Activity\ActivityCause;
use App\Entity\Student\LanguageLevel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Encoder\CsvEncoder;

class RazCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'grimp:raz:data';

    private $entityManager;
    private $connection;
    private $platform;
    private $ressourceDir;

    private $encoder;
    private $fileSystem;

    public function __construct(EntityManagerInterface $entityManager, ParameterBagInterface $params)
    {
        $this->entityManager = $entityManager;
        $this->connection    = $entityManager->getConnection();
        $this->platform      = $this->connection->getDatabasePlatform();

        $this->ressourceDir = $params->get('project_dir').'/res/data';
        
        $this->encoder = new CsvEncoder();
        $this->fileSystem = new Filesystem();

        parent::__construct();
    }

    protected function configure()
    {
        
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
      /** config truncate method */  
      $this->connection->executeQuery('SET FOREIGN_KEY_CHECKS = 0;');

      /*** Raz continent list */
      $this->razContinent();

      /** execute raz country */
      $this->razCountry();

      /** Raz discipline */
      $this->razDiscipline();

      /** Raz language */
      $this->razLanguage();

      /** Raz StudyLevel */
      $this->razLevel();

      

      /** Raz association cause */
      $this->razActivityCause();
      
      /** Raz grade */
      $this->razGrade();

      /** Raz currency */
      $this->razCurrency();

      /** Raz Type organization */
      $this->razOrganizationType();

      $this->entityManager->flush();
      return Command::SUCCESS;
    }

    private function razCountry(){
        /** truncate table */
        $this->connection->executeUpdate($this->platform->getTruncateTableSQL('country'));
        $this->connection->executeUpdate($this->platform->getTruncateTableSQL('nationality'));

        /** get file data */
        $data = $this->getDataFrom("country.csv");

        /** translation data */
        /** save new data */
        foreach ($data as $key => $row) {
            $country = new Country();
            $continent =  $this->entityManager->getRepository(Continent::class)->findOneById($row['continent_id']);
            $country->setName($row['name'])
            ->setNameFr($row['name_fr'])
            ->setCode($row['code'])
            ->setContinent($continent);
            $location = json_decode($row['location']);

            $country->setLocation([
              'lat' => $location[0],
              'lng' => $location[1]
            ]);


            $this->entityManager->persist($country);

            $nationality = new Nationality();
            $nationality->setName($row['name']);
            $this->entityManager->persist($nationality);

            /** populate jsonData */
            $jsonData[$row['id']] =  $row['name'];
            $jsonDataFr[$row['id']] =  $row['name_fr'];
        }
        $jsonData["0"]   = "All";
        $jsonDataFr["0"] = "Tous";

        /**generate translation files */
        $this->fileSystem->dumpFile($this->ressourceDir."/../../assets/vue/lang/translations/country/en.json", json_encode($jsonData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        $this->fileSystem->dumpFile($this->ressourceDir."/../../assets/vue/lang/translations/country/fr.json", json_encode($jsonDataFr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));


        /**generate nationality translation files */
        $this->fileSystem->dumpFile($this->ressourceDir."/../../assets/vue/lang/translations/nationality/en.json", json_encode($jsonData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        $this->fileSystem->dumpFile($this->ressourceDir."/../../assets/vue/lang/translations/nationality/fr.json", json_encode($jsonDataFr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

        return true;
    }

    private function razDiscipline(){
      /** truncate table */
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('discipline'));
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('discipline_category'));
      
      /** get file category data */
      $data = $this->getDataFrom("discipline_category.csv");
      $jsonData      = [];
      $jsonDataFr    = [];

      

      /** save new data */
      foreach ($data as $key => $row) {
        $instance = new Category();
        $instance->setName($row['name'])
        ->setNameFr($row['name_fr']);
        /** populate jsonData */
        $jsonData['category'][$row['id']] = $row['name'];
        $jsonDataFr['category'][$row['id']] = $row['name_fr'];
        $this->entityManager->persist($instance);    
      }

      $jsonData['category']["0"]   = "All";
      $jsonDataFr['category']["0"] = "Tous";

      /**Maybe need to flush */
      $this->entityManager->flush();

      /** get file discipline data */
      $data = $this->getDataFrom("discipline.csv");
      /** save new data */
      foreach ($data as $key => $row) {
        $instance = new Discipline();
       
        $instance->setName($row['name'])
        ->setNameFr($row['name_fr']);
        
        $jsonData[$row['id']]   = $row['name'];
        $jsonDataFr[$row['id']] = $row['name_fr'];

        $categ = $this->entityManager->getRepository(Category::class)->findOneById($row['category_id']); 
        $instance->setCategory($categ);
        $this->entityManager->persist($instance);    
      }
      $jsonData["0"]   = "All";
      $jsonDataFr["0"] = "Tous";

      /** generate dynamic translation files */
      $this->fileSystem->dumpFile($this->ressourceDir."/../../public/translations/discipline/en.json", json_encode($jsonData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
      $this->fileSystem->dumpFile($this->ressourceDir."/../../public/translations/discipline/fr.json", json_encode($jsonDataFr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
      
      return true;
    }

    private function razLanguage(){
      /** truncate table */
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('language'));
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('language_level'));
      
      /** get file data */
      $data = $this->getDataFrom("language.csv");
      $jsonData      = [];
      $jsonDataFr    = [];

      /** save new data */
      foreach ($data as $key => $row) {
        $instance = new Language();
        $instance->setName($row['name'])
        ->setNameFr($row['name_fr'])
        ->setCode($row['code']);

        /** populate jsonData */
        $jsonData[$row['id']] = array(
            "code" => $row['code'] , 
            "name" => $row['name'] 
        );
        $jsonDataFr[$row['id']] = array(
            "code" => $row['code'] , 
            "name" => $row['name_fr'] 
        );
        $this->entityManager->persist($instance);    
      }

      /**generate translation files */
      $this->fileSystem->dumpFile($this->ressourceDir."/../../assets/vue/lang/translations/language/en.json", json_encode($jsonData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
      $this->fileSystem->dumpFile($this->ressourceDir."/../../assets/vue/lang/translations/language/fr.json", json_encode($jsonDataFr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));


      /** get file data */
      $data = $this->getDataFrom("language_level.csv");

      /** save new data */
      foreach ($data as $key => $row) {
        $instance = new LanguageLevel();
        $instance->setName($row['name']);
        $this->entityManager->persist($instance);    
      }
      return true;
    }

    private function razLevel(){
      /** truncate table */
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('level'));
      
      /** get file data */
      $data = $this->getDataFrom("level.csv");

      /** save new data */
      foreach ($data as $key => $row) {
        $instance = new Level();
        $instance->setName($row["name"]);
        $this->entityManager->persist($instance);    
      }
      
      return true;
    }

    private function razContinent(){
      /** truncate table */
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('common_continent'));
      
      /** get file data */
      $data = $this->getDataFrom("continent.csv");

      $jsonData      = [];
      $jsonDataFr    = [];
     
      /** save new data */
      foreach ($data as $key => $row) {
        $instance = new Continent();
        $instance->setName($row['name']);
        $this->entityManager->persist($instance);
        
        /** populate jsonData */
        /** populate jsonData */
        $jsonData[$row['id']] =  $row['name'];
        $jsonDataFr[$row['id']] =  $row['name_fr'];
      }
      $jsonData["0"]   = "All";
      $jsonDataFr["0"] = "Tous";
      /**Flush needed for country search */
      $this->fileSystem->dumpFile($this->ressourceDir."/../../assets/vue/lang/translations/continent/en.json", json_encode($jsonData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
      $this->fileSystem->dumpFile($this->ressourceDir."/../../assets/vue/lang/translations/continent/fr.json", json_encode($jsonDataFr , JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        
      $this->entityManager->flush();
      return true;
    }

    private function razActivityCause(){
      /** truncate table */
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('activity_cause'));
      
      /** get file data */
      $data = $this->getDataFrom("activity_cause.csv");

      /** save new data */
      foreach ($data as $key => $row) {
        $instance = new ActivityCause();
        $instance->setName($row['name']);
        $instance->setLabel($row['label']);
        $this->entityManager->persist($instance);    
      }
      
      return true;
    }

    private function razGrade(){
      /** truncate table */
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('common_grade'));
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('student_formation'));
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('student_school'));
      
      /** get file data */
      $data = $this->getDataFrom("common_grade.csv");

      /** save new data */
      foreach ($data as $key => $row) {
        $instance = new Grade();
        $instance->setName($row['name']);
        $this->entityManager->persist($instance);    
      }
      
      return true;
    }

    private function razCurrency(){
        /** truncate table */
        $this->connection->executeUpdate($this->platform->getTruncateTableSQL('currency'));

        /** get file data */
        $data = $this->getDataFrom("currency.csv");
        $ressData    = [];
        /** save new data */
        foreach ($data as $key => $row) {
            $instance = new Currency();
            $instance->setName($row["name"]);
            $instance->setCode($row["code"]);
            $instance->setUsdRate(floatval($row["usdRate"]));
            
            /** Populate front ressource data */
            $ressData[$row['id']] = array(
              "id" =>   $row['id'], 
              "code" => $row['code'], 
              "name" => $row['name'], 
              "usdRate" => $row['usdRate'] 
            );

            $this->entityManager->persist($instance);
        }

        $this->entityManager->flush();
        /** suite à l'ajout de l'entité currency, rajouter des devises par défaut sur les bourses qui n'en ont pas EURO OU USD */
        /** @var Scholarship[] $scholarships */
        $scholarships = $this->entityManager->getRepository(Scholarship::class)->findBy(['currency' => null]);
        if(!empty($scholarships)) {
            /** @var Currency[] $defaultCurrency USD et EURO*/
            $defaultCurrency = $this->entityManager->getRepository(Currency::class)->findBy(['id' => [1,2]]);
            foreach ( $scholarships as $scholarship) {
                $rand = random_int(0,1);
                $scholarship->setCurrency($defaultCurrency[$rand]);
            }
        }

        /*** generate ressource front data */
        $this->fileSystem->dumpFile($this->ressourceDir."/../../assets/vue/ressources/currency.json", json_encode($ressData , JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

        return true;
    }

    private function razOrganizationType(){
      /** truncate table */
      $this->connection->executeUpdate($this->platform->getTruncateTableSQL('organization_type'));
      
      /** get file data */
      $data = $this->getDataFrom("organization_type.csv");
      $jsonData      = [];
      $jsonDataFr    = [];

      /** save new data */
      foreach ($data as $key => $row) {
        $instance = new OrganizationType();
        $instance->setName($row['name']);

        /** populate jsonData */
        $jsonData[$row['id']] =  $row['name'];
        $jsonDataFr[$row['id']] =  $row['name_fr'];
        $this->entityManager->persist($instance);    
      }

      /**generate translation files */
      $this->fileSystem->dumpFile($this->ressourceDir."/../../assets/vue/lang/translations/organizationType/en.json", json_encode($jsonData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
      $this->fileSystem->dumpFile($this->ressourceDir."/../../assets/vue/lang/translations/organizationType/fr.json", json_encode($jsonDataFr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

      return true;
    }


    private function getDataFrom($file_name){
      $file = file_get_contents($this->ressourceDir."/$file_name");
      return $this->encoder->decode($file, 'csv');
    }

}
