<?php
namespace App\Command\Job;

use App\Entity\Todo\Todo;
use App\Service\Mailer\Mailer;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TodoAlertJob extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'grimp:todo:alert';

    private $entityManager;
    private $todoManager;
    private $mailer;

    public function __construct(EntityManagerInterface $entityManager, Mailer $mailer)
    {
        $this->entityManager = $entityManager;
        $this->todoManager   = $entityManager->getRepository(Todo::class);
        $this->mailer = $mailer;

        parent::__construct();
    }

    protected function configure()
    {
        
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
      $output->writeln('Check user alert : last check -> '.(new \DateTime())->format("Y-m-d h:i:s"));
      
      $alerts = $this->todoManager->getScheduledTodo();
      if($alerts){
        foreach($alerts as $todo){

          $this->mailer->sendTodoAlert($todo);
          $todo->setSent(true);
          $this->entityManager->persist($todo);
          $output->writeln('send email to :'.$todo->getOwner()->getEmail());
        }
        $this->entityManager->flush();
      }else{
        $output->writeln('No alert found');
      }
      $output->writeln('************************************ SUCCESS ***********************************');
      return Command::SUCCESS;
    }

}
