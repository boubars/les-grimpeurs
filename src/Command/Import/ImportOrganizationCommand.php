<?php
namespace App\Command\Import;

use App\Entity\Common\Gallery;
use App\Entity\Common\SocialNetwork;
use App\Entity\Organization\Organization;
use App\Entity\Organization\Statistical;
use DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class for read, and persist csv organization file
 */
class ImportOrganizationCommand extends AbstractImportComand
{
    /**
     * Static name of the command (the part after "bin/console")
     *
     * @var string
     */
    protected static $defaultName = 'grimp:import:data:organization';
    
    /**
     * Command short description
     * 
     * @var string
     */
    protected static $defaultDescription = 'Import organization CSV Data';

    /**
     * Command helper text
     *
     * @var string
     */
    protected $defaultHelper = 'This command allows you to import organization from a CSV file';


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->output->writeln('<info>Import organization started</info>');

        $path = $input->getArgument('path');

        if(!$path) {
            $path = $this->askFilePath($input);
        }
        
        $validData = $this->getCsvData($path);

        if (!$validData) {
            $this->output->writeln('<error>Invalid csv file</error>');   
        }

        return $this->startImport($validData);
    }

    /**
     * Import entry point function
     *
     * @return void
     */
    private function startImport($data){

        if(!$data){
            $this->output->writeln('<error>Unable to import organization on empty data</error>');
            return Command::FAILURE;
        }

        $success = $this->saveData($data);

        $this->output->writeln('<info>Import organization successfully</info>');
        return Command::SUCCESS;
    }
    
    

    /**
     * Loop array csv data and persit 
     *
     * @param Array $array
     * @return Boolean
     */
    private function saveData($array){
        $this->output->writeln("<info>Persist data ...</info>");
        $count = 0;
        foreach ($array as $key => $value) {
            
            $value = array_values($value);

            $this->output->writeln("Start persist -> $value[1]");
            
            $organization = $this->arrayToOrganization($value);
            try {
                $count++;
                $this->entityManager->persist($organization);
                $this->output->writeln("<info> --------------> $value[1] persisted successfully </info>");
                if ($count%500 == 0) {
                    $this->output->writeln("<info> --------------> Flushing $count data </info>");
                    $this->entityManager->flush();
                }
            } catch (\Exception $e){
                dump($e->getMessage());
                dump($e->getTraceAsString());die;
            }


        }

        $this->entityManager->flush();
        return true;
    }

    /**
     * Covert array data into Organization Entity object
     *
     * @param [Array $array
     * @return Organization
     */
    private function arrayToOrganization($array){
        
        $organization = new Organization();
        $organization
           ->setName($array[1])
           ->setInstitutionType($this->getInstitutionType($array[2]))
           ->setLogo($this->uploadFile($array[3], "logo"))
           ->setYearOfCreation($array[4])
           ->setPresidentName($array[5])
           //->setSiretNumber($array[6])
           ->setAccreditation($array[6])
           ->setDescription($array[7])
           ->setCountry($this->getCountryByCode($array[8]))
           ->setCountryActionArray($this->getCountryByCode($array[9], true))
           ->setAdresse($array[10])
           ->setAdresseMap($this->getAdresseMap($array[10]))
           ->setPostalCode($array[11])
           ->setContactName($array[12])
           ->setContactLastName($array[13])
           ->setContactCountry($this->getCountryByCode($array[14]))
           
           ->setContactPostalCode($array[16])
           ->setOrganizationEmail($array[17])
           ->setPhoneNumber($array[18])

           ->setStatistical($this->getStatistical($array))
           ->setWebsite($array[25])
           ->setSocialNetwork($this->getSocialNetwork($array))
           ->setCoverPicture($this->uploadFile($array[31], "picture"))
           ->setGallery($this->getGallery($array))
           ->setFirstname($array[34])
           ->setLastname($array[35])
           //->setDateOfBirth($array[36] ? new \DateTime($array[36]) : null)
           ->setPhone($array[36])
           /*** user fields */
           ->setAdress($array[16])
           ->setAvatar($this->uploadFile($array[20], "avatar"))
           ->setEmail($array[37])
           ->setToken("added_from_admin")
           ->setPassword('grimpeursorg')
           ->setEnable(true);

        return $organization;
    }

    /**
     * Get Statistical object from the array
     *
     * @param Array $array
     * @return Statistical
     */
    private function getStatistical($array){
        return (new Statistical())
        ->setScholarshipAwarded((int)$array[20])
        ->setAcceptanceRate($array[21])
        ->setMenCount((int)$array[22])
        ->setWomenCount((int)$array[23])
        ->setScholarshipCount((int)$array[24]);
    }

    /**
     * Get social network data in the array
     *
     * @param Array $array
     * @return SocialNetwork
     */
    private function getSocialNetwork($array){
        return (new SocialNetwork())
        ->setFacebook($array[26])
            ->setYoutube($array[27])
        ->setTwitter($array[28])
        ->setInstagram($array[29])
        ->setLinkedin($array[30]);
    }

    /**
     * Get gallery object througout data csv
     *
     * @param Array $array
     * @return Gallery
     */
    private function getGallery($array){
        $gallery = $this->getGalleryPhoto(new Gallery(), $array[32]);
        $gallery = $this->getGalleryVideos($gallery, $array[33]);
        return $gallery;
    }


    /**
     * Upload and get pictures gallery
     * @param Gallery $gallery
     * @param String  $pictures
     * @return Gallery
     */
    private function getGalleryPhoto(Gallery $gallery, $pictures){
        /** gallery photo */
        $pictures = explode(',', $pictures);
        foreach ($pictures as $key => $file) {
            if(!$file){
                continue;
            }
            $newFilename = $this->uploadFile($file, "picture");
            $gallery->addPicture($newFilename); 
        }
        return $gallery;
    }

    /**
     * Get and sanitize gallery videos data thoughout csv data
     *
     * @param Gallery $gallery
     * @param String $videos
     * @return Gallery
     */
    private function getGalleryVideos(Gallery $gallery, $videos){
        $videos = explode(',', $videos);

        if(empty($videos)){
            return $gallery;
        }

        $data = [];
        foreach ($videos as $key => $video) {
            $type = strpos($video, 'youtube') ? "YOUTUBE" : (strpos($video, 'vimeo') ? 'VIMEO' : 'INKNOW' );
            $data[] = [
                'type'     => $type,
                'Url'      => $video,
                'thumbUrl' => $this->getVideoThumbnail($video),
                'isValid'  => 1
            ];
        }
        $gallery->setVideos($data);
        return $gallery;
    }
}