<?php
namespace App\Command\Import;

use App\Entity\Common\Continent;
use App\Entity\Common\Country;
use App\Entity\Common\Currency;
use App\Entity\Common\Level;
use App\Entity\Discipline\Category;
use App\Entity\Discipline\Discipline;
use App\Entity\Organization\OrganizationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class  AbstractImportComand extends Command
{
    /**
     * Static name of the command (the part after "bin/console")
     *
     * @var string
     */
    protected static $defaultName = "grimp:import:data";

    /**
     * Command short description
     *
     * @var string
     */
    protected static $defaultDescription = "Abstract command for import organization or scholarhship";

    /**
     * Command helper text
     *
     * @var string
     */
    protected $defaultHelper = 'add option after command name';

    /**
     * Project root Folder
     *
     * @var String
     */
    protected $rootFolder;

    /**
     * Resource root folder path
     *
     * @var String
     */
    protected $resFolder;

    /**
     * Global command output interface
     *
     * @var OutputInterface
     */
    protected $output;
    
    /**
     * Global CsvEncoder 
     *
     * @var CsvEncoder
     */
    protected $encoder;

    /**
     * Global EntityManagerInterface  variable
     *
     * @var [type]
     */
    protected $entityManager;

    /**
     * SlugerInterface  variable
     *
     * @var [type]
     */
    protected $slugger;

    /**
     * Global PramaeterBagInterface variable
     *
     * @var ParameterBagInterface
     */
    protected $params;

    /**
     * Http client interface 
     *
     * @var HttpClientInterface
     */
    protected $http;

    public function __construct(ParameterBagInterface $params, EntityManagerInterface $entityManager , SluggerInterface $slugger, HttpClientInterface $http)
    {
        $this->rootFolder    = $params->get('project_dir');
        $this->encoder       = new CsvEncoder();
        $this->entityManager = $entityManager;
        $this->slugger       = $slugger; 
        $this->params        = $params;
        $this->http          = $http;

        parent::__construct();
    }

    /**
     * Command configuration
     *
     * @return void
     */
    protected function configure()
    {
        $this
        ->setDescription(self::$defaultDescription)
        ->setHelp($this->defaultHelper)
        ->addArgument('path', InputArgument::OPTIONAL, 'CSV File path');
    }


    /**
     * Answer user file path
     *
     * @param [type] $input
     * @return String
     */
    protected function askFilePath($input){
        $helper = $this->getHelper('question');
        $question = new Question('CSV File path :', '/path/to/csvfile.csv');
        $question->setValidator(function($answer){
            if(!file_exists($answer)){
                throw new \RuntimeException('File not found, retype again or ctrl+c for exit');
            }
            return $answer; 
        });
        $path =  $helper->ask($input, $this->output, $question);
        $this->resFolder = dirname($path);
        return $path;
    }

    /**
     * Read and get CSV content as Array, return false if an error occured
     *
     * @return array|Boolean
     */
    protected function getCsvData($path){
      
        $this->output->writeln('Reading source file..... ');
        $aData = [];
        $i = 0;
        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if( $i >= 1 && !empty($data[1])) {
                    $aData[] = $data;
                }
                $i++;
            }
            fclose($handle);
        }

        return $aData;
    }

    /**
     * Find InstitutionType by name
     *
     * @param String $name
     * @return OrganizationType
     */
    protected function getInstitutionType($name){
        $manager = $this->entityManager->getRepository(OrganizationType::class);
        return $manager->find( preg_replace("/[^0-9 ]/", '', $name));
    }

    /**
     * Find continent by name
     * @param String $name
     * @return Continent
     */
    protected function getContinent($name, $multiple = false ){
        $name    = strtolower($name);
        $manager = $this->entityManager->getRepository(Continent::class);
        if($multiple){
            $names = explode(',', $name);
            $names = array_map('ucfirst', $names);
            return $manager->findBy([
                'name' => $names
            ]);
        }
        return $manager->findOneByCode(ucfirst($name)); 
    }

    /**
     * Find country by code
     * @param String $code
     * @return Country
     */
    protected function getCountryByCode($code, $multiple = false ){
        $code    = strtoupper($code);
        $manager = $this->entityManager->getRepository(Country::class);

        if($multiple){
            $codes = explode(',', $code);
            $codes = array_map('strtoupper', $codes);
            return $manager->findBy([
                'code' => $codes
            ]);
        }
        return $manager->findOneByCode($code); 
    }

     /**
     * Upload and retourne file name
     *
     * @param String $name      [File name]
     * @param String $directory [Target directory name]
     * @return String
     */
    protected function uploadFile($originalFilename, $directory){
        if(!$originalFilename){
            return null;
        }

        $originalFilename = str_replace(" ","", $originalFilename);

        $path = sprintf("%s/images/%s", $this->resFolder, $originalFilename);
        $document =  new File($path);

        $safeFilename = $this->slugger->slug($originalFilename);
        $newFilename = $safeFilename.'-'.uniqid().'.'.$document->guessExtension();
        $target = $this->rootFolder."/public/uploads/".$directory."/".$newFilename;

        $success = copy($path, $target);
        
        return $success ? $newFilename : $success;

    }



    /**
     * Find Studyfield (discipline/category) by name
     * @param String $name
     * @return Category
     */
    protected function getStudifield($name, $multiple = false ){
        $manager = $this->entityManager->getRepository(Category::class);
        if($multiple){
            $names = explode(',', $name);
            return $manager->findBy([
                'name' => $names
            ]);
        }
        return $manager->findOneByName($name); 
    }


    /**
     * Find discipline by name
     * @param String $name
     * @return Discipline
     */
    protected function getDiscipline($name, $multiple = false ){
        $manager = $this->entityManager->getRepository(Discipline::class);
        if($multiple){
            $names = explode(',', $name);
            return $manager->findBy([
                'name' => $names
            ]);
        }
        return $manager->findOneByName($name); 
    }

    /**
     * Find Level by name
     * @param String $name
     * @return Level
     */
    protected function getLevel($name, $multiple = false ){
        $manager = $this->entityManager->getRepository(Level::class);
        if($multiple){
            $names = explode(',', $name);
            return $manager->findBy([
                'name' => $names
            ]);
        }
        return $manager->findOneByName($name); 
    }
    
    /**
     * Find currency by code
     *
     * @return Currency
     */
    protected function getCurrency($code, $multiple = false){
        $manager = $this->entityManager->getRepository(Currency::class);
        if($multiple){
            $codes = explode(',', $code);
            return $manager->findBy([
                'code' => $codes
            ]);
        }
        return $manager->findOneByCode($code);  
    }

    /**
     * Get and upload adresse maps image
     *
     * @param String  $adresse
     * @return String|null
     */
    protected function getAdresseMap($adresse){

        if(!$adresse){
            return null;
        }

        $gmapKey = $this->params->get('gmap_key');
        $url = "https://maps.googleapis.com/maps/api/staticmap?center=%s&&markers=size:mid|color:red|%s&zoom=14&size=400x400&key=%s";
        $endPoints = sprintf($url, $adresse, $adresse, $gmapKey);

        try {
            $response = $this->http->request(  'GET', $endPoints);

            $blob = $response->getContent();

            $newFilename = "map-".uniqid().'.png';
            $target = $this->params->get('project_dir')."/public/uploads/maps/".$newFilename;

            return file_put_contents($target, $blob) ? $newFilename : null;
        } catch (\Exception $e) {

            return null;
        }



    }


    protected function getVideoThumbnail($video){
        if(!$video){
            return null;
        }
        
        if(strpos('vimeo', $video)){
            return $this->getVimeoThumbnailUrl($video);
        }

        /** if youtube */
        $video_id = str_replace(['https://youtu.be/', 'https://www.youtube.com/watch?v=', 'https://youtube.com/watch?v='], '', $video);
        return "http://img.youtube.com/vi/$video_id/maxresdefault.jpg";
        
    }

    /**
     * Grab the url of a publicly embeddable video hosted on vimeo
     * @param  String $video_url The "embed" url of a video
     * @return String The url of the thumbnail, or false if there's an error
     */
    private function getVimeoThumbnailUrl($vimeo_url){
        if( !$vimeo_url ){
            return false;
        } 
        $data = json_decode( file_get_contents( 'http://vimeo.com/api/oembed.json?url=' . $vimeo_url ) );

        if( !$data ) return false;

        return $data->thumbnail_url;
    }

}   