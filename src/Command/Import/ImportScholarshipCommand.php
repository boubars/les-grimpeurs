<?php

namespace App\Command\Import;

use App\Entity\Common\Gallery;
use App\Entity\Common\SocialNetwork;
use App\Entity\Organization\Organization;
use App\Entity\Scholarship\Scholarship;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class ImportScholarshipCommand extends AbstractImportComand
{
    /**
     * Static name of the command (the part after "bin/console")
     *
     * @var string
     */
    protected static $defaultName = 'grimp:import:data:scholarship';
    
    /**
     * Command short description
     *
     * @var string
     */
    protected static $defaultDescription = 'Import organization CSV Data';

    /**
     * Command helper text
     *
     * @var string
     */
    protected $defaultHelper = 'This command allows you to import scholarship from a CSV file';

    /**
     * Command entry point method
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return integer
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output = $output;
        $this->output->writeln('<info>scholarhip Import started</info>');

        $path = $input->getArgument('path');

        if(!$path) {
            $path = $this->askFilePath($input);
        }
        
        $validData = $this->getCsvData($path);

        if (!$validData) {
            $this->output->writeln('<error>Invalid csv file</error>');   
        }
        
        $success = $this->startImport($validData);
        
        return Command::SUCCESS;
    }


    /**
     * Scholarhsip import entry point
     *
     * @param Array $path
     * @return boolean
     */
    private function startImport($data){
        $this->output->writeln("<info>Persist data ...</info>");

        foreach ($data as $key => $value) {
            
            $value = array_values($value);

            $this->output->writeln("Start persist -> $value[0]");
            
            $scholarhsip = $this->arrayToScholarship($value);
            
            $this->entityManager->persist($scholarhsip);

            $this->output->writeln("<info> --------------> $value[0] persisted successfully </info>");
        }

        $this->entityManager->flush();
        return true;
    }

    /**
     * Convert an array to Scholarship entity opbject
     *
     * @param Array $array
     * @return Scholarship
     */
    private function arrayToScholarship($array){
        $scholarhsip = new Scholarship();

        return $scholarhsip
        ->setName($array[0])
        ->setOriginCountry($this->getCountryByCode($array[1]))
        ->setAwardOrganizationType($this->getInstitutionType($array[2]))
        ->setRelatedOrganization($this->getOrganizationById($array[3]))
        ->setLogo($this->uploadFile($array[4], 'logo'))
        ->setCadidateOption('external')
        ->setCadidateOptionUrl($array[5])
        ->setCandidatureLimit($array[6] ?? null)
        ->setGrantedContinentArray($this->getContinent($array[7], true))
        ->setStudyFieldArray($this->getStudifield($array[8], true))
        ->setStudyFieldTagArray($this->getDiscipline($array[9], true))
        ->setGrantedLevelArray($this->getLevel($array[10], true))

        /*** change '-' according age separator char */
        ->setMaxAge( explode("-" , $array[11])[0] ?? null)
        ->setMinAge( explode("-" , $array[11])[1] ?? null)

        ->setGrantedSex($array[12])
        
        /** update ',' accordig critére de selection separator  */
        ->setSelectionCriteria(explode(",", $array[13]))

        ->setContactName($array[14])
        ->setContactEmail($array[15])
        ->setContactAdresse($array[16])
        ->setContactAdresseMap($this->getAdresseMap($array[16]))

        ->setContactPhone($array[17])
        ->setAmount($array[18])
        ->setCurrency($this->getCurrency($array[19]))
        ->setCount($array[20])
        ->setDuration($array[21])
        ->setDurationUnit($array[22])

        /** update ',' accordig critére de selection separator  */
        ->setAttachments(explode(',', $array[23]))
        ->setAdvantages(explode(',', $array[24]))
        ->setOtherInfos($array[25])
        ->setStory($array[26])

        /*** à reverifié les cles apres l'ajout de twitter */
        ->setSocialNetwork($this->getSocialNetwork($array))

        ->setGallery($this->getGallery($array[31]));
    }

    /**
     * Find organization by ID
     *
     * @param Integer|String $id
     * @return Organization|null
     */
    private function getOrganizationById($id){
        $manager = $this->entityManager->getRepository(Organization::class);
        return $manager->findById($id);
    }

    /**
     * Get social network data in the array
     *
     * @param Array $array
     * @return SocialNetwork
     */
    private function getSocialNetwork($array){
        return (new SocialNetwork())
        ->setFacebook($array[27])
        ->setTwitter($array[28])
        ->setInstagram($array[29])
        ->setLinkedin($array[30]);
    }

    /**
     * Convert string pictures list into gallery object
     *
     * @param String $pictures
     * @return Gallery
     */
    private function getGallery($pictures){
        $gallery = new Gallery();
       
        $pictures = explode(',', $pictures);
        foreach ($pictures as $key => $file) {
            if(!$file){
                continue;
            }
            $newFilename = $this->uploadFile($file, "picture");
            $gallery->addPicture($newFilename); 
        }
        return $gallery;
    }


}
