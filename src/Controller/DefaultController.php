<?php
namespace App\Controller;

use App\Controller\Api\Scholarship\ViewScholarshipAction as ScholarshipViewScholarshipAction;
use App\Controller\Scholarship\ViewScholarshipAction;
use App\Service\Utilis\Base64Converter;
use PhpParser\Node\Expr\Cast\String_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class DefaultController extends AbstractController
{
    /**
     * @Route("/scholarship/{slug}", name="view_scholarship")
     */
    public function viewScholarship($slug)
    {
        return $this->forward(ScholarshipViewScholarshipAction::class, ['slug' => $slug]);
    }

    /**
     * @Route("/email/test", name="email_test")
     */
    public function quickTestMail()
    {

        return $this->render('emails/user/confirmation_acount.html.twig', array('link' => ''));
    }


    /**
     * @Route("/{path}", name="home", requirements={"path":"^(?!api).*$"})
     */
    public function index($path)
    {
        if ($path === 'scholarship') {

        }
        return $this->render('index.html.twig');
    }
}

