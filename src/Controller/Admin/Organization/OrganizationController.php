<?php

namespace App\Controller\Admin\Organization;

use App\Controller\Admin\AbstractAdminController;
use App\Entity\Common\Gallery;
use App\Entity\Company\Company;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationType as OrganizationOrganizationType;
use App\Entity\University\University;
use App\Form\Admin\OrganizationType;
use App\Repository\Organization\OrganizationRepository;
use App\Service\Utilis\UploadedBase64File;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/{_locale<en|fr>}/admin/organization")
 */
class OrganizationController extends AbstractAdminController
{
    /**
     * OrganizationOrganizationType variable
     *
     * @var OrganizationOrganizationTypeReposotory
     */
    private $organizationTypeManager;

    /**
     * Controller connstructor
     *
     * @param EntityManagerInterface $entityManager
     * @param SluggerInterface $sluger
     */
    public function __construct(EntityManagerInterface $entityManager, SluggerInterface $sluger)
    {
        $this->organizationTypeManager = $entityManager->getRepository(OrganizationOrganizationType::class);    
        parent::__construct($sluger);
    }

    /**
     * Organization admin index route
     *
     * @Route("/", name="admin_organization_index", methods={"GET"})
     * @param  Request $request
     * @param  OrganizationRepository $organizationRepository
     * @return Response
     */
    public function index(Request $request, OrganizationRepository $organizationRepository): Response
    {
        /** rewrite params */
        $params = $this->getPostedParams($request->query->all());

        $data = $this->getDataTablePaginator($request, $organizationRepository, 'organizations', $params);
        return $this->render('admin/organization/index.html.twig', $data);
    }

    /**
     * Admin Add organization action route
     *
     * @Route("/new", name="admin_organization_new", methods={"GET","POST"})
      * @param Request $request
      * @param UserPasswordEncoderInterface $passwordEncoder
      * @return Response
      */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $locale = $request->getLocale();

        $organization = $this->createOrganization($request);
        $form = $this->createForm(OrganizationType::class, $organization, ["locale" => $locale]);
        
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $this->handleChange($form, $request, $organization);

            /*** handle password */
            $organization->setToken("added_from_admin");
            $organization->setPassword($passwordEncoder->encodePassword($organization,  $organization->getPassword()));
            
            
            $entityManager->persist($organization);
            $entityManager->flush();

            return $this->redirectToRoute('admin_organization_index');
        }

        return $this->render('admin/organization/new.html.twig', [
            'organization' => $organization,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Admin show organization detail infos
     *
     * @Route("/{id}", name="admin_organization_show", methods={"GET"})
     * @param  Organization $organization
     * @return Response
     */
    public function show(Organization $organization): Response
    {
        return $this->render('admin/organization/show.html.twig', [
            'organization' => $organization,
        ]);
    }

    /**
     * Admin edit organizaiton method
     *
     * @Route("/{id}/edit", name="admin_organization_edit", methods={"GET","POST"})
     * @param  Request $request
     * @param  Organization $organization
     * @param  UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function edit(Request $request, Organization $organization, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $locale = $request->getLocale();
        
        $form = $this->createForm(OrganizationType::class, $organization ,  ['locale'=> $locale]);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $this->handleChange($form, $request, $organization);
            
            $organization->setPassword($passwordEncoder->encodePassword($organization,  $organization->getPassword()));
            $this->getDoctrine()->getManager()->persist($organization);

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_organization_index');
        }

        return $this->render('admin/organization/edit.html.twig', [
            'organization' => $organization,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Admin handle organization submit change
     *
     * @param FormInterface $form
     * @param Request $request
     * @param Organization $organization
     * @return void
     */
    public function handleChange($form, $request, &$organization):void
    {

        /*** handle logo upload */
        $this->handleUploadLogo($form, $request, $organization);

        /*** handle cover upload */
        $this->handleUploadCover($form, $request, $organization);

        /*** handle avatar upload */
        $this->handleUploadAvatar($form, $request, $organization);
        
        /*** handle gallery upload */
        $this->handleUploadGalleryPhoto($request, $organization);
        $this->handleUploadGalleryVideo($request, $organization);

        /*** handle map image upload */
        $this->handleUploadAdresseMap($form, $request, $organization);
    }

    /**
     * Admin handle on delete organization
     *
     * @Route("/{id}", name="admin_organization_delete", methods={"DELETE"})
     * @param Request $request
     * @param Organization $organization
     * @return Response
     */
    public function delete(Request $request, Organization $organization): Response
    {
        if ($this->isCsrfTokenValid('delete'.$organization->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($organization);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_organization_index');
    }

    /**
     * set and update Organization logo
     *
     * @param  FormInterface $form
     * @param  Request $request
     * @param  Organization $organization
     * @return void
     */
    private function handleUploadLogo($form, $request, &$organization)
    {
        $file = $form['logo']->getData();
        if(!$file){
            //throw new Exception("File logo not found", 1);       
            return false;
        }

        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $file64 = new UploadedBase64File($request->request->get('logo64'),$originalFilename);
        $fileName = $this->uploadDocument($file64, "logo_dir");

        $organization->setLogo($fileName);
    }

    /**
     * set and update organziation cover picture
     *
     * @param  FormInterface $form
     * @param  Request $request
     * @param  Organization $organization
     * @return void
     */
    private function handleUploadCover($form, $request, &$organization)
    {
        $file = $form['coverPicture']->getData();
        if(!$file){
            //throw new Exception("File cover not found", 1);       
            return false;
        }

        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $file64 = new UploadedBase64File($request->request->get('coverPicture64'),$originalFilename);
        $fileName = $this->uploadDocument($file64, "picture_dir");

        $organization->setCoverPicture($fileName);
    }

    /**
     * set and upload organization contact avatar
     *
     * @param  FormInterface $form
     * @param  Request $request
     * @param  Organization $organization
     * @return void
     */
    private function handleUploadAvatar($form, $request, &$organization)
    {
        $file = $form['avatar']->getData();
        if(!$file){
            //throw new Exception("File avatar not found", 1);
            return false;       
        }

        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $file64 = new UploadedBase64File($request->request->get('avatar64'),$originalFilename);
        $fileName = $this->uploadDocument($file64, "avatar_dir");

        $organization->setAvatar($fileName);
    }

    /**
     * handle gallery pictures
     *
     * @param  Request $request
     * @param  Organization $organization
     * @return void
     */
    private function handleUploadGalleryPhoto($request, &$organization){
        $gallery = $organization->getGallery() ?? new Gallery();
        /** white list image gallery */
        $gallery->setPictures([]);
        /** gallery photo */
        $galeryPhotoFiles = $request->files->get('galeryPhoto');
        if(!$galeryPhotoFiles){
            //throw new Exception("File avatar not found", 1);
            return false;       
        }

        foreach ($galeryPhotoFiles as $key => $file) {
            $newFilename = $this->uploadDocument($file, "picture_dir");
            $gallery->addPicture($newFilename); 
        }

        $organization->setGallery($gallery);
    }

    /**
     * set and update organization gallery videos
     *
     * @param  Request $request
     * @param  Organization $organization
     * @return void
     */
    private function handleUploadGalleryVideo($request, &$organization){

        $gallery = $organization->getGallery() ?? new Gallery();
        $galleryVideo = json_decode($request->request->get("galleryVideo"), true);

        if(!$galleryVideo || empty($galleryVideo)){
            return false;
        }

        $gallery->setVideos($galleryVideo);
        $organization->setGallery($gallery);
    }

    /**
     * updat eorganization maps picture
     *
     * @param  FormInterface $form
     * @param  Request $request
     * @param  Organization $organization
     * @return void
     */
    private function handleUploadAdresseMap($form, $request, &$organization)
    {
        $postedData = $form->get("adresseMap")->getData();
        /*** avoid change */
        if(preg_match('%data:image/png;base64,%',$organization->getAdresseMap())){
            $originalFilename = pathinfo("map_", PATHINFO_FILENAME);
            $file64 = new UploadedBase64File($organization->getAdresseMap(), $originalFilename);
            $fileName = $this->uploadDocument($file64, "map_dir");
            $organization->setAdresseMap($fileName);
        }

        /*** update adresseLocation */
        $location = json_decode($request->request->get("adresseLocation"), true);

        if($location){
            $organization->setAdresseLocation($location);
        }

    }

    /**
     * Organization factory method
     *
     * @param  Request $request
     * @return Organization
     */
    private function createOrganization($request):Organization
    {
        $postedData = $request->request->get('organization');
        
        /*** if method is get */
        if(!$postedData){
            return new Organization();
        }

        $institution = $this->organizationTypeManager->findOneById($postedData['institutionType']);
        switch (strtolower($institution->getName())) {
            case 'university':
                return new University();
            break;
            
            case 'company':
                return new Company();
            break;
            
            default:
              return new Organization();
            break;
        }
    }

    /**
     * tranform posted data array 
     *
     * @param [type] $array
     * @return void
     */
    private function getPostedParams($array){
        if(empty($array)){
            return $array;
        }

        if(isset($array['institution'])){
            $institution = $array['institution'];
            
            //** remove array item */
            unset($array['institution']);
            
            /** rename institution value */
            $institution = str_replace('-',' ', $institution);
            $institution = str_replace('obsl','OBSL', $institution);

            if($institution == 'all'){
                return $array;
            }
            
            $institution = $this->organizationTypeManager->findOneByName($institution);
            $array['institutionType'] = $institution;
            return $array;
        }

        return false;
    }

}
