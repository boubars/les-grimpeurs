<?php

namespace App\Controller\Admin\Language;

use App\Controller\Admin\AbstractAdminController;
use App\Entity\Common\Language;
use App\Form\Common\LanguageType;
use App\Repository\Common\LanguageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale<en|fr>}/admin/language")
 */
class AdminLanguageController extends AbstractAdminController
{
    private $languageRepository;
    private $ressourceDir;
    private $fileSystem;

    public function __construct(EntityManagerInterface $entityManager, ParameterBagInterface $params)
    {
        $this->languageRepository = $entityManager->getRepository(Language::class);
        $this->ressourceDir = $params->get('project_dir').'/res/data';

        $this->fileSystem = new Filesystem();

    }
    /**
     * @Route("/", name="admin_language_index", methods={"GET"})
     */
    public function index(Request $request, LanguageRepository $languageRepository): Response
    {
        $data = $this->getDataTablePaginator($request, $languageRepository, 'languages') ;
        return $this->render('admin/language/index.html.twig', $data);
    }

    /**
     * @Route("/new", name="admin_language_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $language = new Language();
        $form = $this->createForm(LanguageType::class, $language);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($language);
            $entityManager->flush();
            $this->razLanguageTranslation();
            return $this->redirectToRoute('admin_language_index');
        }

        return $this->render('admin/language/new.html.twig', [
            'language' => $language,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_language_show", methods={"GET"})
     */
    public function show(Language $language): Response
    {
        return $this->render('admin/language/show.html.twig', [
            'language' => $language,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_language_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Language $language): Response
    {
        $form = $this->createForm(LanguageType::class, $language);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->razLanguageTranslation();
            return $this->redirectToRoute('admin_language_index');
        }

        return $this->render('admin/language/edit.html.twig', [
            'language' => $language,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_language_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Language $language): Response
    {
        if ($this->isCsrfTokenValid('delete'.$language->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($language);
            $entityManager->flush();
            $this->razLanguageTranslation();
        }

        return $this->redirectToRoute('admin_language_index');
    }

    private function razLanguageTranslation(){
       
        /** get file data */
        $data = $this->languageRepository->findAll();
        $jsonData      = [];
        $jsonDataFr    = [];

        
        /** save new data */
        foreach ($data as $key => $row) {
        
          /** populate jsonData */
          $jsonData[$row->getId()] = array(
              "code" => $row->getCode(), 
              "name" => $row->getName() 
          );
          $jsonDataFr[$row->getId()] = array(
              "code" => $row->getCode(), 
              "name" => $row->getNameFr() 
          ); 
        }
  
        /**generate translation files */
        $this->fileSystem->dumpFile($this->ressourceDir."/../../assets/vue/lang/translations/language/en.json", json_encode($jsonData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        $this->fileSystem->dumpFile($this->ressourceDir."/../../assets/vue/lang/translations/language/fr.json", json_encode($jsonDataFr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
      }
}
