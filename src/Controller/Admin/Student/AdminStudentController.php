<?php

namespace App\Controller\Admin\Student;

use App\Controller\Admin\AbstractAdminController;
use App\Entity\Student\Student;
use App\Form\Student\StudentType;
use App\Repository\Student\StudentRepository;
use App\Service\Utilis\UploadedBase64File;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/{_locale<en|fr>}/admin/student")
 */
class AdminStudentController extends AbstractAdminController
{
    /**
     * Admin student index route
     *
     * @Route("/", name="admin_student_index", methods={"GET"})
     * @param Request $request
     * @param StudentRepository $studentRepository
     * @return Response
     */
    public function index(Request $request, StudentRepository $studentRepository): Response
    {
        /** rewrite params */
        $params = $request->query->all();

        $data = $this->getDataTablePaginator($request, $studentRepository, 'students', $params);
        return $this->render('admin/student/index.html.twig', $data);
    }

    /**
     * @Route("/new", name="admin_student_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $student = new Student();
        $locale  = $request->getLocale();
        $form = $this->createForm(StudentType::class, $student, ['locale' => $locale]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $this->handleUploadAvatar($form, $request, $student);
            $student->setToken("added_from_admin");
            $student->setPassword($passwordEncoder->encodePassword($student,  $student->getPassword()));

            $entityManager->persist($student);
            $entityManager->flush();

            return $this->redirectToRoute('admin_student_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin/student/new.html.twig', [
            'student' => $student,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_student_show", methods={"GET"})
     */
    public function show(Student $student): Response
    {
        return $this->render('admin/student/show.html.twig', [
            'student' => $student,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_student_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Student $student): Response
    {
        
        $locale  = $request->getLocale();
        
        $form = $this->createForm(StudentType::class, $student, ['locale' => $locale]);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->handleUploadAvatar($form, $request, $student);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_student_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin/student/edit.html.twig', [
            'student' => $student,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Handle on delete user request
     *
     * @Route("/{id}", name="admin_student_delete", methods={"DELETE"})
     * @param Request $request
     * @param Student $student
     * @return Response
     */
    public function delete(Request $request, Student $student): Response
    {
        if ($this->isCsrfTokenValid('delete'.$student->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($student);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_student_index', [], Response::HTTP_SEE_OTHER);
    }
     /**
     * set and upload student  avatar
     *
     * @param  FormInterface $form
     * @param  Request $request
     * @param  Student $student
     * @return void
     */
    private function handleUploadAvatar($form, $request, &$student)
    {
        $file = $form['avatar']->getData();
        if(!$file){
            return false;       
        }

        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $file64 = new UploadedBase64File($request->request->get('avatar64'),$originalFilename);
        $fileName = $this->uploadDocument($file64, "avatar_dir");

        $student->setAvatar($fileName);
    }
}
