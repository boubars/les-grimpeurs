<?php

namespace App\Controller\Admin\User;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
* @Route("/{_locale<en|fr>}/admin/account/switch/enable" , name="admin_user_switch_account_status")
*/
class SwitchAccountStatus extends AbstractController{

  public function __invoke(Request $request, UserRepository $userManager, EntityManagerInterface $entityManager){
      $user = $request->request->get("user");
      $user = $userManager->findOneById($user);
      $user->setEnable(!$user->isEnable());
      $entityManager->persist($user);
      $entityManager->flush();

      return $this->redirect($request->headers->get('referer'));

  }

}