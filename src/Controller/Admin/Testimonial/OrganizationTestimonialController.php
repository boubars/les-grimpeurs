<?php

namespace App\Controller\Admin\Testimonial;

use App\Controller\FileUploaderAction;
use App\Entity\Testimonial\OrganizationTestimonial;
use App\Form\Testimonial\OrganizationTestimonialType;
use App\Repository\Testimonial\OrganizationTestimonialRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use App\Service\Utilis\UploadedBase64File;

/**
 * @Route("/{_locale<en|fr>}/admin/testimonial/organization")
 */
class OrganizationTestimonialController extends FileUploaderAction
{
    /**
     * @Route("/", name="admin_organization_testimonial_index", methods={"GET"})
     */
    public function index(OrganizationTestimonialRepository $organizationTestimonialRepository): Response
    {
        return $this->render('admin/testimonial/organization_testimonial/index.html.twig', [
            'organization_testimonials' => $organizationTestimonialRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_organization_testimonial_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $organizationTestimonial = new OrganizationTestimonial();
        $form = $this->createForm(OrganizationTestimonialType::class, $organizationTestimonial);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['thumbnail']->getData();
            
            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file64 = new UploadedBase64File($request->request->get('thumbnail64'),$originalFilename);
           
            $fileName = $this->uploadDocument($file64, "avatar_dir");

            $organizationTestimonial->setThumbnail($fileName);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($organizationTestimonial);
            $entityManager->flush();

            return $this->redirectToRoute('admin_organization_testimonial_index');
        }

        return $this->render('admin/testimonial/organization_testimonial/new.html.twig', [
            'organization_testimonial' => $organizationTestimonial,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_organization_testimonial_show", methods={"GET"})
     */
    public function show(OrganizationTestimonial $organizationTestimonial): Response
    {
        return $this->render('admin/testimonial/organization_testimonial/show.html.twig', [
            'organization_testimonial' => $organizationTestimonial,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_organization_testimonial_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, OrganizationTestimonial $organizationTestimonial): Response
    {
        $form = $this->createForm(OrganizationTestimonialType::class, $organizationTestimonial);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['thumbnail']->getData();
            
            if($file){
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $file64 = new UploadedBase64File($request->request->get('thumbnail64'),$originalFilename);
               
                $fileName = $this->uploadDocument($file64, "avatar_dir");
                $organizationTestimonial->setThumbnail($fileName);
            }
            $this->getDoctrine()->getManager()->persist($organizationTestimonial);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_organization_testimonial_index');
        }

        return $this->render('admin/testimonial/organization_testimonial/edit.html.twig', [
            'organization_testimonial' => $organizationTestimonial,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_organization_testimonial_delete", methods={"DELETE"})
     */
    public function delete(Request $request, OrganizationTestimonial $organizationTestimonial): Response
    {
        if ($this->isCsrfTokenValid('delete'.$organizationTestimonial->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($organizationTestimonial);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_organization_testimonial_index');
    }
}
