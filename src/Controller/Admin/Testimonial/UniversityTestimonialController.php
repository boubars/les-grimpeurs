<?php

namespace App\Controller\Admin\Testimonial;

use App\Controller\FileUploaderAction;
use App\Entity\Testimonial\UniversityTestimonial;
use App\Form\Testimonial\UniversityTestimonialType;
use App\Repository\Testimonial\UniversityTestimonialRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Utilis\UploadedBase64File;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/{_locale<en|fr>}/admin/testimonial/university")
 */
class UniversityTestimonialController extends FileUploaderAction
{
    /**
     * @Route("/", name="admin_university_testimonial_index", methods={"GET"})
     */
    public function index(UniversityTestimonialRepository $universityTestimonialRepository): Response
    {
        return $this->render('admin/testimonial/university_testimonial/index.html.twig', [
            'university_testimonials' => $universityTestimonialRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_university_testimonial_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $universityTestimonial = new UniversityTestimonial();
        $form = $this->createForm(UniversityTestimonialType::class, $universityTestimonial);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $file = $form['thumbnail']->getData();
            
            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file64 = new UploadedBase64File($request->request->get('thumbnail64'),$originalFilename);
           
            $fileName = $this->uploadDocument($file64, "avatar_dir");

            $universityTestimonial->setThumbnail($fileName);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($universityTestimonial);
            $entityManager->flush();

            return $this->redirectToRoute('admin_university_testimonial_index');
        }

        return $this->render('admin/testimonial/university_testimonial/new.html.twig', [
            'university_testimonial' => $universityTestimonial,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_university_testimonial_show", methods={"GET"})
     */
    public function show(UniversityTestimonial $universityTestimonial): Response
    {
        return $this->render('admin/testimonial/university_testimonial/show.html.twig', [
            'university_testimonial' => $universityTestimonial,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_university_testimonial_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, UniversityTestimonial $universityTestimonial): Response
    {
        $form = $this->createForm(UniversityTestimonialType::class, $universityTestimonial);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['thumbnail']->getData();
            
            if($file){
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $file64 = new UploadedBase64File($request->request->get('thumbnail64'),$originalFilename);
               
                $fileName = $this->uploadDocument($file64, "avatar_dir");
                $universityTestimonial->setThumbnail($fileName);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($universityTestimonial);
            $entityManager->flush();
            
            return $this->redirectToRoute('admin_university_testimonial_index');
        }

        return $this->render('admin/testimonial/university_testimonial/edit.html.twig', [
            'university_testimonial' => $universityTestimonial,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_university_testimonial_delete", methods={"DELETE"})
     */
    public function delete(Request $request, UniversityTestimonial $universityTestimonial): Response
    {
        if ($this->isCsrfTokenValid('delete'.$universityTestimonial->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($universityTestimonial);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_university_testimonial_index');
    }
}
