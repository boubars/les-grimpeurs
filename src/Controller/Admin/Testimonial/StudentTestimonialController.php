<?php

namespace App\Controller\Admin\Testimonial;

use App\Controller\FileUploaderAction;
use App\Entity\Testimonial\StudentTestimonial;
use App\Form\Testimonial\StudentTestimonialType;
use App\Repository\Testimonial\StudentTestimonialRepository;
use App\Service\Utilis\UploadedBase64File;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/{_locale<en|fr>}/admin/testimonial/student")
 */
class StudentTestimonialController extends FileUploaderAction
{
    /**
     * @Route("/", name="admin_student_testimonial_index", methods={"GET"})
     */
    public function index(StudentTestimonialRepository $studentTestimonialRepository): Response
    {
        return $this->render('admin/testimonial/student_testimonial/index.html.twig', [
            'student_testimonials' => $studentTestimonialRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_student_testimonial_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $studentTestimonial = new StudentTestimonial();
        $form = $this->createForm(StudentTestimonialType::class, $studentTestimonial);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['thumbnail']->getData();
            
            if($file){
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $file64 = new UploadedBase64File($request->request->get('thumbnail64'),$originalFilename);
            
                $fileName = $this->uploadDocument($file64, "avatar_dir");
                
                $studentTestimonial->setThumbnail($fileName);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($studentTestimonial);
            $entityManager->flush();

            return $this->redirectToRoute('admin_student_testimonial_index');
        }

        return $this->render('admin/testimonial/student_testimonial/new.html.twig', [
            'student_testimonial' => $studentTestimonial,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_student_testimonial_show", methods={"GET"})
     */
    public function show(StudentTestimonial $studentTestimonial): Response
    {
        return $this->render('admin/testimonial/student_testimonial/show.html.twig', [
            'student_testimonial' => $studentTestimonial,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_student_testimonial_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, StudentTestimonial $studentTestimonial): Response
    {
        $form = $this->createForm(StudentTestimonialType::class, $studentTestimonial);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['thumbnail']->getData();
            
            if($file){
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $file64 = new UploadedBase64File($request->request->get('thumbnail64'),$originalFilename);
               
                $fileName = $this->uploadDocument($file64, "avatar_dir");
                $studentTestimonial->setThumbnail($fileName);
            }
          
            $this->getDoctrine()->getManager()->persist($studentTestimonial);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('admin_student_testimonial_index');
        }

        return $this->render('admin/testimonial/student_testimonial/edit.html.twig', [
            'student_testimonial' => $studentTestimonial,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_student_testimonial_delete", methods={"DELETE"})
     */
    public function delete(Request $request, StudentTestimonial $studentTestimonial): Response
    {
        if ($this->isCsrfTokenValid('delete'.$studentTestimonial->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($studentTestimonial);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_student_testimonial_index');
    }
}
