<?php

namespace App\Controller\Admin\Scholarship;

use App\Controller\Admin\AbstractAdminController;
use App\Entity\Common\Gallery;
use App\Entity\Discipline\Discipline;
use App\Entity\Scholarship\Scholarship;
use App\Form\Admin\ScholarshipType;
use App\Form\Discipline\DisciplineType;
use App\Repository\Organization\OrganizationRepository;
use App\Repository\Scholarship\ScholarshipRepository;
use App\Service\Utilis\UploadedBase64File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Route("/{_locale<en|fr>}/admin/scholarship")
 */
class ScholarshipController extends AbstractAdminController
{
    /**
     * Global slugger interface variable
     *
     * @var SluggerInterface
     */
    private $slugger;

    /**
     * Global slugger interface variable
     *
     * @var OrganizationRepository
     */
    private $organizationManager;

    /**
     * Class cunstructor
     *
     * @param SluggerInterface $slugger
     * @param OrganizationRepository $orgManager
     */
    public function __construct(SluggerInterface $slugger, OrganizationRepository $orgManager)
    {
        $this->slugger = $slugger;
        $this->organizationManager = $orgManager;
        parent::__construct($slugger);
    }

    /**
     * Admin scholarship Index page
     *
     * @Route("/", name="admin_scholarship_index", methods={"GET"})
     * @param Request $request
     * @param ScholarshipRepository $scholarshipRepository
     * @return Response
     */
    public function index(Request $request, ScholarshipRepository $scholarshipRepository): Response
    {
        
        $organization = $request->query->get('relatedOrganization', false);
        if($organization !== false){
            $organization = $this->organizationManager->findOneById($organization);
            $request->query->set('relatedOrganization', $organization);
        }
        
        $data                  = $this->getDataTablePaginator($request, $scholarshipRepository, 'scholarships');
        $data['organizations'] = $this->organizationManager->findAll();
        
        return $this->render('admin/scholarship/index.html.twig', $data);
    }

    /**
     * Admin new scholarship method
     *
     * @Route("/new", name="admin_scholarship_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $locale = $request->getLocale();

        $scholarship = new Scholarship();
        $form = $this->createForm(ScholarshipType::class, $scholarship, ['locale'=> $locale]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && !$request->isXmlHttpRequest()) {
            $entityManager = $this->getDoctrine()->getManager();
            
            $this->handleChange($form, $request, $scholarship, 'add');

            /*** handle scholarship owner */
            $scholarship->setOwner($this->getUser());

            $entityManager->persist($scholarship);
            $entityManager->flush();
            return $this->redirectToRoute('admin_scholarship_index');
        }

        /** for new discipline */
        $discipline = new Discipline();
        $disciplineForm = $this->createForm(DisciplineType::class, $discipline, ["locale" => $locale]);

        return $this->render('admin/scholarship/new.html.twig', [
            'scholarship'    => $scholarship,
            'form'           => $form->createView(),
            'disciplineForm' => $disciplineForm->createView()
        ]);
    }

    /**
     * Admin show scholarship detail infos
     *
     * @Route("/{id}", name="admin_scholarship_show", methods={"GET"})
     * @param Scholarship $scholarship
     * @return Response
     */
    public function show(Scholarship $scholarship): Response
    {
        return $this->render('admin/scholarship/show.html.twig', [
            'scholarship' => $scholarship,
        ]);
    }

    /**
     * Admin clone scholarship method
     *
     * @Route("/{id}/duplicate", name="admin_scholarship_duplicate", methods={"GET"})
     * @param Scholarship $scholarship
     * @return Response
     */
    public function duplicate(Scholarship $scholarship): Response
    {
        $copy = clone($scholarship);

        /** remove ID */
        $copy->setId(null);
        $copy->setName($copy->getName().' copy');
        
        /** clone gallery */
        $gallery = clone($copy->getGallery());
        $copy->setGallery($gallery->setId(null));
        
        /** clone social network */
        $network = clone($copy->getSocialNetwork());
        $copy->setSocialNetwork($network->setId(null));
        
        /*** remove all dynamic relation  */
        $copy->setInformationRequests(new ArrayCollection());
        $copy->setApplications(new ArrayCollection());
        
        /*** generate a new slug */
        $this->generateSlug($copy);
        
        $entityManager = $this->getDoctrine()->getManager();
        
        $entityManager->persist($copy);
        $entityManager->flush();
        return $this->redirectToRoute('admin_scholarship_edit', ['id'=> $copy->getId()]);
    }

    /**
     * Admin edit scholarship method
     *
     * @Route("/{id}/edit", name="admin_scholarship_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Scholarship $scholarship
     * @return Response
     */
    public function edit(Request $request, Scholarship $scholarship): Response
    {
        $locale = $request->getLocale();

        $form = $this->createForm(ScholarshipType::class, $scholarship, ['locale'=> $locale]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && !$request->isXmlHttpRequest()) {
            
            $this->handleChange($form, $request, $scholarship, "edit");
            
            $this->getDoctrine()->getManager()->persist($scholarship);
            $this->getDoctrine()->getManager()->flush();
            //dd($scholarship);
            return $this->redirectToRoute('admin_scholarship_index');
        }

        /** for new discipline */
        $discipline = new Discipline();
        $disciplineForm = $this->createForm(DisciplineType::class, $discipline, ["locale" => $locale]);

        return $this->render('admin/scholarship/edit.html.twig', [
            'scholarship'     => $scholarship,
            'form'            => $form->createView(),
            'disciplineForm'  => $disciplineForm->createView()
        ]);
    }

    /**
     * Admin delete scholarship method
     *
     * @Route("/{id}", name="admin_scholarship_delete", methods={"DELETE"})
     * @param Request $request
     * @param Scholarship $scholarship
     * @return Response
     */
    public function delete(Request $request, Scholarship $scholarship): Response
    {
        if ($this->isCsrfTokenValid('delete'.$scholarship->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($scholarship);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_scholarship_index');
    }

    /**
     * Handle edit or update request
     *
     * @param FormInterface $form
     * @param Request $request
     * @param Scholarship $scholarship
     * @param string $action
     * @return void
     */
    private function handleChange($form, $request, &$scholarship, $action = 'add'){
        /*** recalculate total */
        $scholarship->setTotalOffered(
            $scholarship->getCount() * $scholarship->getAmount()
        );

        /*** handle logo upload */
        $this->handleUploadLogo($form, $request, $scholarship, $action);

        /*** handle avatar upload */
        $this->handleUploadAvatar($form, $request, $scholarship, $action);

        /*** handle map image upload */
        $this->handleUploadContactAdresseMap($form, $request, $scholarship, $action);
        
        /*** handle gallery upload */
        $this->handleUploadGallery($request, $scholarship, $action);

        /*** handle extras array data */
        $this->handleSelectionCriteria($request, $scholarship);

        $this->handleAdvantages($request, $scholarship);

        $this->handleAttachments($request, $scholarship);
    
      
        /*** generate a new slug */
        $this->generateSlug($scholarship);

        $this->mappedExtraField($scholarship);

        /** set candidature plateform option */
        $scholarship->setCadidateOption(
            $request->request->get("scholarship_cadidateOption")
        );
    }

    /**
     * Update and upload scholaship logo file
     *
     * @param FormInterface $form
     * @param Request $request
     * @param Scholarship $scholarship
     * @param string $action
     * @return void
     */
    private function handleUploadLogo($form, $request, &$scholarship, $action)
    {
        $file = $form['logo']->getData();
        if(!$file){
            if($action == 'add'){
                /*** copy organization logo to scholarship logo and set name */
                $organization = $scholarship->getRelatedOrganization();
                if($organization){
                    $fileName = $organization->getLogo();
                    $scholarship->setLogo($fileName);
                }
            }
            return false;
        }

        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $file64 = new UploadedBase64File($request->request->get('logo64'),$originalFilename);
        $fileName = $this->uploadDocument($file64, "logo_dir");

        $scholarship->setLogo($fileName);
    }
    
    /**
     * Update and upload scholarship contact avatar
     *
     * @param FormInterface $form
     * @param Request $request
     * @param Scholarship $scholarship
     * @param string $action
     * @return void
     */
    private function handleUploadAvatar($form, $request, &$scholarship, $action)
    {
        $file = $form['contactAvatar']->getData();
        if(!$file){
            //throw new Exception("File avatar not found", 1);
            if($action == 'add'){
                $organization = $scholarship->getRelatedOrganization();
                if($organization){
                    $scholarship->setContactAvatar($organization->getAvatar());
                }
            }
            return false;       
        }

        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $file64 = new UploadedBase64File($request->request->get('contactAvatar64'),$originalFilename);
        $fileName = $this->uploadDocument($file64, "avatar_dir");
        $scholarship->setContactAvatar($fileName);
    }
   
    /**
     * Update and upload contact adresse map file 
     *
     * @param FormInterface $form
     * @param Request $request
     * @param Scholarship $scholarship
     * @param string $action
     * @return void
     */
    private function handleUploadContactAdresseMap($form, $request, &$scholarship, $action)
    {
        
        $originalFilename = pathinfo("map_", PATHINFO_FILENAME);
        if(preg_match('%data:image/png;base64,%',$scholarship->getContactAdresseMap())){
            $file64 = new UploadedBase64File($scholarship->getContactAdresseMap(), $originalFilename);
            $fileName = $this->uploadDocument($file64, "map_dir");
            $scholarship->setContactAdresseMap($fileName);
        }else{
            if($action == 'add'){
                $organization = $scholarship->getRelatedOrganization();
                if($organization){
                    $scholarship->setContactAdresseMap($organization->getAdresseMap());
                }
            }
        }
    }

    /**
     * Manage scholatship gallery
     *
     * @param Request $request
     * @param Scholarship $scholarship
     * @param string $action
     * @return void
     */
    private function handleUploadGallery($request, &$scholarship, $action){
        $gallery = clone ($scholarship->getGallery() ?? new Gallery());
        /** white list image gallery */
        $gallery->setPictures([]);

        /** gallery photo */
        $galeryPhotoFiles = $request->files->get('galeryPhoto');
 
        if(!$galeryPhotoFiles){
            $organization = $scholarship->getRelatedOrganization();
            if ($organization && $organization->getGallery()) {
                $organizationGallery = clone ($organization->getGallery());
                if($action == 'add'){
                    //avoid ntegrity constraint violation : Duplicate entry
                    $organizationGallery->setId(null);
                    $scholarship->setGallery($organizationGallery);
                }else{
                    $gallery->setPictures($organizationGallery->getPictures());
                    $gallery->setVideos($organizationGallery->getVideos());
                    $scholarship->setGallery($gallery);
                }
            }

            return false;       

        }


        foreach ($galeryPhotoFiles as $key => $file) {
            $newFilename = $this->uploadDocument($file, "picture_dir");
            $gallery->addPicture($newFilename); 
        }

        $scholarship->setGallery($gallery);
    }

    /**
     * Generate an unik scholarship slug from scholarship name
     *
     * @param Scholarship $scholarship
     * @return void
     */
    private function generateSlug(&$scholarship){
        $slug = $this->slugger->slug(
            sprintf("%s-%s" , 
               $scholarship->getName(),
               $scholarship->getAwardOrganizationType()->getName() 
            ),
            "-"
         );
         $scholarship->setSlug(strtolower($slug));
    }

    /**
     * Manage posted criteria data
     *
     * @param Request $request
     * @param Scholarship $scholarship
     * @return void
     */
    private function handleSelectionCriteria($request,Scholarship &$scholarship){
        $postedData = $request->request->get('selectionCriteria');
        $this->cleanArray($postedData);
        $scholarship->setSelectionCriteria($postedData);
    }

    /**
     * Manage posted advantages data
     *
     * @param Request $request
     * @param Scholarship $scholarship
     * @return void
     */
    private function handleAdvantages($request,Scholarship &$scholarship){
        $postedData = $request->request->get('advantages');
        $this->cleanArray($postedData);
        $scholarship->setAdvantages($postedData);
    }

    /**
     * Manage posted attachements data
     *
     * @param Request $request
     * @param Scholarship $scholarship
     * @return void
     */
    private function handleAttachments($request,Scholarship &$scholarship){
        $postedData = $request->request->get('attachments');
        $this->cleanArray($postedData);
        $scholarship->setAttachments($postedData);
    }

    /**
     * Clean an array passed by reference
     *
     * @param Array $arr
     * @return void
     */
    private function cleanArray(&$arr){
        $clean = [];
        for ($i=0; $i < count($arr) ; $i++) { 
           if(strlen($arr[$i]) > 0){
            $clean[] = $arr[$i];
           }
        }
        $arr = $clean;
    }

    /**
     * Mapped extra field required infos
     *
     * @param Scholarship $scholarship
     * @return void
     */
    private function mappedExtraField(&$scholarship){
        $scholarship->setInfosEmail(
            $scholarship->getContactEmail() ?? null
        );
    }
}
