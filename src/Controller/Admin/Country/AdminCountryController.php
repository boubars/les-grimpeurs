<?php

namespace App\Controller\Admin\Country;

use App\Controller\Admin\AbstractAdminController;
use App\Controller\FileUploaderAction;
use App\Entity\Common\Country;
use App\Form\Common\CountryType;
use App\Repository\Common\CountryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\Admin\AdminControllerTrait;

/**
 * @Route("/{_locale<en|fr>}/admin/country")
 */
class AdminCountryController extends AbstractAdminController
{
    /**
     * @Route("/", name="admin_country_index", methods={"GET"})
     */
    public function index(Request $request, CountryRepository $countryRepository): Response
    {
        $data = $this->getDataTablePaginator($request, $countryRepository, 'countries') ;
        return $this->render('admin/country/index.html.twig', $data);
    }

    /**
     * @Route("/new", name="admin_country_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $country = new Country();
        $form = $this->createForm(CountryType::class, $country);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['symbol']->getData();
            $fileName = $this->uploadDocument($file, "avatar_dir");
            $country->setSymbol($fileName);


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($country);
            $entityManager->flush();

            return $this->redirectToRoute('admin_country_index');
        }

        return $this->render('admin/country/new.html.twig', [
            'country' => $country,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_country_show", methods={"GET"})
     */
    public function show(Country $country): Response
    {
        return $this->render('admin/country/show.html.twig', [
            'country' => $country,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_country_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Country $country): Response
    {
        $form = $this->createForm(CountryType::class, $country);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['symbol']->getData();
            $fileName = $this->uploadDocument($file, "avatar_dir");
            $country->setSymbol($fileName);
            
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_country_index');
        }

        return $this->render('admin/country/edit.html.twig', [
            'country' => $country,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_country_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Country $country): Response
    {
        if ($this->isCsrfTokenValid('delete'.$country->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($country);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_country_index');
    }
}
