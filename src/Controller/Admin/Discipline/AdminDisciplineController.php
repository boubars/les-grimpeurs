<?php
namespace App\Controller\Admin\Discipline;

use App\Controller\Admin\AbstractAdminController;
use App\Entity\Discipline\Category;
use App\Entity\Discipline\Discipline;
use App\Form\Discipline\CategoryType;
use App\Form\Discipline\DisciplineType;
use App\Repository\Discipline\CategoryRepository;
use App\Repository\Discipline\DisciplineRepository;
use App\Service\Lang\Translation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale<en|fr>}/admin/discipline")
 */
class AdminDisciplineController extends AbstractAdminController
{
    /**
     * resousce directory path
     *
     * @var String
     */
    private $ressourceDir;
    
    /**
     * FileSystem object intercation
     *
     * @var FileSystem
     */
    private $fileSystem;

    /**
     * Global entityManager variable
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Lang translator serive
     *
     * @var Translation
     */
    private $translator;
    /**
     * Controller constractor
     *
     * @param EntityManagerInterface $entityManager
     * @param ParameterBagInterface $params
     * @param Translation $translator
     */
    public function __construct(EntityManagerInterface $entityManager, ParameterBagInterface $params, Translation $translator)
    {
        $this->ressourceDir  = $params->get('project_dir').'/public/translations';
        $this->fileSystem    = new Filesystem();
        $this->entityManager = $entityManager;
        $this->translator    = $translator;

    }

    /**
     * Admin discipline index route
     *
     * @Route("/", name="admin_discipline_index")
     * @param  Request $request
     * @param  DisciplineRepository $disciplineRepository
     * @param  CategoryRepository $categoryManager
     * @return Response
     */
    public function index(Request $request, DisciplineRepository $disciplineRepository, CategoryRepository $categoryManager): Response
    {
        $data   = [] ;
        $locale =  $request->getLocale();

        $category     = new Category();
        $categoryForm = $this->createForm(CategoryType::class, $category);
        $data['categoryForm'] = $categoryForm->createView();
        $this->handleUpdateRequest($request,$categoryForm, $category);
        

        $discipline = new Discipline();
        $disciplineForm = $this->createForm(DisciplineType::class, $discipline, ["locale" => $locale]);
        $data['disciplineForm'] = $disciplineForm->createView();
        $this->handleUpdateRequest($request,$disciplineForm, $discipline);

        $dataShort = $locale == 'fr' ? ['name_fr' => 'ASC'] : ['name' => 'ASC'];
        $data['categories'] = $categoryManager->findBy([], $dataShort);
        return $this->render('admin/discipline/index.html.twig', $data);
    }

    /**
     * @Route("/new", name="admin_discipline_new", methods={"POST"})
     */
    public function new(Request $request): Response
    {
        return $this->render('admin/language/new.html.twig', []);
    }

    /**
     * Handle add or edit Object
     *
     * @param  Request $request
     * @param  FormInterface $form
     * @param  [mixt] $category
     * @return void
     */
    private function handleUpdateRequest(Request $request, FormInterface $form, $object){
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($object);
            /** update translate value */
            $this->entityManager->flush();
            $this->updateTranslation($object);

        }
    }
    

    /**
     * Edit category function
     *
     * @Route("/category/{id}/edit", name="admin_edit_discipline_category")
     * @param  Request $request
     * @param Category $category
     * @return Response
     */
    public function editCategory(Request $request, Category $category){
        $categoryForm = $this->createForm(CategoryType::class, $category);
        $this->handleUpdateRequest($request,$categoryForm, $category);
        return $this->redirectToRoute('admin_discipline_index');
    }

    /**
     * Delete category function
     *
     * @Route("/category/{id}/remove", name="admin_remove_discipline_category")
     * @param Category $category
     * @return Response
     */
    public function deleteCategory(Category $category){
        $this->entityManager->remove($category);
        $this->entityManager->flush();
        return $this->redirectToRoute('admin_discipline_index');
    }
    
    /**
     * Update discipline translation files
     *
     * @param [type] $discipline
     * @param [type] $nameFr
     * @return void
     */
    private function updateTranslation($discipline){
        /** get file data */
        $dataFr = ($this->translator->getResource('fr'))['discipline'];
        $dataEn = ($this->translator->getResource('en'))['discipline'];
        
        if($discipline instanceof Category){
            $dataEn['category'][$discipline->getId()] = $discipline->getName(); 
            $dataFr['category'][$discipline->getId()] = $discipline->getNameFr(); 
        }else{
            $dataEn[$discipline->getId()] = $discipline->getName(); 
            $dataFr[$discipline->getId()] = $discipline->getNameFr();  
        }
        
        /**generate translation files */
        $fileSystem = new Filesystem();
        $fileSystem->dumpFile($this->ressourceDir."/discipline/en.json", json_encode($dataEn, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        $fileSystem->dumpFile($this->ressourceDir."/discipline/fr.json", json_encode($dataFr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));  
    }
    /**
     * Delete discipline function
     *
     * @Route("/{id}/remove", name="admin_discipline_delete", methods={"POST"})
     * @param Discipline $category
     * @return Response
     */
    public function deleteDiscipline(Discipline $discipline){
        $this->entityManager->remove($discipline);
        $this->entityManager->flush();
        return $this->redirectToRoute('admin_discipline_index');
    }

    /**
     * Edit discipline function
     *
     * @Route("/{id}/edit", name="admin_edit_discipline_category")
     * @param  Request $request
     * @param  Discipline $discipline
     * @return Response
     */
    public function editDiscipline(Request $request, Discipline $discipline){
        $locale =  $request->getLocale();
        $disciplineForm = $this->createForm(DisciplineType::class, $discipline, ["locale" => $locale]);
        
        $this->handleUpdateRequest($request,$disciplineForm, $discipline);
        return $this->redirectToRoute('admin_discipline_index');
    }

}
