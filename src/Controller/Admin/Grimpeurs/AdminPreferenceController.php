<?php

namespace App\Controller\Admin\Grimpeurs;

use App\Entity\Grimpeurs\SitePrefereceParams;
use App\Entity\Grimpeurs\SitePreference;
use App\Repository\Grimpeurs\SitePreferenceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/{_locale<en|fr>}/admin/preference")
 */
class AdminPreferenceController extends AbstractController
{
    /**
     * @var SitePreferenceRepository
     */
    private $preferenceManager;

    private $entityManager;
    private $translator;

    private $params;
    public function __construct(EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        $this->entityManager     = $entityManager;    
        $this->preferenceManager = $entityManager->getRepository(SitePreference::class);
        $this->translator        = $translator;
    }
    
    /**
     * @Route("/", name="admin_preference_index", methods={"GET","POST"})
     */
    public function index(Request $request): Response
    {
        $locale   = $request->getLocale();
        $this->params =  $this->getSitePreferences($locale);

        if($request->getMethod() == 'POST'){
            $this->updatePreferences($request);

        }
        return $this->render('admin/grimpeurs/preference/index.html.twig', [
            'preferences' => $this->getSitePreferences($locale),
        ]);
    }

    private function updatePreferences(Request $request){

        foreach ($this->params as $key => $value) {
            $field = $value['key'];  
            $value = $request->request->get($field);
            $lang  = $request->request->get($field."_lang");
            $param = $this->getParam($field, $lang);
            $param->setValue($value)
            ->setLang($lang);
            $this->entityManager->persist($param);
        }
        $this->entityManager->flush();
    }

    private function getParam($key, $lang = false){
        
        $param = $this->preferenceManager->findOneByName($key);
        
        if($lang){
            $param = $this->preferenceManager->findOneBy([
                'name' => $key,
                'lang' => $lang
            ]);
        }

        return $param ? $param : (new SitePreference())->setName($key);



    }
    private function getSitePreferences($lang){
       // dd($this->params);
       $data = SitePrefereceParams::params($this->translator);
       
        foreach ($data as $key => $param) {
            $value = $this->getParam($param['key'], $lang);
            $data[$key]['value'] = $value->getValue(); 
        }
        return $data;
    }

}
