<?php

namespace App\Controller\Admin\Grimpeurs;

use App\Controller\Admin\AbstractAdminController;
use App\Entity\Grimpeurs\Concept;
use App\Entity\Grimpeurs\FAQ\Question;
use App\Entity\Grimpeurs\FAQ\Thematic;
use App\Form\Admin\Grimpeurs\ConceptType;
use App\Form\Admin\Grimpeurs\FAQ\QuestionType;
use App\Form\Admin\Grimpeurs\FAQ\ThematicType;
use App\Repository\Grimpeurs\ConceptRepository;
use App\Service\Utilis\UploadedBase64File;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/{_locale<en|fr>}/admin/concept")
 */
class AdminConceptController extends AbstractAdminController {

    /**
     * Entity manager
     *
     * @var EntityManagerInterface
     */
    private $entityManager;


    /**
     * Concept repository object
     *
     * @var ConceptRepository
     */
    private $conceptManager;

    private $slugger;

    /**
     * Controller constuctor function
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager, SluggerInterface $slugger)
    {
        $this->entityManager   = $entityManager;
        $this->conceptManager = $entityManager->getRepository(Concept::class);
        $this->slugger = $slugger;
        parent::__construct($slugger);
    }

    /**
     * Admin FAQ entry point
     * 
     * @Route("/",name="admin_concept_index")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request){

        $locale = $request->getLocale();
        $concepts = $this->conceptManager->findAll();
       
        return $this->render("admin/grimpeurs/concept/index.html.twig", [
            "concepts" => $concepts,
        ]);
    }

    /**
     * Route for add new concept
     * @Route("/new", name="admin_concept_new")
     * 
     * @param Request $request
     * @return void
     */
    public function new(Request $request){
        $concept = new Concept();
        $form    = $this->createForm(ConceptType::class, $concept);
        
        $redirect = $this->handleUpdate($request, $form, $concept);
        
        if($redirect){
            return $this->redirectToRoute('admin_concept_index');
        }

        return $this->render("admin/grimpeurs/concept/form.html.twig", [
            "form" => $form->createView(),
        ]);
    }

    /**
     * Update concept
     * @Route("/{id}/edit", name="admin_concept_edit")
     *
     * @param Concept $concept
     * @param Request $request
     * @return void
     */
    public function edit(Concept $concept, Request $request){
        $form = $this->createForm(ConceptType::class, $concept);
        $redirect = $this->handleUpdate($request, $form, $concept);
        if($redirect){
            return $this->redirectToRoute('admin_concept_index');
        }

        return $this->render("admin/grimpeurs/concept/form.html.twig", [
            "form" => $form->createView(),
        ]);
    }

    public function handleUpdate(Request $request, FormInterface $form, Concept $concept){
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            /**  treating images $file **/
            $file = $form['image']->getData();
            if ($file) {
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $file64 = new UploadedBase64File($request->request->get('concept_image_64'), $originalFilename);
                $fileName = $this->uploadDocument($file64, "picture_dir");
                $concept->setImage($fileName);
            } else {
                $concept->setImage($request->request->get('concept_image_64'));
            }
            $aValeurs = [];

            foreach ($concept->getValeurs() as $key => $valeur) {
                $name = "concept_valeurs_".$key."_image_64";
                $imgFile = $valeur['image'];
                if ($imgFile) {
                    $originalFilename = pathinfo($imgFile->getClientOriginalName(), PATHINFO_FILENAME);
                    $file64 = new UploadedBase64File($request->request->get($name), $originalFilename);

                    $fileName = $this->uploadDocument($file64, "picture_dir");
                    $aValeurs[$key]['image'] = $fileName;
                } else {
                    $aValeurs[$key]['image'] = $request->request->get($name);
                }
                $aValeurs[$key]['title'] = $valeur['title'];
                $aValeurs[$key]['text'] = $valeur['text'];
            }

            $concept->setValeurs($aValeurs);
            $entityManager->persist($concept);
            $entityManager->flush();
            return true;
        }
        return false;
    }
}
