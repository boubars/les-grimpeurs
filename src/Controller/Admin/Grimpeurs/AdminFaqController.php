<?php

namespace App\Controller\Admin\Grimpeurs;

use App\Entity\Grimpeurs\FAQ\Question;
use App\Entity\Grimpeurs\FAQ\Thematic;
use App\Form\Admin\Grimpeurs\FAQ\QuestionType;
use App\Form\Admin\Grimpeurs\FAQ\ThematicType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale<en|fr>}/admin/faq")
 */
class AdminFaqController extends AbstractController{

    /**
     * Entity manager
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Question repository
     * @var QuestionRepository
     */
    private $questionManager;
    
    /**
     * Thematic repository object
     *
     * @var ThematicRepository
     */
    private $thematicManager;

    /**
     * Controller constuctor function
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager   = $entityManager;
        $this->questionManager = $entityManager->getRepository(Question::class);
        $this->thematicManager = $entityManager->getRepository(Thematic::class);
    }

    /**
     * Admin FAQ entry point
     * 
     * @Route("/",name="admin_faq_index")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request){
        $locale = $request->getLocale();

        $question     = (new Question())->setLang($locale);
        $questionForm = $this->createForm(QuestionType::class, $question);
        
        $thematic     = (new Thematic())->setLang($locale);
        $thematicForm = $this->createForm(ThematicType::class, $thematic);
        
        $this->handleUpdateQuestionRequest($request, $question, $questionForm);
        $this->handleNewThematicRequest($request, $thematic, $thematicForm);


        $faqs      = $this->questionManager->findByLang($locale);
        $thematics = $this->thematicManager->findByLang($locale);

        return $this->render("admin/grimpeurs/faq/index.html.twig", [
            "faqs"         => $faqs,
            "thematics"    => $thematics,
            "thematicForm" => $thematicForm->createView(),
            "questionForm" => $questionForm->createView()
        ]);
    }

    /**
     * Persist and flush new Question
     *
     * @param  Request  $request
     * @param  Question $question
     * @param  QuestionForm $questionForm
     * @return RedirectResponse
     */
    private function handleUpdateQuestionRequest($request, $question, $questionForm){
        $questionForm->handleRequest($request);

        if($questionForm->isSubmitted() && $questionForm->isValid()) {
            $this->entityManager->persist($question);
            $this->entityManager->flush();
        }
        return $this->redirectToRoute("admin_faq_index");
        
    }

    /**
     * Persist and flush new Question themetic
     *
     * @param Request $request
     * @param Thematic $thematic
     * @param ThematicForm $thematicForm
     * @return void
     */
    private function handleNewThematicRequest($request, $thematic, $thematicForm){
        $thematicForm->handleRequest($request);

        if($thematicForm->isSubmitted() && $thematicForm->isValid()) {
          $this->entityManager->persist($thematic);
          $this->entityManager->flush();
        }
    }


    /**
     * Handle edit thematci
     *
     * @Route(path="/thematic/{id}/edit", name="admin_faq_thematic_edit")
     * @param  Thematic $thematic
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function thematicEdit(Thematic $thematic, Request $request){
        $data = $request->request->get("thematic");

        $newName = $data['name'] ?? false ;
        if($newName){
            $thematic->setName($newName);
            $this->entityManager->persist($thematic);
            $this->entityManager->flush();
        }
        return $this->redirectToRoute("admin_faq_index");
    }

    /**
     * Handle delete thematci
     *
     * @Route(path="/thematic/{id}/delete", name="admin_faq_thematic_delete")
     * @param Request $request
     * @return RedirectResponse
     */
    public function deleteThematic(Thematic $thematic){
        if($thematic){
            $this->entityManager->remove($thematic);
            $this->entityManager->flush();
        }
        return $this->redirectToRoute("admin_faq_index");
    }

    /**
     * Handle edit question request
     *
     * @Route(path="/{id}/edit", name="admin_faq_question_edit")
     * @param Question $question
     * @param Request $request
     * @return RedirectResponse
     */
    public function editQuestion(Question $question, Request $request){
        $questionForm = $this->createForm(QuestionType::class, $question);
        return $this->handleUpdateQuestionRequest($request, $question, $questionForm);
    }

    /**
     * Handle delete question
     *
     * @Route(path="/question/{id}/delete", name="admin_faq_question_delete")
     * @param Request $request
     * @return RedirectResponse
     */
    public function deleteQuestion(Question $question){
        if($question){
            $this->entityManager->remove($question);
            $this->entityManager->flush();
        }
        return $this->redirectToRoute("admin_faq_index");
    }
}