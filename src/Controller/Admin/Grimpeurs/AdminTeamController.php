<?php

namespace App\Controller\Admin\Grimpeurs;

use App\Controller\Admin\AbstractAdminController;
use App\Entity\Grimpeurs\Team;
use App\Form\Admin\Grimpeurs\TeamType;
use App\Repository\Grimpeurs\TeamRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Utilis\UploadedBase64File;

/**
 * @Route("/{_locale<en|fr>}/admin/grimpeurs-team")
 */
class AdminTeamController extends AbstractAdminController
{
    /**
     * @Route("/", name="admin_team_index", methods={"GET"})
     */
    public function index(TeamRepository $teamRepository): Response
    {
        return $this->render('admin/grimpeurs/team/index.html.twig', [
            'teams' => $teamRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_team_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $team = new Team();
        $form = $this->createForm(TeamType::class, $team);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $team = $this->handleUploadAvatar($form, $request, $team);

            $entityManager->persist($team);
            $entityManager->flush();

            return $this->redirectToRoute('admin_team_index');
        }

        return $this->render('admin/grimpeurs/team/new.html.twig', [
            'team' => $team,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_team_show", methods={"GET"})
     */
    public function show(Team $team): Response
    {
        return $this->render('admin/grimpeurs/team/show.html.twig', [
            'team' => $team,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_team_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Team $team): Response
    {
        $form = $this->createForm(TeamType::class, $team);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $team = $this->handleUploadAvatar($form, $request, $team);

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_team_index');
        }

        return $this->render('admin/grimpeurs/team/edit.html.twig', [
            'team' => $team,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_team_delete", methods={"POST"})
     */
    public function delete(Request $request, Team $team): Response
    {
        if ($this->isCsrfTokenValid('delete'.$team->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($team);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_team_index');
    }

    private function handleUploadAvatar($form, $request, $team)
    {
        $file = $form['avatar']->getData();
        if(!$file){
            //throw new Exception("File avatar not found", 1);
            return false;       
        }

        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $file64 = new UploadedBase64File($request->request->get('avatar64'),$originalFilename);
        $fileName = $this->uploadDocument($file64, "avatar_dir");

        return $team->setAvatar($fileName);
        
    }
}
