<?php

namespace App\Controller\Admin\Grimpeurs;

use App\Controller\Admin\AbstractAdminController;
use App\Entity\Grimpeurs\Post;
use App\Form\Admin\Grimpeurs\PostType;
use App\Repository\Grimpeurs\PostRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale<en|fr>}/admin/grimpeurs-post")
 */
class AdminPostController extends AbstractAdminController
{
    /**
     * @Route("/", name="admin_post_index", methods={"GET"})
     */
    public function index(Request $request, PostRepository $postRepository): Response
    {
        /*
        $params = $request->query->all();
        return $this->render('admin/grimpeurs/post/index.html.twig', [
            'posts' => $postRepository->findBy($params),
        ]);
        */
        $data = $this->getDataTablePaginator($request, $postRepository, 'posts');
        return $this->render('admin/grimpeurs/post/index.html.twig', $data);
    }

    /**
     * @Route("/new", name="admin_post_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $locale   = $request->getLocale();
        
        $post = (new Post())->setLang($locale);
        
        /*** set default type */
        $postType = strtoupper($request->query->get('type', Post::POST_TYPE[0]));
        $post->setType($post->getType() ? $post->getType() : $postType);

        $form = $this->createForm(PostType::class, $post, ['locale' => $locale , 'type' => $postType]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($post);
            $entityManager->flush();

            return $this->redirectToRoute('admin_post_index', $request->query->all());
        }

        return $this->render('admin/grimpeurs/post/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_post_show", methods={"GET"})
     */
    public function show(Post $post): Response
    {
        return $this->render('admin/grimpeurs/post/show.html.twig', [
            'post' => $post,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_post_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Post $post): Response
    {
        $locale   = $request->getLocale();
        $form = $this->createForm(PostType::class, $post, ['locale' => $locale, 'type'=> $post->getType()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $postType = strtolower($post->getType());
            return $this->redirectToRoute('admin_post_index', ['type' => $postType]);
        }

        return $this->render('admin/grimpeurs/post/edit.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_post_delete", methods={"POST"})
     */
    public function delete(Request $request, Post $post): Response
    {
        if ($this->isCsrfTokenValid('delete'.$post->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($post);
            $entityManager->flush();
        }
        return $this->redirectToRoute('admin_post_index');
    }
}
