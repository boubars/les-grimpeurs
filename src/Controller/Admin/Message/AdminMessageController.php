<?php

namespace App\Controller\Admin\Message;

use App\Controller\Admin\AbstractAdminController;
use App\Repository\Common\ContactRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale<en|fr>}/admin")
 */
class AdminMessageController extends AbstractAdminController
{
    /**
     * @Route("/live-chat", name="admin_message_live_chat")
     */
    public function liveChat(): Response
    {
        return $this->render('admin/message/live_chat.html.twig');
    }

    /**
     * @Route("/messages", name="admin_message_contact")
     */
    public function contactMessages(Request $request, ContactRepository $messageManager): Response
    {
        $data = $this->getDataTablePaginator($request, $messageManager, 'messages');
        return $this->render('admin/message/contact.html.twig', $data);
 
    }
}
