<?php

namespace App\Controller\Admin;

use App\Controller\FileUploaderAction;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractAdminController extends FileUploaderAction 
{
    protected function getDataTablePaginator(Request $request, $repository, string $label , $params = false)
    {
        /**
         * @var int item per page
         */
        $limit =  12; 
 
        if(!$params){
            $params = $request->query->all();
        }
        
        $data = $repository->findAllData($params,$limit);

        $totalData = $data->count();
        $maxPages = ($totalData / $limit)+1;

        return [
            $label => $data->getIterator(),
            'totalData' => $totalData,
            'thisPage'   => $params['page'] ?? 1,
            'maxPages'      => $maxPages
        ];
    }

}
