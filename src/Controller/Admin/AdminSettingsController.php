<?php
namespace App\Controller\Admin;

use App\Entity\Admin\Admin;
use App\Form\User\ChangePasswordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
 * @Route("/{_locale<en|fr>}/admin/settings" , name="admin_setting_index")
 */
class AdminSettingsController extends AbstractController
{
  private $tab = '';
  private $passwordEncoder;
  private $entityManager;

  public function __construct(UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $em)
  {
    $this->passwordEncoder = $passwordEncoder;
    $this->entityManager   = $em;
  }

  public function __invoke(Request $request)
  {

    /*** change password setting */
    $passwordFormBuilder = new ChangePasswordType(); 
    $changePasswordform  = $passwordFormBuilder->buildForm($this->createFormBuilder([]));
    $changePasswordform->handleRequest($request);
    if($changePasswordform->isSubmitted()) {
        $this->handleChangePassword($changePasswordform);
    }


    /*** admnistrator list */
    $adminManager = $this->entityManager->getRepository(Admin::class);
    $admins       = $adminManager->findAll();
    
    return $this->render('admin/settings/index.html.twig',[
        'changePasswordform' => $changePasswordform->createView(),
        'admins'             => $admins,
        'tab'                => $this->tab
    ]);
  

  }

  private function handleChangePassword($changePasswordform){
    if($changePasswordform->isValid()){
        $data = $changePasswordform->getData();
        $user = $this->getUser();
        $user->setPassword($this->passwordEncoder->encodePassword($user,  $data['newPassword']));
        $this->entityManager->persist($user);
        $this->entityManager->flush();
        
        if($data['logout']){
            return $this->redirectToRoute('logout');
        }
    }
    $this->tab = "user-account";
  }


} 
