<?php
namespace App\Controller\Admin;

use App\Entity\Organization\Organization;
use App\Entity\Application\Application;
use App\Entity\Scholarship\Scholarship;
use App\Entity\Student\Student;
use App\Repository\Student\StudentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/{_locale<en|fr>}/admin/index" , name="admin_index")
 */
class AdminDashboard extends AbstractController
{

  private $entityManager;

  /**
   * Controller contructor
   *
   * @param EntityManagerInterface $entityManager
   */
  public function __construct(EntityManagerInterface $entityManager)
  {
    $this->entityManager = $entityManager;
  }


  /**
   * Invokable method 
   *
   * @return Response
   */
  public function __invoke()
  {
    /**
     * @var StudentRepository $studentManager
     */
    $studentManager = $this->entityManager->getRepository(Student::class);
    $students   = $studentManager->count([]);

    /**
     * @var OrganizationRespository $organizationManager
     */
    $organizationManager = $this->entityManager->getRepository(Organization::class);
    $organizations   = $organizationManager->count([]);

    /**
    * @var ScholarshipRespository $scholarshipManager
    */
    $scholarshipManager = $this->entityManager->getRepository(Scholarship::class);
    $scholarships   = $scholarshipManager->count([]);
    
    /**
    * @var ApplicationRespository $applicationManager
    */
    $applicationManager = $this->entityManager->getRepository(Application::class);
    $applications   = $applicationManager->count([]);

    

    return $this->render('admin/dashboard/index.html.twig', [
      "students"      => $students,
      "organizations" => $organizations,
      "scholarships" => $scholarships,
      "applications"  => $applications,
    ]);
  }
} 
