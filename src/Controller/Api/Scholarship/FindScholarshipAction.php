<?php
namespace App\Controller\Api\Scholarship;

use App\Entity\Scholarship\Scholarship;
use App\Repository\Scholarship\ScholarshipRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/scholarship/find", methods={"get"})
 */
class FindScholarshipAction extends AbstractFOSRestController
{
    /** @var ScholarshipRepository  */
    private $scholarshipManager;

    /** @var EntityManager */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->scholarshipManager = $this->em->getRepository(Scholarship::class);
    }

    public function __invoke( Request $request ){

        $maxResult = $request->query->get("maxResult");

        if($maxResult == 1){
            $result = $this->findOneBy($request);
        }
        else {
            $searchCriteria = $request->query->get('criteria')?? [];
            $searchCriteria = json_decode($searchCriteria, true);
            $disciplineIds = $disciplines = [];
            if(!empty($searchCriteria['disciplines'])) {
                $disciplineIds = array_map(function ($discipline) {
                    return $discipline['id'];
                },
                $searchCriteria['disciplines']);
            }

            $countryIds = $countries =[];
            if(!empty($searchCriteria['grantedCountries'])) {
                $countryIds = array_map(
                    function ($country) {
                        return $country['id'];
                    },
                    $searchCriteria['grantedCountries']);
            }

            $levelIds = $levels = [];
            if(!empty($searchCriteria['grantedLevels'])) {
                $levelIds = array_map(function ($level) {
                    return $level['id'];
                },
                    $searchCriteria['grantedLevels']);
            }
            $onlyFavorited = json_decode($request->query->get('onlyFavorited'))?? false;
           
            $currentUser = $this->getUser();
           
            $order = json_decode($request->query->get('order'))?? [];
            $searchSort = $order->sort?? null;
            $searchOrder = $order->order?? null;
            $searchLimit = $request->query->get('limit')?? 10;
            $searchOffset = $request->query->get('offset')?? 0;
            $searchCriteria = $request->query->get('criteria')?? [];
            $result = $this->scholarshipManager->findByCriteria( $countryIds, $levelIds, $disciplineIds, $searchSort, $searchLimit, $searchOffset, $searchOrder, $currentUser, $onlyFavorited);

        }

        $context = new Context();
        $context->setGroups(['details']);

        $view = $this->view(array(
        "scholarship"    =>  $result
        ), null, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);

        $view->setContext($context);
        return $this->handleView( $view,Response::HTTP_OK);
    }

    private function findOneBy(Request $request){
        $criteria = json_decode($request->query->get('criteria'), true);
        return $this->scholarshipManager->findOneBy(
            $criteria
        );
    }
}