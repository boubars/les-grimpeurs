<?php
namespace App\Controller\Api\Scholarship;

use App\Controller\Admin\Testimonial\StudentTestimonialController;
use App\Entity\Common\Country;
use App\Entity\Scholarship\Scholarship;
use App\Entity\Student\Student;
use App\Repository\Scholarship\ScholarshipRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/total-scholarship/specific", methods={"get"})
 */
class TotalSpecificScholarshipAction extends AbstractFOSRestController
{
    /** @var ScholarshipRepository  */
    private $scholarshipManager;

    /** @var EntityManager */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->scholarshipManager = $this->em->getRepository(Scholarship::class);
    }

    public function __invoke( Request $request ){
        $country = $request->query->get('country')? json_decode($request->query->get('country')) : null;
        $scholarships = [];
        $countries = [];
        $countryObj = null;
        /** @var Student $currentUser */
        $currentUser = $this->getUser();
        if($currentUser instanceof Student) {
            $countries = $currentUser->getDesiredCountries();
            if (!empty($country) && !empty($country->id)) {
                /** @var Country $countryObj */
                $countryObj = $this->em->getRepository(Country::class)->find($country->id);
            }
            if(!$countryObj instanceof Country) {
                $countryObj = isset($countries[0]) ? $countries[0] : null;
            }
            if($countryObj instanceof Country) {
                $onlyFavorited = json_decode($request->query->get('onlyFavorited')) ?? false;
                try {
                    $scholarships = $this->scholarshipManager
                        ->findTotalAndCountByUserAndCountry($countryObj, $currentUser, $onlyFavorited);
                }
                catch (\Exception $exception){
                    $context = new Context();
                    $context->setGroups(['details']);
                    $view = $this->view(array(
                        "message"    =>  $exception->getMessage(),
                    ), null, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);

                    $view->setContext($context);
                    return $this->handleView( $view,Response::HTTP_NO_CONTENT);
                }
            }
        }


        $context = new Context();
        $context->setGroups(['details']);

        $view = $this->view(array(
            "scholarships"    =>  $scholarships,
        ), null, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);

        $view->setContext($context);
        return $this->handleView( $view,Response::HTTP_OK);
    }
}