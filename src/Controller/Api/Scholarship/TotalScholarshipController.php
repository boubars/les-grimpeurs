<?php

namespace App\Controller\Api\Scholarship;

use App\Repository\Scholarship\ScholarshipRepository;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/total-scholarship", name="total_scholarship", methods={"get", "post"})
 */
class TotalScholarshipController extends AbstractFOSRestController
{

    private $scholarshipManager;

    public function __construct(ScholarshipRepository $scholarshipManager)
    {
        $this->scholarshipManager = $scholarshipManager;
    }

    public function __invoke( Request $request )
    {
        $searchCriteria = $request->query->get('criteria')?? [];
        $searchCriteria = json_decode($searchCriteria, true);
        $disciplineIds = $disciplines = [];
        if(!empty($searchCriteria['disciplines'])) {
            $disciplineIds = array_map(function ($discipline) {
                return $discipline['id'];
            },
                $searchCriteria['disciplines']);
        }

        $countryIds = $countries =[];
        if(!empty($searchCriteria['grantedCountries'])) {
            $countryIds = array_map(function ($country) {
                return $country['id'];
            },
                $searchCriteria['grantedCountries']);
        }

        $levelIds = $levels = [];
        if(!empty($searchCriteria['grantedLevels'])) {
            $levelIds = array_map(function ($level) {
                return $level['id'];
            },
                $searchCriteria['grantedLevels']);
        }
        $onlyFavorited = $request->query->get('onlyFavorited')?? false;
        $onlyFavorited =  json_decode($onlyFavorited);
        $currentUser = $this->getUser();
       
        $result = $this->scholarshipManager->findTotalAndCount($countryIds, $levelIds, $disciplineIds, false, $currentUser, $onlyFavorited);

        $context = new Context();
        $context->setGroups(['details']);

        $view = $this->view(array(
            "scholarships"    =>  $result
        ));

        $view->setContext($context);
        return $this->handleView( $view,Response::HTTP_OK);
    }


}
