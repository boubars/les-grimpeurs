<?php
namespace App\Controller\Api\Scholarship;

use App\Controller\Admin\Testimonial\StudentTestimonialController;
use App\Entity\Common\Country;
use App\Entity\Organization\Organization;
use App\Entity\Scholarship\Scholarship;
use App\Entity\Student\Student;
use App\Entity\University\University;
use App\Repository\Scholarship\ScholarshipRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/scholarship/find/specific", methods={"get"})
 */
class FindSpecificScholarshipAction extends AbstractFOSRestController
{
    /** @var ScholarshipRepository  */
    private $scholarshipManager;

    /** @var EntityManager */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->scholarshipManager = $this->em->getRepository(Scholarship::class);
    }

    public function __invoke( Request $request ){
        $country = $request->query->get('country')? json_decode($request->query->get('country')) : null;
        $countries = [];
        $scholarships = [];
        $organisations = [];
        $countryObj = null;
        $result = [];

        /** handle specific university search */
        $university = json_decode($request->query->get('university'), true) ?? false;
        
        /** @var Student $currentUser */
        $currentUser = $this->getUser();
        if($currentUser instanceof Student) {
            $countries = $currentUser->getDesiredCountries();
            if (!empty($country) && !empty($country->id)) {
                /** @var Country $countryObj */
                $countryObj = $this->em->getRepository(Country::class)->findOneById($country->id);
            }



            if(!$countryObj instanceof Country) {
                $countryObj = isset($countries[0]) ? $countries[0] : null;
            }
            if($countryObj instanceof Country) {
                $onlyFavorited = json_decode($request->query->get('onlyFavorited')) ?? false;
                $order = json_decode($request->query->get('order')) ?? [];
                $searchSort = $order->sort ?? null;
                $searchOrder = $order->order ?? null;
                $searchLimit = $request->query->get('limit') ?? 12;
                $searchOffset = $request->query->get('offset') ?? 0;
                /** @var Scholarship $scholarships */
                $scholarships = $this->scholarshipManager
                ->findByUserAndCountry(
                    $countryObj,
                    $searchSort,
                    $searchLimit,
                    $searchOffset,
                    $searchOrder,
                    $currentUser,
                    $onlyFavorited,
                    $university
                );

                foreach($scholarships as $scholarship){
                    $organization = $scholarship->getRelatedOrganization();
                    if($organization instanceof University){
                        $organisations[] = $organization;
                    }
                    /*
                    else {
                        $scholarshipOrg = $scholarship->getAwardOrganizationType()->getName();
                        if(!empty($scholarshipOrg)) {
                            if($scholarshipOrg == 'univestity') {
                                $org = $scholarship->getRelatedOrganization();
                                if($org instanceof University){
                                    $organisations[] = $org;
                                }
                            }
                        }
                    }*/
                }
            }
        }


        $context = new Context();
        $context->setGroups(['details']);

        $view = $this->view(array(
            "scholarships"    =>  $scholarships,
            'desiredCountries' => $countries,
            'specificCountry' => $countryObj,
            'organisations' => $organisations
        ), null, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);

        $view->setContext($context);
        return $this->handleView( $view,Response::HTTP_OK);
    }
}