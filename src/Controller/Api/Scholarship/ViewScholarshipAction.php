<?php

namespace App\Controller\Api\Scholarship;

use App\Entity\Scholarship\Scholarship;
use App\Factory\User\SeenScholarshipFactory;
use App\Repository\Scholarship\ScholarshipRepository;
use App\Repository\Student\Activity\ActivityCauseRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ViewScholarshipAction extends AbstractFOSRestController
{
  
  private $manager;
  private $entityManager;

  public function __construct(EntityManagerInterface $entityManager)
  {
    $this->manager = $entityManager->getRepository(Scholarship::class);
    $this->entityManager = $entityManager;
  }
  
  public function __invoke($slug)
  {
    $scholarship = $this->manager->findOneBySlug($slug);
    
    if(!$scholarship){
      return $this->redirect('/page-not-found');
    }

    return $this->render('scholarship/view.html.twig',[
      'scholarship' => $scholarship
    ]);

  }

  /**
   * @Route("/api/scholarship/{id}/seenby")
   */
  public function viewCounter(Scholarship $scholarship, SeenScholarshipFactory $factory){
      
      $user = $this->getUser();
      
      if(!$user || !$scholarship){
        return new JsonResponse(['success'=> false]);
      }
      
      if($user->haveSeen($scholarship)){
        return new JsonResponse(['success'=> false]);
      }

      $user->addSeenScholarship(
        $factory::create($scholarship)
      );

      $this->entityManager->persist($user);
      $this->entityManager->flush();

      return new JsonResponse(['success'=> true]);
  }
}
