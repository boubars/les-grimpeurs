<?php

namespace App\Controller\Api\Scholarship;

use App\Entity\Scholarship\Scholarship;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/scholarship/{id}/form", methods={"get"})
 */
class GetApplicationFormAction extends AbstractFOSRestController
{
  public function __invoke(Scholarship $scholarship)
  {
    
    $form = $scholarship->getForm();
    
    $view = $this->view(array(
        "form"    =>  $form
    ), null, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);

    $context = new Context();
    $context->setGroups(['details','form']);
    $view->setContext($context);
    
    return $this->handleView( $view, Response::HTTP_OK);
  }
}
