<?php

namespace App\Controller\Api\Scholarship;

use App\Entity\Scholarship\Scholarship;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/scholarship/delete", methods={"delete"})
 */
class DeleteScholarshipAction extends AbstractFOSRestController
{
  public function __invoke(Request $request, EntityManagerInterface $entityManager)
  {
    $scholarship = $request->get('id');

    if(!$scholarship){
        return $this->handleView($this->view([ "success" => false, "error" => 'Bad request' ], Response::HTTP_BAD_REQUEST));
    }
    
    /** @var ServiceEntityRepository */
    $scholarshipManager = $entityManager->getRepository(Scholarship::class);
    
    /** @var Scholarship */
    $oScholarship       = $scholarshipManager->findOneById($scholarship);

    if(!$oScholarship){
        return $this->handleView($this->view([ "success" => false, "error" => 'Bad request' ], Response::HTTP_BAD_REQUEST));
    }
    
    /** check organization owner */
    if($oScholarship->getRelatedOrganization() != $this->getUser()){
        return $this->handleView($this->view([ "success" => false, "error" => 'Permission denied' ], Response::HTTP_FORBIDDEN));
    }

    /** soft delete scholarship */
    $oScholarship->setDeleted(true);
    $oScholarship->removeAllApplications();
    $entityManager->persist($oScholarship);
    $entityManager->flush();

    return $this->handleView($this->view([ "success" => true ], Response::HTTP_OK));
  }
}
