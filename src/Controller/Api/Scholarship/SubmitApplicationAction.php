<?php
namespace App\Controller\Api\Scholarship;

use App\Entity\Application\Application;
use App\Entity\Application\ApplicationSheet;
use App\Repository\Application\ApplicationRepository;
use App\Repository\Scholarship\ScholarshipRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;

class SubmitApplicationAction extends AbstractFOSRestController
{

    /**
     * Handle submit application request
     *
     * @Route("/api/scholarship/application/submit", methods={"POST"})
     * @IsGranted("ROLE_STUDENT")
     *
     * @param Request $request
     * @param ScholarshipRepository $scholarshipManager
     * @param EntityManagerInterface $entityManager
     * @param ApplicationRepository $applicationManager
     * @return Response
     * @throws \Exception
     */
   public function submit( Request $request , ScholarshipRepository $scholarshipManager, EntityManagerInterface $entityManager, ApplicationRepository $applicationManager){
      $scholarship = $request->request->get('scholarship');
      $scholarship = $scholarship ? $scholarshipManager->findOneById($scholarship) : false;

      $student = $this->getUser();

      if(!$scholarship){
        return $this->handleView(
          $this->view(array(
              "error"    =>  "scholarship_not_found"
              )
          ),Response::HTTP_NO_CONTENT); 
      }

      /***
       * check if allready send
      */
      $application = $applicationManager->findOneBy(array(
        'scholarship' => $scholarship,
        'student'     => $student
      ));


      if (!$application) {
        //$published = $request['publish'] ?? false;
      	$application = new Application();
        $application->setStudent($student)
          //->setPublish($published)
          ->setScholarship($scholarship)->setDateApply(new \DateTime());

          $entityManager->persist($application);
          $entityManager->flush();
      }

      $context = new Context();
      $context->setGroups(['details','form']);
      $view = $this->view([
        'application' => $application
      ]);
      $view->setContext($context);
      return $this->handleView($view);
   }

     

  /**
   * Persit and publish application sheet data
   * 
   * @Route("/api/application/publish", methods={"GET"})
   * @IsGranted("ROLE_STUDENT")
   * @param  Request $request
   * @param  EntityManagerInterface $em
   * @return JsonResponse
   */
   public function publishApplication(Request $request, EntityManagerInterface $em){

      $application = $request->query->get('application');
      $application = $em->getRepository(Application::class)->findOneById($application);
      if(!$application){
        return new JsonResponse([
          'error'   => Response::HTTP_BAD_REQUEST,
          'success' => false
        ]);
      }

      $application->setStatus(Application::ENCOURS);
      $application->setDateApply(new DateTime());
      $em->persist($application);
      $em->flush();

      return new JsonResponse([
        'success' => true,
        'id'      => $application->getId()
      ]);

   }
}