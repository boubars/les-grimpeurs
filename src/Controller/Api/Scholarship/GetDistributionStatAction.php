<?php 
namespace App\Controller\Api\Scholarship;

use App\Repository\Scholarship\ScholarshipRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/scholarship/distribution-stat", name="scholarship_distribution_stat")
 */
class GetDistributionStatAction extends AbstractFOSRestController {
  public function __invoke(Request $reqeuest, ScholarshipRepository $scholarshipManager)
  {

    $context = $reqeuest->request->get('context');

    $distribution = null;
    $distribution['foundation']  = $scholarshipManager->findTotalAndCount([], [], [], 'foundation', null);
    $distribution['university'] = $scholarshipManager->findTotalAndCount([], [],     [], 'university', null);
    $distribution['company']    = $scholarshipManager->findTotalAndCount([], [],     [], 'company');
    $distribution['administration']    = $scholarshipManager->findTotalAndCount([],  [], [], 'Admnistration');
    $distribution['association']    = $scholarshipManager->findTotalAndCount([], [], [], 'Association and OSBL');
    $distribution['reseach_center']    = $scholarshipManager->findTotalAndCount([],  [], [], 'Research center');
    $distribution['international_organization']    = $scholarshipManager->findTotalAndCount([], [], [], 'International organization');

    $total = count($scholarshipManager->findAll());

    $view = $this->view(
      [
        "total"           => $total,
        "distribution"    =>  $distribution
      ]);
    return $this->handleView( $view,Response::HTTP_OK);
  }
}
