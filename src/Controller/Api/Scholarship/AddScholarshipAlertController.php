<?php

namespace App\Controller\Api\Scholarship;

use App\Entity\Common\Country;
use App\Entity\Common\Level;
use App\Entity\Discipline\Discipline;
use App\Entity\Scholarship\ScholarshipAlert;
use App\Entity\Student\Student;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddScholarshipAlertController extends AbstractFOSRestController
{

    /** @var EntityManager */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/api/scholarship/add/scholarship/alert", name="scholarship_add_scholarship_alert")
     *
     * @param Request $request
     */
    public function __invoke( Request $request )
    {
        $levels = $request->query->get('levels')?? null;
        $disciplines = $request->query->get('disciplines')?? null;
        $countries = $request->query->get('countries')?? null;
        $currentUser = $this->getUser();
        if($currentUser instanceof Student){
            $alert = $currentUser->getScholarshipAlert();
            if(!$alert instanceof ScholarshipAlert){
                $alert = new ScholarshipAlert();
                $this->em->persist($alert);
                $alert->setCreatedAt(new \DateTime());
                $alert->setStudent($currentUser);
            }
            else {
                $alert->clear();
            }
            if(!empty($levels) || !empty($disciplines) || !empty($countries)){

                if(!empty($levels)){
                    foreach ($levels as $level){
                        $level = json_decode($level, true);
                        $levelObj = $this->em->getRepository(Level::class)->find($level['id']);
                        if($levelObj){
                            $alert->addLevel($levelObj);
                        }

                    }
                }
                if(!empty($disciplines)){
                    foreach ($disciplines as $discipline){
                        $discipline = json_decode($discipline, true);
                        $disciplineObj = $this->em->getRepository(Discipline::class)->find($discipline['id']);
                        if($disciplineObj){
                            $alert->addDiscipline($disciplineObj);
                        }

                    }
                }
                if(!empty($countries)){
                    foreach ($countries as $country){
                        $country = json_decode($country, true);
                        $countryObj = $this->em->getRepository(Country::class)->find($country['id']);
                        if($countryObj){
                            $alert->addCountry($countryObj);
                        }
                    }
                }
                $this->em->flush();
                return $this->handleView(
                    $this->view(["success" => true, "id" => $alert->getId()]),
                    Response::HTTP_OK
                );
            }
            else {
                $alert->clear();
                $this->em->remove($alert);
                $this->em->flush();
                return $this->handleView(
                    $this->view(["success" => true, "id" => null]),
                    Response::HTTP_OK
                );
            }
        }
        else {
            return $this->handleView(
                $this->view(["error"  =>  "student not found"]),
                Response::HTTP_NO_CONTENT
            );
        }

    }
}
