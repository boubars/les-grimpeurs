<?php

namespace App\Controller\Api\Scholarship;

use App\Entity\Common\Country;
use App\Entity\Common\Level;
use App\Entity\Discipline\Discipline;
use App\Entity\Scholarship\ScholarshipAlert;
use App\Entity\Student\Student;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GetScholarshipAlertController extends AbstractFOSRestController
{

    /** @var EntityManager */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/api/scholarship/get/scholarship/alert", name="scholarship_get_scholarship_alert")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function __invoke( Request $request )
    {
        $currentUser = $this->getUser();
        if($currentUser instanceof Student){
            $alert = $currentUser->getScholarshipAlert();
            $context = new Context();
            $context->setGroups(['details']);

            $view = $this->view(array(
                "scholarshipAlert"    =>  $alert
            ), null, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);

            $view->setContext($context);
            return $this->handleView( $view,Response::HTTP_OK);

        }
        else {
            return $this->handleView(
                $this->view(["error"  =>  "student not found"]),
                Response::HTTP_NO_CONTENT
            );
        }

    }
}
