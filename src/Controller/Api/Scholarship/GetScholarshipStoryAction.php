<?php
namespace App\Controller\Api\Scholarship;

use App\Entity\Application\Application;
use App\Repository\Application\ApplicationRepository;
use App\Repository\Scholarship\ScholarshipRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * @Route("/api/scholarship/story", methods={"GET"})
 */
class GetScholarshipStoryAction extends AbstractFOSRestController
{

   public function __invoke( Request $request , ScholarshipRepository $scholarshipManager){
        
      $scholarship = $request->query->get('scholarship');
      $scholarship = $scholarship ? $scholarshipManager->findOneById($scholarship) : false;

     
      if(!$scholarship){
        return $this->handleView(
          $this->view(array(
              "error"    =>  "scholarship_not_found"
              )
          ),Response::HTTP_NO_CONTENT); 
      }

      $page =  $request->query->get('page', 1);
      $limit = 8;
    
      $data = $scholarshipManager->getUserStory($scholarship, $page, $limit);


      $context = new Context();
      $context->setGroups(['']);
      $view = $this->view([
        "story"         =>  $data,
        'currentPage'   => $page,
        'scholarship'   => [
          "id"          => $scholarship->getId(),
          "name"        => $scholarship->getName(),
          "logo"        => $scholarship->getLogo(),
          "slug"        => $scholarship->getSlug(),
        ]
      ]);

    $view->setContext($context);
    return $this->handleView( $view,Response::HTTP_OK);

    }

}