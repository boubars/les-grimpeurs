<?php 
namespace App\Controller\Api\Scholarship;

use App\Entity\Scholarship\Scholarship;
use App\Repository\Scholarship\ScholarshipRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/scholarship/stat", name="scholarship_stat")
 */
class GetScholarshipStatAction extends AbstractFOSRestController {
  
    
    /**
     * Undocumented variable
     *
     * @var [Scholarship]
     */
    private $scholarship;
    
    /**
     * Undocumented variable
     *
     * @var [ScholarshipRepository]
     */
    private $scholarshipManager;

    /**
     * Statistic action contructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->scholarshipManager = $entityManager->getRepository(Scholarship::class);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return ViewHandlerInterface
     */
    public function __invoke(Request $request)
    {
        $scholarships = $request->query->get('scholarship', false);

        if(!$scholarships){
            return $this->handleView($this->view([
                'error' => 'content not found'
            ]), Response::HTTP_NO_CONTENT);
        }

        
        $this->scholarships = is_array($scholarships) ? $scholarships : [$scholarships];

        
        $context = $request->query->get('context');
        
        $view = null;

        switch ($context) {
            case 'evolution':
                $view = $this->evolutionStat($request);
                break;
            
            case 'range_sexe':
                $view = $this->rangeSexeStat($request);
                break;
            
            case 'range_level':
                $view = $this->rangeLevelStat($request);
                break;
            
            case 'range_discipline':
                $view = $this->rangeDisciplineStat($request);
                break;
            
            case 'range_age':
                $view = $this->rangeAgeStat($request);
                break;
            
            case 'range_country':
                $view = $this->rangeCountryStat($request);
                break;
            
            default:
                # code...
                break;
        }
        
        return $this->handleView($view,Response::HTTP_OK);
    }



    private function evolutionStat(Request $request){
        
        $date        = $request->query->get('date');
        $serie       = $request->query->get('serie');

        $stat = $this->scholarshipManager->getEvolutionStat($this->scholarships, $serie, $date);

        /*** pics data */
        $todayPic =  $this->scholarshipManager->getEvolutionStat($this->scholarships, $serie, 'today');
        $yearPic  =  $this->scholarshipManager->getEvolutionStat($this->scholarships, $serie, 'thisyear');

        $pic  = [
            'today' => $todayPic[0]['count'],
            'year'  => $yearPic[0]['count']
        ];
        return $this->view([
            'stat' => $stat,
            'pic'  => $pic
        ]);
    }

    private function rangeSexeStat(Request $request){
        $period        = $request->query->get('period', false);
        $serie         = $request->query->get('serie');
        $range = $this->scholarshipManager->getRangeSexeStat($this->scholarships, $period);

        return $this->view([
            'stat' => $range
        ]);
    } 

    private function rangeLevelStat(Request $request){
        $period = $request->query->get('period', false);
        $serie  = $request->query->get('serie');
        $range  = $this->scholarshipManager->getRangeLevelStat($this->scholarships, $period);
        
        return $this->view([
            'stat' => $range
        ]);
    } 

    private function rangeAgeStat(Request $request){
        $period = $request->query->get('period', false);
        $serie  = $request->query->get('serie');
        $range = $this->scholarshipManager->getRangeAgeStat($this->scholarships,$period);

        return $this->view([
            'stat' => $range
        ]);
    } 

    private function rangeCountryStat(Request $request){
        $period = $request->query->get('period', false);
        $serie  = $request->query->get('serie');
        $range = $this->scholarshipManager->getRangeCountryStat($this->scholarships, $period);

        return $this->view([
            'stat' => $range
        ]);
    } 

    private function rangeDisciplineStat(Request $request){
        $period = $request->query->get('period', false);
        $serie  = $request->query->get('serie');
        $range = $this->scholarshipManager->getRangeDisciplineStat($this->scholarships, $period);
        
        return $this->view([
            'stat' => $range
        ]);
    } 

}
