<?php
namespace App\Controller\Api\Scholarship;

use App\Entity\Scholarship\InformationRequest;
use App\Repository\Scholarship\InformationRequestRepository;
use App\Repository\Scholarship\ScholarshipRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * @Route("/api/scholarship/request/infos", methods={"POST"})
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 */
class RequestInfosAction extends AbstractFOSRestController
{

   public function __invoke( Request $request , ScholarshipRepository $scholarshipManager, EntityManagerInterface $entityManager, InformationRequestRepository $messageManager){
     
      $scholarship = $request->request->get('scholarship');
      $scholarship = $scholarship ? $scholarshipManager->findOneById($scholarship) : false;

      if(!$scholarship){
        return $this->handleView(
          $this->view(array(
              "error"    =>  "scholarship_not_found"
              )
          ),Response::HTTP_NO_CONTENT); 
      }

      /***
       * check if allready send
      */
      $fromEmail = $request->request->get("from");
      $found = $messageManager->findOneBy(array(
        'scholarship' => $scholarship,
        'fromEmail'     => $fromEmail
      ));

      if($found){
        return $this->handleView(
          $this->view(array(
              "error"    =>  "duplicate_request"
              )
          ),Response::HTTP_NO_CONTENT); 
      }

      try {
       
        $message = new InformationRequest();
        $message->setFromEmail($fromEmail)
        ->setScholarship($scholarship)
        ->setMessage($request->request->get('message'));

        $entityManager->persist($message);
        $entityManager->flush();
      } catch (Exception $e) {
        return $this->handleView(
          $this->view(array(
              "error"    =>  "internal_error",
              "message"  => $e->getMessage()
              )
          ),Response::HTTP_BAD_GATEWAY); 
      }

      return $this->handleView(
      $this->view(array(
          "success"    =>  true
          )
      ),Response::HTTP_OK);
   }

}