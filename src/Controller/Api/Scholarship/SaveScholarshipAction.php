<?php
namespace App\Controller\Api\Scholarship;

use App\Controller\FileUploaderAction;
use App\Entity\Common\Continent;
use App\Entity\Common\Country;
use App\Entity\Common\Currency;
use App\Entity\Common\Gallery;
use App\Entity\Common\Level;
use App\Entity\Common\Nationality;
use App\Entity\Common\SocialNetwork;
use App\Entity\Discipline\Category;
use App\Entity\Discipline\Discipline;
use App\Entity\Organization\Organization;
use App\Entity\Organization\OrganizationType;
use App\Entity\Scholarship\Scholarship;
use App\Form\Scholarship\ScholarshipType;
use App\Service\Utilis\UploadedBase64File;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Exception;

/**
 * Invokable class for add or edit scholarhsip 
 * 
 * @Route("/api/scholarship/save", methods={"POST"})
 */
class SaveScholarshipAction extends FileUploaderAction
{  

   /**
    * Global variable
    *
    * @var EntityManagerInterface
    */
   private  $entityManager; 
   
   /**
    * Global variable
    *
    * @var Scholarship
    */
   private $scholarship;
   
   /**
    * Global variable
    *
    * @var ServiceEntityRepository;
    */
   private $studyFieldManager; 
   
   /**
    * Global variable
    *
    * @var ServiceEntityRepository
    */
   private $studyFieldTagManager;
   
   /**
    * Global variable
    *
    * @var ServiceEntityRepository
    */
   private $continentManager;
   
   /**
    * Global variable
    *
    * @var ServiceEntityRepository
    */
   private $countryManager;
   
   /**
    * Global variable
    *
    * @var ServiceEntityRepository
    */
   private $nationalityManager;
   
   /**
    * Global variable
    *
    * @var ServiceEntityRepository
    */
   private $levelManager;
   
   /**
    * Global variable
    *
    * @var ServiceEntityRepository
    */
   private $currencyManager;
   
   /**
    * Global variable
    *
    * @var ServiceEntityRepository
    */
   private $organizationManager;
   
   /**
    * Global variable
    *
    * @var ServiceEntityRepository
    */
   private $scholarshipManager;
   
   /**
    * Global variable
    *
    * @var SluggerInterface
    */
   private $slugger;

   public function __construct( EntityManagerInterface $entityManager,  SluggerInterface $slugger)
   {
      $this->entityManager        = $entityManager;
      $this->studyFieldManager    = $entityManager->getRepository(Category::class);
      $this->studyFieldTagManager = $entityManager->getRepository(Discipline::class);
      $this->continentManager     = $entityManager->getRepository(Continent::class);
      $this->countryManager       = $entityManager->getRepository(Country::class);
      $this->nationalityManager   = $entityManager->getRepository(Nationality::class);
      $this->levelManager         = $entityManager->getRepository(Level::class);
      $this->currencyManager      = $entityManager->getRepository(Currency::class);
      $this->organizationManager  = $entityManager->getRepository(Organization::class);
      $this->scholarshipManager   = $entityManager->getRepository(Scholarship::class);
      
      $this->slugger              = $slugger;

      parent::__construct($slugger);
   }

   /**
    * Request entry point
    *
    * @param Request $request
    * @return JsonResponse
    */
   public function __invoke(Request $request){
      
      $formData = $request->request;

      $oScholarship      = $this->getRequestedScholarship($formData);
   
      $this->scholarship = $oScholarship;


      $form = $this->createForm(ScholarshipType::class, $this->scholarship);
      
      /*** Mapped elementor var */
      $form->submit($formData->all());

      // maped owner
      $this->scholarship->setOwner($this->getUser());

      //maped date
      $dateLimit = $formData->get('candidatureLimit', false);
      $this->scholarship->setCandidatureLimit($dateLimit ? new \DateTime($dateLimit) : null);
     
      // mapped granted continent
      $this->updateGrantedContinent($formData->get('grantedContinent'));
      
      // mapped granted countries
      $this->updateGrantedCountries($formData->get('grantedCountries'));
      
      // mapped granted nationality
      $this->updateGrantedNationalities($formData->get('grantedNationalities'));
   
      // handle study fields
      $this->updateGrantedStudyFields($formData->get('studyField'));
      
      // handle study field tag
      $this->updateGrantedStudyFieldTags($formData->get('studyFieldTags'));
      
      // handle other study field tag
      $otherOstudyField = $formData->get('otherStudyFields');
      if($otherOstudyField){
         $this->updateOtherStudyFieldTags($formData->get('otherStudyFieldTags'));
      }
      
      // handle granted Level
      $this->updateGrantedLevel($formData->get('grantedLevel'));

      /** Handle social netwrok */
      if($formData->get('socialNetwork'))
      $this->handleUpdatesocialNetwork($formData->get('socialNetwork'));
      
      /** Handle social currency */
      if($formData->get('currency'))
         $this->handleUpdateCurrency($formData->get('currency'));

      /**
      * mapped selectionCriteria
      */
      $this->scholarship
      ->setSelectionCriteria(json_decode($formData->get('selectionCriteria'), true))
      ->setAttachments(json_decode($formData->get('attachments'), true))
      ->setAdvantages(json_decode($formData->get('advantages'), true));
      
      /** Handle upload logo */
      $this->handleUploadLogo(
         $request
      );

      
      /** Handle contact Avatar */
     $this->handleUploadContactAvatar(
      $request->files->get('contactAvatar')
     );
      
      /** Handle contactAdresseMap */
      $fileContactAdresseMap = $request->files->get('contactAdresseMap');
      if($fileContactAdresseMap){
         $newMapName = $this->uploadDocument($fileContactAdresseMap, "map_dir"); 
         $this->scholarship->setContactAdresseMap($newMapName);
      }
      

      /** Handle gallery upload file */
      $this->updateGallery($request->files->get("galery"));
      
      

      /*** handle awardOrganizationType */
      $this->handleAwardOrganizationType(
         $formData->get('awardOrganizationType')
      );


      /** Handle relatedOrganization */
      $this->handleRelatedOrganization(
         $formData->get('relatedOrganization')
      );

      /** Handle origin contry */
      $this->handleOriginCountry();
      

      /** generate slugUrl **/
      $slug = $this->slugger->slug(
         sprintf("%s-%s" , 
            $this->scholarship->getName(),
            $this->scholarship->getAwardOrganizationType()->getName() 
         ),
         "-"
      );

      $this->scholarship->setSlug(strtolower($slug));
      $this->entityManager->persist($this->scholarship);
      $this->entityManager->flush();


      return new JsonResponse([
         'success' => true,
         'slug'    => $this->scholarship->getSlug()
      ], 200);
   }

   /**
    * Get requested scholarship if edit and return new Scholarship OBJECT if new
    *
    * @param InputBag $request
    * @return Scholarship
    */
   private function getRequestedScholarship($request){
      $sid = $request->get('id', false);
      $oScholarhship = $sid ? $this->scholarshipManager->findOneById($sid) : false;

      if($sid && $oScholarhship){
         /** check scholarship owner */
         $owner = $oScholarhship->getRelatedOrganization();
         if($owner != $this->getUser()){
            throw new Exception("Error Processing Request, Permission denied", 1);
         }
      }

      return $oScholarhship ? $oScholarhship : new Scholarship();
   }

   /**
    * Fetch and update schloarship granted contient
    *
    * @param  Array $data
    * @return void
    */
   private function updateGrantedContinent($data){
      if($data[0]['id'] != 0){
         foreach ($data as $key => $continent) {
            $continent = $this->continentManager->findOneById($continent['id']);
            $this->scholarship->addGrantedContinent(
               $continent
            );
         }
      }
   }

   /**
    * Mapped scholarship currency
    *
    * @param  Array $data
    * @return void
    */
   private function handleUpdateCurrency($data){
      
      $id = $data['id'] ?? 1;
      $currency = $this->currencyManager->findOneById($id);
      $this->scholarship->setCurrency($currency);
   }

   /**
    * fetch and update scholarship countries
    *
    * @param  Array $data
    * @return void
    */
   private function updateGrantedCountries($data){
      if($data[0]['id'] != 0){
         foreach ($data as $key => $country) {
            $country = $this->countryManager->findOneById($country['id']);
            $this->scholarship->addGrantedCountry(
               $country
            );
         }
      }
   }

   /**
    * fetch and update scholarship granted nationalities
    *
    * @param  Array $data
    * @return void
    */
   private function updateGrantedNationalities($data){
      if($data[0]['id'] != 0){
         foreach ($data as $key => $nationality) {
            $nationality = $this->nationalityManager->findOneById($nationality['id']);
            $this->scholarship->addGrantedNationality(
               $nationality
            );
         }
      }
   }

   /**
    * Fetch and update scholarship discipline [Discipline Category]
    *
    * @param  Array $data
    * @return void
    */
   private function updateGrantedStudyFields($data){
      if($data[0]['id'] != 0){   
         foreach ($data as $key => $studyField) {
            $studyField = $this->studyFieldManager->findOneById($studyField['id']);
            $this->scholarship->addStudyField(
               $studyField
            );
         }
      }
   }

   /**
    * Fetch and update scholarhsip studyfiledtags [Discipline]
    *
    * @param Array $data
    * @return void
    */
   private function updateGrantedStudyFieldTags($data){
      if(!$data || empty($data)){
         return false; 
      }
      
      if(count($data) == 1 && $data[0]['id'] == 0){
         return false;
      }
      
      foreach ($data as $key => $tag) {  
         $studyFieldTag = null;
         /*** if new tag by other input */
         if(isset($tag['id'])){
            $studyFieldTag = $this->studyFieldTagManager->findOneById($tag['id']);
         }else{
            $studyFieldTag = (new Discipline())->setName($tag['name']);
         }
         $this->scholarship->addStudyFieldTag(
            $studyFieldTag
         );
      }

   }

   /**
    * On other Displine posted, add this as new discipline
    *
    * @param String $data
    * @return void
    */
   private function updateOtherStudyFieldTags($data){
      $data = json_decode($data, true);
      foreach ($data as $key => $tag) {  
         $newStudyFieldTag = $this->studyFieldTagManager->findOneByName($tag);
         $newStudyFieldTag = $newStudyFieldTag ? $newStudyFieldTag : (new Discipline())->setName($tag);
         $this->scholarship->addStudyFieldTag(
            $newStudyFieldTag
         );
      }
     
   }

   /**
    * Fetch and update scholarship  granted level
    *
    * @param  Array $data
    * @return void
    */
   private function updateGrantedLevel($data){
      if($data[0]['id'] != 0){   
         foreach ($data as $key => $tag) {  
            $level = $this->levelManager->findOneById($tag['id']);
            $this->scholarship->addGrantedLevel(
               $level
            );
         }
      }
   }

   /**
    * Update scholarship social network
    *
    * @param  Array $data
    * @return void
    */
   private function handleUpdateSocialNetwork($data){
      $socialNetwork = new SocialNetwork();
      $socialNetwork
      ->setFacebook($data['facebook'])
      ->setInstagram($data['instagram'])
      ->setYoutube($data['youtube'])
      ->setTwitter($data['twitter'])
      ->setLinkedin($data['linkedin']);
      $this->scholarship->setSocialNetwork($socialNetwork);
   }
   
   /**
    * Update scholarhship gallery 
    *
    * @param  Array $data
    * @return void
    */
   private function updateGallery($data){
      if(!$data && $this->scholarship->getId()){
         return 1;
      }

      $scholarshipGallery = new Gallery();
      foreach ($data as $key => $file) {
         $fileType = $file->getClientMimeType();
         $newfileName = $this->uploadDocument($file, "picture_dir"); 
         if($fileType == "application/pdf"){
            $scholarshipGallery->addDocument($newfileName);
         }else{
            $scholarshipGallery->addPicture($newfileName);
         }
      }
      $this->scholarship->setGallery($scholarshipGallery);
   }

   /**
    * set and update Scholarship organization owner type
    *
    * @param Array $data
    * @return void
    */
   private function handleAwardOrganizationType($data){
      /** @var OrganizationTypeRepository */
      $typeManager = $this->entityManager->getRepository(OrganizationType::class);
      $organizationType = $typeManager->findOneById($data['id']);
     
      $this->scholarship->setAwardOrganizationType($organizationType ?? null);
      return true;
   }

   /**
    * Find and Update scholarship owner
    *
    * @param  Array $data
    * @return void
    */
   private function handleRelatedOrganization($data){
      $organization = $this->organizationManager->findOneById($data['id']);
      dd($data, $organization);
      $this->scholarship->setRelatedOrganization($organization ?? null);
      return true;
   }

   /**
    * Update scholarhsip origin country
    *
    * @return void || Exception
    */
   private function handleOriginCountry(){
      /*** get scholarship owner by organization name */
      $country = $this->scholarship->getRelatedOrganization()->getCountry();
      $this->scholarship->setOriginCountry(
         $country ?? null
      );

      return true;
   }

   /**
    * Handle scholarship update logo
    *
    * @param  Request $fileLogo
    * @return Boolean
    */
   private function handleUploadLogo(Request $request){
      $fileLogo = $request->files->get('logo');

      if(!$fileLogo && !($this->scholarship->getId())){
         $logo = $request->request->get('logo');
         if(!isset($logo['base64'])){
            throw new Exception("Logo file is required", 1);
         }
         $this->uploadLogoFile($logo, true);
         return true;
      }

      if(!isset($fileLogo['file'])){
         throw new Exception("Logo file is required", 1);
      }

      $this->uploadLogoFile($fileLogo);
      return true;
      
   }

   private function uploadLogoFile($file, $base64 = false){
      
      if($base64){
         $file['file'] = new UploadedBase64File($file['base64']);
      }
      $newLogoFilename = $this->uploadDocument($file['file'], "logo_dir"); 
      $this->scholarship->setLogo($newLogoFilename);
   }

   /**
    * Handle scholarship update Avatar
    *
    * @param [mixed] $fileAvatar
    * @return void
    */
   private function handleUploadContactAvatar($fileAvatar){
      if(!$fileAvatar && !($this->scholarship->getId())){
        throw new Exception("Contact avatar file is required", 1);
      }
      if(isset($fileAvatar['file'])){
         $newAvatarFilename = $this->uploadDocument($fileAvatar['file'], "avatar_dir"); 
         $this->scholarship->setContactAvatar($newAvatarFilename);
      }
   }
}