<?php

namespace App\Controller\Api\Contact;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Manager\Contact\ContactManager;

/**
 * Class SendMessageAction
 * @package App\Controller
 */
class SendMessageAction extends AbstractController
{

    /**
     * @Route("/api/contact/send", name="sendMessage" , methods={"POST"})
     * @param Request $request
     * @param ContactManager $contactManager
     * @return JsonResponse
     */
    public function __invoked(Request $request, ContactManager $contactManager):JsonResponse
    {
        $response = $contactManager->sendMessage($request);

        return new JsonResponse(
            [
                'state' => $response['state'] ,
                'message' => $response['message']
            ]
        );
    }

}
