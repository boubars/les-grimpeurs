<?php
namespace App\Controller\Api\Grimpeurs\SitePreference;

use App\Repository\Grimpeurs\PostRepository;
use App\Repository\Grimpeurs\SitePreferenceRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/preference",name="api_get_preference", methods="GET") 
 */
class GetPreferenceAction extends AbstractFOSRestController
{

    public function __invoke(Request $request, SitePreferenceRepository $manager)
    {
        $params   = $request->query->all();
        $data     = $manager->findBy($params);
        return $this->handleView(
            $this->view(['params' => $data]),Response::HTTP_OK); 
    }
}