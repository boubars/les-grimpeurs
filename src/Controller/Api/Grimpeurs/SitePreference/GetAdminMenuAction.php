<?php
namespace App\Controller\Api\Grimpeurs\SitePreference;

use App\Twig\Admin\Menu\MenuInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/menu-admin",name="api_get_admin_menu", methods="GET") 
 */
class GetAdminMenuAction extends AbstractFOSRestController
{

    public function __invoke(Request $request)
    {
        
        $data = [];
        
        $lang = $request->query->get('locale', 'fr');
        foreach (MenuInterface::ADMIN_MENU as $key => $item) {
           
           if(!("#" == $item['route'])){
               $item['absolute_path'] = $this->generateUrl($item['route'], ['_locale' => $lang]);
               $item['label']         = $item['label'][$lang];
               $data[]                = $item;
           }
           if(isset($item['children'])){
               foreach ($item['children'] as $key => $subItem) {
                    $subItem['label']         = $subItem['label'][$lang];
                    if($subItem['route']){
                        $subItem['absolute_path'] = $this->generateUrl($subItem['route'], ['_locale' => $lang]);
                        $data[]                   = $subItem;
                    }

                    if(isset($subItem['url'])){
                        $subItem['absolute_path'] = $subItem['url'];
                    }
                    
               }
           }
           

        }
       
        return $this->handleView(
            $this->view(['menu' => $data]),
            Response::HTTP_OK
        ); 
    }
}