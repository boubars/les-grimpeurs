<?php
namespace App\Controller\Api\Grimpeurs\Faq;

use App\Repository\Grimpeurs\FAQ\ThematicRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/faq/thematic",name="api_faq_thematic", methods="GET") 
 */
class GetThematicAction extends AbstractFOSRestController
{

    public function __invoke(ThematicRepository $manager)
    {
        return $this->handleView(
            $this->view([
                'thematics' => $manager->findAll()
            ]
        ),Response::HTTP_OK); 
    }
}
