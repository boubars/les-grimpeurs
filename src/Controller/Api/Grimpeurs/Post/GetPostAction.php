<?php
namespace App\Controller\Api\Grimpeurs\Post;

use App\Repository\Grimpeurs\PostRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/post",name="api_get_post", methods="GET") 
 */
class GetPostAction extends AbstractFOSRestController
{

    public function __invoke(Request $request, PostRepository $postManager)
    {
        $params   = $request->query->all();
        $data     = $postManager->getByCriteria($params);
        return $this->handleView(
            $this->view([
                'posts' => $data
            ]
        ),Response::HTTP_OK); 
    }
}