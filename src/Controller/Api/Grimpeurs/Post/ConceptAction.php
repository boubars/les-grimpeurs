<?php
namespace App\Controller\Api\Grimpeurs\Post;

use App\Repository\Grimpeurs\ConceptRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/page/concept", name="api_grimpreus_concept", methods="GET")
 */
class ConceptAction extends AbstractFOSRestController
{

    public function __invoke(Request $request, ConceptRepository $manager)
    {
        $params = $request->query->all();
        return $this->handleView(
            $this->view([
                'concepts' => $manager->findBy($params)
            ]
        ),Response::HTTP_OK); 
    }
}
