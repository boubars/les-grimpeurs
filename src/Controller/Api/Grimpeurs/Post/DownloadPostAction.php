<?php

namespace App\Controller\Api\Grimpeurs\Post;

use App\Entity\Grimpeurs\Post;
use App\Service\Utilis\Base64Converter;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/post/{id}/download", name="post_download", methods={"GET"})
 */
class DownloadPostAction extends AbstractController
{
    public function __invoke(Post $post)
    {
        $options = new Options();
        $options->set('defaultFont', 'Nunoto');
        
        $dompdf = new Dompdf($options);
    
       
        $html =$this->renderView('pdf/post/default.html.twig', [
            'post'     => $post
        ]);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        
        $dompdf->stream("grimpeurs-article.pdf", [
            "Attachment" => true
        ]);
    }
}
