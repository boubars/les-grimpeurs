<?php

namespace App\Controller\Api\User;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/user/me", name="user_profil", methods={"GET"})
 */
class GetProfilAction extends AbstractFOSRestController
{
    public function __invoke()
    {
        $user = $this->getUser();
        $organization = null;
        if (in_array('ROLE_ORGANIZATION', $user->getRoles())) {
            $organization = $user->getOrganization();
        }

        $view = $this->view(array(
                    "user"    =>  $user,
                    "organization" => $organization
                ));
        $context = new Context();
        $context->setGroups(['details']);
        $view->setContext($context);
        return $this->handleView($view,Response::HTTP_OK);
    }
}
