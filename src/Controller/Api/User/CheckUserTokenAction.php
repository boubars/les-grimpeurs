<?php

namespace App\Controller\Api\User;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/user/token/check", name="user_token_check", methods={"GET"})
 */
class CheckUserTokenAction extends AbstractFOSRestController
{
    public function __invoke()
    {
        $user = $this->getUser();
        return $this->handleView(
             $this->view(array(
                 "token"    =>  $user ? true : false,
                 "roles"    =>  $user ? $user->getRoles() : false   
             )
         ),Response::HTTP_OK); 
    }
}
