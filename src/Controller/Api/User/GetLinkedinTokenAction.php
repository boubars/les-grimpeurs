<?php

namespace App\Controller\Api\User;

use App\Manager\User\ActivationAccountManager;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/api/linkedinoauth/accessToken", name="api_linkedin_exchange_token", methods={"POST"})
 */
class GetLinkedinTokenAction extends AbstractController
{
    public function __invoke(Request $request)
    {
        $httpClient = HttpClient::create();
        $data =json_decode($request->getContent(), true);

        $token = $this->getToken($data);

        if(!$token){
            return new JsonResponse(
                [
                    'success' => false,
                    'error'   => 'code invalid' 
                ]

                );
        }


        $ch = curl_init('https://api.linkedin.com/v2/me');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $headers = array(
            'Authorization: Bearer '. $token,
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $userInfos =  json_decode(curl_exec($ch),true);

        /***
         * get email
         */
        curl_setopt($ch, CURLOPT_URL,"GET https://api.linkedin.com/v2/emailAddress");
        $userInfos['email'] = json_decode(curl_exec($ch),true);
        
        
        curl_close($ch);

        return new JsonResponse(
            $userInfos
        );

     }

     private function getToken($data){
        $ch = curl_init('https://www.linkedin.com/oauth/v2/accessToken');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,sprintf(
            "client_id=%s&client_secret=%s&code=%s&grant_type=%s&redirect_uri=%s",
            $data['client_id'],
            $data['client_secret'],
            $data['code'],
            $data['grant_type'],
            $data['redirect_uri']
        ));
        $response = json_decode(curl_exec($ch),true);
        curl_close($ch);
        return $response['access_token'] ?? false;
     }
}
