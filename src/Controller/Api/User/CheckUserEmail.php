<?php

namespace App\Controller\Api\User;

use App\Repository\UserRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/user/email/check", name="user_email_check", methods={"POST"})
 */
class CheckUserEmail extends AbstractFOSRestController
{
    public function __invoke(Request $request, UserRepository $manager)
    {
        $request = json_decode($request->getContent(), true);
        $email = $request['email'];

        $exist = $email ? ($manager->findOneByEmail($email) ? true : false) : false ;
        
        return $this->handleView(
             $this->view(array(
                 "exist"    =>  $exist
             )
         ),Response::HTTP_OK); 
    }
}
