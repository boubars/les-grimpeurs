<?php

namespace App\Controller\Api\User;

use App\Entity\Scholarship\Scholarship;
use App\Entity\User\FavoriteScholarship;
use App\Entity\User\User;
use App\Factory\User\FavoriteScholarshipFactory;
use App\Repository\Scholarship\ScholarshipRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FavoriteScholarshipAction extends AbstractFOSRestController
{
    private $scholarshipManager, $entityManager, $favoriteScholarshipFactory;

    public function __construct(EntityManagerInterface $entityManager, FavoriteScholarshipFactory $factory)
    {
      $this->entityManager      = $entityManager;
      $this->scholarshipManager = $entityManager->getRepository(Scholarship::class);
      $this->favoriteScholarshipFactory = $factory;
    }
    
    /**
    * @Route("/api/user/favoritescholarship/list", methods={"GET"})
    */
    public function index(ScholarshipRepository $scholarshipManager){

      $user = $this->getUser();


      $data = $scholarshipManager->getFoviteByUser($user);

      return $this->handleView(
        $this->view([ "scholarships" => $data ? $data : false ]),
        Response::HTTP_OK
      );
    }

    /**
    * @Route("/api/user/add/favoritescholarship", methods={"POST"})
    */
    public function add(Request $request){
      /** @var User $user */
      $user = $this->getUser();
      
      $scholarship = $request->request->get('scholarship');
      /** @var Scholarship $scholarship */
      $scholarship = $scholarship ? $this->scholarshipManager->find($scholarship) : false;

      if(!$scholarship){
        return $this->handleView(
          $this->view(array(
              "error"    =>  "scholarship_not_found"
              )
          ),Response::HTTP_NO_CONTENT); 
      }
      //try {
        
        $user->addFavoriteScholarship(
          $this->favoriteScholarshipFactory::create($scholarship)
        );
        $this->entityManager->persist($user);
        $this->entityManager->flush();
        /*
      } catch (Exception $e) {
        return $this->handleView(
          $this->view(array(
              "error"    =>  "internal_error"
              )
          ),Response::HTTP_INTERNAL_SERVER_ERROR); 
      }*/

      return $this->handleView(
        $this->view(array(
            "success"    =>  true
            )
        ),Response::HTTP_OK);
    }

    /**
    * @Route("/api/user/remove/favoritescholarship", methods={"POST"})
    */
    public function remove(Request $request){
      
      $user = $this->getUser();
      
      $scholarship = $request->request->get('scholarship');
     
      $user->removeFavoriteScholarship($scholarship);

      $this->entityManager->persist($user);
      $this->entityManager->flush();
      
      return $this->handleView(
      $this->view(array(
          "success"    =>  true
          )
      ),Response::HTTP_OK);


    }

    /**
    * @Route("/api/user/is/favoritescholarship", methods={"POST"})
    */
    public function isFavoriteScholarship(Request $request){
      /** @var User $user */
      $user = $this->getUser();
      if(!$user){
        return $this->handleView(
          $this->view(array(
              "error"    =>  "user_not_found"
              )
          ),Response::HTTP_NO_CONTENT); 
      }
      
      $scholarship = $request->request->get('scholarship');
      
      $favorite = $scholarship ? $user->favoriteScholarshipsContain($scholarship) : false;  

      return $this->handleView(
        $this->view(array(
            "isFavorite"    =>  $favorite
            )
      ),Response::HTTP_OK);
    }
   
}
