<?php

namespace App\Controller\Api\User;

use App\Entity\Student\Student;
use App\Entity\User\User;
use App\Manager\User\ActivationAccountManager;
use App\Repository\UserRepository;
use App\Service\Mailer\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\AbstractFOSRestController;



class ActivationAccountAction extends AbstractFOSRestController
{

  private $manager ;

  public function __construct(ActivationAccountManager $manager)  
  {
    $this->manager = $manager;
  }

  /**
  * @Route("/api/confirmation/{token}", name="user_activation_account", methods={"GET"})
  */
  public function activationByToken($token)
  {

      $user = $this->manager->handle($token);
      
        if(!$user)
        return $this->redirect("/fr/403");
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token)); 
        return  $this->redirect('/user/activation/successfull');
    }

  /**
  * @Route("/api/activation/code/resend", name="user_activation_account_resend", methods={"POST"})
  */

  public function resendCode(Request $request, EntityManagerInterface $em, Mailer $mailer){
    $user = $em->getRepository(User::class)->findOneBy([
      'email'   => $request->request->get('email'),
      'enable' => false
    ]);

    if (!$user) {
      return $this->handleView(
        $this->view(array(
            "success"    =>  false
        )
      ),Response::HTTP_NOT_FOUND); 
    }

    $user->setCodeActivate(strval(rand(111111,999999)));
    $now = new \DateTime() ; 
    $now->add(new \DateInterval('PT20M'));
    $user->setDateExpired($now);
    
    $em->persist($user);
    $em->flush();
    $mailer->sendUserActivationCode($user);
    
    return $this->handleView(
      $this->view(array(
          "success"    =>  true
      )
    ),Response::HTTP_OK); 
  }

  /**
  * @Route("/api/activation/link/resend", name="user_activation_link_resend", methods={"POST"})
  */

  public function resendActivationLink(Request $request, EntityManagerInterface $em, Mailer $mailer){
    $user = $em->getRepository(User::class)->findOneBy([
      'email'   => $request->request->get('email'),
      'enable' => false
    ]);

    if (!$user) {
      return $this->handleView(
        $this->view(array(
            "success"    =>  false
        )
      ),Response::HTTP_NOT_FOUND); 
    }

    try{
      
      $mailer->sendUserActivationLink($user);

    }catch(Exception $e){
      return $this->handleView(
        $this->view(array(
            "success"    =>  false
        )
      ),Response::HTTP_BAD_REQUEST); 
    }
    
    return $this->handleView(
      $this->view(array(
          "success"    =>  true
      )
    ),Response::HTTP_OK); 
  }

  /**
  * @Route("/api/confirmationbycode", name="user_activation_account_by_code", methods={"POST"})
  */
  public function activationByCode(Request $request)
    {

        $user = $this->manager->activationByCode($request);
        if(!$user) {
          return $this->handleView(
            $this->view(array(
                "success"    =>  false
            )
          ),Response::HTTP_NOT_FOUND); 
        }else{
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token)); 
        return $this->handleView(
          $this->view(array(
              "success"    =>  true,
              "user" => $user
          )
        ),Response::HTTP_OK); 
      }
     }



}