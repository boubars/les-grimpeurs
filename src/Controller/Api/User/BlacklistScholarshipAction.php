<?php

namespace App\Controller\Api\User;

use App\Entity\Scholarship\Scholarship;
use App\Entity\User\User;
use App\Repository\Scholarship\ScholarshipRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlacklistScholarshipAction extends AbstractFOSRestController
{
    private $scholarshipManager, $entityManager;

    public function __construct( ScholarshipRepository $scholarshipManager, EntityManagerInterface $entityManager)
    {
      $this->scholarshipManager = $scholarshipManager;
      $this->entityManager      = $entityManager;
    }
    
    /**
    * @Route("/api/user/blacklistscholarship/list", methods={"GET"})
    */
    public function index(){
      /** @var User */
      $user = $this->getUser();
      $data = $user->getBlacklistScholarship();

      return $this->handleView(
        $this->view([ "scholarships"    =>  $data ? $data : false ]),
        Response::HTTP_OK
      );
    }

    /**
    * @Route("/api/user/add/blacklistscholarship", methods={"POST"})
    */
    public function add(Request $request){
      /** @var User $user */
      $user = $this->getUser();
      
      $scholarship = $request->request->get('scholarship');
      /** @var Scholarship $scholarship */
      $scholarship = $scholarship ? $this->scholarshipManager->find($scholarship) : false;

      if(!$scholarship){
        return $this->handleView(
          $this->view(array(
              "error"    =>  "scholarship_not_found"
              )
          ),Response::HTTP_NO_CONTENT); 
      }
      try {
        $user->addBlacklistScholarship($scholarship);
        $this->entityManager->persist($user);
        $this->entityManager->flush();
      } catch (Exception $e) {
        return $this->handleView(
          $this->view(array(
              "error"    =>  "internal_error"
              )
          ),Response::HTTP_INTERNAL_SERVER_ERROR); 
      }
      return $this->handleView(
      $this->view(array(
          "success"    =>  true
          )
      ),Response::HTTP_OK);
    }

    /**
    * @Route("/api/user/remove/blacklistscholarship", methods={"POST"})
    */
    public function remove(Request $request){
      
      $user = $this->getUser();
      
      $scholarship = $request->request->get('scholarship');
      /** @var Scholarship|boolean $scholarship */
      $scholarship = $scholarship ? $this->scholarshipManager->find($scholarship) : false;

      if(!$scholarship){
        return $this->handleView(
          $this->view(array(
              "error"    =>  "scholarship_not_found"
              )
          ),Response::HTTP_NO_CONTENT); 
      }
      try {
        $user->removeBlacklistScholarship($scholarship);
        $this->entityManager->persist($user);
        $this->entityManager->flush();
      } catch (Exception $e) {
        return $this->handleView(
          $this->view(array(
              "error"    =>  "internal_error"
              )
          ),Response::HTTP_INTERNAL_SERVER_ERROR); 
      }
      return $this->handleView(
      $this->view(array(
          "success"    =>  true
          )
      ),Response::HTTP_OK);
    }

    /**
    * @Route("/api/user/is/blacklistscholarship", methods={"POST"})
    */
    public function isBlacklistScholarship(Request $request){
      /** @var User $user */
      $user = $this->getUser();
      if(!$user){
        return $this->handleView(
          $this->view(array(
              "error"    =>  "user_not_found"
              )
          ),Response::HTTP_NO_CONTENT); 
      }
      
      $scholarship = $request->request->get('scholarship');
      /** @var Scholarship $scholarship */
      $scholarship = $scholarship ? $this->scholarshipManager->findOneById($scholarship) : false;

      if(!$scholarship){
        return $this->handleView(
          $this->view(array(
              "error"    =>  "scholarship_not_found"
              )
          ),Response::HTTP_NO_CONTENT); 
      }


      $myBlacklist = $user->getBlacklistScholarship();
      if(!$myBlacklist){
        return $this->handleView(
          $this->view(array(
              "isBlacklist"    =>  false
              )
        ),Response::HTTP_OK);
      }
        if($myBlacklist->contains($scholarship)){
          return $this->handleView(
            $this->view(array(
                "isBlacklist"    =>  true
                )
          ),Response::HTTP_OK);
        }

      return $this->handleView(
        $this->view(array(
            "isBlacklist"    =>  false
            )
      ),Response::HTTP_OK);
    }
   
}
