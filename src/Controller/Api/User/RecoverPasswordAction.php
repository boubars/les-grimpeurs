<?php

namespace App\Controller\Api\User;

use App\Repository\UserRepository;
use App\Service\Mailer\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Service\Utilis\TokenGenerator;
use App\Manager\User\ActivationAccountManager;
use App\Manager\Student\RegistrationManager;

class RecoverPasswordAction extends AbstractFOSRestController
{

  private $entityManager;
  private $mailer;
  private $managerActvation;

  public function __construct(EntityManagerInterface $manager , Mailer $mailer , ActivationAccountManager $managerActvation){
    $this->entityManager = $manager;
    $this->managerActvation  = $managerActvation;
    $this->mailer = $mailer;

  }

   
    
    /**
     * @Route("/api/recover/password", name="user_recover_password_request", methods={"POST"})
     */
    public function Request(Request $request, UserRepository $userManager)
    {
        $email = $request->request->get("email");
        /**
         * @var User
         */
        $user = $userManager->findOneByEmail($email);
        if(!$user){ //mail not exist
          return $this->handleView(
            $this->view(array(
                "error_code"    =>  "form.mail_not_exist"
            )
          ),Response::HTTP_OK); 
        }else {
         
          if(!$user->isEnable()){ //compte desactivé
            if(!in_array('ROLE_STUDENT',$user->getRoles())){
              $this->managerActvation->sendLinkActivate($user);
            }else{
               $this->managerActvation->sendCodeActivate($user);
            }
        
            return $this->handleView(
              $this->view(array(
                  "success" => false,
                  "error_code"    =>  "form.account_disabled",
                  "authentification" => in_array('ROLE_STUDENT',$user->getRoles())
              )
          ),Response::HTTP_OK); 
        }
      }
        $user->setToken(TokenGenerator::generate(15));
        $this->mailer->sendUserRecoveryLink($user);
        $this->entityManager->persist($user);
        $this->entityManager->flush();
        return $this->handleView(
          $this->view(array(
              "success"    =>  true
          )
        ),Response::HTTP_OK); 
    }

    /**
     * @Route("/api/recover/password/{token}", name="user_recover_password_accept", methods={"GET"})
     */
    public function Accept($token, UserRepository $userManager)
    {
        
        $user = $userManager->findOneByToken($token);

        if(!$user){
          return $this->handleView(
            $this->view(array(
                "error_code"    =>  "form.mail_not_exist"
            )
          ),Response::HTTP_OK); 
        }
        
     
          $user->setToken("");
          $this->entityManager->persist($user);
          $this->entityManager->flush();
          $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
          $this->get('security.token_storage')->setToken($token);
          $this->get('session')->set('_security_main', serialize($token));
        
        return  $this->redirect('/user/change/password');
    }
    /**
     * @Route("/api/update/password", name="user_update_password", methods={"POST"})
     */
    public function Update(Request $request, UserPasswordEncoderInterface $encoder , RegistrationManager $manager)
    {
        
        $user = $this->getUser();

        if(!$user){
          return $this->handleView(
            $this->view(array(
                "error"    =>  "user_not_found"
            )
          ),Response::HTTP_OK); 
        }
       
        $newPassword = $request->request->get("password");

        if(!$manager->validPassword($request->request->get("password"))) {
          return $this->handleView(
            $this->view(array(
                "error"    =>  "word.form.password_invalid"
            )
          ),Response::HTTP_OK); 
        }
        $user->setPassword($encoder->encodePassword($user, $newPassword));

        
        $this->entityManager->persist($user);
        $this->entityManager->flush();


        return $this->handleView(
          $this->view(array(
              "success"    =>  true
          )
        ),Response::HTTP_OK); 
      
        
    }


  
}