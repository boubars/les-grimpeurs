<?php

namespace App\Controller\Api\Bot\Message;

use App\Entity\Messaging\Bot\Conversation;
use App\Entity\Messaging\Bot\Message;
use App\Entity\Messaging\Bot\Participant;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/bot/message/new", methods={"POST"})
 */
class NewMessageAction extends AbstractFOSRestController
{
    public function __invoke(Request $request, EntityManagerInterface $em)
    {
        
        /** @var ParticipantRepository */
        $pManager    = $em->getRepository(Participant::class);
        $participant = $request->request->get('owner');
        $participant = $pManager->findOneByUid($participant);
        if(!$participant){
            return $this->getBadrequestResponse('participant invalid');  
        }

        /** @var ConversationRepository */
        $cManager     = $em->getRepository(Conversation::class);
        $conversation = $request->request->get('conversation');
        $conversation = $cManager->findOneById($conversation);
        if(!$conversation){
            return $this->getBadrequestResponse('invalid posted conversation');  
        }

        $mContent     = $request->request->get('content');
        if(!$mContent){
            return $this->getBadrequestResponse('field content required');  
        }
        
        $oMessage = (new Message())
        ->setOwner($participant)
        ->setConversation($conversation)
        ->setContent($mContent);

        $em->persist($oMessage);
        $em->flush();

        return $this->handleView(
             $this->view(array(
                'success' => true,
                'message' => $oMessage
             )
         ),Response::HTTP_OK); 
    }

    private function getBadrequestResponse($error){
        return $this->handleView(
            $this->view(array(
               'success'      => false,
               'error' => $error
            )
        ),Response::HTTP_BAD_REQUEST); 
    }
}
