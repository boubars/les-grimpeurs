<?php

namespace App\Controller\Api\Bot\Conversation;


use App\Repository\Messaging\Bot\ConversationRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class GetConversationAction extends AbstractFOSRestController
{
    /**
     * @Route("/api/bot/conversation", methods={"GET"})
     */
    public function viewConversation(Request $request, ConversationRepository $manager)
    {
        
        $conversation = null;

        $uid = $request->query->get('uid');
        if($uid){
            $conversation = $manager->findByUid($uid);
        }

        $cId = $request->query->get('id');
        if($cId){
            $conversation = $manager->findById($cId);
        }

        
        return $this->handleView(
             $this->view(array(
                'success'      => true,
                'conversation' => $conversation
             )
         ),Response::HTTP_OK); 
    }

    /**
     * Admin only acces for listing all conversation
     *
     * @Route("/api/bot/conversation/all", methods={"GET"})
     * @isGranted("ROLE_ADMIN")
     */
    public function getAllConversation(ConversationRepository $manager){
        return $this->handleView(
            $this->view(array(
               'success'      => true,
               'conversations' => $manager->findAll()
            )
        ),Response::HTTP_OK); 
    }
}
