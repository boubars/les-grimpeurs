<?php

namespace App\Controller\Api\Bot\Conversation;

use App\Entity\Messaging\Bot\Conversation;
use App\Entity\Messaging\Bot\Message;
use App\Entity\Messaging\Bot\Participant;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/bot/conversation/new", methods={"POST"})
 */
class CreateConversationAction extends AbstractFOSRestController
{
    public function __invoke(Request $request, EntityManagerInterface $entityManager)
    {
        $aParticipant = $request->request->get('participant');
        $oParticipant = (new Participant())


        ->setPseudo($aParticipant['pseudo'] ?? 'Guest')
        ->setEmail($aParticipant['email'] ?? 'guest@email.com')
        ->setUid($aParticipant['uid']);

        $conversation = (new Conversation())
        ->setSlug(uniqid('grimpeurs-bot-conversation-'))
        ->addParticipant($oParticipant);

        $aMessage = $request->request->get('message');
        if($aMessage){
            $oMessage = (new Message())
            ->setContent($aMessage['content'])  
            ->setOwner($oParticipant);
            $conversation->addMessage($oMessage);
        }
        

        $entityManager->persist($conversation);
        $entityManager->flush();

        return $this->handleView(
             $this->view(array(
                'success'      => true,
                'conversation' => $conversation
             )
         ),Response::HTTP_OK); 
    }
}
