<?php

namespace App\Controller\Api\Bot\Conversation;

use App\Entity\Messaging\Bot\Conversation;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/api/bot/conversation/{id}/delete", methods={"DELETE"})
 * @isGranted("ROLE_ADMIN")
 * 
 */
class DeleteConversationAction extends AbstractFOSRestController
{
    public function __invoke(Conversation $conversation, EntityManagerInterface $entityManager)
    {
        //$conversation->setDeleted(true);
        $entityManager->remove($conversation);
        $entityManager->flush();
        return $this->handleView(
             $this->view(array(
                'success'      => true
             )
         ),Response::HTTP_OK); 
    }

}
