<?php

namespace App\Controller\Api\Bot\Participant;

use App\Entity\Messaging\Bot\Participant;
use App\Repository\Messaging\Bot\ConversationRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class GetAdminUserAction extends AbstractFOSRestController
{
    /**
     * Admin only acces for generating participant data
     *
     * @Route("/api/bot/participant/admin", methods={"GET"})
     * @isGranted("ROLE_ADMIN")
     */
    public function getAllConversation(EntityManagerInterface $em){
        
        /** @var ParticipantRepository */
        $pManager     = $em->getRepository(Participant::class);
        $oUser        = $this->getUser();
        $oParticipant = $pManager->findOneByUser($oUser);

        if(!$oParticipant){
            $oParticipant = (new Participant())
            ->setPseudo($oUser->getFirstname(). ' '.$oUser->getlastname() )
            ->setUid(uniqid('grimpeurs-bot-admi-user--'))
            ->setUser($oUser);

            $em->persist($oParticipant);
            $em->flush();
        }
        return $this->handleView(
            $this->view(array(
               'success'      => true,
               'participant' => $oParticipant
            )
        ),Response::HTTP_OK); 
    }
}
