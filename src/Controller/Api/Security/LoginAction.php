<?php
namespace App\Controller\Api\Security;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/login", name="login", methods={"POST"})
 */
class LoginAction extends AbstractFOSRestController
{
    public function __invoke()
    {
        $user = $this->getUser();
        return $this->handleView(
             $this->view(array(
                 "user"    =>  $user
             )
         ),Response::HTTP_OK); 
    }
}
