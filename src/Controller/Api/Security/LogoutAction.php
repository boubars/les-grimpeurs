<?php
namespace App\Controller\Api\Security;

// ...
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * @Route("/api/logout", name="logout")
 */
class LogoutAction extends AbstractController
{
    public function __invoke(Request $request)
    {
        /**
         * this action will never be executed, check security.yaml file
         */
    }
}
