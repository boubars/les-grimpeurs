<?php

namespace App\Controller\Api\University;

use App\Repository\University\UniversityRepository;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/university/list", methods={"GET"})
 */
class GetUniversityListAction extends AbstractFOSRestController
{
    public function __invoke(UniversityRepository $manager)
    {
      
        $context = new Context();
        $context->setGroups(['list']);

        $view = $this->view(array(
            "list"    =>  $manager->getList()
        ));
        
        $view->setContext($context);
        return $this->handleView($view);
        return $this->handleView($view ,Response::HTTP_OK); 
    }
}
