<?php

namespace App\Controller\Api\Todo;

use App\Entity\Todo\Todo;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/user/todo/add", methods={"POST"})
 */
class AddAction extends AbstractFOSRestController
{
    public function __invoke(Request $request, EntityManagerInterface $entityManager)
    {      
      $user = $this->getUser();
      $data =json_decode($request->getContent(), true);
     

      /*** check if not an existing todo */
      $todoManager = $entityManager->getRepository(Todo::class);
      $exist = $todoManager->findOneBy([
        'name'     => $data['name'],
        'fullDate' => new \DateTime($data['full_date'])
      ]);

      if($exist){
        $view = $this->view(array(
          "success" => false,
          "error"   => 'duplicate_content'
        ));
        return $this->handleView($view ,Response::HTTP_OK); 
      }

      $todo = new Todo();
      $todo->setOwner($user)
      ->setName($data['name'])
      ->setFullDate(new \DateTime($data['full_date']))
      ->setFullHour($data['full_hour'])
      ->setAlertOn($data['alert']['on'])
      ->setAlertDuration($data['alert']['duration'])
      ->setAdditionalInfos([$data['additional_infos']]);

      $entityManager->persist($todo);
      $entityManager->flush();

      $view = $this->view(array(
          "success" => true,
          "id"      => $todo->getId()
      ));
      return $this->handleView($view ,Response::HTTP_OK); 
    }
}
