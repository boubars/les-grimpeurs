<?php

namespace App\Controller\Api\Todo;

use App\Entity\Todo\Todo;
use App\Repository\Todo\TodoRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/user/todo/edit", methods={"POST"})
 */
class EditAction extends AbstractFOSRestController
{
    public function __invoke(Request $request, EntityManagerInterface $entityManager)
    {      
      $user = $this->getUser();
      $data =json_decode($request->getContent(), true);
      
      /**
       * @var TodoRepository
       */
      $totoManager = $entityManager->getRepository(Todo::class);
      $todo = $totoManager->findOneById($data['id']);

      $todo->setOwner($user)
      ->setName($data['name'])
      ->setFullDate(new \DateTime($data['full_date']))
      ->setFullHour($data['full_hour'])
      ->setAlertOn($data['alert']['on'])
      ->setAlertDuration($data['alert']['duration'])
      ->setAdditionalInfos([$data['additional_infos']]);

      $entityManager->persist($todo);
      $entityManager->flush();
      
      $view = $this->view(array(
          "success" => true
      ));
      return $this->handleView($view ,Response::HTTP_OK); 
    }
}
