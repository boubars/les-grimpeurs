<?php

namespace App\Controller\Api\Todo;

use App\Entity\Todo\Todo;
use App\Repository\Todo\TodoRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/user/todo/remove", methods={"POST"})
 */
class RemoveAction extends AbstractFOSRestController
{
    public function __invoke(Request $request, EntityManagerInterface $entityManager)
    {      
      $user = $this->getUser();
      $data =json_decode($request->getContent(), true);
      /**
       * @var TodoRepository
       */
      $totoManager = $entityManager->getRepository(Todo::class);

      /** check owner */
      $todo = $totoManager->findOneBy([
         'id'    => $data['id'],
         'owner' => $user
      ]);
      
      if($todo){
        $entityManager->remove($todo);
        $entityManager->flush();
        $view = $this->view(array(
            "success" => true
        ));
        return $this->handleView($view ,Response::HTTP_OK); 
      }
      $view = $this->view(array(
          "success" => false,
          "error"   => 'permission_denied'
      ));
      return $this->handleView($view ,Response::HTTP_OK); 
    }
}
