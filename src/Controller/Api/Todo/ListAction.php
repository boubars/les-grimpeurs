<?php

namespace App\Controller\Api\Todo;


use App\Entity\User\User;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/user/todo/list", methods={"GET"})
 */
class ListAction extends AbstractFOSRestController
{
    public function __invoke()
    {      
      /**
       * @var User
       */
      $user = $this->getUser();
      $view = $this->view(array(
          "list" => $user->getTodoList()
      ));
      return $this->handleView($view ,Response::HTTP_OK); 
    }
}
