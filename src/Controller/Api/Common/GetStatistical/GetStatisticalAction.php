<?php

namespace App\Controller\Api\Common\GetStatistical;

use App\Entity\Common\Statistical;
use App\Repository\Common\StatisticalRepository;
use App\Repository\Discipline\DisciplineRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/statistical", name="count", methods={"GET"})
 */
class GetStatisticalAction extends AbstractFOSRestController
{
    public function __invoke(StatisticalRepository $static)
    {

        return $this->handleView(
             $this->view(array(
                 "scholarships"       =>  $static->countScholarship() ,
                 "foundations"        =>  $static->countFoundation(),
                 "scholarshipsTotal"  =>  $static->countTotalScholarship(),
                 "candidates"         =>  $static->countCandidate(),
                 "contry"             =>  $static->countCountry(),
                 "etablishement"      =>  $static->countEtablishement(),
                 "registered"         =>  $static->countRegister(),
                 "formation"          =>  $static->countFormation(),
                 "scholarshipsTotal"  => $static->countTotalScholarship()
             )
         ),Response::HTTP_OK); 
    }
}
