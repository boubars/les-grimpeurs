<?php

namespace App\Controller\Api\Common\Country;

use App\Entity\Student\Student;
use App\Entity\User\User;
use App\Repository\Common\CountryRepository;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/country/userList", name="api_country_user_list", methods={"GET"})
 */
class GetUserListAction extends AbstractFOSRestController
{
    public function __invoke(Request $request, CountryRepository $manager)
    {
        /**
         * @var array returned result
         */
        $data = [] ; 

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        if($currentUser instanceof Student){
            $data = $currentUser->getDesiredCountries();
        }

        
        $context = new Context();
        $context->setGroups(['list']);

         $view = $this->view(array(
              "countries"    =>  $data
         ));
         return $this->handleView($view,Response::HTTP_OK);
    }
}
