<?php

namespace App\Controller\Api\Common\Country;

use App\Entity\Common\Country;
use App\Repository\Common\CountryRepository;
use App\Repository\Scholarship\ScholarshipRepository;
use App\Repository\Student\StudentRepository;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/country/{id}/stat", name="api_country_stat", methods={"GET"})
 */
class GetStatAction extends AbstractFOSRestController
{
    public function __invoke(Country $country, CountryRepository $countryManager, StudentRepository $studentManager)
    {
        
        $data = $countryManager->getTotalAndCountSchoalrship($country);
        
        $scholarshipCount   = $data['count'];
        $scholarshipAmount  = $data['total'];
        $scholarshipNb      = $data['nb'];
        $scholarshipAmountUnit  = $data['totalUnit'];
    
        $students = $studentManager->countByNationalities($country->getId());
        
        $view = $this->view(array(
            "stat"    =>  [
                "scholarshipCount"      => $scholarshipCount,
                "scholarshipNb"         => $scholarshipNb,
                "scholarshipAmount"     => $scholarshipAmount,
                "scholarshipAmountUnit" => $scholarshipAmountUnit,
                "students"          => $students,
                "symbol"            => $country->getSymbol()
            ]
        ));
        return $this->handleView($view,Response::HTTP_OK); 
    }
}
