<?php

namespace App\Controller\Api\Common\Country;

use App\Repository\Common\CountryRepository;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/country/list", name="api_country_list", methods={"GET"})
 */
class GetListAction extends AbstractFOSRestController
{
    public function __invoke(Request $request, CountryRepository $manager)
    {
        /**
         * @var array returned result
         */
        $data = [] ; 

        /**
         * @var array Posted continents IDs
         */
        $continents = $request->query->get('continents') ?? false;
        if($continents){
            $data = $manager->getByContinents($continents);
        }else{
           $data = $manager->findAll();
        }
        
      $context = new Context();
      $context->setGroups(['list']);

      $view = $this->view(array(
          "countries"    =>  $data
      ));
      return $this->handleView($view,Response::HTTP_OK); 
    }
}
