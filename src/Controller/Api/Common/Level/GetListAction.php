<?php

namespace App\Controller\Api\Common\Level;

use App\Repository\Common\LevelRepository;
use App\Repository\Discipline\DisciplineRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/level/list", name="level_list", methods={"GET"})
 */
class GetListAction extends AbstractFOSRestController
{
    public function __invoke(LevelRepository $manager)
    {
        return $this->handleView(
             $this->view(array(
                 "levels"    =>  $manager->findAll()
             )
         ),Response::HTTP_OK); 
    }
}
