<?php

namespace App\Controller\Api\Common\SocialNetwork;

use App\Entity\Organization\Organization;
use App\Entity\Student\Student;
use App\Repository\Organization\OrganizationRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/social-network/infos", name="social_network_infos", methods={"GET"})
 */
class GetInfoAction extends AbstractFOSRestController
{
    public function __invoke(Request $request, OrganizationRepository $organizationManager)
    {
       
        /**
         * @var Integer Organization ID
         */
        $organization = $request->query->get('organization'); 
        
        $organization = $organization ? $organizationManager->findOneById($organization) : false;

        $data = $organization ? $organization->getSocialNetwork() : null;

        return $this->handleView(
             $this->view(array(
                 "socialNetwork" => $data 
             )
         ),Response::HTTP_OK); 
    }
}
