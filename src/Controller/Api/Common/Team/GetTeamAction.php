<?php

namespace App\Controller\Api\Common\Team;

use App\Repository\Grimpeurs\TeamRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/team/list", name="api_team_list", methods={"GET"})
 */
class GetTeamAction extends AbstractFOSRestController
{
    public function __invoke(TeamRepository $manager)
    {
        return $this->handleView(
             $this->view(array(
                 "teams"    =>  $manager->findAll()
             )
         ),Response::HTTP_OK); 
    }
}
