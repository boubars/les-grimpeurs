<?php

namespace App\Controller\Api\Common\Distinction;

use App\Repository\Student\DisctinctionRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Context\Context;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/distinction/list", methods={"GET"})
 */
class GetListAction extends AbstractFOSRestController
{
    public function __invoke(DisctinctionRepository $manager)
    {
        $context = new Context();
        $context->setGroups(['list']);

        $view = $this->view(array(
            "distinctions"    =>  $manager->findAll()
        ));

        $view->setContext($context);
        return $this->handleView($view);
        return $this->handleView($view ,Response::HTTP_OK); 
    }
}
