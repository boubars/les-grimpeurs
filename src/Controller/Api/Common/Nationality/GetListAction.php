<?php

namespace App\Controller\Api\Common\Nationality;

use App\Repository\Common\NationalityRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/nationality/list", name="nationality_list", methods={"GET"})
 */
class GetListAction extends AbstractFOSRestController
{
    public function __invoke(NationalityRepository $manager)
    {
        return $this->handleView(
             $this->view(array(
                 "nationality"    =>  $manager->findAll()
             )
         ),Response::HTTP_OK); 
    }
}
