<?php

namespace App\Controller\Api\Common\Language;

use App\Repository\Common\LanguageRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/language/list", name="language_list", methods={"GET"})
 */
class GetListAction extends AbstractFOSRestController
{
    public function __invoke(LanguageRepository $manager)
    {
        return $this->handleView(
             $this->view(array(
                 "languages"    =>  $manager->findAll()
             )
         ),Response::HTTP_OK); 
    }
}
