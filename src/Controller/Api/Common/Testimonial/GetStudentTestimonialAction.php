<?php

namespace App\Controller\Api\Common\Testimonial;

use App\Repository\Testimonial\StudentTestimonialRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
  * @Route("/api/testimonial/student", name="testimonial_student_list", methods={"GET"})
 */
class GetStudentTestimonialAction extends AbstractFOSRestController
{
    
        public function __invoke(StudentTestimonialRepository $repositiry)
        {
    
            return $this->handleView(
                 $this->view(
                    $repositiry->findAll()
                 
             ),Response::HTTP_OK); 
        }
}