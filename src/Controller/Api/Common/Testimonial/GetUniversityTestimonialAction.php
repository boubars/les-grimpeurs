<?php

namespace App\Controller\Api\Common\Testimonial;

use App\Repository\Testimonial\UniversityTestimonialRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
  * @Route("/api/testimonial/university", name="testimonial_university_list", methods={"GET"})
 */
class GetUniversityTestimonialAction extends AbstractFOSRestController
{
    
        public function __invoke(UniversityTestimonialRepository $repositiry)
        {
    
            return $this->handleView(
                 $this->view(
                    $repositiry->findAll()
                 
             ),Response::HTTP_OK); 
        }
}