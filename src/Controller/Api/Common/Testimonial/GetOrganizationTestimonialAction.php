<?php

namespace App\Controller\Api\Common\Testimonial;

use App\Repository\Testimonial\OrganizationTestimonialRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
  * @Route("/api/testimonial/organization", name="testimonial_organization_list", methods={"GET"})
 */
class GetOrganizationTestimonialAction extends AbstractFOSRestController
{
    
        public function __invoke(OrganizationTestimonialRepository $repositiry)
        {
    
            return $this->handleView(
                 $this->view( $repositiry->findAll()
                 
             ),Response::HTTP_OK); 
        }
    }