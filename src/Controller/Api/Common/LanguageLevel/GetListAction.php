<?php

namespace App\Controller\Api\Common\LanguageLevel;

use App\Repository\Common\LanguageRepository;
use App\Repository\Student\LanguageLevelRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/language/level/list", name="languagelevel_list", methods={"GET"})
 */
class GetListAction extends AbstractFOSRestController
{
    public function __invoke(LanguageLevelRepository $manager)
    {
        return $this->handleView(
             $this->view(array(
                 "levels"    =>  $manager->findAll()
             )
         ),Response::HTTP_OK); 
    }
}
