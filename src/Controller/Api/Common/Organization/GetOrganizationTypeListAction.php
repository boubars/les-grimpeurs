<?php

namespace App\Controller\Api\Common\Organization;

use App\Repository\Organization\OrganizationTypeRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/organization/type/list", methods={"GET"})
 */
class GetOrganizationTypeListAction extends AbstractFOSRestController
{
    public function __invoke(OrganizationTypeRepository $manager)
    {
        return $this->handleView(
             $this->view(array(
                 "types"    =>  $manager->findAll()
             )
         ),Response::HTTP_OK); 
    }
}
