<?php

namespace App\Controller\Api\Common\Media;

use App\Controller\FileUploaderAction;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGenerator;

/**
 * @Route("/api/media/upload", name="api_media_upload", methods={"POST"})
 */
class UploadMediaAction extends FileUploaderAction
{
    /**
     * Invokable upload file method
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request):Response
    {


        $file = $request->files->get("file");
        

        
        if(!$file){
            
        }
        
        
        $type = $file->getClientMimeType();        
        $directory = str_contains($type,'image') ? 'picture_dir' : 'document_dir'; 

        $filename  = $this->uploadDocument($file, $directory);
        $baseUrl   = $this->generateUrl('home', ['path'=>''], UrlGenerator::ABSOLUTE_URL);
        $url       = $baseUrl."uploads/".($directory == 'picture_dir' ? 'picture' : 'documents')."/$filename";
        return $this->handleView(
             $this->view(array(
                 "success"    =>  true,
                 "file"       => [
                    "name"    => $filename,
                    "type"    => $type,
                    "url"     => $url
                 ]
             )
         ),Response::HTTP_OK); 
    }
}
