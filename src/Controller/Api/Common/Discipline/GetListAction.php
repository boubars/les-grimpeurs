<?php

namespace App\Controller\Api\Common\Discipline;

use App\Entity\Student\Student;
use App\Repository\Discipline\DisciplineRepository;
use App\Repository\Student\StudentRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/discipline/list", name="dicipline_list", methods={"GET"})
 */
class GetListAction extends AbstractFOSRestController
{
    public function __invoke(DisciplineRepository $manager)
    {
        $currentUser = $this->getUser();
        if($currentUser instanceof Student){
            $list = $currentUser->getDesiredDisciplines();
        }
        else {
            $list = $manager->findAll();
        }
        return $this->handleView(
             $this->view(array(
                 "diciplines" => $list
             )
         ),Response::HTTP_OK); 
    }
}
