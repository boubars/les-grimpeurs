<?php

namespace App\Controller\Api\Common\Discipline;

use App\Entity\Discipline\Discipline;
use App\Entity\Discipline\Category;
use App\Entity\Student\Student;
use App\Repository\Discipline\DisciplineRepository;
use App\Repository\Student\StudentRepository;
use App\Service\Lang\Translation;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/discipline/add", name="dicipline_add", methods={"POST"})
 */
class AddAction extends AbstractFOSRestController
{
    private $translator, $ressourceDir;

    public function __construct(Translation $translator, ParameterBagInterface $params)
    {
        $this->translator = $translator;
        $this->ressourceDir = $params->get('project_dir');
    }
    public function __invoke(Request $request, EntityManagerInterface $entityManager)
    {
        $request = json_decode($request->getContent(), true);

        $request = $request['data'];
        $categoryManager = $entityManager->getRepository(Category::class);
        $category = $request['category'];

        $category = $category ? $categoryManager->findOneById($category) : false ;

        if(!$category){
            throw new HttpException(400, "Organization not found.");
        }
      
        $discipline = new Discipline();
        $discipline->setCategory($category)
        ->setName($request['name'])
        ->setNameFr($request['name_fr']);

        $entityManager->persist($discipline);
        $entityManager->flush();
      
        $this->updateTranslation($discipline);

        return $this->handleView(
            $this->view(array(
                "success"    => true,
                "discipline" => $discipline
            )
        ),Response::HTTP_OK); 
    }

    private function updateTranslation($discipline){
        /** get file data */
        $dataFr = ($this->translator->getResource('fr'))['discipline'];
        $dataEn = ($this->translator->getResource('en'))['discipline'];
        
        $dataEn[$discipline->getId()] = $discipline->getName(); 
        $dataFr[$discipline->getId()] = $discipline->getNameFr(); 

        /**generate translation files */
        $fileSystem = new Filesystem();
        $fileSystem->dumpFile($this->ressourceDir."/public/translations/discipline/en.json", json_encode($dataEn, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        $fileSystem->dumpFile($this->ressourceDir."/public/translations/discipline/fr.json", json_encode($dataFr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
      }
}
