<?php

namespace App\Controller\Api\Common\Grade;

use App\Repository\Common\GradeRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/grade/list", name="grade_list", methods={"GET"})
 */
class GetListAction extends AbstractFOSRestController
{
    public function __invoke(GradeRepository $manager)
    {
        return $this->handleView(
             $this->view(array(
                 "grades"    =>  $manager->findAll()
             )
         ),Response::HTTP_OK); 
    }
}
