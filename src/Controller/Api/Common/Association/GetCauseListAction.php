<?php

namespace App\Controller\Api\Common\Association;

use App\Repository\Student\Activity\ActivityCauseRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/association/cause/list", methods={"GET"})
 */
class GetCauseListAction extends AbstractFOSRestController
{
    public function __invoke(ActivityCauseRepository $manager)
    {
        return $this->handleView(
             $this->view(array(
                 "causes"    =>  $manager->findAll()
             )
         ),Response::HTTP_OK); 
    }
}
