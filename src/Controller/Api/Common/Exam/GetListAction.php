<?php

namespace App\Controller\Api\Common\Exam;

use App\Repository\Student\ExamRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Context\Context;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/exam/list", methods={"GET"})
 */
class GetListAction extends AbstractFOSRestController
{
    public function __invoke(ExamRepository $manager)
    {
        $context = new Context();
        $context->setGroups(['list']);

        $view = $this->view(array(
            "exams"    =>  $manager->findAll()
        ));

        $view->setContext($context);
        return $this->handleView($view);
        return $this->handleView($view ,Response::HTTP_OK); 
    }
}
