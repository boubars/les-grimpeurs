<?php

namespace App\Controller\Api\Common\StudyField;

use App\Repository\Discipline\CategoryRepository as StudyFieldRepository;
use App\Repository\Discipline\DisciplineRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GetListAction extends AbstractFOSRestController
{

    private $categoryManager;
    private $disciplineManager;
    
    public function __construct(DisciplineRepository $disciplineManager, StudyFieldRepository $categoryManager)
    {
        $this->disciplineManager = $disciplineManager;
        $this->categoryManager   = $categoryManager;
    }


    /**
    * @Route("/api/studyfield/find", name="study_field_search", methods={"GET"})
    */
    public function findBy(Request $request){
        $context = $request->query->get("context");
        $data    = [] ;
        switch (strtoupper($context)) {
            case 'ORGANIZATION':
                    $data = $this->findByOrganization($request);
                break;
            
            default:
                # code...
                break;
        }

        return $this->handleView(
            $this->view(array(
                "studyFields"    =>  $data
            )
        ),Response::HTTP_OK); 


    }
    private function findByOrganization(Request $request){
        $organizations = $request->query->get('organizations', [$this->getUser()->getId()]); 
        return $this->categoryManager->getByOrganizations($organizations); 
        
    }

    /**
     * @Route("/api/studyfield/list", name="study_field_list", methods={"GET"})
     */
    public function index(Request $request)
    {
        $context      = $request->query->get("context") ?? "category";
        $categoryIds  = $request->query->get("category") ?? false;

        $data = [];

        if($context === "category"){
            $data = $this->categoryManager->findBy(array(), array('name' => 'ASC'));
        }else{
            if($categoryIds){
                $data = !empty($categoryIds && $categoryIds[0] != 0 ) ? $this->disciplineManager->getByCateIds($categoryIds) : $this->disciplineManager->findBy(array(), array('name' => 'ASC'));
            }
        }

        return $this->handleView(
             $this->view(array(
                 "studyFields"    =>  $data
             )
         ),Response::HTTP_OK); 
    }
}
