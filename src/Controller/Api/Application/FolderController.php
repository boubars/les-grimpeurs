<?php

namespace App\Controller\Api\Application;

use App\Entity\Application\Folder;
use App\Entity\Application\Form;
use App\Entity\Application\Sheet;
use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class FolderController extends AbstractFOSRestController
{
    
    /**
     * Global entity manager interface variable
     *
     * @var [type]
     */
    private $em;
    
    /**
     * Global slugger interface variable
     *
     * @var [SluggerInterface]
     */
    private $slugger;

    /**
     * Class constructor
     *
     * @param EntityManagerInterface $entityManager
     * @param SluggerInterface $slugger
     */
    public function __construct( EntityManagerInterface $entityManager, SluggerInterface $slugger) {
        $this->em      = $entityManager;
        $this->slugger = $slugger;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     *
     * @Route("/api/folder/add", name="application_folder_add", methods={"POST"})
     */
    public function addFolder(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $user = $this->getUser();
        if (false == $user instanceof User ) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Not connected"
            ], 403);
        }
        if (is_null($request)) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Bad request"
            ], Response::HTTP_BAD_REQUEST);
        }

        $oFolder = new Folder();
        $oFolder->setName($request['name']);

        $slug = uniqid($this->slugger->slug(strtolower($request['name']))."-");
        $oFolder->setSlug($slug);
        $oFolder->setDateCreation(new \DateTime());
        $oFolder->setOrganisation($user);
        $this->em->persist($oFolder);
        $this->em->flush();

        return new JsonResponse([
            'success' => true,
            'folder_id'    => $oFolder->getId(),
            'slug'         => $oFolder->getSlug()
        ], 200);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     *
     * @Route("/api/folder/{id}/edit", name="application_folder_edit", methods={"POST"})
     */
    public function editFolder(Request $request, Folder $oFolder)
    {
        $request = json_decode($request->getContent(), true);
        $user = $this->getUser();
        if (false == $user instanceof User ) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Not connected"
            ], 403);
        }
        if (is_null($request)) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Bad request"
            ], Response::HTTP_BAD_REQUEST);
        }

        if ($request['name']) {
            $oFolder->setName($request['name']);

            $slug = uniqid($this->slugger->slug(strtolower($request['name']))."-");
            $oFolder->setSlug($slug);

            $this->em->persist($oFolder);
            $this->em->flush();
        }

        return new JsonResponse([
            'success' => true,
            'folder_id'    => $oFolder->getId(),
        ], 200);

    }

    /**
     * @param Request $request
     * @param Folder $oFolder
     * @return JsonResponse
     * @throws \Exception
     *
     * @Route("/api/folder/{id}/show", name="application_folder_show", methods={"GET"})
     */
    public function showFolder(Request $request, Folder $oFolder)
    {
        $user = $this->getUser();
        if (false == $user instanceof User ) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Not connected"
            ], Response::HTTP_FORBIDDEN);
        }

        $view = $this->view(array(
            "folder"    =>  $oFolder
        ), null, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);

        $context = new Context();
        $context->setGroups(['details']);
        $view->setContext($context);

        return $this->handleView( $view, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return JsonResponse|Response
     *
     * @Route("/api/folder/list", name="application_folder_list", methods={"GET"})
     */
    public function listFolder(Request $request)
    {
       $user = $this->getUser();
        if (false == $user instanceof User ) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Not connected"
            ], Response::HTTP_FORBIDDEN);
        }
        $result = $user->getFolders();

        $context = new Context();
        $context->setGroups(['list']);
        $view = $this->view(array(
            "folders"    =>  $result
        ), null, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);
        $view->setContext($context);

       return $this->handleView( $view, Response::HTTP_OK);

    }

    /**
     * @param Request $request
     * @param Folder $oFolder
     * @return JsonResponse
     * @throws \Exception
     *
     * @Route("/api/folder/{id}/delete", name="application_folder_delete", methods={"GET"})
     */
    public function removeFolder(Request $request, Folder $oFolder)
    {
        $user = $this->getUser();
        if (false == $user instanceof User) {
            return new JsonResponse([
                'success' => false,
                'message' => "Not connected"
            ], Response::HTTP_FORBIDDEN);
        }
        if ($user != $oFolder->getOrganisation()) {
            return new JsonResponse([
                'success' => false,
                'message' => "Unauthorized"
            ], Response::HTTP_FORBIDDEN);
        }

        $this->em->remove($oFolder);
        $this->em->flush();
        return new JsonResponse([
            'success' => true,
            'message' => "Folder removed",
        ], 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse|Response
     *
     * @Route("/api/folder/find", name="application_folder_find", methods={"GET"})
     */
    public function findFolder(Request $request)
    {
       $user = $this->getUser();
        if (false == $user instanceof User ) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Not connected"
            ], Response::HTTP_FORBIDDEN);
        }

        $criteria = $request->query->all();
        $max = $criteria['max'] ?? false;

        $result = [];
        
        if ($max == 1) {
            unset($criteria['max']);
            $result = ["folder"  => $this->em->getRepository(Folder::class)->findOneBy($criteria)];
        }else {
            $result = ["folders" => $this->em->getRepository(Folder::class)->findByQuery($criteria)];
        }
        
        
        $context = new Context();
        $context->setGroups(['list']);
        $view = $this->view($result, null, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);
        $view->setContext($context);

       return $this->handleView( $view, Response::HTTP_OK);

    }
}
