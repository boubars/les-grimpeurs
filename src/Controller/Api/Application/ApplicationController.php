<?php

namespace App\Controller\Api\Application;

use App\Entity\Application\Application;
use App\Entity\Application\ApplicationSheet;
use App\Entity\Application\Form;
use App\Entity\Application\Sheet;
use App\Entity\Scholarship\Scholarship;
use App\Entity\User\User;
use App\Repository\Application\ApplicationRepository;
use App\Service\Mailer\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Dompdf\Options;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class ApplicationController extends AbstractFOSRestController
{
    
    /**
     * Global entity manager interface variable
     *
     * @var [type]
     */
    private $em;

    private $mailer;


    /**
     * ApplicationController constructor.
     * @param EntityManagerInterface $entityManager
     * @param Mailer $mailer
     */
    public function __construct( EntityManagerInterface $entityManager, Mailer $mailer) {
        $this->em      = $entityManager;
        $this->mailer      = $mailer;
    }


    /**
     * @param Application $application
     * @return JsonResponse
     * @throws \Exception
     *
     * @Route("/api/apply/{id}/sheet", name="application_scholarship_apply_sheet", methods={"POST"})
     * @isGranted("ROLE_USER")
     */
    public function sheetApplication(Request $request, Application $application)
    {
        $request = json_decode($request->getContent(), true);

        $user = $this->getUser();
        if (false == $user instanceof User ) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Not connected"
            ], Response::HTTP_FORBIDDEN);
        }

        $request = $request['formdata'];

        foreach ($request as $key => $value) {
            if (empty($value['sheet']) || empty($value['data'])) {
                return new JsonResponse([
                    'success' => false,
                    'message'    => "Bad params"
                ], Response::HTTP_BAD_REQUEST);
            }
    
            $oSheet = $this->em->getRepository(Sheet::class)->findOneById($value['sheet']['id']);
            $sheetApplication = $this->em->getRepository(ApplicationSheet::class)->findOneBy(
                array(
                    'Sheet' => $oSheet,
                    'Application' => $application,
                    'Student' => $user
                )
            );

            
            if (!$sheetApplication) {
                $sheetApplication = new ApplicationSheet();
                $sheetApplication->setStudent($user);
                $sheetApplication->setApplication($application);
                $sheetApplication->setSheet($oSheet);
                $application->addApplicationSheet($sheetApplication);
            }

            $sheetApplication->setContent($value['data']);
            $this->em->persist($sheetApplication);
        }
        
        $this->em->persist($application);
        $this->em->flush();

        return new JsonResponse([
            'success' => true,
        ], Response::HTTP_OK);

    }



    /**
     * @param Request $request
     * @param Scholarship $scholarship
     * @return JsonResponse|Response
     *
     * @Route("/api/application-scholarship/{id}/list", name="application_scholarship_list", methods={"GET"})
     * @isGranted("ROLE_USER")
     */
    public function findByScholarship(Request $request, Scholarship $scholarship) {
        $request = json_decode($request->getContent(), true);

        $aApplications = $scholarship->getApplications();
        $aCandidates = [];
        foreach ($aApplications as $application) {
            if ($application->getStatus() > Application::EDIT)
                $aCandidates[] = $application->getStudent();
        }
        return $this->handleView(
            $this->view(array(
                    "success"    =>  true,
                    "students" => $aCandidates
                )
            ),Response::HTTP_OK);
    }


    /**
     * @param Request $request
     * @param Form $form
     * @return JsonResponse|Response
     *
     * @Route("/api/application-form/{id}/list", name="application_form_list_student", methods={"GET"})
     * isGranted("ROLE_USER")
     */
    public function findByForm(Request $request, Form $form) {
        
        $request = json_decode($request->getContent(), true);

        $aScholarships = $form->getScholarship();
        $aCandidates = [];
        foreach ($aScholarships as $scholarship) {
            $aApplications = $scholarship->getApplications();
            foreach ($aApplications as $application) {
                if ($application->getStatus() > Application::EDIT)
                    $aCandidates[] = $application->getStudent();
            }
        }

        return $this->handleView(
            $this->view(array(
                    "success"    =>  true,
                    "students" => $aCandidates
                )
            ),Response::HTTP_OK);
    }

     /**
     * @Route("/api/application/show", name="application_show", methods={"GET"})
     * @isGranted("ROLE_USER")
     * @return JsonResponse|Response
     */
    public function showApplication(Request $request, ApplicationRepository $manager){

        $application = $request->query->get('id');

        $application = $manager->findOneById($application);

        $context = new Context();
        $context->setGroups(['scholarship', 'details', 'form', 'student']);
        $view = $this->view(array(
            "application"    =>  $application
        ));

        $view->setContext($context);

        return $this->handleView( $view, Response::HTTP_OK, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);
    }


    /**
     * @param Request $request
     * @param ApplicationRepository $manager
     *
     * @Route("/api/application/note/update", name="application_update_note", methods={"POST"})
     * @isGranted("ROLE_USER")
     * @return JsonResponse|Response
     * @throws \Exception
     */
    public function updateNoteApplication(Request $request, ApplicationRepository $manager){

        $id   = $request->get('id', false);
        $application = $manager->findOneById($id);
        if (false == $application instanceof Application) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Application not found"
            ], Response::HTTP_BAD_REQUEST);
        }
        
        $note = $this->calculApplicationAverage($application);
        $application->setAverageNote($note);
        $application->setDateNote(new \DateTime());
        $this->em->persist($application);
        $this->em->flush();
        $view = $this->view(array(
            "success"    =>  true,
        ));

        return $this->handleView( $view, Response::HTTP_OK, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);
    }

    /**
     * @Route("/api/application/note", name="application_get_note", methods={"GET"})
     * @isGranted("ROLE_USER")
     * @return JsonResponse|Response
     */
    public function getNoteApplication(Request $request, ApplicationRepository $manager){

        $application = $request->query->get('id');

        $application = $manager->findOneById($application);
        $average     = $this->calculApplicationAverage($application);
        $context = new Context();
        $view = $this->view(array(
            "average"       =>  $average,
            "last_average"  => $application->getAverageNote(),
            'date'  => $application->getDateNote(),
        ));

        $view->setContext($context);

        return $this->handleView( $view, Response::HTTP_OK, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);
    }

    private function calculApplicationAverage($application){
        $aSheet = $application->getApplicationSheets();
        $total = $sum = 0;
        foreach ($aSheet as $item) {
            if ($item instanceof ApplicationSheet && !is_null($item->getNote())) {
                $total += $item->getNote()*$item->getPercent();
                $sum += $item->getPercent();
            }
        }
        return $total / ( $sum == 0 ? 1 : $sum );
    }

    /**
     * @param Request $request
     * @param Form $form
     * @return JsonResponse|Response
     *
     * @Route("/api/application/status/update", name="application_status_update", methods={"POST"})
     * @isGranted("ROLE_ORGANIZATION")
     */
    public function updateApplication(Request $request, ApplicationRepository $applicationManager) {

        $request = json_decode($request->getContent(), true);

        $application    = $applicationManager->findOneById($request['application']);
        $status         = $request['status'];
        
        $subject = $request['subject'];
        $content = $request['content'];
        $application->setStatus($status);
        $application->setNotificationSubject($subject);
        $application->setNotification($content);
        $this->em->persist($application);
        $this->em->flush();
        $this->mailer->sendNotificationApplication($application, $subject, $content);

        return $this->handleView(
            $this->view(array(
                    "success"    =>  true,
                    "application" => $application
                )
            ),Response::HTTP_OK);

    }

    /**
     * @Route("/api/application/{id}/download", name="application_download_pdf", methods={"GET"})
     */
    public function downloadApplication(Application $application)
    {
        $options = new Options();
        $options->set('defaultFont', 'Roboto');

        $dompdf = new Dompdf($options);
    
        //return $this->render('pdf/application/document.pdf.html.twig', ["application" => $application]);
        $html   = $this->renderView('pdf/application/document.pdf.html.twig', ["application" => $application]);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        
        $dompdf->stream("user-application-document.pdf", [
            "Attachment" => true
        ]);
    }
}
