<?php

namespace App\Controller\Api\Application;

use App\Entity\Application\Folder;
use App\Entity\Application\Form;
use App\Entity\Application\Sheet;
use App\Entity\Scholarship\Scholarship;
use App\Entity\User\User;
use App\Repository\Application\FormRepository;
use App\Repository\Scholarship\ScholarshipRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FormController extends AbstractFOSRestController
{
    /**
     * Global entity manager interface variable
     *
     * @var [type]
     */
    private $em;
    
    /**
     * Global slugger interface variable
     *
     * @var [SluggerInterface]
     */
    private $slugger;

    private $params;

    /**
     * Class constructor
     *
     * @param EntityManagerInterface $entityManager
     * @param SluggerInterface $slugger
     * @param ParameterBagInterface $params
     */

    public function __construct( EntityManagerInterface $entityManager, SluggerInterface $slugger, ParameterBagInterface $params) {
        $this->em      = $entityManager;
        $this->slugger = $slugger;
        $this->params = $params;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     *
     * @Route("/api/form/sheet/add", name="application_form_sheet_add", methods={"POST"})
     */
    public function addSheetForm(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $user = $this->getUser();
        if (false == $user instanceof User ) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Not connected"
            ], 403);
        }
        $sheet = new Sheet();
        $name = !empty($request['name']) ? $request['name'] : 'No name';
        $sheet->setName($name);
        $sheet->setDateCreation(new \DateTime());
        if (!$request['content']){
            return new JsonResponse([
                'success' => false,
                'message'    => "Form content must does not null"
            ], 200);
        }
        //dd($request['content']);
        $sheet->setContent($request['content']);

        if(isset($request['order'])){
            $sheet->setOrder($request['order']);
        }

        if (isset($request['form']) && $request['form'] > 0) {
            $oForm = $this->em->getRepository(Form::class)->find($request['form']);
        } else {
            $oForm = new Form();
            $oForm->setOrganization($this->getUser());
            $oForm->setName('New Form');
            $oForm->setDateCreation(new \DateTime());
            $this->em->persist($oForm);
        }
        $sheet->setForm($oForm);
        $this->em->persist($sheet);

        $this->em->flush();

        return new JsonResponse([
            'success' => true,
            'form_id'    => $oForm->getId(),
            'sheet_id'    => $sheet->getId()
        ], 200);

    }

    /**
     * @param Request $request
     * @param Sheet   $sheet
     * @return JsonResponse
     * @throws \Exception
     *
     * @Route("/api/form/sheet/{id}/edit", name="application_form_sheet_edit", methods={"POST"})
     */
    public function editSheetForm(Request $request, Sheet $sheet)
    {
        $request = json_decode($request->getContent(), true);
        $user = $this->getUser();
        if (false == $user instanceof User ) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Not connected"
            ], 403);
        }
        $name = !empty($request['name']) ? $request['name'] : 'No name';
        $sheet->setName($name);

        if (!$request['content']){
            return new JsonResponse([
                'success' => false,
                'message'    => "Form content must does not null"
            ], 200);
        }
        $sheet->setContent($request['content']);

        if(isset($request['order'])){
            $sheet->setOrder($request['order']);
        }

        $this->em->persist($sheet);

        $this->em->flush();
        
        return new JsonResponse([
            'success' => true,
            'sheet_id'    => $sheet->getId()
        ], 200);
        
    }
    
    /**
     * Delete form sheet
     * @Route("/api/form/sheet/delete", name="application_form_sheet_delete", methods={"DELETE"})
     * 
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteSheet(Request $request){
        $id = $request->query->get('id');
        $sheet = $this->em->getRepository(Sheet::class)->findOneById($id);
        
        if(!$sheet){
            return new JsonResponse([
                'success' => false,
                'message' => 'Object not found'
            ], Response::HTTP_NO_CONTENT); 
        }
        $this->em->remove($sheet);
        $this->em->flush();
        return new JsonResponse([
            'success' => true
        ], 200);
    }
    
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     *
     * @Route("/api/form/add", name="application_form_add", methods={"POST"})
     */
    public function addForm(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $user = $this->getUser();
        if (false == $user instanceof User ) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Not connected"
            ], 403);
        }

        $name = !empty($request['name']) ? $request['name'] : 'new Form';
        $slug = uniqid($this->slugger->slug(strtolower($name))."-");
        $template = $request['template'] ?? false;
        $oForm = $template == 'default' ? $this->cloneDefaultForm() : new Form();

        $oForm->setOrganization($this->getUser());
        $oForm->setName($name);
        $oForm->setSlug($slug);
        $oForm->setDateCreation(new \DateTime());
        
        
        if (!empty($request['folder']) ) {
            $oFolder = $this->em->getRepository(Folder::class)->find($request['folder']);
            if (false == $oFolder instanceof Folder) {
                return new JsonResponse([
                    'success' => false,
                    'message'    => "Folder id not found"
                ], Response::HTTP_NOT_FOUND);
            }
            $oForm->setFolder($oFolder);
        }

        $this->em->persist($oForm);
        $this->em->flush();

        return new JsonResponse([
            'success' => true,
            'form_id'    => $oForm->getId(),
            'slug'    =>  $oForm->getSlug()
        ], 200);

    }

    /**
     * @param Request $request
     * @param Form $oForm
     * @return JsonResponse
     * @throws \Exception
     *
     * @Route("/api/form/{id}/edit", name="application_form_edit", methods={"POST"})
     */
    public function editForm(Request $request, Form $oForm)
    {
        $request = json_decode($request->getContent(), true);
        $user = $this->getUser();
        if (false == $user instanceof User ) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Not connected"
            ], Response::HTTP_FORBIDDEN);
        }
        if (null == $request){
            return new JsonResponse([
                'success' => false,
                'message'    => "request must does not null"
            ], Response::HTTP_BAD_REQUEST);
        }

        if (!empty($request['name'])) {
            $oForm->setName($request['name']);
            $slug = uniqid($this->slugger->slug(strtolower($request['name']))."-");
            $oForm->setSlug($slug);
        }

        if (isset($request['folder']) && $request['folder'] > 0 ) {
            $fid = $request['folder']['id'] ?? $request['folder']; 
            $oFolder = $this->em->getRepository(Folder::class)->findOneById($fid);
            
            if (false == $oFolder instanceof Folder) {
                return new JsonResponse([
                    'success' => false,
                    'message'    => "Folder id not found"
                ], Response::HTTP_NOT_FOUND);
            }
            $oForm->setFolder($oFolder);
        }

        if (isset($request['folder']) && 0 == $request['folder']) {
            $oForm->setFolder(null);
        }

        $this->em->persist($oForm);
        $this->em->flush();

        return new JsonResponse([
            'success' => true,
            'form_id' =>  $oForm->getId(),
            'slug'    =>  $oForm->getSlug()
        ], Response::HTTP_OK);

    }
    
    /**
     * @param Request $request
     * @param Form $oForm
     * @return JsonResponse
     * @throws \Exception
     *
     * @Route("/api/form/{id}/show", name="application_form_show", methods={"GET"})
     */
    public function showForm(Request $request, Form $oForm)
    {
        $user = $this->getUser();
        if (false == $user instanceof User ) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Not connected"
            ], Response::HTTP_FORBIDDEN);
        }

        $view = $this->view(array(
            "form"    =>  $oForm
        ), null, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);

        $context = new Context();
        $context->setGroups(['form', 'details']);
        $view->setContext($context);
        
        return $this->handleView( $view, Response::HTTP_OK);
    }


    /**
     * @param Request $request
     * @param Form $oForm
     * @return JsonResponse
     * @throws \Exception
     *
     * @Route("/api/form/{id}/sheets", name="application_form_sheets", methods={"GET"})
     */
    public function showFormSheets(Request $request, Form $oForm)
    {
        $user = $this->getUser();
        if (false == $user instanceof User ) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Not connected"
            ], Response::HTTP_FORBIDDEN);
        }

        $view = $this->view(array(
            "sheets"    =>  $oForm->getSheets()
        ), null, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);

        $context = new Context();
        $context->setGroups(['form']);
        $view->setContext($context);
        
        return $this->handleView( $view, Response::HTTP_OK);
    }



    /**
     * @param Request $request
     * @return JsonResponse|Response
     *
     * @Route("/api/form/list", name="application_form_list", methods={"GET"})
     */
    public function listForm(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $user = $this->getUser();
        if (false == $user instanceof User ) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Not connected"
            ], Response::HTTP_FORBIDDEN);
        }
        if (null == $request){
            return new JsonResponse([
                'success' => false,
                'message'    => "request must does not null"
            ], Response::HTTP_BAD_REQUEST);
        }
        $result = $user->getForms();

        $context = new Context();
        $context->setGroups(['details']);
        $view = $this->view(array(
            "forms"    =>  $result
        ), null, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);
        $view->setContext($context);

       return $this->handleView( $view, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return JsonResponse|Response
     *
     * @Route("/api/form/find", name="application_form_find", methods={"GET"})
     */
    public function findForm(Request $request)
    {
        $user = $this->getUser();
        if (false == $user instanceof User ) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Not connected"
            ], Response::HTTP_FORBIDDEN);
        }

        $criteria = $request->query->all();
        $max = $criteria['max'] ?? false;

        $result = [];

        if ($max == 1) {
            unset($criteria['max']);
            $result = ["form"  => $this->em->getRepository(Form::class)->findOneBy($criteria)];
        }else{
            $result = ["forms" => $this->em->getRepository(Form::class)->findByQuery($criteria)];
        }


        $context = new Context();
        $context->setGroups(['list','form','details']);
        $view = $this->view($result, null, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);
        $view->setContext($context);

        return $this->handleView( $view, Response::HTTP_OK);
    }
    /**
     * @param Request $request
     * @return JsonResponse|Response
     *
     * @Route("/api/form/remove", name="application_form_remove", methods={"GET"})
     */
    public function deleteIds(Request $request, FormRepository $formManager)
    {
        $user = $this->getUser();
        if (false == $user instanceof User ) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Not connected"
            ], Response::HTTP_FORBIDDEN);
        }

        $ids = $request->query->get('ids', false);

        if(!$ids){
            return new JsonResponse([
                'success' => false,
                'message'    => "Param IDS required"
            ], Response::HTTP_BAD_REQUEST);
        }
        
        $ids         = is_array($ids) ? $ids : explode(',', $ids);
        $forceDelete = $request->query->get('force', false);

        if($forceDelete){
            $forms = $formManager->findById($ids);
            foreach ($forms as $key => $form) {
                $aScholarship = $form->getScholarship();
                foreach ($aScholarship as $scholarship) {
                    $scholarship->setForm(null);
                    $this->em->persist($scholarship);
                }
            }
            $this->em->flush();
        }
        
        $response = $formManager->deleteByIds($ids);

        return new JsonResponse([
            'success' => true,
            'nb' => $response,
        ], Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return JsonResponse|Response
     *
     * @Route("/api/form/{id}/affect", name="application_form_affect", methods={"POST"})
     */
    public function affectToScholarship(Request $request, Form $form, ScholarshipRepository $scholarshipManager)
    {
        $user = $this->getUser();
        if (false == $user instanceof User ) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Not connected"
            ], Response::HTTP_FORBIDDEN);
        }
        
        $ids = $request->request->get('scholarships', false);
        if(!$ids){
            return new JsonResponse([
                'success' => false,
                'message'    => "Param IDS required"
            ], Response::HTTP_BAD_REQUEST);
        }

        $ids = is_array($ids) ? $ids : explode(',', $ids);

        $aScholarship = $scholarshipManager->findBy(array('id' => $ids));

        $aScholarship = $scholarshipManager->findBy(array('id' => $ids));
        foreach ($aScholarship as $scholarship) {
            if ($scholarship instanceof  Scholarship) {
                $scholarship->setForm($form);
                $form->addScholarship($scholarship);
                $this->em->persist($form);
                $this->em->persist($scholarship);
            }
        };
        $this->em->flush();


        return new JsonResponse([
            'success' => true,
        ], Response::HTTP_OK);
    }

    /**
     * Clonne form endpoint
     *
     * @Route("/api/form/clone" , name="application_form_clone", methods={"POST"})
     * @param Request $request
     * @param FormRepository $formManager
     * @return JsonResponse
     */
    public function clone(Request $request, FormRepository $formManager)
    {
        $forms = $request->request->get('id');
        if(!$forms){
            return new JsonResponse([
                'success' => false,
                'error'   => 'Object not found'
            ], Response::HTTP_NO_CONTENT);
        }

        $forms = $formManager->findById($forms);

        foreach ($forms as $form) {
            $clone = $this->clooneForm($form);
            $this->em->persist($clone);
        }
        
        $this->em->flush();
        return new JsonResponse([
            'success' => true
        ], Response::HTTP_OK);

    }

    /**
     * Clone default template form
     * @return Form
     */
    private function cloneDefaultForm(){
        $formManager = $this->em->getRepository(Form::class);
        $slug        = $this->params->get('default_application_form');
        $template    = $formManager->findOneBySlug($slug);
        return $this->clooneForm($template);
    }

    /**
     * Clonne form method
     *
     * @param Form $template
     * @return Form
     */
    private function clooneForm(Form $template){
        $clone       = new Form();
        $mSheets     = $template->getSheets();
        foreach ($mSheets as $key => $sheet) {
            $cSheet = clone($sheet);
            $cSheet->setId(null);
            $this->em->persist($cSheet);
            $clone->addSheet($cSheet);
        }
        $newName =$template->getName().' copy';
        $clone->setFolder($template->getFolder());
        $clone->setName($newName);
        $clone->setSlug(uniqid($this->slugger->slug(strtolower($newName))."-"));
        $clone->setDateCreation(new DateTime());
        return $clone;
    }

    /**
     * Find scholarhsips by form
     * @Route("/api/form/{id}/scholarships" , name="application_form_scholarships", methods={"GET"})
     * 
     * @param Form $form
     * @return JsonResponse
     */
    public function getFormScholarships(Form $form){
        $data = [
            'scholarships' => $form->getScholarship()
        ];
        $context = new Context();
        $context->setGroups(['list']);
        $view = $this->view($data, null, []);
        $view->setContext($context);

        return $this->handleView( $view, Response::HTTP_OK);
    }

}
