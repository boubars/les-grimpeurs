<?php

namespace App\Controller\Api\Application;

use App\Entity\Application\Application;
use App\Entity\Application\ApplicationCommentaire;
use App\Entity\Application\ApplicationSheet;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class SheetApplicationController extends AbstractFOSRestController
{
    
    /**
     * Global entity manager interface variable
     *
     * @var [type]
     */
    private $em;
    


    /**
     * ApplicationController constructor.
     * @param EntityManagerInterface $entityManager
     * @param Mailer $mailer
     */
    public function __construct( EntityManagerInterface $entityManager) {
        $this->em      = $entityManager;
    }



    /**
     * @param Request $request
     * @param ApplicationSheet $sheet
     * @return JsonResponse|Response
     *
     * @Route("/api/sheet-note/add", name="application_sheet_note_add", methods={"POST"})
     * @isGranted("ROLE_USER")
     */
    public function addNoteSheet(Request $request) {
        $request = json_decode($request->getContent(), true);

        if (empty($request['note']) || empty($request['percent'])) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Bad params, (need note, percent)"
            ], Response::HTTP_BAD_REQUEST);
        }
        $sheet = $this->em->getRepository(ApplicationSheet::class)->findOneById($request['sheet']);
        $sheet->setNote($request['note']);
        $sheet->setPercent($request['percent']);
        $this->em->persist($sheet);
        $this->em->flush();

        return $this->handleView(
            $this->view(array(
                    "success"    =>  true,
                )
            ),Response::HTTP_OK);
    }


    /**
     * @param Request $request
     * @param ApplicationSheet $sheet
     * @return JsonResponse|Response
     *
     * @Route("/api/sheet-note/{id}/remove", name="application_sheet_note_remove", methods={"GET"})
     * @isGranted("ROLE_USER")
     */
    public function removeNoteSheet(Request $request, ApplicationSheet $sheet) {
        $request = json_decode($request->getContent(), true);

        $sheet->setNote(null);
        $sheet->setPercent(null);
        $this->em->persist($sheet);
        $this->em->flush();

        return $this->handleView(
            $this->view(array(
                    "success"    =>  true,
                )
            ),Response::HTTP_OK);
    }


    /**
     * @param Request $request
     * @param ApplicationSheet $sheet
     * @return JsonResponse|Response
     * @throws \Exception
     *
     * @Route("/api/sheet-comments/add", name="application_sheet_comment_add", methods={"POST"})
     * @isGranted("ROLE_USER")
     */
    public function commentSheetAdd(Request $request) {
        $request = json_decode($request->getContent(), true);

        if (!isset($request['content'])) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Bad params, (need content)"
            ], Response::HTTP_BAD_REQUEST);
        }
        $sheet = $this->em->getRepository(ApplicationSheet::class)->findOneById($request['sheet']);
        $commentaire = new ApplicationCommentaire();
        $commentaire->setDateCreation(new \DateTime());
        $commentaire->setApplicationSheet($sheet);
        $commentaire->setContent($request['content']);
        $commentaire->setConstructive($request['constructive']);
        $commentaire->setOwner($this->getUser());
        $this->em->persist($commentaire);
        $sheet->addApplicationCommentaire($commentaire);
        $this->em->persist($sheet);
        $this->em->flush();

        return $this->handleView(
            $this->view(array(
                    "success"    =>  true,
                )
            ),Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param ApplicationSheet $sheet
     * @return JsonResponse|Response
     * @throws \Exception
     *
     * @Route("/api/sheet-comments/edit", name="application_sheet_comment_edit", methods={"POST"})
     * @isGranted("ROLE_USER")
     */
    public function commentSheetEdit(Request $request) {
        $request = json_decode($request->getContent(), true);
        $commentaire = $this->em->getRepository(ApplicationCommentaire::class)->findOneById($request['id']);
        if (false == $commentaire instanceof ApplicationCommentaire) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Bad params, (need content)"
            ], Response::HTTP_BAD_REQUEST);
        }
        if (!empty($request['content'])) {
            $commentaire->setContent($request['content']);
        }
        if (!empty($request['constructive'])) {
            $commentaire->setConstructive($request['constructive']);
        }
        $this->em->persist($commentaire);
        $this->em->flush();

        return $this->handleView(
            $this->view(array(
                    "success"    =>  true,
                )
            ),Response::HTTP_OK);
    }


    /**
     * @param Request $request
     * @param ApplicationCommentaire $commentaire
     * @return JsonResponse|Response
     *
     * @Route("/api/sheet-comments/{id}/remove", name="application_sheet_comments_remove", methods={"GET"})
     * @isGranted("ROLE_USER")
     */
    public function removeCommentsSheet(Request $request, ApplicationCommentaire $commentaire) {
        $request = json_decode($request->getContent(), true);

        $this->em->remove($commentaire);
        $this->em->flush();

        return $this->handleView(
            $this->view(array(
                    "success"    =>  true,
                )
            ),Response::HTTP_OK);
    }
}
