<?php
namespace App\Controller\Api\Dev;

use App\Service\Utilis\Base64Converter;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/scholarship/pdf/preview" , name="pdf_preview")
 */
class PdfPreview extends AbstractController
{
    public function __invoke($messages = 10, KernelInterface $kernel)
    {
       
        $logo = Base64Converter::toBase64(
            sprintf( "%s/assets/vue/assets/img/logo_grimp.png", $this->getParameter('project_dir')),
            'png'
        );
        $fontDir = $this->getParameter('project_dir')."/assets/vue/assets/css/fonts";

       
        $series   = [];
        $series[] = [
            "title" => "Répartition des étudiants intéressée par pays",
            "size"  => "md",
            "chart" => Base64Converter::toBase64(
                sprintf( "%s/public/build/images/stats/graph1.png", $this->getParameter('project_dir')),
                'png'
            )
        ];
        $series[] = [
            "title" => "Répartition Par discipline",
            "size"  => "md",
            "chart" => Base64Converter::toBase64(
                sprintf( "%s/public/build/images/stats/graph2.png", $this->getParameter('project_dir')),
                'png'
            )
        ];
        $series[] = [
            "title" => "Nombre des résultats des étudiants au teste",
            "size"  => "lg",
            "chart" => Base64Converter::toBase64(
                sprintf( "%s/public/build/images/stats/tab3.png", $this->getParameter('project_dir')),
                'png'
            )
        ];

        $params = [
            "logo"    => $logo,
            "fontDir" => $fontDir,
            "series"  => $series
        ];


        return $this->render("pdf/organization/dashboardstat.html.twig",$params);
    }
}