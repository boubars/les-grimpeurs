<?php
namespace App\Controller\Api\Dev;

use App\Service\Mailer\Mailer as MailerMailer;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/ping/email" , name="teste_email")
 */
class TesteEmailAction extends AbstractController
{
    public function __invoke(MailerMailer $mailer)
    {
        $mailer->ping("andry@hightao-mg.com");
        
        
        
        return new JsonResponse(['success']);

    }
} 
