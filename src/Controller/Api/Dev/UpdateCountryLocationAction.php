<?php

namespace App\Controller\Api\Dev;

use App\Entity\Common\Country;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @Route("/api/dev/upcountry" , name="dev_update_countries")
 */
class UpdateCountryLocationAction extends AbstractController
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function __invoke(EntityManagerInterface $em)
    {
      $countryManager = $em->getRepository(Country::class);
      $countries = $countryManager->findAll();
      foreach ($countries as $key => $country) {
            
            $response = $this->getCountryLocation($country->getName());
            
            if($response['status'] == 'OK'){
                $result = $response['results'];
                $persist = false;

                /*** update code */
                if(isset($result[0]["address_components"])){
                    foreach ($result[0]["address_components"] as $key => $value) {
                        try {
                            if(in_array("country", $value["types"])){
                                
                                if(strlen($value["short_name"]) > 2 ){
                                    dd($value, $result);
                                }
                                
                                $country->setCode($value["short_name"]);
                                $persist = true;
                                break;
                            }
                           
                        } catch (\Throwable $th) {
                          dd($result);
                        }
                    }

                }

                /*** update location */
                if(isset($result[0]["geometry"]["location"])){
                    $country->setLocation($result[0]["geometry"]["location"]);
                    $persist = true;
                }

                /*** persist change */
                if($persist){
                    $em->persist($country);
                }
            }
        }
      $em->flush();
      return new JsonResponse(['success']);

    }

    public function getCountryLocation($country): array
    {

        $endPoints = sprintf("https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=AIzaSyDTwj2O4qoWP23dnIS3Uxz7pVFg9KxDS60", $country);
        $response = $this->client->request(
            'GET',
            $endPoints
        );

        $statusCode = $response->getStatusCode();
        $contentType = $response->getHeaders()['content-type'][0];

        $content = $response->getContent();
        $content = $response->toArray();
        return $content;
    }
} 
