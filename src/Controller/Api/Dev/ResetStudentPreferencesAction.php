<?php

namespace App\Controller\Api\Dev;

use App\Entity\Organization\Organization;
use App\Entity\Student\SearchPreference;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/api/dev/resest/student-preferences" , name="reset_student_preferences")
 */
class ResetStudentPreferencesAction extends AbstractController
{
    public function __invoke(EntityManagerInterface $em, SluggerInterface $sluger)
    {
      $manager     = $em->getRepository(SearchPreference::class);

      $preferences = $manager->findAll();
    
      foreach ($preferences as $p) {
        $p->setFormationByProfil(1)
          ->setScholashipByProfil(1)
          ->setScholarshipByDiscipline(1)
          ->setNewGrimpeurByProfil(1)
          ->setNewGrimpeur(1)
          ->setFormationByProfil(1)
          ->setOpportunityByProfil(1);
        $em->persist($p);
        
      }

      
      $em->flush();
      //dd("teste", $preferences);
      return new JsonResponse(['success']);
    }
} 
