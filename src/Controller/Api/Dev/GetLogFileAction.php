<?php
namespace App\Controller\Api\Dev;

use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/log/{env}/{line}" , name="dev_api_log")
 */
class GetLogFileAction extends AbstractController
{
    public function __invoke(String $env, $line, ParameterBagInterface $params)
    {
      $fileSystem = new Filesystem();
      $filePath   = $params->get('project_dir')."/var/log/$env.log";
      $file = file_get_contents($filePath);
      $lines = `tail -$line $filePath`;
      dd($lines);
      return new JsonResponse(['user removed succefully']);
    }
} 
