<?php
namespace App\Controller\Api\Dev;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/reset/application" , name="dev_reset_application")
 */
class ResetApplicationAction extends AbstractController
{
    public function __invoke(EntityManagerInterface $entityManager)
    {
      $connection = $entityManager->getConnection();
      $platform   = $connection->getDatabasePlatform();
      $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 0;');
      $connection->executeUpdate($platform->getTruncateTableSQL('application_commentaire'));
      $connection->executeUpdate($platform->getTruncateTableSQL('application_sheet'));
      $connection->executeUpdate($platform->getTruncateTableSQL('application'));

      return new JsonResponse(['user removed succefully']);

    }
} 
