<?php

namespace App\Controller\Api\Dev;

use App\Entity\Common\Country;
use App\Entity\Organization\Organization;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @Route("/api/dev/uporganization" , name="dev_update_organization_location")
 */
class UpdateOrganizationLocationAction extends AbstractController
{
    private $client;
    private $params;
    private $http;

    public function __construct(HttpClientInterface $client, ParameterBagInterface $params)
    {
        $this->client = $client;
        $this->http = $client;
        $this->params = $params;
    }

    public function __invoke(EntityManagerInterface $em)
    {
      
      /*
        $organizatonManager = $em->getRepository(Organization::class);
        $organizations = $organizatonManager->findAll();
        foreach ($organizations as $key => $organization) {
                
                $response = $this->getOrganizationLocation($organization->getAdresse());
                
                if($response['status'] == 'OK'){
                    $result = $response['results'];
        
                    if(isset($result[0]["geometry"]["location"])){
                        $organization->setAdresseLocation($result[0]["geometry"]["location"]);
                        $em->persist($organization);
                        
                    }
                }
            }
        $em->flush();
      */
      $content = $this->getAdresseMap("15-21 Rue de l'École de Médecine, 75006 Paris, France");

      
      return new JsonResponse(['success']);

    }

    public function getOrganizationLocation($adresse): array
    {

        $endPoints = sprintf("https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=AIzaSyDTwj2O4qoWP23dnIS3Uxz7pVFg9KxDS60", $adresse);
        $response = $this->client->request(
            'GET',
            $endPoints
        );

        $statusCode = $response->getStatusCode();
        $contentType = $response->getHeaders()['content-type'][0];

        $content = $response->getContent();
        $content = $response->toArray();
        return $content;
    }
    private function getAdresseMap($adresse){
        $gmapKey = $this->params->get('gmap_key');
        $url = "https://maps.googleapis.com/maps/api/staticmap?center=%s&&markers=size:mid|color:red|%s&zoom=14&size=400x400&key=%s";
        $endPoints = sprintf($url, $adresse, $adresse, $gmapKey);

        $response = $this->http->request(
            'GET',
            $endPoints
        );

        $blob = $response->getContent();

        $newFilename = "map-sorbone-".uniqid().'.png';
        $target = $this->params->get('project_dir')."/public/uploads/maps/".$newFilename;
        file_put_contents($target, $blob);
    }
} 
