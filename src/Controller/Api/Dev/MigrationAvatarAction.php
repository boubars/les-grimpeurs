<?php
namespace App\Controller\Api\Dev;

use App\Repository\Organization\OrganizationRepository;
use App\Repository\Student\StudentRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class MigrationAvatarAction extends AbstractController
{
    /**
     * @Route("/api/migrate/getavatars" , name="dev_get_avatars_json")
     */
    public function index(OrganizationRepository $orgManager, StudentRepository $studentManager)
    {
        $data = [];

        $students = $studentManager->findAll();

        foreach ($students as $key => $value) {
           $data[] = [
                "id" => $value->getId(),
                "avatar" => $value->getAvatar()
           ];
        }
        
        $organizations = $orgManager->findAll();

        foreach ($organizations as $key => $value) {
            $data[] = [
                 "id" => $value->getId(),
                 "avatar" => $value->getAvatar()
            ];
        }


        return new JsonResponse(
            $data
        );
    }
    /**
     * @Route("/api/update/avatars" , name="dev_update_avatars_json")
     */
    public function update(EntityManagerInterface $em, UserRepository $userManager){
        $avatars = json_decode($this->getData(), true);

        foreach ($avatars as $key => $value) {
           $user = $userManager->findOneById($value["id"]);
           if($user){
               $user->setAvatar($value["avatar"]);
               $em->persist($user);
           }
        }
        $em->flush();

       return new JsonResponse(['success']);
    }

    private function getData(){
        return '
            [{"id":1,"avatar":"20200315-162532-606747a935cbd.jpeg"},{"id":6,"avatar":"000001-Hocine-607431751b6d4.jpeg"},{"id":11,"avatar":"the-best-6077e8c0e96c0.jpeg"},{"id":121,"avatar":"20210530-145826-60b5e34a2a09a.jpeg"},{"id":183,"avatar":"000001-Hocine-60b204959487b.jpeg"},{"id":30,"avatar":"erasmus-1-608ef8c770189.png"},{"id":31,"avatar":"allemagne-608f04727b53b.png"},{"id":32,"avatar":"blue-aqua-school-education-minister-student-608fb96f8dca8.png"},{"id":33,"avatar":"education-jasso-608fc30c224d4.png"},{"id":34,"avatar":"Capture-608ffb55938c7.png"},{"id":35,"avatar":"th-1-60910d4904f68.png"},{"id":36,"avatar":"Lendza-60912b3d34364.png"},{"id":37,"avatar":"Corvias-Foundation-60912c9892445.png"},{"id":38,"avatar":"Scholarship-Foundation-of-Santa-Barbara-609133a203d7c.png"},{"id":39,"avatar":"NextStepU-609136ab94272.png"},{"id":41,"avatar":"patterson-3-609140f99149e.png"},{"id":42,"avatar":"greater-kansas-1-609145dec3f0a.png"},{"id":43,"avatar":"100-Club-of-Arizona-609150f7b17d9.png"},{"id":44,"avatar":"Patterson-Law-Group-6091520b04c32.png"},{"id":45,"avatar":"medical-scholarship-60915e35eab0c.png"},{"id":46,"avatar":"Greater-Kansas-City-Community-Foundation-60915fc111929.png"},{"id":47,"avatar":"evangelsit-1-609175b064e87.png"},{"id":48,"avatar":"wheelchair-2-60923f08bc1b4.png"},{"id":49,"avatar":"Bounce-Energy-1100x551-1-60923f8816410.png"},{"id":50,"avatar":"WI-4H-RGB-Feb-2016-1-609243eaae9dc.png"},{"id":51,"avatar":null},{"id":53,"avatar":"Winston-Salem-Foundation-60924b111b802.png"},{"id":54,"avatar":"a1-609251912377e.png"},{"id":55,"avatar":null},{"id":56,"avatar":"WFACF-horizontal-logo-e1604353157342-6092586d4d475.png"},{"id":57,"avatar":null},{"id":58,"avatar":null},{"id":59,"avatar":null},{"id":60,"avatar":null},{"id":61,"avatar":null},{"id":62,"avatar":null},{"id":63,"avatar":"ertyuio-6092a7696cf06.png"},{"id":64,"avatar":"turc-6092afb06a485.png"},{"id":65,"avatar":"mesro-1-6092bb425b2ec.png"},{"id":66,"avatar":null},{"id":67,"avatar":null},{"id":68,"avatar":"WI-4H-RGB-Feb-2016-1-6093977c79aeb.png"},{"id":69,"avatar":"american-culinary-1-6093986fbfa0c.png"},{"id":70,"avatar":null},{"id":71,"avatar":"telechargement-3-60939bf7be6c9.png"},{"id":72,"avatar":"nsps-logo-6093a06263368.png"},{"id":73,"avatar":null},{"id":74,"avatar":null},{"id":75,"avatar":"Aaja-logo-2017-1-6093a64634b1f.png"},{"id":76,"avatar":"bccf-stacked-mobile-logo-1-6093aafed7761.png"},{"id":77,"avatar":"abbott-6093b196762f5.png"},{"id":79,"avatar":"isaacs-isaacs-6093b97590549.png"},{"id":80,"avatar":"telechargement-6-6093beab59682.png"},{"id":81,"avatar":null},{"id":82,"avatar":null},{"id":83,"avatar":null},{"id":84,"avatar":"japanese-6093ca353f4b8.png"},{"id":85,"avatar":null},{"id":86,"avatar":"broadcast-1-6093db543fcfd.png"},{"id":87,"avatar":null},{"id":88,"avatar":"society-plastic-6093f73c508ed.png"},{"id":89,"avatar":null},{"id":90,"avatar":null},{"id":91,"avatar":null},{"id":92,"avatar":null},{"id":93,"avatar":null},{"id":94,"avatar":"ACI-Michigan-Logo-6094f440c8020.png"},{"id":95,"avatar":null},{"id":96,"avatar":"ACONE-Wings-Logo-6094f73858793.png"},{"id":97,"avatar":null},{"id":98,"avatar":"telechargement-10-1-6094fd64a8479.png"},{"id":99,"avatar":"telechargement-9-2-609500397d903.png"},{"id":100,"avatar":null},{"id":101,"avatar":null},{"id":102,"avatar":"Emmy-Logo-609508345419b.png"},{"id":103,"avatar":null},{"id":104,"avatar":null},{"id":105,"avatar":"1611755232469-609515bd0b50f.png"},{"id":106,"avatar":"allemagne-608f04727b53b-60951c1f7ab56.png"},{"id":107,"avatar":null},{"id":108,"avatar":null},{"id":109,"avatar":null},{"id":110,"avatar":null},{"id":111,"avatar":null},{"id":112,"avatar":"telechargement-12-1-60953c75268b0.png"},{"id":113,"avatar":null},{"id":114,"avatar":null},{"id":115,"avatar":null},{"id":116,"avatar":null},{"id":117,"avatar":"telechargement-13-1-6095461b50468.png"},{"id":118,"avatar":null},{"id":120,"avatar":"pumptedpng-0-1-60954c640de33.png"},{"id":122,"avatar":"1200px-Sub-Pop-6098d119bf3eb.png"},{"id":123,"avatar":null},{"id":124,"avatar":null},{"id":125,"avatar":"Prescott-College-Logo-1-6098d8fe78c5f.png"},{"id":126,"avatar":null},{"id":127,"avatar":null},{"id":128,"avatar":"telechargement-13-2-6098df3d14193.png"},{"id":129,"avatar":null},{"id":130,"avatar":"th-4-6098e62e21ef8.png"},{"id":131,"avatar":"418XTY9q2QL-6098e679a6ac2.png"},{"id":132,"avatar":null},{"id":133,"avatar":"th-6-6098ebed55b89.png"},{"id":134,"avatar":"OIP-7-6098ef046f7fe.png"},{"id":135,"avatar":"telechargement-14-2-6098f06263ff2.png"},{"id":136,"avatar":"ACAP-6098f32f73c17.png"},{"id":137,"avatar":"agris-scholarship-min-6098f6903d58c.png"},{"id":138,"avatar":"telechargement-15-2-6098f7b606988.png"},{"id":139,"avatar":"R496acdf91a91fc0a8e5219e2876f2cd4-1-609904fb57cf9.png"},{"id":140,"avatar":null},{"id":141,"avatar":"telecharger-6-60990d5a52288.png"},{"id":142,"avatar":"th-10-609913f584648.png"},{"id":143,"avatar":"OIP-16-609916f998503.png"},{"id":144,"avatar":"OIP-17-60991c0ef422a.png"},{"id":145,"avatar":"OIP-18-60991eafede33.png"},{"id":146,"avatar":"neha-aas-image-609926012f6d8.png"},{"id":147,"avatar":"OIP-20-60992e4a83a4b.png"},{"id":148,"avatar":"OIP-22-609934da9ebf8.png"},{"id":149,"avatar":"logo-6099386c6707d.png"},{"id":150,"avatar":"OIP-23-60994071321ef.png"},{"id":151,"avatar":"mdgov-logo-black-1-609a3c5fa68d6.png"},{"id":152,"avatar":"th-609a4240a1571.png"},{"id":153,"avatar":"th-1-609a4586436fb.png"},{"id":154,"avatar":"th-2-609a55131a881.png"},{"id":155,"avatar":null},{"id":156,"avatar":"3-1-609a5bf3ce5d1.png"},{"id":157,"avatar":"e657921a-4276-4d57-ad5e-6f381c213f91-609a755a74190.png"},{"id":158,"avatar":"R2039c1af7b4d662082b09408511fdd0e-1-609a7f228b87a.png"},{"id":159,"avatar":"amhs-logo-2016-1-609b882eaf717.png"},{"id":160,"avatar":null},{"id":161,"avatar":"OIP-2-609b8d3605aca.png"},{"id":162,"avatar":null},{"id":163,"avatar":"OIP-4-609b93787e59f.png"},{"id":164,"avatar":null},{"id":165,"avatar":"hdds-logos-1-609bcd1844116.png"},{"id":166,"avatar":"florida-realtors-logo-609bd68847f47.png"},{"id":167,"avatar":null},{"id":168,"avatar":"logo-7cf11e2fb80a32eb04667097b35712fd-1x-1-609e486b43a62.png"},{"id":169,"avatar":"OIP-7-609e4c8645946.png"},{"id":170,"avatar":"logo-2-1-609e4ea154d3f.png"},{"id":171,"avatar":"th-4-609e515cc346b.png"},{"id":172,"avatar":"OIP-10-609e5545e9344.png"},{"id":173,"avatar":"th-6-609e573cb164f.png"},{"id":174,"avatar":"UNF-Logo-1-609e59a146d3e.png"},{"id":175,"avatar":"synedy-609e5a178d06b.png"},{"id":176,"avatar":"BPW-TRI-Logo-trans-1-609e5cbb2b233.png"},{"id":177,"avatar":"aises-logo-2-0-1-609e60e8c4b7f.png"},{"id":178,"avatar":"men-gov-ma-609e6b0b1c055.png"},{"id":179,"avatar":"telecharger-2-60abafe3cc4c1.png"},{"id":180,"avatar":"th-10-60accb8a35270.png"},{"id":181,"avatar":"logo-4-1-60acf566c9163.png"},{"id":182,"avatar":"8122-StudentAidFundLogo-Small-60acf9074b78f.png"},{"id":184,"avatar":"KEDGE-e-60b37611baa49.png"},{"id":185,"avatar":"000001-Hocine-60bcaeccbbd38.jpeg"},{"id":187,"avatar":"images-60ccbd3c0cc3a.png"}]
        ';
    }


} 
