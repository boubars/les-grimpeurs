<?php

namespace App\Controller\Api\Dev;

use App\Entity\Organization\Organization;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/api/dev/organization/slugs" , name="update_organization_slug")
 */
class UpdateOrganizationSlugAction extends AbstractController
{
    public function __invoke(EntityManagerInterface $em, SluggerInterface $sluger)
    {
      $userManager = $em->getRepository(Organization::class);
      $orgs = $userManager->findAll();
      foreach ($orgs as $key => $organization) {
        $name = $organization->getName();
        $slug = $sluger->slug(strtolower($name));
        $organization->setSlug($slug);
        $em->persist($organization);
      }
      $em->flush();
      return new JsonResponse(['success']);

    }
} 
