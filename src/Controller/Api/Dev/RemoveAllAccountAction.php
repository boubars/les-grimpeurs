<?php
namespace App\Controller\Api\Dev;

use App\Entity\User\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/truncate/user" , name="dev_remove_account")
 */
class RemoveAllAccountAction extends AbstractController
{
    public function __invoke(EntityManagerInterface $entityManager)
    {
      return false;
      $connection = $entityManager->getConnection();
      $platform   = $connection->getDatabasePlatform();
      $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 0;');
      $connection->executeUpdate($platform->getTruncateTableSQL('user'));

      return new JsonResponse(['user removed succefully']);

    }
} 
