<?php
namespace App\Controller\Api\Dev;

use App\Entity\User\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/truncate/request/info" , name="dev_remove_request")
 */
class RemoveRequestAction extends AbstractController
{
    public function __invoke(EntityManagerInterface $entityManager)
    {
      $connection = $entityManager->getConnection();
      $platform   = $connection->getDatabasePlatform();
      $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 0;');
      $connection->executeUpdate($platform->getTruncateTableSQL('scholarship_information_request'));

      return new JsonResponse(['user removed succefully']);

    }
} 
