<?php

namespace App\Controller\Api\Dev;

use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/active/users" , name="dev_active_users")
 */
class ActiveAllUserAction extends AbstractController
{
    public function __invoke(EntityManagerInterface $em)
    {
      $userManager = $em->getRepository(User::class);
      $users = $userManager->findAll();
      foreach ($users as $key => $user) {
        $user->setEnable(true);
        $em->persist($user);
      }

      $em->flush();
      return new JsonResponse(['success']);

    }
} 
