<?php
namespace App\Controller\Api\Organization\Profil;

use App\Entity\Organization\Organization;
use Doctrine\ORM\EntityManagerInterface;
use App\Controller\FileUploaderAction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;


/**
 * Class UpdateLogoAction
 * @package App\Controller\Api\Organization\Profil
 * @Route("/api/organization/profil/logo/{id}", methods={"POST"})
 * @IsGranted("ROLE_ORGANIZATION")
 */
class UpdateLogoAction extends FileUploaderAction
{
    public function __invoke(Request $request, EntityManagerInterface $entityManager, $id = null)
    {
       $organization = $entityManager->getRepository(Organization::class)->find($id);

       $fileLogo = $request->files->get('logo');
       if(!$fileLogo){
            return new JsonResponse([
                'success'=> false,
                'error'  => 'Image non trouvé'
            ]);
       }
       
       $newFilename = $this->uploadDocument($fileLogo, "logo_dir"); 

       $organization->setLogo($newFilename);
         
       $entityManager->persist($organization);
       $entityManager->flush();

       return new JsonResponse([
            "success" => true,
            "logo"    => $organization->getLogo()
       ]);
     }
}