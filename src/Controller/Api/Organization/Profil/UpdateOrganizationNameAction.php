<?php
namespace App\Controller\Api\Organization\Profil;

use App\Entity\Organization\Organization;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/api/organization/profil/name/{id}", methods={"PUT"})
 * @IsGranted("ROLE_ORGANIZATION")
 */
class UpdateOrganizationNameAction extends AbstractFOSRestController
{
    public function __invoke(Request $request, EntityManagerInterface $entityManager, $id = null)
    {
        $data = json_decode($request->getContent(), true);
        $contact = $this->getUser();

        $organization = !is_null($id) ? $entityManager->getRepository(Organization::class)->find($id) : new Organization();
        $organization->setName($data['organizationName']);
        $organization->addContact($contact);
        $entityManager->persist($organization);
        $entityManager->flush();
        $view = $this->view(array(
            "success"=> true,
            "organization" => $organization
        ));
        $context = new Context();
        $context->setGroups(['details']);
        $view->setContext($context);
        return $this->handleView($view,Response::HTTP_OK);

     }
}