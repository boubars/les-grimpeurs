<?php
namespace App\Controller\Api\Organization\Profil;

use App\Entity\Organization\Organization;
use Doctrine\ORM\EntityManagerInterface;
use App\Controller\FileUploaderAction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;


/**
 * @Route("/api/organization/profil/avatar/{id}", methods={"POST"})
 * @IsGranted("ROLE_ORGANIZATION")
 */
class UpdateAvatarAction extends FileUploaderAction
{
    public function __invoke(Request $request, EntityManagerInterface $entityManager, $id)
    {
        $contact = $this->getUser();

        $organization = $entityManager->getRepository(Organization::class)->find($id);

        $fileLogo = $request->files->get('avatar');
        if($fileLogo){
            $newFilename = $this->uploadDocument($fileLogo, "avatar_dir");
            $contact->setAvatar($newFilename);
        }
       
       
      

       /** update adresse maps */
       $adresseMapFile = $request->files->get('adresseMap');
       if($adresseMapFile){
            $newFilename = $this->uploadDocument($adresseMapFile, "map_dir"); 
            $organization->setAdresseMap($newFilename);
       }
       
       if(!$fileLogo && !$adresseMapFile){
          return new JsonResponse([
              'success'=> false,
              'error'  => 'Image non trouvé'
          ]);
     }
     
         
       $entityManager->persist($organization);
       $entityManager->persist($contact);
       $entityManager->flush();

       return new JsonResponse([
            "success" => true,
            "avatar"    => $organization->getLogo()
       ]);
     }
}