<?php
namespace App\Controller\Api\Organization\Profil;

use App\Entity\Common\Country;
use App\Entity\Common\SocialNetwork;
use App\Entity\Organization\Organization;
use App\Entity\Organization\Statistical;
use App\Form\Organization\OrganizationType;
use App\Repository\Common\CountryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Organization\OrganizationType as InstitutionType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * @Route("/api/organization/profil/infos/{id}", methods={"PUT"})
 * @IsGranted("ROLE_ORGANIZATION")
 */
class UpdateOrganizationInfosAction extends AbstractController
{
     private $entityManager;
     private $countryManager;

     public function __construct(EntityManagerInterface $em, CountryRepository $countryManager)
     {
          $this->entityManager  = $em  ;
          $this->countryManager = $countryManager ;
     }
    public function __invoke(Request $request, EntityManagerInterface $entityManager, $id = null)
    {
       $organization = $entityManager->getRepository(Organization::class)->find($id);
       $request =json_decode($request->getContent(), true);
       $context = $request['context'];

       switch ($context) {
          case 'adresse':
             $organization = $this->handleUpdateAdresse($organization, $request) ;
          break;
          case 'statistical':
              $organization = $this->handleUpdateStatistical($organization, $request['organization']) ;
          break;
          case 'socialNetwork':
              $organization = $this->handleUpdatesocialNetwork($organization, $request['organization']['social_network']) ;
          break;
          case 'infos':
              $organization = $this->handleUpdateInfos($organization, $request) ;
          break;
       }

       $entityManager->persist($organization);
       $entityManager->flush();
          
     
       return new JsonResponse(
          [
               "success"=> true
          ]
       );

     }

     private function getCountry($array){
          return $this->countryManager->findOneById($array['id']);
     }

     private function handleUpdateAdresse(Organization &$organization, $request){
          //$form = $this->createForm(OrganizationType::class, $organization, ['context'=>'adresse']);
          //$form->submit($data);
         $data = $request['organization'];

          $organization
          ->setAdresse($data['adresse'])
          ->setAdresseLocation($data['adresse_location'])
          ->setPostalCode($data['postal_code'])
          //->add('avatar')
          ->setPhoneNumber($data['phone_number'])
          ->setOrganizationEmail($data['organization_email']);

          /** check if all */
          if(isset($data['country'])){
               if($data['country']['id'] != 0){
                    $organization->setCountry(
                         $this->getCountry(
                              $data['country']
                         )
                    );
               }else{
                    $organization->setCountry(null);
               }
          }
          $countryActions =  $data['country_action'];

          if($countryActions){
               $organization->setCountryAction(new ArrayCollection());

               if($countryActions[0]['id'] != 0){
                    foreach ($countryActions as  $country) {
                         $organization->addCountryAction(
                              $this->getCountry(
                                   $country
                              )
                         );
                    }
               }
          }
          return $organization;

     } 
     private function handleUpdateStatistical(&$organization, $request){

        $data = $request['statistical'];

          $statistical = new Statistical();
          $statistical->setScholarshipAwarded(
               isset($data['scholarship_awarded']) ?  (int) $data['scholarship_awarded'] : null
          );
          $statistical->setAcceptanceRate($data['acceptance_rate']);
          $statistical->setMenCount(
               isset($data['men_count']) ? (int) $data['men_count'] : null
          );
          $statistical->setWomenCount(
               isset($data['women_count']) ? (int) $data['women_count'] : null
          );
          $statistical->setScholarshipCount(
               isset($data['scholarship_count']) ? (int) $data['scholarship_count'] : null
          );
          
          $organization->setStatistical($statistical);
          return $organization;
     }
     private function handleUpdateSocialNetwork(&$organization, $data){

          $socialNetwork = new SocialNetwork();
          $socialNetwork
          ->setFacebook($data['facebook'])
          ->setInstagram($data['instagram'])
          ->setYoutube($data['youtube'])
          ->setTwitter($data['twitter'])
          ->setLinkedin($data['linkedin']);

          $organization->setSocialNetwork($socialNetwork);
          return $organization;
     }

     public function handleUpdateInfos(&$organization, $request)
    {
        $data = $request['organization'];
       $organization->setAccreditation($data['accreditation']);
       $organization->setDescription($data['description']);
       /**
        * @var OrganizationTypeRepository
        */
       $typeManager = $this->entityManager->getRepository(InstitutionType::class);
       $institutionType = $typeManager->findOneById($data['institution_type']['id']);
       
       $organization->setInstitutionType($institutionType);

       $organization->setPresidentName($data['president_name']);
       $organization->setSiretNumber($data['siret_number']);
       $organization->setWebsite($data['website']);
       $organization->setYearOfCreation($data['year_of_creation']);
       return $organization; 
     
     }


}