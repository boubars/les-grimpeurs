<?php
namespace App\Controller\Api\Organization\Profil;

use App\Repository\Organization\OrganizationRepository;
use App\Repository\UserRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @Route("/api/organization/profil/logo", methods={"GET"})
 * @IsGranted("ROLE_ORGANIZATION")
 */
class GetLogoAction extends AbstractFOSRestController
{
     public function __invoke(Request $request, OrganizationRepository $organizationManager)
     {
          $organization = $request->query->get('organization');               
     
          $organization = $organization ? $organizationManager->findOneById($organization) : false;
          
          if(!$organization){
               throw new HttpException(400, "Organization not found.");
          }

          $rs = $organization->getLogo() ? [ "url" => $request->getUriForPath("/uploads/logo/".$organization->getLogo()),
                    "fileName" =>  $organization->getLogo()
          ] : false;

          return $this->handleView(
               $this->view(array(
                   "logo" => $rs
               )
          ) ,Response::HTTP_OK); 

     }
}