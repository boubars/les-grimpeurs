<?php
namespace App\Controller\Api\Organization\Profil;

use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @Route("/api/organization/profil/hidden-fields", methods={"POST"})
 * @IsGranted("ROLE_ORGANIZATION")
 */
class UpdateHiddenFieldsAction extends AbstractFOSRestController
{
     public function __invoke(Request $request, EntityManagerInterface $entityManager)
     {
         
        $organizaiton = $this->getUser();
        $fields       = $request->request->get('fields', false);

        if($fields === false){
            return $this->handleView(
                $this->view(array(
                    "success" => false,
                    "error"   => 'param fields invalid '
                )
           ) ,Response::HTTP_BAD_REQUEST); 
        }

        $organizaiton->setHiddenFields($fields);
        $entityManager->persist($organizaiton);
        $entityManager->flush();

        return $this->handleView(
            $this->view(array(
                "success" => true
            )
       ) ,Response::HTTP_OK); 

          

     }
}