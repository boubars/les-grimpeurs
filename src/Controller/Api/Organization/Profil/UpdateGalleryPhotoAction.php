<?php
namespace App\Controller\Api\Organization\Profil;

use App\Controller\FileUploaderAction;
use App\Entity\Common\Gallery;
use App\Entity\Organization\Organization;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;


/**
 * @Route("/api/organization/profil/gallery/photo/{id}", methods={"POST"})
 * @IsGranted("ROLE_ORGANIZATION")
 */
class UpdateGalleryPhotoAction extends FileUploaderAction
{
    public function __invoke(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer, $id)
    {
       $request->request;
       $organization = $entityManager->getRepository(Organization::class)->find($id);

  
       $galeryPhoto = $request->files->get('galeryPhoto');
       if (!$galeryPhoto) {
            return new JsonResponse([
                'success'=> false,
                'error'  => 'Image non trouvé'
            ]);
        }

        $gallery = $organization->getGallery() ?? new Gallery();
        /** white list image gallery */
        $gallery->setPictures([]);
        foreach($galeryPhoto as $photo) {
            $newFilename = $this->uploadDocument($photo, 'picture_dir'); 
            $gallery->addPicture($newFilename);
        }
       $organization->setGallery($gallery);
       $entityManager->persist($organization);
       $entityManager->flush();


       return new JsonResponse([
            "success" => true,
            "gallery" => $serializer->serialize($organization->getGallery(),'json'),
        ]); 
     }

}