<?php
namespace App\Controller\Api\Organization\Profil;

use App\Controller\FileUploaderAction;
use App\Entity\Organization\Organization;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;


/**
 * @Route("/api/organization/profil/cover/{id}", methods={"POST"})
 * @IsGranted("ROLE_ORGANIZATION")
 */
class UpdateCoverAction extends FileUploaderAction
{
    public function __invoke(Request $request, EntityManagerInterface $entityManager, $id)
    {
       $request->request;
       $organization = $entityManager->getRepository(Organization::class)->find($id);

        $fileCover = $request->files->get('cover');
        if(!$fileCover){
            return new JsonResponse([
                'success'=> false,
                'error'  => 'Image non trouvé'
            ]);
        }
       
       $newFilename = $this->uploadDocument($fileCover, 'picture_dir'); 

       $organization->setCoverPicture($newFilename);
         
       $entityManager->persist($organization);
       $entityManager->flush();

       return new JsonResponse([
            "success" => true,
            "cover"   => $organization->getCoverPicture()
       ]);
     }
    

}