<?php
namespace App\Controller\Api\Organization\Profil;

use App\Controller\FileUploaderAction;
use App\Entity\Common\Gallery;
use App\Entity\Organization\Organization;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api/organization/profil/gallery/video/{id}", methods={"POST"})
 * @IsGranted("ROLE_ORGANIZATION")
 */
class UpdateGalleryVideoAction extends AbstractController
{
    public function __invoke(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer, $id)
    {
       $organization = $entityManager->getRepository(Organization::class)->find($id);
       $request = json_decode($request->getContent(), true);

       $galeryVideo = $request['galeryVideo'];
        if(!$galeryVideo){
            return new JsonResponse([
                'success'=> false,
                'error'  => 'Invalid gallery'
            ]);
        }

        $gallery = $organization->getGallery() ?? new Gallery();
        /** white list videos gallery */
        $gallery->setVideos($galeryVideo);
        
    
       $organization->setGallery($gallery);
       $entityManager->persist($organization);
       $entityManager->flush();


       return new JsonResponse([
            "success" => true,
            "gallery" => $serializer->serialize($organization->getGallery(),'json'),
         ]); 
     }
    

}