<?php
/**
 * Copyright (c) 2022.
 * Boba <boubarisoa@gmail.com>
 *
 */

namespace App\Controller\Api\Organization;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/contact/my-organization", methods={"GET"})
 */
class MyOrganizationAction extends AbstractFOSRestController
{
    public function __invoke()
    {
        $user = $this->getUser();
        if (!in_array('ROLE_ORGANIZATION', $user->getRoles())) {

            return new JsonResponse();
        }

        $organization = $user->getOrganization();
        $view = $this->view(array(
            "organization" => $organization
        ));
        $context = new Context();
        $context->setGroups(['details']);
        $view->setContext($context);
        return $this->handleView($view, Response::HTTP_OK);
    }
}
