<?php

namespace App\Controller\Api\Organization;

use App\Manager\Organization\RegistrationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/organization/register", name="organization_registration", methods={"POST"})
 */
class RegistrationAction extends AbstractController
{
    /**
     * @param Request $request
     * @param RegistrationManager $registrationManager
     * 
     * @return [JsonResonse]
     */
    public function __invoke(Request $request, RegistrationManager $registrationManager):JsonResponse
    {
        $result = $registrationManager->handle($request);
        if($result === true ){
            return new JsonResponse( ['success'=> $result], 200);
        }
        
        return new JsonResponse( 
            [
                'success' => false,
                'error'   => $result
            ], 200);


    }
}
