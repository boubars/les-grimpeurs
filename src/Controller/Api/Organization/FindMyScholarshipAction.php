<?php

namespace App\Controller\Api\Organization;

use App\Entity\Organization\Organization;
use App\Manager\Organization\RegistrationManager;
use App\Repository\Scholarship\ScholarshipRepository;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;

/**
 * @IsGranted("ROLE_ORGANIZATION")
 * @Route("/api/organization/myscholarships", name="organization_scholarships", methods={"GET"})
 */
class FindMyScholarshipAction extends AbstractFOSRestController
{
    /**
     * @param Request $request
     * @param RegistrationManager $registrationManager
     * 
     * @return [JsonResonse]
     */
    public function __invoke(Request $request, ScholarshipRepository $manager)
    {

        
        $sContext = $request->query->get('context', 'list');
        $aContext = is_array($sContext) ? $sContext : [$sContext];
        $context  = (new Context())->setGroups($aContext);
        

        $sOrder        = $request->query->get('order', false);
        $aOrder        = $sOrder ? json_decode($sOrder, true) : false;
        $order         = $aOrder ? [$aOrder['sort'] => $aOrder['order']] : [];
        $organization = $this->getUser();
        $result       = $manager->findByRelatedOrganization($organization, $order);
        
        $view = $this->view([
            "scholarships"    =>  $result
        ]);


        $view->setContext($context);
        return $this->handleView( $view,Response::HTTP_OK);
    }
}
