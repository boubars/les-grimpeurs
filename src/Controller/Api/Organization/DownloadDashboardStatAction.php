<?php

namespace App\Controller\Api\Organization;

use App\Service\Utilis\Base64Converter;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/organization/download/stat", name="organization_stat_download", methods={"POST", "GET"})
 */
class DownloadDashboardStatAction extends AbstractController
{
    public function __invoke(Request $request)
    {
        $options = new Options();
        $options->set('defaultFont', 'Roboto');

        /*** dev options */
        $options->set("debugLayout");
        $options->set("debugLayoutBlocks");
        $options->set("debugLayoutPaddingBox");
        /*** end dev options */


        $dompdf = new Dompdf($options);
    
        
        $logo = Base64Converter::toBase64(
            sprintf( "%s/assets/vue/assets/img/logo_grimp.png", $this->getParameter('project_dir')),
            'png'
        );
        $fontDir = $this->getParameter('project_dir')."/assets/vue/assets/css/fonts";


        $request =json_decode($request->getContent(), true);
        $series    = $request['series'];

        usort($series, function($a, $b){
            return $a["order"] - $b["order"];
        });
        


        
        $params = [
            "logo"    => $logo,
            "fontDir" => $fontDir,
            "series"  => $series
        ];

        $html =$this->renderView('pdf/organization/dashboardstat.html.twig', $params);
       

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        
        $dompdf->stream("organization-dashboard.pdf", [
            "Attachment" => true
        ]);
    }

   
}
