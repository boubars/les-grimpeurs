<?php

namespace App\Controller\Api\Organization;

use App\Repository\Organization\OrganizationRepository;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/organization", methods={"GET"})
 */
class GetOrganizationListAction extends AbstractFOSRestController
{
    public function __invoke(Request $request, OrganizationRepository $manager)
    {
        $type = $request->query->get('institutionType');
        $type = $type ? json_decode($type, true) : false;

        $view = $this->view(array(
            "organizations"    =>  $manager->getList($type)
        ));
       
        
        $context = new Context();
        $context->setGroups(['list']);
        $view->setContext($context);
        return $this->handleView($view);
        return $this->handleView($view ,Response::HTTP_OK); 
    }
}
