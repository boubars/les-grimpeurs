<?php

namespace App\Controller\Api\Organization;

use App\Repository\Application\ApplicationRepository;
use App\Repository\User\SeenScholarshipRepository;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StatsOrganizationAction extends AbstractFOSRestController
{
    /**
     * @param ApplicationRepository $applicationRepository
     * @param SeenScholarshipRepository $seenScholarshipRepository
     * @Route("/api/organization/statistic/get", name="application_organization_stats",  methods={"GET"})
     * @return Response
     */
    public function getOneBySlug(ApplicationRepository $applicationRepository, SeenScholarshipRepository $seenScholarshipRepository)
    {
        $aApplication = $applicationRepository->countApplicationAndType($this->getUser()->getId());
        $aSeen = $seenScholarshipRepository->countSeenByOrganization($this->getUser()->getId());
        $total = $aApplication['total'];
        $accepted = $total > 0 ? $aApplication['sub_total'] * 100/$total : 0;
        $seen = $aSeen['nb'] > 0 ? $total*100/$aSeen['nb'] : 0;
        $view = $this->view(array(
            "accepted"    =>  $accepted,
            "by_seen"     => $seen,
        ));
      
        $context = new Context();
        $context->setGroups(['details']);
        $view->setContext($context);
        return $this->handleView($view);
    }
}
