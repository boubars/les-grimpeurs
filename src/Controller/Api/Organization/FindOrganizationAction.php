<?php

namespace App\Controller\Api\Organization;

use App\Repository\Organization\OrganizationRepository;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FindOrganizationAction extends AbstractFOSRestController
{
    /**
     * @Route("/api/organization/{slug}", methods={"GET"})
     */
    public function getOneBySlug(string $slug, OrganizationRepository $manager)
    {
          
        $view = $this->view(array(
            "organization"    =>  $manager->findOneBySlug($slug)
        ));
      
        $context = new Context();
        $context->setGroups(['details']);
        $view->setContext($context);
        return $this->handleView($view);
        return $this->handleView($view ,Response::HTTP_OK); 
    }
}
