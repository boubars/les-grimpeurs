<?php 
namespace App\Controller\Api\Organization;

use App\Repository\Application\ApplicationRepository;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;

/**
 * @IsGranted("ROLE_ORGANIZATION")
 * @Route("/api/organization/application/list", name="application_organization_list", methods={"GET"})
 */

class GetApplicationAction extends AbstractFOSRestController {

    /**
     * Invokable application search method
     *
     * @param Request $request
     * @param ApplicationRepository $applicationManager
     * @return JsonResponse
     */
    public function __invoke(Request $request, ApplicationRepository $applicationManager){
        $user           = $this->getUser();
        $status         = $request->query->get('status', false);
        $scholarship    = $request->query->get('scholarship', false);
        $max            = $request->query->get('max', false);
        $form           = $request->query->get('form', false);
        $aOrder         = json_decode($request->query->get('order', false), true) ?? false;
        
        if($aOrder){
            $aOrder = [
                'order'     => $aOrder['sort'],
                'direction' => $aOrder['order'],
            ];
        }
        
        $params = [
            'organization' => $user,
            'status'       => $status,
            'scholarship'  => $scholarship,
            'max'          => $max,
            'form'         => $form
        ];

        list($allApplication, $count) = $applicationManager->getByOrganization($params, $aOrder);
        
        $params = [
            'organization' => $user,
            'status'       => false,
            'scholarship'  => false,
            'max'          => $max
        ];

        $total = $applicationManager->getByOrganization($params, $aOrder, true);

        $context = new Context();
        $context->setGroups(['details','student']);

        $view = $this->view(array(
            "applications"    =>  $allApplication,
            "nb"    =>  $count,
            "total"    =>  $total,
        ));

        $view->setContext($context);

        return $this->handleView( $view,Response::HTTP_OK);
    }

}

