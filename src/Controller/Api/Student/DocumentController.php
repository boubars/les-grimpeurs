<?php 
namespace App\Controller\Api\Student;

use App\Entity\Student\Document;
use App\Entity\Student\Student;
use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Mime\FileinfoMimeTypeGuesser;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * @IsGranted("ROLE_STUDENT")
 * @Route("/api/student/document/")
 */

class DocumentController extends AbstractFOSRestController {

    /**
     * @return JsonResponse|Response
     * @Rest\Route("list", name="document_student_list", methods={"GET"}))
     */
    public function __invoke(){

        $user = $this->getUser();
        if (false == $user instanceof Student) {
            return new JsonResponse([
                'success'=> false,
                'error'  => 'Not connected'
            ], Response::HTTP_FORBIDDEN);
        }
        $allDocuments = $user->getDocuments();
        
        $context = new Context();
        $context->setGroups(['details','scholarship']);

        $view = $this->view(array(
            "documents"    =>  $allDocuments
        ));

        $view->setContext($context);

        return $this->handleView( $view,Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return BinaryFileResponse|JsonResponse
     * @Rest\Route("download", name="document_student_download", methods={"GET"}))
     */
    public function delete(Request $request, EntityManagerInterface $entityManager){

        $user = $this->getUser();
        if (false == $user instanceof Student) {
            return new JsonResponse([
                'success'=> false,
                'error'  => 'Not connected'
            ], Response::HTTP_FORBIDDEN);
        }
        $id = $request->query->get('id', null);
        if (is_null($id)) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Bad request"
            ], Response::HTTP_BAD_REQUEST);
        }
        $document = $entityManager->getRepository(Document::class)->find($id);
        $filename = $document->getFilename();
        $path = $this->getParameter('document_dir');
        $response = new BinaryFileResponse($path.$filename);
        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();
        if($mimeTypeGuesser->isGuesserSupported()){
            $response->headers->set('Content-Type', $mimeTypeGuesser->guessMimeType($path.$filename));
        }else{
            $response->headers->set('Content-Type', 'text/plain');
        }

        // Set content disposition inline of the file
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );

        return $response;
    }

}

