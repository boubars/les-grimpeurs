<?php
namespace App\Controller\Api\Student;

use App\Entity\Student\Student;
use App\Repository\Student\StudentRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/student/find", methods={"get"})
 */
class FindStudentAction extends AbstractFOSRestController
{
    /** @var StudentRepository  */
    private $studentManager;

    /** @var EntityManager */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->studentManager = $this->em->getRepository(Student::class);
    }

    public function __invoke( Request $request ){

        $maxResult = $request->query->get("max");

        $nb_interested = 0;
        if ($maxResult == 1) {
            $count = 1;
            $result = $this->findOneBy($request);
        } else {
            $searchCriteria = $request->query->get('criteria')?? '';
            $searchCriteria = json_decode($searchCriteria, true);
            $disciplineIds = $disciplines = [];
            if(!empty($searchCriteria['disciplines'])) {
                $disciplineIds = array_map(function ($discipline) {
                    return $discipline['id'];
                },
                $searchCriteria['disciplines']);
            }

            $countryIds = $countries =[];
            if (!empty($searchCriteria['country'])) {
                $countryIds = array_map(
                    function ($country) {
                        return $country['id'];
                    },$searchCriteria['country']);
            }
            $desiredDountryIds =[];
            if(!empty($searchCriteria['countryd'])) {
                $desiredDountryIds = array_map(
                    function ($country) {
                        return $country['id'];
                    },$searchCriteria['countryd']);
            }

            $levelIds = $levels = [];
            if(!empty($searchCriteria['level'])) {
                $levelIds = array_map(function ($level) {
                    return $level['id'];
                }, $searchCriteria['level']);
            }
            $tag = $request->query->get('tag', null);
            $currentUser = $this->getUser();
            $order = json_decode($request->query->get('order'))?? [];
            $searchSort = $order->sort?? null;
            $searchOrder = $order->order?? null;
            $searchLimit = $request->query->get('limit')?? 10;
            $searchOffset = $request->query->get('offset')?? 0;
            $searchCriteria = $request->query->get('criteria')?? [];
            list($result, $count )= $this->studentManager->findByCriteria($desiredDountryIds, $countryIds, $levelIds, $disciplineIds,
                $searchSort, $searchLimit, $searchOffset, $searchOrder, $currentUser, $tag );
            //Get count interesed
            list(, $nb_interested )= $this->studentManager->findByCriteria($desiredDountryIds, $countryIds, $levelIds, $disciplineIds,
                $searchSort, $searchLimit, $searchOffset, $searchOrder, $currentUser, 'interested' );

        }

        $context = new Context();
        $view = $this->view(array(
            'total' => $count,
            'total_interested' => $nb_interested,
            "students"    =>  $result
        ), null, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);

        $view->setContext($context);

        return $this->handleView( $view, Response::HTTP_OK);
    }

    private function findOneBy(Request $request){
        $criteria = json_decode($request->query->get('criteria'), true);
        return $this->studentManager->findOneBy(
            $criteria
        );
    }
}