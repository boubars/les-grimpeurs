<?php 
namespace App\Controller\Api\Student;

use App\Repository\Scholarship\ScholarshipRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/student/scholarship-stat", name="student_get_stat")
 */
class GetStatAction extends AbstractFOSRestController {
  public function __invoke(ScholarshipRepository $scholarshipManager)
  {
    $student = $this->getUser();
    $stat = $scholarshipManager->getStudentStat($student);
    $view = $this->view(
      [
        "stat"    =>  $stat
      ]);
    return $this->handleView( $view,Response::HTTP_OK);
  }
}
