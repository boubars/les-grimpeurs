<?php
namespace App\Controller\Api\Student;

use App\Repository\Scholarship\ScholarshipRepository;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GetSuggestionScholarshipAction extends AbstractFOSRestController 
{

  /**
   * @Route("/api/student/scholarship-suggestion", methods={"get"})
   */
   public function __invoke(ScholarshipRepository $scholarshipManager)
   {
    
    $student = $this->getUser();
    
    $result = $scholarshipManager->getSuggestion( $student);
    
    $context = new Context();
    $context->setGroups(['details']);

    $view = $this->view(
      [
        "scholarship"    =>  $result
      ]);

    $view->setContext($context);
    return $this->handleView( $view,Response::HTTP_OK);
   }
}