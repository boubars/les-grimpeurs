<?php

namespace App\Controller\Api\Student\Profil;

use App\Entity\Student\Student;
use App\Service\Utilis\Base64Converter;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/api/student/{id}/download/cv", name="student_cv_download", methods={"GET"})
 * @Route("/{_locale}/student/{id}/download/cv",     name="student_cv_download_preview", methods={"GET"})
 */
class DownloadCvAction extends AbstractController
{
    public function __invoke(Student $student, SluggerInterface $slugger)
    {
        $options = new Options();
        $options->set('defaultFont', 'Roboto');
        $options->set('enableCssFloat', true);
        $options->set('debugCss', true);

        // $options->set('debugLayout', 1);
        
        $dompdf = new Dompdf($options);
        
        $data = array(
          'headline' => 'my headline'
        );
        $avatar = $student->getAvatar() ?? false;
        if($avatar){
            $avatar = Base64Converter::toBase64(
                sprintf( "%s/%s", $this->getParameter('avatar_dir'), $avatar),
                'jpg'
            );
        }
       
        $experienceIcon =  Base64Converter::toBase64(
            sprintf( "%s/assets/vue/assets/img/experience-icon.svg", $this->getParameter('project_dir')),
            'svg'
        );

        $formationIcon =  Base64Converter::toBase64(
            sprintf( "%s/assets/vue/assets/img/formation-icon.svg", $this->getParameter('project_dir')),
            'svg'
        );

        $locationIcon = Base64Converter::toBase64(
            sprintf( "%s/assets/vue/assets/img/localisation-map-white.svg", $this->getParameter('project_dir')),
            'svg'
          );

        $phoneIcon = Base64Converter::toBase64(
            sprintf( "%s/assets/vue/assets/img/Tel.svg", $this->getParameter('project_dir')),
            'svg'
          );

        $mailIcon = Base64Converter::toBase64(
            sprintf( "%s/assets/vue/assets/img/icon-email-full.svg", $this->getParameter('project_dir')),
            'svg'
          );

        $webIcon = Base64Converter::toBase64(
            sprintf( "%s/assets/vue/assets/img/web-click-white.svg", $this->getParameter('project_dir')),
            'svg'
          );

        $twitterIcon = Base64Converter::toBase64(
            sprintf( "%s/assets/vue/assets/img/twitter-white.svg", $this->getParameter('project_dir')),
            'svg'
          );

        $linkedinIcon = Base64Converter::toBase64(
            sprintf( "%s/assets/vue/assets/img/linkedin-no-border.svg", $this->getParameter('project_dir')),
            'svg'
          );

        $starIcon = Base64Converter::toBase64(
            sprintf( "%s/assets/vue/assets/img/Etoile.svg", $this->getParameter('project_dir')),
            'svg'
          );

        $fontDir    = $this->getParameter('project_dir')."/assets/vue/assets/css/fonts";
        $viewParams = [
          'student'        => $student,
          'avatar'         => $avatar,
          'fontDir'        => $fontDir,
          'experienceIcon' => $experienceIcon,
          'formationIcon'  => $formationIcon,
          'locationIcon'   => $locationIcon,
          'phoneIcon'      => $phoneIcon,
          'mailIcon'       => $mailIcon,
          'webIcon'        => $webIcon,
          'twitterIcon'    => $twitterIcon,
          'linkedinIcon'   => $linkedinIcon,
          'starIcon'       => $starIcon,
        ];
        $template = 'pdf/student/cv/cv.pdf.html.twig';
        $html     = $this->renderView($template, $viewParams);


        //return $this->render($template, $viewParams);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $fileName = $slugger->slug(
          strtolower(sprintf("cv-%s-%s.pdf", $student->getFirstname(), $student->getLastname()))
        );
        $dompdf->stream($fileName, [
            "Attachment" => true
        ]);
    }
}
