<?php
namespace App\Controller\Api\Student\Profil;

use App\Repository\Student\StudentLanguageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/api/student/profil/language/remove", methods={"DELETE"})
 * @IsGranted("ROLE_STUDENT")
 */
class RemoveLanguageAction extends AbstractController
{

    public function __invoke(
      Request $request, 
      EntityManagerInterface $entityManager, StudentLanguageRepository $langRepository
    ){

      $request =json_decode($request->getContent(), true);
      $lang= $langRepository->findOneById($request['id']);
      $statRemove  = false;
      try {
        $entityManager->remove($lang);
        $entityManager->flush();
        $statRemove  = true;
      }catch(Exception $e) {
        echo $e;
      };
      return new JsonResponse(
        [
              "success"=> $statRemove
        ]
      );
    }
}