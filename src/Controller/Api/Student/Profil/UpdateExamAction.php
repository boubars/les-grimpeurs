<?php
namespace App\Controller\Api\Student\Profil;

use App\Entity\Student\Exam;
use App\Repository\Student\ExamRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Exception;

/**
 * @Route("/api/student/profil/exam/update", methods={"POST"})
 * @IsGranted("ROLE_STUDENT")
 */
class UpdateExamAction extends AbstractController
{
  
    public function __invoke(
      Request $request, 
      EntityManagerInterface $entityManager,
      ExamRepository $examManager
      ){

      $student = $this->getUser();
      $request =json_decode($request->getContent(), true);
      

    
      /*** create distinction  object */
      $newExam = ($request['id'] == null ) ? new Exam() : $examManager->findOneBy(['id' => $request['id']]);

      $newExam->setName($request['name'])
      ->setResult($request['result'])
      ->setEmissionDate(new \DateTime($request['emissionDate']))
      ->setDescription($request['description']);

      /** add new exam to current user profil*/
      $student->addExam($newExam);
      $status = false;
      try {
        $entityManager->persist($student);
        $entityManager->flush();
        $status = true;   
      }catch(Exception $e )  {
        echo $e;
      }
    
      return new JsonResponse(
        [
              "success"=> $status
        ]
      );
     }

   

}