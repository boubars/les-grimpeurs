<?php
namespace App\Controller\Api\Student\Profil;

use App\Entity\Student\Exam;
use App\Entity\Student\SearchPreference;
use App\Repository\Student\ExamRepository;
use App\Repository\Student\SearchPreferenceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/api/student/profil/preference/edit", methods={"POST"})
 * @IsGranted("ROLE_STUDENT")
 */
class EditSearchPreferenceAction extends AbstractController
{
  
    public function __invoke(Request $request, EntityManagerInterface $entityManager, SearchPreferenceRepository $preferenceManager ){

      $student = $this->getUser();
      $request = json_decode($request->getContent(), true);
      
      $preferences = $student->getSearchPreferences() ?? new SearchPreference();

      $preferences->setFormationByProfil($request['formation_by_profil'])
      ->setScholashipByProfil($request['scholaship_by_profil'])
      ->setScholarshipByDiscipline($request['scholarship_by_discipline'])
      ->setNewGrimpeurByProfil($request['new_grimpeur_by_profil'])
      ->setNewGrimpeur($request['new_grimpeur'])
      ->setFormationByProfil($request['formation_by_profil'])
      ->setOpportunityByProfil($request['opportunity_by_profil']);
      /*** create distinction  object */
      
      $student->setSearchPreferences($preferences);

      $entityManager->persist($student);
      $entityManager->flush();
          
      return new JsonResponse(
        [
              "success"=> true
        ]
      );
     }

   

}
