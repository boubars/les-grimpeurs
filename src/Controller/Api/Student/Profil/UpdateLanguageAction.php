<?php
namespace App\Controller\Api\Student\Profil;

use App\Entity\Common\Country;
use App\Entity\Student\StudentLanguage;
use App\Repository\Common\CountryRepository;
use App\Repository\Common\LanguageRepository;
use App\Repository\Student\LanguageLevelRepository;
use App\Repository\Student\StudentLanguageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Exception;

/**
 * @Route("/api/student/profil/language/update", methods={"POST"})
 * @IsGranted("ROLE_STUDENT")
 */
class UpdateLanguageAction extends AbstractController
{
     
    public function __invoke(
      Request $request, 
      EntityManagerInterface $entityManager, 
      LanguageRepository $languageManager,
      LanguageLevelRepository $levelManager,
      StudentLanguageRepository $studentLanguageRepository
    ){
      $student = $this->getUser();
      $request =json_decode($request->getContent(), true);
      // transform posted data
      $postedLanguage = $languageManager->findOneById($request['language']['id']);
      $postedLevel    = $levelManager->findOneById($request['level']['id']);

      // Create a new StudentLanguage object
      $studentLanguage = ($request['id'] == null) ? new StudentLanguage() : $studentLanguageRepository->findOneBy(['id' => $request['id']]);
      $studentLanguage->setLanguage($postedLanguage) ;
      $studentLanguage->setLevel($postedLevel);

      // add student language
      $status = false;
      try {
        $student->addLanguage($studentLanguage);
        $entityManager->persist($student);
        $entityManager->flush();
        $status = true ;
      }catch(Exception $e) {
        echo $e ;
      }
    
          
      return new JsonResponse(
        [
              "success"=> $status
        ]
      );
     }

     private function getCountry($array){
          $country = $this->countryManager->findOneBy($array);

          if(!$country){
               $country = new Country();
               $country->setCode($array['code']);
               $country->setName($array['name']);
          }
          return $country;

     }

     

}