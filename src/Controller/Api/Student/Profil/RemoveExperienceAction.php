<?php
namespace App\Controller\Api\Student\Profil;

use App\Repository\Student\ExperienceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/api/student/profil/experience/remove", methods={"DELETE"})
 * @IsGranted("ROLE_STUDENT")
 */
class RemoveExperienceAction extends AbstractController
{

    public function __invoke(
      Request $request, 
      EntityManagerInterface $entityManager, ExperienceRepository $parcoursRepository
    ){

      $request =json_decode($request->getContent(), true);
      $parcours= $parcoursRepository->findById($request['id']);

      $statRemove  = false;
      try {
        $entityManager->remove($parcours[0]);
        $entityManager->flush();
        $statRemove  = true;
      }catch(Exception $e) {
        echo $e;
      };
      return new JsonResponse(
        [
              "success"=> $statRemove
        ]
      );
    }
}