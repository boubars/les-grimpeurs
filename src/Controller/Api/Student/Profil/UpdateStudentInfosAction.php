<?php
namespace App\Controller\Api\Student\Profil;

use App\Entity\Common\Country;
use App\Entity\Common\Level;
use App\Entity\Common\Nationality;
use App\Entity\Common\SocialNetwork;
use App\Entity\Discipline\Category as Discipline;
use App\Entity\Student\Student;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/api/student/profil/infos", methods={"POST"})
 * @IsGranted("ROLE_STUDENT")
 */
class UpdateStudentInfosAction extends AbstractController
{

     /**
      * Undocumented variable
      *
      * @var [EntityManagerInterface]
      */
     private  $entityManager;
    
     /**
     * Undocumented variable
     *
     * @var [Student]
     */
     private  $student;

     public function __construct(EntityManagerInterface $entityManager)
     {
          $this->entityManager =  $entityManager;
     }

     public function __invoke(Request $request){
          $this->student = $this->getUser();
          
          $data =json_decode($request->getContent(), true);
          $context = $data['context'];

          switch ($context) {
               case 'infos':
                    $this->updateInfos($data['profil']);
                    break;
               
               case 'basic_infos':
                    $this->updateBasicInfos($data['profil']);
                    break;
               
               case 'desired_infos':
                    $this->updateDesiredInfos($data['profil']);
                    break;
               
               case 'social_network_infos':
                    $this->UpdateSocialNetwork($data['profil']);
                    break;
          }

          $this->entityManager->persist($this->student);
          $this->entityManager->flush();    
          
          return new JsonResponse(
               [
                    "success"=> true
               ]
          );

     }

     private function updateInfos($data){
          /*** Mapped nationality list */
          $nationalities = $data['nationalities'];
          $this->student->setNationalities(new ArrayCollection());
          foreach ($nationalities as  $nationality) {
               $n = $this->entityManager->getRepository(Nationality::class)->findOneById($nationality['id']);
               if($n){
                    $this->student->addNationality($n);
               }
          }

          /*** Mapped discipline */
          $disciplines = $data['disciplines'];
          $this->student->setDisciplines(new ArrayCollection());
          foreach ($disciplines as  $discipline){
               $d = $this->entityManager->getRepository(Discipline::class)->findOneById($discipline['id']);
               if($d){
                    $this->student->addDiscipline($d);
               }
          }  

          /**** Mapped level */
          $level = $data['current_level'];
          $level = $this->entityManager->getRepository(Level::class)->findOneById($level['id']);
          if($level){
               $this->student->setCurrentLevel($level);
          }
     }
     private function updateBasicInfos($data){
          /*** Mapped country */
          $country = $data['country'];
          $country = $this->entityManager->getRepository(Country::class)->findOneById($country['id']);
          $this->student->setCountry($country);

          $this->student
          ->setFirstname($data['firstname'])
          ->setLastname($data['lastname'])
          ->setSexe($data['sexe'])
          ->setDateOfBirth(new \DateTime($data['date_of_birth']))
          ->setPhone($data['phone'])
          ->setAdress($data['adress'])
          ->setUpdatedAt(new \DateTime());
          
     }
     private function updateDesiredInfos($data){
          /*** Mapped country list */
          $countries = $data['desired_countries'];
          $this->student->setDesiredCountries(new ArrayCollection());
          foreach ($countries as  $country) {
               if(!empty($country['id'])) {
                    $n = $this->entityManager->getRepository(Country::class)->find($country['id']);
                    if ($n instanceof Country) {
                         $this->student->addDesiredCountry($n);
                    }
               }
          }

          /*** Mapped discipline */
          $disciplines = $data['desired_disciplines'];
          $this->student->setDesiredDisciplines(new ArrayCollection());
          foreach ($disciplines as  $discipline){
               $d = $this->entityManager->getRepository(Discipline::class)->findOneById($discipline['id']);
               if($d){
                    $this->student->addDesiredDiscipline($d);
               }
          }  

          /**** Mapped level */
          $level = $data['desired_study_level'];
          $level = $this->entityManager->getRepository(Level::class)->findOneById($level['id']);
          if($level){
               $this->student->setDesiredStudyLevel($level);
          }
     }

     private function UpdateSocialNetwork($profil){
          $data = $profil['social_network'];

          $socialNetwork = new SocialNetwork();
          $socialNetwork
          ->setFacebook($data['facebook'] ?? '')
          ->setInstagram($data['instagram'] ?? '')
          ->setYoutube($data['youtube'] ?? '')
          ->setTwitter($data['twitter']?? '')
          ->setLinkedin($data['linkedin']?? '');

          $this->student->setSocialNetwork($socialNetwork);
          $this->student->setSiteUrl($profil['site_url'] ?? '');
     }

  
}