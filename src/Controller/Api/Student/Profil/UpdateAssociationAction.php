<?php
namespace App\Controller\Api\Student\Profil;

use App\Entity\Student\Organization\Organization;
use App\Entity\Student\Association;
use App\Repository\Student\Organization\OrganizationRepository;
use App\Repository\Organization\OrganizationTypeRepository;
use App\Repository\Student\AssociationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Exception;

/**
 * @Route("/api/student/profil/association/update", methods={"POST"})
 * @IsGranted("ROLE_STUDENT")
 */
class UpdateAssociationAction extends AbstractController
{
  
    public function __invoke(
      Request $request, 
      EntityManagerInterface $entityManager,
      OrganizationRepository $organizationManager,
      OrganizationTypeRepository $typeManager,
      AssociationRepository $association
      ){

      $student = $this->getUser();
      $request =json_decode($request->getContent(), true);
      
      
      /**** Maped association organisme */
      $postedOrganization = $request['organization'];
      $organizationType = $typeManager->findOneBy($postedOrganization['type']);
      
      $organization = $organizationManager->findOneBy([
        "name" => $postedOrganization['name'],
        "type" => $organizationType
        ]
      );

      $organization = $organization ? $organization : (new Organization)->setName($request['organization']['name'])->setType($organizationType);


  
      /*** create association object */
      $newAssociation = ($request['id'] == null ) ? new Association() : $association->findOneBy(['id' => $request['id']]) ;
      $newAssociation->setOrganization($organization)
      
      
      ->setresResponsability($request['responsability'])
      ->setCurrentPosition($request['currentPosition'])
      ->setStartDate(new \DateTime($request['startDate']))
      ->setEndDate(new \DateTime($request['endDate']))
      ->setDescription($request['description']);

      /** add new activity to current user profil*/
      $status = false;
      try {
        $student->addAssociation($newAssociation);
        $entityManager->persist($student);
        $entityManager->flush();
        $status = true ;
      }catch(Exception $e) {}
  
          
      return new JsonResponse(
        [
              "success"=> $status
        ]
      );
     }

   

}