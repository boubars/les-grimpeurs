<?php
namespace App\Controller\Api\Student\Profil;

use App\Repository\Student\StudentRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/student/profil/position", methods={"GET"})
 */
class GetCurrentPositionAction extends AbstractController
{
   
    public function __invoke(Request $request, StudentRepository $manager){
        
        $sid = $request->query->get('student');
        $student = $manager->findOneById($sid);
        
        if(!$student){
            return new JsonResponse([
                'success' => false,
                'message' => "Object student with an ID $sid  not found"
            ]);
        }


        /*** get latest know experience */
        $currentExperience   = $this->getCurrentExperience($student);
        if($currentExperience){
            return new JsonResponse([
                'post'        => $currentExperience->getTitle(),
                'institution' => $currentExperience->getCompany(),
                'place'       => $currentExperience->getPlace(),
                'since'       => $currentExperience->getStartDate()->format("M Y")
            ]);
        }

        /*** get last known formation */
        $currentFormation = $this->getCurrentFormation($student);
        if($currentFormation){
            return new JsonResponse([
                'post'        => 'student',
                'institution' => $currentFormation->getSchool()->getName(),
                'place'       => $currentFormation->getSchool()->getCountry()->getName(),
                'since'       => $currentFormation->getYearStart()
            ]);
        }

        /*** get latest experience when no current postion */
        $lastExperience      = $this->getLatestExperience($student);
        if($lastExperience){
            return new JsonResponse([
                'post'        => $lastExperience->getTitle(),
                'institution' => $lastExperience->getCompany(),
                'place'       => $lastExperience->getPlace(),
                'since'       => $lastExperience->getStartDate()->format("M Y"),
                'endDate'     => $lastExperience->getEndDate()->format("M Y")
            ]);
        }

        /*** get latest formation when no current postion */
        $lastFormation = $this->getLatestFormation($student);
        if($lastFormation){
            return new JsonResponse([
                'post'        => 'student',
                'institution' => $lastFormation->getSchool()->getName(),
                'place'       => $lastFormation->getSchool()->getCountry()->getName(),
                'since'       => $lastFormation->getYearStart(),
                'endDate'     => $lastFormation->getYearEnd()
            ]);
        }


        /*** otherwise return null */
        return new JsonResponse([]);
      

     }

     private function getCurrentFormation($student){
        $formations = $student->getFormations();

        if(!$formations)
            return false;

        foreach ($formations as $key => $formation) {
            if($formation->isCurrentPosition()){
                    return $formation;
                break;
            }
        }
        return false;
     }

     private function getLatestFormation($student){
        $formations = $student->getFormations();

        if(!$formations || empty($formations))
            return false;

        $latest = $formations[0];
        foreach ($formations as $key => $formation) {
            if($latest->getYearEnd() < $formation->getYearEnd()){
                $latest = $formation;
            }
        }
        return $latest;
     }

     private function getLatestExperience($student){
        $experiences = $student->getExperiences();

        if(!$experiences || empty($experiences))
            return false;

        $latest = $experiences[0];
        foreach ($experiences as $key => $experience) {
            
            if($latest->getEndDate() < $experience->getEndDate()){
                $latest = $experience;
            }
        }
        return $latest;
     }

     private function getCurrentExperience($student){
        $experiences = $student->getExperiences();

        if(!$experiences || empty($experiences))
            return false;

            foreach ($experiences as $key => $experience) {
            if($experience->isCurrentPosition()){
                    return $experience;
                break;
            }
        }
        return false;
     }



     private function getLatestActivity($student){
        $experiences = $student->extraActivities();

        if(!$experiences || empty($experiences))
            return false;

        $latest = $experiences[0];
        foreach ($experiences as $key => $experience) {
            if($latest->getEndDate() < new $experience->getEndDate()){
                $latest = $experience;
            }
        }
        return $latest;
     }

     private function getCurrentActivity($student){
        $experiences = $student->extraActivities();

        if(!$experiences || empty($experiences))
            return false;

            foreach ($experiences as $key => $experience) {
            if($experience->isCurrentPosition()){
                    return $experience;
                break;
            }
        }
        return false;
     }






}