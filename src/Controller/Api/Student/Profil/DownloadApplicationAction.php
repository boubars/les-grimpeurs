<?php

namespace App\Controller\Api\Student\Profil;

use App\Entity\Student\Student;
use App\Service\Utilis\Base64Converter;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/api/student/apply/download", name="student_application_download", methods={"GET"})
 */
class DownloadApplicationAction extends AbstractController
{
    public function __invoke()
    {
        $options = new Options();
        $options->set('defaultFont', 'Roboto');
        // $options->set('debugLayout', 1);
        
        $dompdf = new Dompdf($options);

        $html =$this->renderView('pdf/student/application/document.pdf.html.twig');

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        
        $dompdf->stream("user-candidature-cv.pdf", [
            "Attachment" => true
        ]);
    }
}
