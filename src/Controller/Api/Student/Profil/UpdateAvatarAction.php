<?php
namespace App\Controller\Api\Student\Profil;

use App\Controller\FileUploaderAction;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;


/**
 * @Route("/api/student/profil/avatar", methods={"POST"})
 * @IsGranted("ROLE_STUDENT")
 */
class UpdateAvatarAction extends FileUploaderAction
{
    public function __invoke(Request $request, EntityManagerInterface $entityManager)
    {
        $request->request;
        $student = $this->getUser();

        $file = $request->files->get('avatar');
        if(!$file){
            return new JsonResponse([
                'success'=> false,
                'error'  => 'Image non trouvé'
            ]);
        }
        
        $newFilename = $this->uploadDocument($file, 'avatar_dir'); 
        
        $student->setAvatar($newFilename);
        
        $entityManager->persist($student);
        $entityManager->flush();

        return new JsonResponse([
            'success' => true,
            'avatar'  => $student->getAvatar()
            ]);
        }
    

}