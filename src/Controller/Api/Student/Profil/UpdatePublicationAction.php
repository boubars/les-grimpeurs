<?php
namespace App\Controller\Api\Student\Profil;

use App\Manager\Student\PublicationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/api/student/profil/publication/update", methods={"POST"})
 * @IsGranted("ROLE_STUDENT")
 */
class UpdatePublicationAction extends AbstractController
{
  
    private $publicationManager;


    public function __construct(PublicationManager $publicationManager)
    {
        $this->publicationManager = $publicationManager;
    }


    public function __invoke(Request $request){

      $updateFormation = $this->publicationManager->savePublication($request, $this->getUser()) ;
    
      return new JsonResponse(
        
              $updateFormation
        
      );
     }

}