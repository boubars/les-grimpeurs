<?php
namespace App\Controller\Api\Student\Profil;

use App\Repository\Student\FormationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/api/student/profil/formation/remove", methods={"DELETE"})
 * @IsGranted("ROLE_STUDENT")
 */
class RemoveFormationAction extends AbstractController
{

    public function __invoke(
      Request $request, 
      EntityManagerInterface $entityManager, FormationRepository $formationRepository
    ){

      $request =json_decode($request->getContent(), true);
      $formation = $formationRepository->findById($request['id']);
      $statRemove  = false;
      try {
        $entityManager->remove($formation[0]);
        $entityManager->flush();
        $statRemove  = true;
      }catch(Exception $e) {
          echo $e;
      };
      return new JsonResponse(
        [
              "success"=> $statRemove
        ]
      );
    }
}