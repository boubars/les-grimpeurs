<?php
namespace App\Controller\Api\Student\Profil;

use App\Entity\Common\Country;
use App\Entity\Common\Grade;
use App\Entity\Discipline\Discipline;
use App\Entity\Student\Formation;
use App\Entity\Student\School;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/api/student/profil/formation/update", methods={"POST"})
 * @IsGranted("ROLE_STUDENT")
 */
class UpdateFormationAction extends AbstractController
{
    /**
     * Undocumented variable
     *
     * @var [SchoolRepository]
     */
    private $schoolManager;     

    /**
     * Undocumented variable
     *
     * @var [GradeRepository]
     */

    private $gradeManager;    
    /**
     * Undocumented variable
     *
     * @var [DisciplineRepository]
     */ 
    private $studyFieldManager;  
    
    /**
     * Undocumented variable
     *
     * @var [FormationRepository]
     */
    private $formationRepository;  

    /**
     * Undocumented variable
     *
     * @var [CountryRepository]
     */
    private $countryManager;

    /**
     * Undocumented variable
     *
     * @var [EntityManagerInterface]
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
      $this->entityManager       = $entityManager;
      $this->schoolManager       = $entityManager->getRepository(School::class);
      $this->gradeManager        = $entityManager->getRepository(Grade::class);
      $this->studyFieldManager   = $entityManager->getRepository(Discipline::class);
      $this->formationRepository = $entityManager->getRepository(Formation::class);
      $this->countryManager      = $entityManager->getRepository(Country::class);
    }


    public function __invoke( Request $request){
      $student = $this->getUser();

      $request =json_decode($request->getContent(), true);
      
      /** map school */
      $school = $this->getPostedSchool($request['school']);
      
      /** map grade */
      $grade = $this->getPostedGrade($request['grade']);

      /** map studyField */
      $studyField = $this->getPostedStudyField($request['studyField']);
      
      /** Create formation object */
      $formation = ($request['id'] == null ? new Formation() : $this->formationRepository->findOneBy(['id' => $request['id']]) ) ;
      

      $formation->setSchool($school) ; 
      
      $formation->setGrade($grade) ; 
      $formation->setStudyField($studyField);
      $formation->setYearStart($request['yearStart']['value'] ?? null);
      $formation->setYearEnd($request['yearEnd']['value'] ?? null);
      $formation->setCurrentPosition($request['currentPosition']?? false);


      $formation->setObtainedResult($request['obtainedResult']);
      $formation->setActivities($request['activities']);
      $formation->setDescription($request['description']);

      // Update student infos
      $status = false;
      try{
        $student->addFormation($formation);

        $this->entityManager->persist($student);
        $this->entityManager->flush();
        $status = true ;
      }catch(Exception $e) {
        echo $e ;
      }
        
      return new JsonResponse(
        [
              "success"=> $status
        ]
      );
    }

    private function getPostedSchool(?array $data){
      $school = $this->schoolManager->findOneByName($data['name']);
      if($school){
        return $school;
      }
      $country = $this->countryManager->findOneById($data['country']['id']);
      return (new School())->setName($data['name'])->setCountry($country);

    }

    private function getPostedGrade($name){
      $grade = $this->gradeManager->findOneByName($name);
      $grade = $grade ? $grade : (new Grade())->setName($name);
      return $grade; 
    }

    private function getPostedStudyField($post){

      $data = $this->studyFieldManager->findOneById($post["id"]);
      return $data ? $data : null; 
    }

}