<?php
namespace App\Controller\Api\Student\Profil;

use App\Entity\Student\Disctinction;
use App\Repository\Student\DisctinctionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Exception;

/**
 * @Route("/api/student/profil/distinction/update", methods={"POST"})
 * @IsGranted("ROLE_STUDENT")
 */
class UpdateDistinctionAction extends AbstractController
{
  
    public function __invoke(
      Request $request, 
      EntityManagerInterface $entityManager,
      DisctinctionRepository $distinctionManager
      ){

      $student = $this->getUser();
      $request =json_decode($request->getContent(), true);
      

      /**** Maped related distionction */
      $related = isset($request['related']) ? $distinctionManager->findOneBy($request['related']) : false;
      
      /*** create distinction  object */
      $newDistionction = ($request['id'] == null) ? new Disctinction() : $distinctionManager->findOneBy(['id' => $request['id']]);

      if($related){
        $newDistionction->setRelated($related->getId());
      }
      $newDistionction->setTitle($request['title'])
      ->setAwardingEntity($request['awardingEntity'])
      ->setEmissionDate(new \DateTime($request['emissionDate']))
      ->setDescription($request['description']);

      /** add new distinction to current user profil*/
      $student->addDistinction($newDistionction);

      $status = false;

      try {
        $entityManager->persist($student);
        $entityManager->flush();
        $status = true ;
    

      }catch(Exception $e) {

      }

      return new JsonResponse(
        [
              "success"=> $status
        ]
      );
     }

   

}