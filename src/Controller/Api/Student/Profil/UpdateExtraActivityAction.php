<?php
namespace App\Controller\Api\Student\Profil;

use App\Entity\Student\Activity\Activity;
use App\Entity\Student\Organization\Organization;
use App\Repository\Student\Organization\OrganizationRepository;
use App\Repository\Organization\OrganizationTypeRepository;
use App\Repository\Student\Activity\ActivityCauseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Repository\Student\Activity\ActivityRepository;
use Exception;

/**
 * @Route("/api/student/profil/extra-activity/update", methods={"POST"})
 * @IsGranted("ROLE_STUDENT")
 */
class UpdateExtraActivityAction extends AbstractController
{
  
    public function __invoke(
      Request $request, 
      EntityManagerInterface $entityManager,
      OrganizationRepository $organizationManager,
      OrganizationTypeRepository $orgTypeManager,
      ActivityRepository $extraActivityRepository,
      ActivityCauseRepository $causeManager
    ){

      $student = $this->getUser();
      $request =json_decode($request->getContent(), true);
      
      /** mapped Maped cause */
      $cause = $causeManager->findOneById($request['cause']['id']);

      /**** Maped activity organisme */
      $organization = $organizationManager->findOneByName($request['organization']);
      $organization = $organization ? $organization : (new Organization)->setName($request['organization']);

      /*** create extra-activity object */
      

      $newActivity = ($request['id'] == null) ? new Activity() : $extraActivityRepository->findOneBy(['id' => $request['id'] ]);
      $newActivity->setOrganization($organization)
      ->setCause($cause)
      ->setPlace($request['place'])
      ->setCurrentPosition($request['currentPosition'])
      ->setStartDate(new \DateTime($request['startDate']))
      ->setEndDate(new \DateTime($request['endDate']))
      ->setDescription($request['description']);

      /** add new activity to current user profil*/
      $student->addActivity($newActivity);
      $status = false;
      try {
        $entityManager->persist($student);
        $entityManager->flush();
        $status = true;
            
      }catch(Exception $e) {
        echo $e;
      }
  
      return new JsonResponse(
        [
              "success"=> $status
        ]
      );
     }

   

}