<?php
namespace App\Controller\Api\Student\Profil;

use App\Entity\Student\Experience;
use App\Repository\Student\ExperienceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Exception;

/**
 * @Route("/api/student/profil/experience/update", methods={"POST"})
 * @IsGranted("ROLE_STUDENT")
 */
class UpdateExperienceAction extends AbstractController
{
  
    public function __invoke(
      Request $request, 
      EntityManagerInterface $entityManager,
      ExperienceRepository $experienceRepository
    ){

      $student = $this->getUser();
      $request =json_decode($request->getContent(), true);

      /** Create Experience object */
      $experience = ($request['id'] == null ? new Experience() : $experienceRepository->findOneBy(['id' => $request['id']]) ) ;
      $experience->setTitle($request['title']);
      $experience->setCompany($request['company']);
      $experience->setPlace($request['place']);
      $experience->setCurrentPosition($request['currentPosition']);
      $experience->setStartDate(new \DateTime($request['startDate']));
      $experience->setEndDate(new \DateTime($request['endDate']));
      $experience->setDescription($request['description']);

      // Update student infos

      $status = false;
      try{
        $student->addExperience($experience);
        $entityManager->persist($student);
        $entityManager->flush();
        $status = true ;
      }catch(Exception $e) {
        echo $e ;
      }

      return new JsonResponse(
        [
              "success"=> $status
        ]
      );
     }

   

}