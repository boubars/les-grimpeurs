<?php
/**
 * Created by PhpStorm.
 * User: bouba
 * Date: 17/06/2021
 * Time: 15:28
 */

namespace App\Controller\Api\Student;


use App\Controller\FileUploaderAction;
use App\Entity\Organization\Organization;
use App\Entity\Scholarship\Scholarship;
use App\Entity\Student\Document;
use App\Entity\Student\Student;
use App\Entity\Student\StudentTag;
use App\Repository\Student\DocumentRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/student")
 */
class StudentActionController extends FileUploaderAction
{
    /**
     * @param Request $request
     * @param $tag
     * @param Student $student
     * @param int $type
     * @return Response
     * @throws \Exception
     *
     * @Route("/tag/{tag}/{id}/type/{type}", name="api_student_tag")
     */
    public function new(Request $request, $tag, Student $student, $type = 1): Response
    {
        $user = $this->getUser();
        if (false == $user instanceof Organization) {
            $view = $this->view(array(
                "status"    =>  false
            ), null, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);

            return $this->handleView( $view, Response::HTTP_FORBIDDEN);
        }
        $tagRepos = $this->getDoctrine()->getRepository(StudentTag::class);
        if ('favorite' == $tag) {
            $tagRepos->setFavorites($student, $user, $type);
        } elseif ('remove' == $tag) {
            $tagRepos->setRemoved($student, $user, $type);
        } else {
            throw new \Exception('Invalide tag type');
        }

        $view = $this->view(array(
            "status"    =>  true
        ), null, ['X-Debug-Token'=>true, 'X-Debug-Token-Link' => true]);

        return $this->handleView( $view, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     *
     * @Route("/document/add/new/", name="api_student_add_document", methods={"POST"})
     */
    public function newDocument(Request $request)
    {
        $student = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();

        $id = $request->request->get('id');
        $file = $request->files->get('file');
        if (!$file && is_null($id)) {
            return new JsonResponse([
                'success'=> false,
                'error'  => 'Fichier non trouvé'
            ], Response::HTTP_BAD_REQUEST);
        }
        $title = $request->request->get('title');
        $category = $request->request->get('category');
        $oDocument = is_null($id) ? new Document() : $entityManager->getRepository(Document::class)->find($id);
        $oDocument->setStudent($student)
            ->setTitle($title)
            ->setCategory($category)
            ->setStatus(1)
            ->setDateCreate(new \DateTime());
        if ($file) {
            $newFilename = $this->uploadDocument($file, 'document_dir');
            if ($newFilename) {
                $oDocument->setFilename($newFilename);
            }
        }
        $entityManager->persist($oDocument);
        $entityManager->flush();
        $timestamp = $oDocument->getDateCreate()->getTimestamp();
        $dateformatee = strftime('%d %B %Y', $timestamp); //
        return new JsonResponse([
            'success'=> true,
            'file'  =>  $oDocument->getFilename(),
            'date' =>$dateformatee
        ]);

    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse|Response
     * @Route("/document/delete", name="document_student_remove", methods={"GET"})
     */
    public function deleteDocument(Request $request, EntityManagerInterface $entityManager){

        $user = $this->getUser();
        if (false == $user instanceof Student) {
            return new JsonResponse([
                'success'=> false,
                'message'  => 'Not connected'
            ], Response::HTTP_FORBIDDEN);
        }
        $id = $request->query->get('id', null);
        if (is_null($id)) {
            return new JsonResponse([
                'success' => false,
                'message'    => "Bad request"
            ], Response::HTTP_BAD_REQUEST);
        }
        $document = $entityManager->getRepository(Document::class)->find($id);
        $entityManager->remove($document);
        $entityManager->flush();

        return new JsonResponse([
            'success'=> true,
            'message'  => 'Succefuly removed'
        ]);
    }
}