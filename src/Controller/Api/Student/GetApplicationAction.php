<?php 
namespace App\Controller\Api\Student;

use App\Entity\User\User;
use App\Repository\Application\ApplicationRepository;
use App\Repository\UserRepository;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("is_granted('ROLE_STUDENT') or is_granted('ROLE_ORGANIZATION')")
 * @Route("/api/student/application/list", name="application_student_list", methods={"GET"})
 */

class GetApplicationAction extends AbstractFOSRestController {

     /**
      * Invoked method to fetch the user application list
      *
      * @param Request $request
      * @param ApplicationRepository $applicationManager
      * @param UserRepository $user
      * @return JsonResponse|Response
      */
    public function __invoke(Request $request, ApplicationRepository $applicationManager, UserRepository $userManager){

        $user = $this->getRequestedUser($request, $userManager);
        
        $allApplication = $applicationManager->findBy(['student' => $user]);
        $context = new Context();
        $context->setGroups(['details','scholarship']);
        
        $view = $this->view(array(
            "applications"    =>  $allApplication
        ));
        
        $view->setContext($context);
        
        return $this->handleView( $view,Response::HTTP_OK);
    }

    private function getRequestedUser(Request $request, UserRepository $userManager){
        $userID = $request->query->get('student', false);

        if($userID){
            return $userManager->findOneById($userID);
        }

        return $this->getUser();
    }

}

