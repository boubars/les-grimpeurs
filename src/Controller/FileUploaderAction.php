<?php
namespace App\Controller;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class FileUploaderAction extends AbstractFOSRestController
{
    private $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    protected function uploadDocument(UploadedFile $document, $dirParam )
    {
        $originalFilename = pathinfo($document->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $newFilename = $safeFilename.'-'.uniqid().'.'.$document->guessExtension();
        try {
            $document->move(
                $this->getParameter($dirParam),
                $newFilename
            );
        } catch (FileException $e) {
            return false;
        }
        return $newFilename;
    }
}