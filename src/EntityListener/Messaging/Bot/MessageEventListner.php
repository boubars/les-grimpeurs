<?php
namespace App\EntityListener\Messaging\Bot;

use App\Entity\Messaging\Bot\Message as BotMessage;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class MessageEventListner
{

    private $bus;
    /**
     * Undocumented variable
     *
     * @var ParameterBagInterface
     */
    private $params;
    
    public function __construct(MessageBusInterface $bus, ParameterBagInterface $params){
        $this->bus    = $bus;
        $this->params = $params;
    }

    public function postPersist(BotMessage $message, LifecycleEventArgs $event):void
    {
        $roomID = $message->getConversation()->getSlug();
        $topic  = sprintf(
            "%s/bot-conversation/%s",
            $this->params->get('vue_app_front_url'),
            $roomID
        );

        $update = new Update(
            $topic,
            json_encode([
                'content' => $message->getContent(),
                'owner'   => [
                    'id'   =>  $message->getOwner()->getId(),
                    'uid'  => $message->getOwner()->getUid()
                ],
                'conversation' => [
                    'id'   => $message->getConversation()->getId(),
                    'slug' => $message->getConversation()->getSlug()
                ]
            ])
        );
        $this->bus->dispatch($update);
    }

}