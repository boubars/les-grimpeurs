<?php


namespace App\EntityListener\Scholarship;


use App\Entity\Scholarship\Scholarship;
use App\Entity\Scholarship\ScholarshipAlert;
use App\Manager\Scholarship\ScholarshipAlertManager;
use App\Service\Mailer\Mailer;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class ScholarshipEventListner
{

    private $mailer;
    private $alertManager;
    public function __construct( Mailer $mailer, ScholarshipAlertManager $alertManager)
    {
        $this->mailer = $mailer;
        $this->alertManager = $alertManager;
    }

    public function postPersist(Scholarship $scholarship, LifecycleEventArgs $event):void
    {
        /** @var ScholarshipAlert[] $machedAlets */
        $machedAlets = $this->alertManager->getMatchedAlerts($scholarship);
        if(count($machedAlets)> 0) {
            foreach ($machedAlets as $alert){
                $send = $this->mailer->sendScholarshipAlert($alert, $scholarship);
            }

        }

    }

}