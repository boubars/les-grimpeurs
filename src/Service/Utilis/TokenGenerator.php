<?php


namespace App\Service\Utilis;


class TokenGenerator
{
    
    /**
     * Method generate
     *
     * @param int $length [explicite description]
     *
     * @return void
     */
    public static function generate(int $length = 15)
    {
        return bin2hex(random_bytes($length));
    }
}
