<?php 
namespace App\Service\Utilis;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadedBase64File extends UploadedFile
{

    public function __construct(?string $base64Content, string $originalName = null)
    {
        $filePath     = tempnam(sys_get_temp_dir(), 'UploadedFile');
        $data         = base64_decode($this->getBase64String($base64Content));
        $originalName = $originalName ? $originalName : uniqid('uploaded-file-');

        file_put_contents($filePath, $data);
        $error     = null;
        $mimeType  = null;
        $test      = true;
        parent::__construct($filePath, $originalName, $mimeType, $error, $test);
    }

    private function getBase64String(?string $base64Content)
    {
        if(!$base64Content){
            return "";
        }

        $data = explode(';base64,', $base64Content);

        return $data[1] ?? false;

    }

}