<?php
namespace App\Service\Utilis;

class JsonFileDecoder {

  public static function decode($file){
    $data = file_get_contents($file);
    return json_decode($data, true);
  }
  
}
