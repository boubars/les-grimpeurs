<?php
namespace App\Service\Utilis;

class Base64Converter {

    public static function toBase64( $img_path = false, $img_type = 'png' ){
        if( $img_path ){
            $img_data = fopen ( $img_path, 'rb' );
            $img_size = filesize ( $img_path );
            $binary_image = fread ( $img_data, $img_size );
            fclose ( $img_data );
    
            $img_src = "data:image/".$img_type.";base64,".str_replace ("\n", "", base64_encode($binary_image));
    
            return $img_src;
        }
    
        return false;
    }
}
