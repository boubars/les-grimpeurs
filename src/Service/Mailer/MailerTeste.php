<?php

namespace App\Service\Mailer;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

trait MailerTeste
{
    public function ping(String $email)
    {
        $message = (new TemplatedEmail())
            ->from($this->fromEmail)
            ->to($email)
            ->subject("Email teste : ping")
            ->htmlTemplate('emails/dev/ping.html.twig')
            ->context([]);
        $this->sender->send($message);
    }

}