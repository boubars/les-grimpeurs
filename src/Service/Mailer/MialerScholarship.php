<?php

namespace App\Service\Mailer;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use App\Entity\Scholarship\ScholarshipAlert;
use App\Entity\Scholarship\InformationRequest;
use App\Entity\Scholarship\Scholarship;
use App\Entity\Todo\Todo;

trait MialerScholarship
{

    /**
     *  Send an email to the shcholarship contact
     */
    public function sendScholarshipRequestInfos(InformationRequest $message){
        $scholarship = $message->getScholarship();

        $email = (new TemplatedEmail())
        ->from($this->fromEmail)
        ->to($scholarship->getContactEmail())
        ->subject("Demande des informations | ".$scholarship->getName())
        ->htmlTemplate('emails/scholarship/request_infos.html.twig')
        ->context([
            'applicantEmail'   => $message->getFromEmail(),
            'scholarship' => $scholarship,
            'message'     => $message->getMessage()

        ]);

        $this->sender->send($email);
        
        /** sent repport for student */
        $email->to($message->getFromEmail())
        ->htmlTemplate('emails/scholarship/request_infos_delivery_report.html.twig');
        $this->sender->send($email);

    }

    public function sendScholarshipAlert(ScholarshipAlert $alert, Scholarship $scholarship)
    {
        $email = (new TemplatedEmail())
            ->from($_ENV['FROM_EMAIL'])
            ->subject("Alerte nouvelle bourse Les Grimpeurs ")
            ->htmlTemplate('emails/scholarship/scholarship_alert.html.twig')
            ->context([
                'scholarship' => $scholarship,
                'user' => $alert->getStudent()
            ]);

        $email->to($alert->getStudent()->getEmail());
        $this->sender->send($email);
    }

    public function sendTodoAlert(Todo $todo)
    {
        $email = (new TemplatedEmail())
        ->from($_ENV['FROM_EMAIL'])
        ->subject("Alert Todo")
        ->htmlTemplate('emails/todo/todo_alert.html.twig')
        ->context([
            'todo' => $todo,
        ]);
        $email->to($todo->getOwner()->getEmail());
        $this->sender->send($email);
    }

}