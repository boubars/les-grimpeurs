<?php


namespace App\Service\Mailer;

use App\Entity\User\User;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use App\Service\Utilis\TokenGenerator;

class Mailer
{
    /**
     * @var MailerInterface
     */
    private $sender;
    /**
     * @var string
     */
    private $fromEmail;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var mixed
     */
    private $toEmail;

    /**
     * @param MailerInterface $mailer
     * @param RouterInterface $router
     */
    public function __construct(MailerInterface $mailer, RouterInterface $router)
    {
        $this->fromEmail = $_ENV['FROM_EMAIL'];
        $this->toEmail = $_ENV['TO_EMAIL'];
        $this->sender = $mailer;
        $this->router = $router;
    }


    /*** Global grimpeurs email service */
    use MailerGrimpeurs;

    /*** Global user account mailer */
    use MailerUser;

    /*** All scholarship email */
    Use MialerScholarship;

    /*** All Organization email service */
    use MailerOrganization;
    
    /*** for student emailing */
    use MailerStudent;

    /**** Mailer teste */
    use MailerTeste;


  
}
