<?php

namespace App\Service\Mailer;

use App\Entity\Common\Contact;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

trait MailerGrimpeurs
{

    public function sendMessageContact(Contact $contact)
    {
        $message = (new TemplatedEmail())
            ->from($this->fromEmail)
            ->to($this->toEmail)
            ->subject($contact->getSubject() ?? "Message user")
            ->htmlTemplate('emails/user/message.html.twig')
            ->context([
                'message' => $contact->getMessage(),
                'fullname' => $contact->getFullname(),
                'phone' => $contact->getPhone(),
                'Email' => $contact->getEmail()
            ]);
        $this->sender->send($message);
    }
}