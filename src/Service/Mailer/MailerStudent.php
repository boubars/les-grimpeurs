<?php

namespace App\Service\Mailer;

use App\Entity\Application\Application;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

trait MailerStudent
{
    /**
     * @param Application $application
     * @param string $subject
     * @param string $content
     */
    public function sendNotificationApplication(Application $application, $subject = '', $content = '')
    {
        $from = $_ENV['FROM_EMAIL'];

        $object  = sprintf("Candidature à %s", $application->getScholarship()->getName());
        $user   = $application->getStudent();
        $email = (new TemplatedEmail())
            ->from($from)
            ->to($user->getEmail())
            ->subject($object)
            ->htmlTemplate('emails/application/notification.html.twig')
            ->context([
                'user' => $user,
                'content' => $content,
                'subject' => $subject
            ]);
        $this->sender->send($email);
    }
}