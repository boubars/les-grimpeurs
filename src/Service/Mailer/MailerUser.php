<?php

namespace App\Service\Mailer;

use App\Entity\User\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


trait MailerUser 
{
      /**
     * @return [type]
     */
    public function sendUserActivationCode($user)
    {
        $email = (new TemplatedEmail())
            ->from($this->fromEmail)
            ->to($user->getEmail())
            ->subject('Validez votre compte user !')
            ->htmlTemplate('emails/user/confirmation_code.html.twig')
            ->context([
                'code' => $user->getCodeActivate()
            ]);
        $this->sender->send($email);
    }

    /**
     * @param User $user
     * 
     * @return [type]
     */
    public function sendUserRecoveryLink(User $user)
    {
        $url = $this->router->generate(
            'user_recover_password_accept', [
            'token' => $user->getToken()
        ],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
        $email = (new TemplatedEmail())
            ->from($this->fromEmail)
            ->to($user->getEmail())
            ->subject('Demande de restauration de mot de passe !')
            ->htmlTemplate('emails/user/recovery_password.html.twig')
            ->context([
                'link' => $url
            ]);
        $this->sender->send($email);
    }

    /**
     * @param User $user
     * 
     * @return [type]
     */
    
     public function sendUserActivationLink(User $user)
    {
        $url = $this->router->generate(
            'user_activation_account', [
            'token' => $user->getToken()
        ],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
        $email = (new TemplatedEmail())
            ->from($this->fromEmail)
            ->to($user->getEmail())
            ->subject('Validez votre compte user !')
            ->htmlTemplate('emails/user/confirmation_acount.html.twig')
            ->context([
                'link' => $url
            ]);
        $this->sender->send($email);
    }
}