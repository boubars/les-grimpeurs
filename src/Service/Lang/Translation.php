<?php

namespace App\Service\Lang;

use App\Service\Utilis\JsonFileDecoder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Entity\Common\Sexe;
use App\Entity\Grimpeurs\Post;

class Translation {
  private $params;
  public function __construct(ParameterBagInterface $params)
  {
    $this->params =     $params;
  }

  public function getResource($lang="fr"){

    $resourcesDir = $this->params->get("project_dir").'/assets/vue/lang/translations';

    $translations['country']         = JsonFileDecoder::decode("$resourcesDir/country/$lang.json");
    $translations['nationality']     = JsonFileDecoder::decode("$resourcesDir/nationality/$lang.json");
    $translations['continent']       = JsonFileDecoder::decode("$resourcesDir/continent/$lang.json");
    $translations['level']           = (JsonFileDecoder::decode("$resourcesDir/student/$lang.json"))['level'];
    $translations['institutionType'] = JsonFileDecoder::decode("$resourcesDir/organizationType/$lang.json");
    $translations['sexe']            = ($this->getSexeTranslations())[$lang];
    $translations['durations']       = ($this->getSexeDurations())[$lang];
    $translations['post_type']       = ($this->getPostTypes())[$lang];
    
    
    /*** dynamic lang files is located at public/translations folder */
    $otherDir = $this->params->get("project_dir").'/public/translations';
    $translations['discipline']      = JsonFileDecoder::decode("$otherDir/discipline/$lang.json");
    $translations['language']        = (JsonFileDecoder::decode("$resourcesDir/language/$lang.json"));
    return $translations;
  }

  private function getSexeTranslations(){
    $fr = [
          "Homme"      => Sexe::MAN,
          "Femme"      => Sexe::WOMAN,
          "Transgenre" => Sexe::TRANSGENRE,
          "Tous"       => "all",
    ];
    $en = [
          "Man"      => Sexe::MAN,
          "Woman"      => Sexe::WOMAN,
          "Transgenre" => Sexe::TRANSGENRE,
          "All"       => "all",
    ];
    return ["fr"=> $fr, "en" => $en ];
  }

  private function getSexeDurations(){
    $fr = [
      'Mois'  => 'month',
      'Année' => 'year',
    ];
    
    $en = [
      'Month'  => 'month',
      'Year'   => 'year',
    ];
    
    return ["fr"=> $fr, "en" => $en ];
  }
  private function getPostTypes(){
    $en = [
      'Post' => 'POST',
      'Page' => 'PAGE',
      'Press'=> 'PRESS'
    ];
    $fr = [
      'Article' => 'POST',
      'Page'    => 'PAGE',
      'Presse'  => 'PRESS'
    ];
    return ["fr"=> $fr, "en" => $en ];
  }
  
}
