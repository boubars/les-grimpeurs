<?php
namespace App\Doctrine\Extensions;

use Doctrine\ORM\Mapping\ClassMetaData;
use Doctrine\ORM\Query\Filter\SQLFilter;

class SoftDeleteFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if ($targetEntity->hasField("deleted")) {
            return $targetTableAlias.".deleted IS NULL OR ".$targetTableAlias.".deleted = 0";
        }
        return "";
    }
}