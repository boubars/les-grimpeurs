<?php

namespace App\Entity\Admin;

use App\Repository\AdminRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\User\User;
use App\Entity\Traits\DateTimeEntityTrait;

/**
 * @ORM\Entity(repositoryClass=AdminRepository::class)
 * @ORM\Table(name="user_admin")
 */
class Admin extends User
{
    /** Date created and date update */
    Use DateTimeEntityTrait;
}
