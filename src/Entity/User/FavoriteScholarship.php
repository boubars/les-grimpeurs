<?php

namespace App\Entity\User;

use App\Entity\Scholarship\Scholarship;
use App\Repository\User\FavoriteScholarshipRepository;
use JMS\Serializer\Annotation as Serializer;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FavoriteScholarshipRepository::class)
 * @ORM\Table(name="user_favorite_scholarship")
 * @ORM\HasLifecycleCallbacks() 
 * @Serializer\ExclusionPolicy("all") 
 */
class FavoriteScholarship
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Serializer\Expose()
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="favoriteScholarships")
     * @ORM\JoinColumn(nullable=false)
     * @Serializer\Expose()
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Scholarship::class, inversedBy="interestedUsers")
     * @ORM\JoinColumn(nullable=false)
     * @Serializer\Expose()
     */
    private $scholarship;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getScholarship(): ?Scholarship
    {
        return $this->scholarship;
    }

    public function setScholarship(?Scholarship $scholarship): self
    {
        $this->scholarship = $scholarship;

        return $this;
    }
    
    /** 
    * @ORM\PrePersist
    */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }
}
