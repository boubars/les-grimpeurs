<?php

namespace App\Entity\User;

use App\Entity\Scholarship\Scholarship;
use App\Entity\Todo\Todo;
use App\Entity\Traits\PersonTrait;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"student"="\App\Entity\Student\Student", "organization"="\App\Entity\Organization\Contact", "university"="\App\Entity\University\University", "company"="\App\Entity\Company\Company", "Admin"="App\Entity\Admin\Admin"})
 * @Serializer\ExclusionPolicy("all") 
 */
abstract class User implements UserInterface
{
    /**
     * @Serializer\Groups({"details"})
     */
    Use PersonTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     * @Assert\Email(
     *      message = "mail_invalid"
     * )
     */
    private $email;
    
    /**
     * @ORM\Column(type="json")
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

   

    /**
     * @ORM\Column(type="boolean")
     */
    private $enable = false;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * @ORM\OneToMany(targetEntity=Todo::class, mappedBy="owner")
     */
    private $todoList;

    /**
     * @ORM\OneToMany(targetEntity=Scholarship::class, mappedBy="owner")
     */
    private $myScholarships;

    /**
     * @ORM\ManyToMany(targetEntity=Scholarship::class)
     * @ORM\JoinTable(name="user_blacklist_scholarship")
     */
    private $blacklistScholarship;

    /**
     * @ORM\OneToMany(targetEntity=FavoriteScholarship::class, mappedBy="user", orphanRemoval=true,  cascade={"persist"})
     */
    private $favoriteScholarships;

    /**
     * @ORM\OneToMany(targetEntity=SeenScholarship::class, mappedBy="user", orphanRemoval=true, cascade={"persist"})
     */
    private $seenScholarships;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"})
     */
    private $avatar;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $ip;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastLogin;



    public function __construct()
    {
        $this->todoList = new ArrayCollection();
        $this->myScholarships = new ArrayCollection();
        $this->blacklistScholarship = new ArrayCollection();
        $this->favoriteScholarships = new ArrayCollection();
        $this->seenScholarships = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
       // $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }



    public function getEnable(): ?bool
    {
        return $this->enable;
    }
    public function isEnable(): ?bool
    {
        return $this->enable;
    }

    public function setEnable(bool $enable): self
    {
        $this->enable = $enable;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }


    /**
     * @return Collection|Todo[]
     */
    public function getTodoList(): Collection
    {
        return $this->todoList;
    }

    public function addTodoList(Todo $todoList): self
    {
        if (!$this->todoList->contains($todoList)) {
            $this->todoList[] = $todoList;
            $todoList->setOwner($this);
        }

        return $this;
    }

    public function removeTodoList(Todo $todoList): self
    {
        if ($this->todoList->removeElement($todoList)) {
            // set the owning side to null (unless already changed)
            if ($todoList->getOwner() === $this) {
                $todoList->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Scholarship[]
     */
    public function getMyScholarships(): Collection
    {
        return $this->myScholarships;
    }

    public function addMyScholarship(Scholarship $myScholarship): self
    {
        if (!$this->myScholarships->contains($myScholarship)) {
            $this->myScholarships[] = $myScholarship;
            $myScholarship->setOwner($this);
        }

        return $this;
    }

    public function removeMyScholarship(Scholarship $myScholarship): self
    {
        if ($this->myScholarships->removeElement($myScholarship)) {
            // set the owning side to null (unless already changed)
            if ($myScholarship->getOwner() === $this) {
                $myScholarship->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Scholarship[]
     */
    public function getBlacklistScholarship(): Collection
    {
        return $this->blacklistScholarship;
    }

    public function addBlacklistScholarship(Scholarship $blacklistScholarship): self
    {
        if (!$this->blacklistScholarship->contains($blacklistScholarship)) {
            $this->blacklistScholarship[] = $blacklistScholarship;
        }

        return $this;
    }

    public function removeBlacklistScholarship(Scholarship $blacklistScholarship): self
    {
        $this->blacklistScholarship->removeElement($blacklistScholarship);

        return $this;
    }

    /**
     * @return Collection|FavoriteScholarship[] ||
     */
    public function getFavoriteScholarships(): Collection
    {
        return $this->favoriteScholarships;
    }

    /**
     * @return Array|Scholarship[] ||
     */
    public function getFavoriteScholarshipsArray(): Array
    {
        $array = [];
        foreach ($this->favoriteScholarships as $value) {
            $array[] = $value->getScholarship();
        }
        return $array;
    }

    public function addFavoriteScholarship(FavoriteScholarship $favoriteScholarship): self
    {
        if (!$this->favoriteScholarships->contains($favoriteScholarship)) {
            $this->favoriteScholarships[] = $favoriteScholarship;
            $favoriteScholarship->setUser($this);
        }

        return $this;
    }

    /***
     * @param FavoriteScholarship|Scholarship|Integer $object
     */
    public function removeFavoriteScholarship($object): self
    {
        $favoriteScholarship = $object instanceof FavoriteScholarship ? $object : null;

        if($object instanceof Scholarship || $object > 0){
            $scholarshipID = $object instanceof Scholarship ? $object->getId() : $object;

            foreach ($this->favoriteScholarships as $value) {
                if($value->getScholarship()->getId() == $scholarshipID){
                    $favoriteScholarship = $value;
                    break;
                }
            }
        }

        if ($this->favoriteScholarships->removeElement($favoriteScholarship)) {
            // set the owning side to null (unless already changed)
            if ($favoriteScholarship->getUser() === $this) {
                $favoriteScholarship->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SeenScholarship[]
     */
    public function getSeenScholarships(): Collection
    {
        return $this->seenScholarships;
    }

    public function addSeenScholarship(SeenScholarship $seenScholarship): self
    {
        if (!$this->seenScholarships->contains($seenScholarship)) {
            $this->seenScholarships[] = $seenScholarship;
            $seenScholarship->setUser($this);
        }

        return $this;
    }

    public function removeSeenScholarship(SeenScholarship $seenScholarship): self
    {
        if ($this->seenScholarships->removeElement($seenScholarship)) {
            // set the owning side to null (unless already changed)
            if ($seenScholarship->getUser() === $this) {
                $seenScholarship->setUser(null);
            }
        }

        return $this;
    }

    public function favoriteScholarshipsContain($scholarship){
        
        if($scholarship instanceof Scholarship){
            $scholarship = $scholarship->getId();
        }

        if(!$this->favoriteScholarships || !$scholarship){
            return false;
        }

        foreach ($this->favoriteScholarships as $value) {
           if($value->getScholarship()->getId() == $scholarship){
               return true;
           }
        }
        
        return false;
    }

    public function haveSeen($scholarship){
        
        $scholarshipID = $scholarship instanceof Scholarship ? $scholarship->getId() : $scholarship;

        if($scholarship instanceof Scholarship){
            $scholarship = $scholarship->getId();
        }

        if(!$this->seenScholarships || !$scholarship){
            return false;
        }

        
        foreach ($this->seenScholarships as $value) {
            if($value->getScholarship()->getId() == $scholarship){
                return true;
            }
         }
         

    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(?string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getLastLogin(): ?\DateTimeInterface
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?\DateTimeInterface $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

}
