<?php

namespace App\Entity\User;

use App\Entity\Scholarship\Scholarship;
use App\Repository\User\SeenScholarshipRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SeenScholarshipRepository::class)
 * @ORM\Table(name="user_seen_scholarship")
 * @ORM\HasLifecycleCallbacks() 
 */
class SeenScholarship
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="seenScholarships")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Scholarship::class, inversedBy="seenUsers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $scholarship;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getScholarship(): ?Scholarship
    {
        return $this->scholarship;
    }

    public function setScholarship(?Scholarship $scholarship): self
    {
        $this->scholarship = $scholarship;

        return $this;
    }
    
    /** 
    * @ORM\PrePersist
    */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }
}
