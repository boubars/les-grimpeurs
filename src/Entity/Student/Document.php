<?php

namespace App\Entity\Student;

use App\Repository\Student\DocumentRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=DocumentRepository::class)
 * @Serializer\ExclusionPolicy("all")
 */
class Document
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Student::class, inversedBy="documents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $student;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $filename;


    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $category;

    /**
     * @ORM\Column(type="smallint")
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $date_create;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $date_alert;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStudent(): ?Student
    {
        return $this->student;
    }

    public function setStudent(?Student $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDateCreate(): ?\DateTimeInterface
    {
        return $this->date_create;
    }

    public function setDateCreate(\DateTimeInterface $date_create): self
    {
        $this->date_create = $date_create;

        return $this;
    }

    public function getDateAlert(): ?\DateTimeInterface
    {
        return $this->date_alert;
    }

    public function setDateAlert(?\DateTimeInterface $date_alert): self
    {
        $this->date_alert = $date_alert;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param $category
     * @return Document
     */
    public function setCategory($category): self
    {
        $this->category = $category;

        return $this;
    }


}
