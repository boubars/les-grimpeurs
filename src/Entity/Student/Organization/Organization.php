<?php

namespace App\Entity\Student\Organization;

use App\Entity\Organization\OrganizationType;
use App\Entity\Student\Association;
use App\Repository\Organization\OrganizationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=OrganizationRepository::class)
 * @ORM\Table(name="student_organization")
 * @Serializer\ExclusionPolicy("all") 
 */
class Organization
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose() 
     * @Serializer\Groups({"list", "details"}) 
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})  
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=OrganizationType::class, inversedBy="studentOrganizations" , cascade={"persist"})
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})  
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity=Association::class, mappedBy="organization")
     * @Serializer\Groups({"details"}) 
     */
    private $associations;

    public function __construct()
    {
        $this->associations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }   

    public function getType(): ?OrganizationType
    {
        return $this->type;
    }

    public function setType(?OrganizationType $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Association[]
     */
    public function getAssociations(): Collection
    {
        return $this->associations;
    }

    public function addAssociation(Association $association): self
    {
        if (!$this->associations->contains($association)) {
            $this->associations[] = $association;
            $association->setOrganization($this);
        }

        return $this;
    }

    public function removeAssociation(Association $association): self
    {
        if ($this->associations->removeElement($association)) {
            // set the owning side to null (unless already changed)
            if ($association->getOrganization() === $this) {
                $association->setOrganization(null);
            }
        }

        return $this;
    }
}
