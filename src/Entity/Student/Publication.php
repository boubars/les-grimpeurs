<?php

namespace App\Entity\Student;

use App\Repository\Student\PublicationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=PublicationRepository::class)
 * @ORM\Table(name="student_publication")
 * @Serializer\ExclusionPolicy("all") 
 */
class Publication
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})  
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     *  message = "field_required"
     * )
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})  
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     *  message = "field_required"
     * )
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})  
     */
    private $editor;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank(
     *  message = "field_required"
     * )
     * @Serializer\Expose()
     * @Serializer\Groups({"details"}) 
     */
    private $publicationDate;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     *  message = "field_required"
     * )
     * @Serializer\Expose()
     * @Serializer\Groups({"details"}) 
     */
    private $author;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"}) 
     */
    private $url;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"}) 
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Student::class, inversedBy="publications")
     */
    private $student;

    /* @Assert\NotBlank(
    *  message = "field_required"
    * )
    * @Assert\GreaterThan(0)(
    *  message="dqs"
    )
    */

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getEditor(): ?string
    {
        return $this->editor;
    }

    public function setEditor(?string $editor): self
    {
        $this->editor = $editor;

        return $this;
    }

    public function getPublicationDate(): \DateTime
    {
        return $this->publicationDate;
    }

    public function setPublicationDate(\DateTime $publicationDate): self
    {
       $this->publicationDate = $publicationDate; 
       return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStudent(): ?Student
    {
        return $this->student;
    }

    public function setStudent(?Student $student): self
    {
        $this->student = $student;

        return $this;
    }
}
