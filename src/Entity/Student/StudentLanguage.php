<?php

namespace App\Entity\Student;

use App\Entity\Common\Language;
use App\Repository\Student\StudentLanguageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=StudentLanguageRepository::class)
 * @Serializer\ExclusionPolicy("all") 
 */
class StudentLanguage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose() 
     * @Serializer\Groups({"details","list"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Language::class)
     * @Serializer\Expose() 
     * @Serializer\Groups({"details","list"})
     */
    private $language;

    /**
     * @ORM\ManyToOne(targetEntity=LanguageLevel::class)
     * @Serializer\Expose() 
     * @Serializer\Groups({"details","list"})
     */
    private $level;

    /**
     * @ORM\ManyToMany(targetEntity=Student::class, mappedBy="languages")
     */
    private $students;


    public function __construct()
    {
        $this->students = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getLevel(): ?LanguageLevel
    {
        return $this->level;
    }

    public function setLevel(?LanguageLevel $level): self
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return Collection|Student[]
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function addStudent(Student $student): self
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
            $student->addLanguage($this);
        }

        return $this;
    }

    public function removeStudent(Student $student): self
    {
        if ($this->students->removeElement($student)) {
            $student->removeLanguage($this);
        }

        return $this;
    }
}
