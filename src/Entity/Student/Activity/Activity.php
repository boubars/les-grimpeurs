<?php

namespace App\Entity\Student\Activity;

use App\Repository\Student\Activity\ActivityRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use App\Entity\Student\Organization\Organization;
use App\Entity\Student\Student;

/**
 * @ORM\Entity(repositoryClass=ActivityRepository::class)
 * @ORM\Table(name="student_activity")
 * @Serializer\ExclusionPolicy("all") 
 */
class Activity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose() 
     * @Serializer\Groups({"list", "details"}) 
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})  
     */
    private $startDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})  
     */
    private $endDate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})  
     */
    private $currentPosition;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose() 
     * @Serializer\Groups({"list", "details"}) 
     */
    private $place;

    /**
     * @ORM\Column(type="text")
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})  
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Organization::class , cascade={"persist"})
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"}) 
     */
    private $organization;

    /**
     * @ORM\ManyToOne(targetEntity=Student::class, inversedBy="extraActivities")
     * @Serializer\Expose() 
     */
    private $student;

    /**
     * @ORM\ManyToOne(targetEntity=ActivityCause::class, inversedBy="extraActivities")
     * @Serializer\Expose()
     * @Serializer\Groups({"details"}) 
     */
    private $cause;
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getCurrentPosition(): ?bool
    {
        return $this->currentPosition;
    }

    public function setCurrentPosition(bool $currentPosition): self
    {
        $this->currentPosition = $currentPosition;

        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(string $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    public function setOrganization(?Organization $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    public function getStudent(): ?Student
    {
        return $this->student;
    }

    public function setStudent(?Student $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getCause(): ?ActivityCause
    {
        return $this->cause;
    }

    public function setCause(?ActivityCause $cause): self
    {
        $this->cause = $cause;

        return $this;
    }
}
