<?php

namespace App\Entity\Student\Activity;

use App\Repository\Student\Activity\ActivityCauseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=ActivityCauseRepository::class)
 * @ORM\Table(name="activity_cause")
 * @Serializer\ExclusionPolicy("all") 
 */
class ActivityCause
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"}) 
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"}) 
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Serializer\Groups({"list", "details"}) 
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity=Activity::class, mappedBy="cause")
     */
    private $extraActivities;

    public function __construct()
    {
        $this->extraActivities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Activity[]
     */
    public function getExtraActivities(): Collection
    {
        return $this->extraActivities;
    }

    public function addActivity(Activity $extraActivity): self
    {
        if (!$this->extraActivities->contains($extraActivity)) {
            $this->extraActivities[] = $extraActivity;
            $extraActivity->setCause($this);
        }

        return $this;
    }

    public function removeActivity(Activity $extraActivity): self
    {
        if ($this->extraActivities->removeElement($extraActivity)) {
            // set the owning side to null (unless already changed)
            if ($extraActivity->getCause() === $this) {
                $extraActivity->setCause(null);
            }
        }

        return $this;
    }
}
