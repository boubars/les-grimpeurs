<?php

namespace App\Entity\Student;

use App\Repository\Student\DisctinctionRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=DisctinctionRepository::class)
 * @ORM\Table(name="student_distinction")
 * @Serializer\ExclusionPolicy("all") 
 */
class Disctinction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})
     */
    private $title;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $related;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $awardingEntity;

    /**
     * @ORM\Column(type="date")
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $emissionDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Student::class, inversedBy="distinctions")
     */
    private $student;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getRelated(): ?int
    {
        return $this->related;
    }

    public function setRelated(?int $related): self
    {
        $this->related = $related;

        return $this;
    }

    public function getAwardingEntity(): ?string
    {
        return $this->awardingEntity;
    }

    public function setAwardingEntity(string $awardingEntity): self
    {
        $this->awardingEntity = $awardingEntity;

        return $this;
    }

    public function getEmissionDate(): ?\DateTimeInterface
    {
        return $this->emissionDate;
    }

    public function setEmissionDate(\DateTimeInterface $emissionDate): self
    {
        $this->emissionDate = $emissionDate;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStudent(): ?Student
    {
        return $this->student;
    }

    public function setStudent(?Student $student): self
    {
        $this->student = $student;

        return $this;
    }
}
