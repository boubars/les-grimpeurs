<?php

namespace App\Entity\Student;

use App\Entity\Common\Grade;
use App\Entity\Discipline\Discipline as StudyField;
use App\Repository\Student\FormationRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=FormationRepository::class)
 * @ORM\Table(name="student_formation")
 * @Serializer\ExclusionPolicy("all") 
 */
class Formation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"list", "details"})
     * @Serializer\Expose() 
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=School::class, inversedBy="grade", cascade={"persist"})
     * @Serializer\Groups({"list", "details"})
     * @Serializer\Expose()
     */
    private $school;

    /**
     * @ORM\ManyToOne(targetEntity=Grade::class, inversedBy="formations", cascade={"persist"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $grade;

    /**
     * @ORM\ManyToOne(targetEntity=StudyField::class, inversedBy="formations", cascade={"persist"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $studyField;

    /**
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $yearStart;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $yearEnd;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $activities;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $description;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $medias = [];

    /**
     * @ORM\ManyToOne(targetEntity=Student::class, inversedBy="formations")
     */
    private $student;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $obtainedResult;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Serializer\Groups({"list","details"})
     * @Serializer\Expose()
     */
    private $currentPosition;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSchool(): ?School
    {
        return $this->school;
    }

    public function setSchool(?School $school): self
    {
        $this->school = $school;

        return $this;
    }

    public function getGrade(): ?Grade
    {
        return $this->grade;
    }

    public function setGrade(?Grade $grade): self
    {
        $this->grade = $grade;

        return $this;
    }

    public function getStudyField(): ?StudyField
    {
        return $this->studyField;
    }

    public function setStudyField(?StudyField $studyField): self
    {
        $this->studyField = $studyField;

        return $this;
    }

    public function getYearStart(): ?int
    {
        return $this->yearStart;
    }

    public function setYearStart(int $yearStart): self
    {
        $this->yearStart = $yearStart;

        return $this;
    }

    public function getYearEnd(): ?int
    {
        return $this->yearEnd;
    }

    public function setYearEnd(int $yearEnd): self
    {
        $this->yearEnd = $yearEnd;

        return $this;
    }

    public function getActivities(): ?string
    {
        return $this->activities;
    }

    public function setActivities(?string $activities): self
    {
        $this->activities = $activities;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getMedias(): ?array
    {
        return $this->medias;
    }

    public function setMedias(array $medias): self
    {
        $this->medias = $medias;

        return $this;
    }

    public function getStudent(): ?Student
    {
        return $this->student;
    }

    public function setStudent(?Student $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getObtainedResult(): ?string
    {
        return $this->obtainedResult;
    }

    public function setObtainedResult(?string $obtainedResult): self
    {
        $this->obtainedResult = $obtainedResult;
        return $this;
    }

    public function getCurrentPosition(): ?bool
    {
        return $this->currentPosition;
    }

    public function isCurrentPosition(): ?bool
    {
        return $this->currentPosition;
    }

    public function setCurrentPosition(?bool $currentPosition): self
    {
        $this->currentPosition = $currentPosition;

        return $this;
    }
}
