<?php

namespace App\Entity\Student;

use App\Repository\Student\StudentTagRepository;
use App\Entity\Organization\Organization;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=StudentTagRepository::class)
 *
 * Class for action in student by organization
 */
class StudentTag
{

    public function __construct(Student $student, Organization $organization)
    {
        $this->student = $student;
        $this->organization = $organization;
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Student::class)
     */
    private $student;

    /**
     * @ORM\ManyToOne(targetEntity=Organization::class, inversedBy="student_tag")
     * @Serializer\Groups({"details"})
     */
    private $organization;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $date_favorite;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $date_remove;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStudent()
    {
        return $this->student;
    }

    public function setStudent($student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getOrganization()
    {
        return $this->organization;
    }

    public function setOrganization($organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    public function getDateFavorite(): ?\DateTimeInterface
    {
        return $this->date_favorite;
    }

    public function setDateFavorite(?\DateTimeInterface $date_favorite): self
    {
        $this->date_favorite = $date_favorite;

        return $this;
    }

    public function getDateRemove(): ?\DateTimeInterface
    {
        return $this->date_remove;
    }

    public function setDateRemove(?\DateTimeInterface $date_remove): self
    {
        $this->date_remove = $date_remove;

        return $this;
    }
}
