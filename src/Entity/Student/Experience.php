<?php

namespace App\Entity\Student;

use App\Repository\Student\ExperienceRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=ExperienceRepository::class)
 * @ORM\Table(name="student_experience")
 * @Serializer\ExclusionPolicy("all") 
 */
class Experience
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"}) 
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"}) 
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"}) 
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"}) 
     */
    private $place;

    /**
     * @ORM\Column(type="boolean")
     * @Serializer\Expose()
     * @Serializer\Groups({"details"}) 
     */
    private $currentPosition;

    /**
     * @ORM\Column(type="date")
     * @Serializer\Expose()
     * @Serializer\Groups({"details"}) 
     */
    private $startDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Serializer\Expose() 
     * @Serializer\Groups({"details"})
     */
    private $endDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Expose() 
     * @Serializer\Groups({"details"})
     */
    private $description;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Serializer\Expose() 
     * @Serializer\Groups({"details"})
     */
    private $medias = [];

    /**
     * @ORM\ManyToOne(targetEntity=Student::class, inversedBy="experiences")
     */
    private $student;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(?string $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getCurrentPosition(): ?bool
    {
        return $this->currentPosition;
    }
    
    public function isCurrentPosition(): ?bool
    {
        return $this->currentPosition;
    }

    public function setCurrentPosition(bool $currentPosition): self
    {
        $this->currentPosition = $currentPosition;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getMedias(): ?array
    {
        return $this->medias;
    }

    public function setMedias(?array $medias): self
    {
        $this->medias = $medias;

        return $this;
    }

    public function getStudent(): ?Student
    {
        return $this->student;
    }

    public function setStudent(?Student $student): self
    {
        $this->student = $student;

        return $this;
    }
}
