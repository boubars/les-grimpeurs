<?php

namespace App\Entity\Student;

use App\Repository\Student\SearchPreferenceRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=SearchPreferenceRepository::class)
 * @ORM\Table(name="student_preference")
 * @Serializer\ExclusionPolicy("all") 
 */
class SearchPreference
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"list", "details"})
     * @Serializer\Expose() 
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default"="1"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $scholashipByProfil;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default"="1"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $scholarshipByDiscipline;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default"="1"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $newGrimpeurByProfil;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default"="1"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $newGrimpeur;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default"="1"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $formationByProfil;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default"="1"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $opportunityByProfil;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScholashipByProfil(): ?bool
    {
        return $this->scholashipByProfil;
    }

    public function setScholashipByProfil(?bool $scholashipByProfil): self
    {
        $this->scholashipByProfil = $scholashipByProfil;

        return $this;
    }

    public function getScholarshipByDiscipline(): ?bool
    {
        return $this->scholarshipByDiscipline;
    }

    public function setScholarshipByDiscipline(?bool $scholarshipByDiscipline): self
    {
        $this->scholarshipByDiscipline = $scholarshipByDiscipline;

        return $this;
    }

    public function getNewGrimpeurByProfil(): ?bool
    {
        return $this->newGrimpeurByProfil;
    }

    public function setNewGrimpeurByProfil(?bool $newGrimpeurByProfil): self
    {
        $this->newGrimpeurByProfil = $newGrimpeurByProfil;

        return $this;
    }

    public function getNewGrimpeur(): ?bool
    {
        return $this->newGrimpeur;
    }

    public function setNewGrimpeur(?bool $newGrimpeur): self
    {
        $this->newGrimpeur = $newGrimpeur;

        return $this;
    }

    public function getFormationByProfil(): ?bool
    {
        return $this->formationByProfil;
    }

    public function setFormationByProfil(?bool $formationByProfil): self
    {
        $this->formationByProfil = $formationByProfil;

        return $this;
    }

    public function getOpportunityByProfil(): ?bool
    {
        return $this->opportunityByProfil;
    }

    public function setOpportunityByProfil(?bool $opportunityByProfil): self
    {
        $this->opportunityByProfil = $opportunityByProfil;

        return $this;
    }
}
