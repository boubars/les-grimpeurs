<?php

namespace App\Entity\Student;

use App\Entity\Application\ApplicationSheet;
use App\Entity\Common\Country;
use App\Entity\Common\Level;
use App\Entity\Common\Nationality;
use App\Entity\Common\SocialNetwork;
use App\Entity\Discipline\Category as Discipline;
use App\Entity\Application\Application;
use App\Entity\Scholarship\ScholarshipAlert;
use App\Entity\User\User;
use App\Entity\Student\Activity\Activity;
use App\Entity\Student\StudentTag;
use App\Repository\Student\StudentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\DateTimeEntityTrait;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=StudentRepository::class)
 * @ORM\Table(name="user_student")
 * @Serializer\ExclusionPolicy("all")
 */
class Student extends User
{
    /** Date created and date update */
    Use DateTimeEntityTrait;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $codeActivate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateExpired;

    /**
     * @ORM\ManyToMany(targetEntity=Nationality::class, inversedBy="students", cascade={"persist"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $nationalities;

    /**
     * @ORM\ManyToMany(targetEntity=Discipline::class, inversedBy="students")
     * @ORM\JoinTable(name="student_discipline_category")
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $disciplines;

    /**
     * @ORM\OneToMany(targetEntity=Formation::class, mappedBy="student", cascade={"persist"})
     * @ORM\OrderBy({"yearStart" = "DESC"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $formations;

    /**
     * @ORM\OneToMany(targetEntity=StudentTag::class, mappedBy="student", cascade={"persist"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $tag;

    /**
     * @ORM\OneToMany(targetEntity=Experience::class, mappedBy="student", cascade={"persist"})
     * @ORM\OrderBy({"startDate" = "DESC"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $experiences;

    /**
     * @ORM\ManyToMany(targetEntity=StudentLanguage::class, inversedBy="students", cascade={"persist"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $languages;

    /**
     * @ORM\ManyToOne(targetEntity=Level::class, inversedBy="students")
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $currentLevel;

    /**
     * @ORM\OneToMany(targetEntity=Publication::class, mappedBy="student", cascade={"persist"})
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     * @ORM\OrderBy({"publicationDate" = "DESC"})
     */
    private $publications;

    /**
     * @ORM\OneToMany(targetEntity=Activity::class, mappedBy="student", cascade={"persist"})
     * @ORM\OrderBy({"startDate" = "DESC"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $extraActivities;

    /**
     * @ORM\OneToMany(targetEntity=Association::class, mappedBy="student", cascade={"persist"})
     * @ORM\OrderBy({"startDate" = "DESC"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $associations;

    /**
     * @ORM\OneToMany(targetEntity=Disctinction::class, mappedBy="student", cascade={"persist"})
     * @ORM\OrderBy({"emissionDate" = "DESC"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $distinctions;

    /**
     * @ORM\OneToMany(targetEntity=Exam::class, mappedBy="student", cascade={"persist"}))
     * @ORM\OrderBy({"emissionDate" = "DESC"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $exams;

    /**
     * @ORM\ManyToMany(targetEntity=Country::class, inversedBy="students")
     * @ORM\JoinTable(name="student_desired_countries")
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $desiredCountries;

    /**
     * @ORM\ManyToOne(targetEntity=Level::class)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $desiredStudyLevel;

    /**
     * @ORM\ManyToMany(targetEntity=Discipline::class, inversedBy="desiredStudents")
     * @ORM\JoinTable(name="student_desired_discipline")
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $desiredDisciplines;

    /**
     * @ORM\OneToOne(targetEntity=SocialNetwork::class, cascade={"persist", "remove"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $socialNetwork;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $siteUrl;

    /**
     * @ORM\OneToOne(targetEntity=SearchPreference::class, cascade={"persist", "remove"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $searchPreferences;

    /**
     * @ORM\OneToMany(targetEntity=Application::class, mappedBy="student")
     */
    private $applications;

    /**
     * @ORM\OneToOne(targetEntity=ScholarshipAlert::class, mappedBy="student", cascade={"persist", "remove"})
     */
    private $scholarshipAlert;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="originStudents")
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $sexe;

    /**
     * @ORM\OneToMany(targetEntity=ApplicationSheet::class, mappedBy="Student")
     */
    private $applicationSheets;

    /**
     * @ORM\OneToMany(targetEntity=Document::class, mappedBy="student", orphanRemoval=true)
     */
    private $documents;

    public function __construct()
    {
        $this->nationalities = new ArrayCollection();
        $this->disciplines = new ArrayCollection();
        $this->formations = new ArrayCollection();
        $this->tag = new ArrayCollection();
        $this->experiences = new ArrayCollection();
        $this->languages = new ArrayCollection();
        $this->publications = new ArrayCollection();
        $this->extraActivities = new ArrayCollection();
        $this->associations = new ArrayCollection();
        $this->distinctions = new ArrayCollection();
        $this->exams = new ArrayCollection();
        $this->desiredDisciplines = new ArrayCollection();
        $this->applications = new ArrayCollection();
        $this->desiredCountries = new ArrayCollection();
        $this->applicationSheets = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }

    public function getCodeActivate(): ?string
    {
        return $this->codeActivate;
    }

    public function setCodeActivate(?string $codeActivate): self
    {
        $this->codeActivate = $codeActivate;
        return $this;
    }
    /**
     * @return Collection|Nationality[]
     */

    public function getNationalities(): Collection
    {
        return $this->nationalities;
    }
    public function setNationalities(ArrayCollection $array): self
    {
        $this->nationalities = $array;
        return $this;
    }

    public function addNationality(Nationality $nationality): self
    {
        if (!$this->nationalities->contains($nationality)) {
            $this->nationalities[] = $nationality;
        }

        return $this;
    }

    public function getDateExpired(): ?\DateTimeInterface
    {
        return $this->dateExpired;
    }

    public function setDateExpired(\DateTimeInterface $dateExpired): self
    {
        $this->dateExpired = $dateExpired;
        return $this;
    }
    public function removeNationality(Nationality $nationality): self
    {
        $this->nationalities->removeElement($nationality);

        return $this;
    }

    /**
     * @return Collection|Discipline[]
     */
    public function getDisciplines(): Collection
    {
        return $this->disciplines;
    }

    public function addDiscipline(Discipline $discipline): self
    {
        if (!$this->disciplines->contains($discipline)) {
            $this->disciplines[] = $discipline;
        }

        return $this;
    }
    public function setDisciplines(ArrayCollection $array): self
    {
        $this->disciplines = $array;
        return $this;
    }

    public function removeDiscipline(Discipline $discipline): self
    {
        $this->disciplines->removeElement($discipline);

        return $this;
    }

    /**
     * @return Collection|Formation[]
     */
    public function getFormations(): Collection
    {
        return $this->formations;
    }

    public function addFormation(Formation $formation): self
    {
        if (!$this->formations->contains($formation)) {
            $this->formations[] = $formation;
            $formation->setStudent($this);
        }

        return $this;
    }

    public function removeFormation(Formation $formation): self
    {
        if ($this->formations->removeElement($formation)) {
            // set the owning side to null (unless already changed)
            if ($formation->getStudent() === $this) {
                $formation->setStudent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StudentTag[]
     */
    public function getTags(): Collection
    {
        return $this->tag;
    }

    public function addTag(StudentTag $tag): self
    {
        if (!$this->tag->contains($tag)) {
            $this->tag[] = $tag;
            $tag->setStudent($this);
        }

        return $this;
    }

    public function removeTag(StudentTag $tag): self
    {
        if ($this->tag->removeElement($tag)) {
            // set the owning side to null (unless already changed)
            if ($tag->getStudent() === $this) {
                $tag->setStudent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Experience[]
     */
    public function getExperiences(): Collection
    {
        return $this->experiences;
    }

    public function addExperience(Experience $experience): self
    {
        if (!$this->experiences->contains($experience)) {
            $this->experiences[] = $experience;
            $experience->setStudent($this);
        }

        return $this;
    }

    public function removeExperience(Experience $experience): self
    {
        if ($this->experiences->removeElement($experience)) {
            // set the owning side to null (unless already changed)
            if ($experience->getStudent() === $this) {
                $experience->setStudent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StudentLanguage[]
     */
    public function getLanguages(): Collection
    {
        return $this->languages;
    }

    public function addLanguage(StudentLanguage $language): self
    {
        if (!$this->languages->contains($language)) {
            $this->languages[] = $language;
        }

        return $this;
    }

    public function removeLanguage(StudentLanguage $language): self
    {
        $this->languages->removeElement($language);

        return $this;
    }

    public function getCurrentLevel(): ?Level
    {
        return $this->currentLevel;
    }

    public function setCurrentLevel(?Level $currentLevel): self
    {
        $this->currentLevel = $currentLevel;

        return $this;
    }

    /**
     * @return Collection|Publication[]
     */
    public function getPublications(): Collection
    {
        return $this->publications;
    }

    public function addPublication(Publication $publication): self
    {
        if (!$this->publications->contains($publication)) {
            $this->publications[] = $publication;
            $publication->setStudent($this);
        }
        return $this;
    }

    /**
     * @return Collection|Activity[]
     */
    public function getExtraActivities(): Collection
    {
        return $this->extraActivities;
    }

    public function addActivity(Activity $extraActivity): self
    {
        if (!$this->extraActivities->contains($extraActivity)) {
            $this->extraActivities[] = $extraActivity;
            $extraActivity->setStudent($this);
        }

        return $this;
    }

    public function removePublication(Publication $publication): self
    {
        if ($this->publications->removeElement($publication)) {
            // set the owning side to null (unless already changed)
            if ($publication->getStudent() === $this) {
                $publication->setStudent(null);
            }
        }
        return $this;
    }
    public function removeActivity(Activity $extraActivity): self
    {
        if ($this->extraActivities->removeElement($extraActivity)) {
            // set the owning side to null (unless already changed)
            if ($extraActivity->getStudent() === $this) {
                $extraActivity->setStudent(null);

            }
        }
        return $this;
    }

    /**
     * @return Collection|Association[]
     */
    public function getAssociations(): Collection
    {
        return $this->associations;
    }

    public function addAssociation(Association $association): self
    {
        if (!$this->associations->contains($association)) {
            $this->associations[] = $association;
            $association->setStudent($this);
        }

        return $this;
    }

    public function removeAssociation(Association $association): self
    {
        if ($this->associations->removeElement($association)) {
            // set the owning side to null (unless already changed)
            if ($association->getStudent() === $this) {
                $association->setStudent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Disctinction[]
     */
    public function getDistinctions(): Collection
    {
        return $this->distinctions;
    }

    public function addDistinction(Disctinction $distinction): self
    {
        if (!$this->distinctions->contains($distinction)) {
            $this->distinctions[] = $distinction;
            $distinction->setStudent($this);
        }

        return $this;
    }

    public function removeDistinction(Disctinction $distinction): self
    {
        if ($this->distinctions->removeElement($distinction)) {
            // set the owning side to null (unless already changed)
            if ($distinction->getStudent() === $this) {
                $distinction->setStudent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Exam[]
     */
    public function getExams(): Collection
    {
        return $this->exams;
    }

    public function addExam(Exam $exam): self
    {
        if (!$this->exams->contains($exam)) {
            $this->exams[] = $exam;
            $exam->setStudent($this);
        }

        return $this;
    }

    public function removeExam(Exam $exam): self
    {
        if ($this->exams->removeElement($exam)) {
            // set the owning side to null (unless already changed)
            if ($exam->getStudent() === $this) {
                $exam->setStudent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Country[]
     */
    public function getDesiredCountries(): Collection
    {
        return $this->desiredCountries;
    }

    public function setDesiredCountries(ArrayCollection $array): self
    {
        $this->desiredCountries = $array;
        return $this;
    }

    public function addDesiredCountry(Country $desiredCountry): self
    {
        if (!$this->desiredCountries->contains($desiredCountry)) {
            $this->desiredCountries[] = $desiredCountry;
        }

        return $this;
    }

    public function removeDesiredCountry(Country $desiredCountry): self
    {
        $this->desiredCountries->removeElement($desiredCountry);

        return $this;
    }

    public function getDesiredStudyLevel(): ?Level
    {
        return $this->desiredStudyLevel;
    }

    public function setDesiredStudyLevel(?Level $desiredStudyLevel): self
    {
        $this->desiredStudyLevel = $desiredStudyLevel;

        return $this;
    }

    /**
     * @return Collection|Discipline[]
     */
    public function getDesiredDisciplines(): Collection
    {
        return $this->desiredDisciplines;
    }
    public function setDesiredDisciplines(ArrayCollection $array): self
    {
        $this->desiredDisciplines = $array;
        return $this;
    }

    public function addDesiredDiscipline(Discipline $desiredDiscipline): self
    {
        if (!$this->desiredDisciplines->contains($desiredDiscipline)) {
            $this->desiredDisciplines[] = $desiredDiscipline;
        }

        return $this;
    }

    public function removeDesiredDiscipline(Discipline $desiredDiscipline): self
    {
        $this->desiredDisciplines->removeElement($desiredDiscipline);

        return $this;
    }

    public function getSocialNetwork(): ?SocialNetwork
    {
        return $this->socialNetwork;
    }

    public function setSocialNetwork(?SocialNetwork $socialNetwork): self
    {
        $this->socialNetwork = $socialNetwork;

        return $this;
    }

    public function getSiteUrl(): ?string
    {
        return $this->siteUrl;
    }

    public function setSiteUrl(?string $siteUrl): self
    {
        $this->siteUrl = $siteUrl;

        return $this;
    }

    public function getSearchPreferences(): ?SearchPreference
    {
        return $this->searchPreferences;
    }

    public function setSearchPreferences(?SearchPreference $searchPreferences): self
    {
        $this->searchPreferences = $searchPreferences;

        return $this;
    }

    /**
     * @return Collection|Application[]
     */
    public function getApplications(): Collection
    {
        return $this->applications;
    }

    public function addApplication(Application $application): self
    {
        if (!$this->applications->contains($application)) {
            $this->applications[] = $application;
            $application->setStudent($this);
        }

        return $this;
    }

    public function removeApplication(Application $application): self
    {
        if ($this->applications->removeElement($application)) {
            // set the owning side to null (unless already changed)
            if ($application->getStudent() === $this) {
                $application->setStudent(null);
            }
        }

        return $this;
    }

    public function getScholarshipAlert(): ?ScholarshipAlert
    {
        return $this->scholarshipAlert;
    }

    public function setScholarshipAlert(ScholarshipAlert $scholarshipAlert): self
    {
        // set the owning side of the relation if necessary
        if ($scholarshipAlert->getStudent() !== $this) {
            $scholarshipAlert->setStudent($this);
        }

        $this->scholarshipAlert = $scholarshipAlert;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(?string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * @return Collection|ApplicationSheet[]
     */
    public function getApplicationSheets(): Collection
    {
        return $this->applicationSheets;
    }

    public function addApplicationSheet(ApplicationSheet $applicationSheet): self
    {
        if (!$this->applicationSheets->contains($applicationSheet)) {
            $this->applicationSheets[] = $applicationSheet;
            $applicationSheet->setStudent($this);
        }

        return $this;
    }

    public function removeApplicationSheet(ApplicationSheet $applicationSheet): self
    {
        if ($this->applicationSheets->removeElement($applicationSheet)) {
            // set the owning side to null (unless already changed)
            if ($applicationSheet->getStudent() === $this) {
                $applicationSheet->setStudent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setStudent($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            // set the owning side to null (unless already changed)
            if ($document->getStudent() === $this) {
                $document->setStudent(null);
            }
        }

        return $this;
    }

}