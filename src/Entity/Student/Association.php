<?php

namespace App\Entity\Student;

use App\Entity\Student\Organization\Organization;
use App\Repository\Student\AssociationRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=AssociationRepository::class)
 * @ORM\Table(name="student_association")
 * @Serializer\ExclusionPolicy("all") 
 */
class Association
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})  
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Organization::class, inversedBy="associations", cascade={"persist"})
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})  
     */
    private $organization;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})  
     */
    private $responsability;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})  
     */
    private $currentPosition;

    /**
     * @ORM\Column(type="date")
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})  
     */
    private $startDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})  
     */
    private $endDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})  
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Student::class, inversedBy="associations")
     */
    private $student;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    public function setOrganization(?Organization $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    public function getResponsability(): ?string
    {
        return $this->responsability;
    }

    public function setresResponsability(?string $responsability): self
    {
        $this->responsability = $responsability;

        return $this;
    }

    public function getCurrentPosition(): ?bool
    {
        return $this->currentPosition;
    }

    public function setCurrentPosition(bool $currentPosition): self
    {
        $this->currentPosition = $currentPosition;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStudent(): ?Student
    {
        return $this->student;
    }

    public function setStudent(?Student $student): self
    {
        $this->student = $student;

        return $this;
    }
}
