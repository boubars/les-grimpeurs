<?php

namespace App\Entity\Student;

use App\Entity\Common\Country;
use App\Repository\Student\SchoolRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=SchoolRepository::class)
 * @ORM\Table(name="student_school")
 * @Serializer\ExclusionPolicy("all") 
 */
class School
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"list", "details"})
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Groups({"list", "details"})
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Formation::class, mappedBy="school")
     */
    private $grade;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="schools")
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $country;

    public function __construct()
    {
        $this->grade = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Formation[]
     */
    public function getGrade(): Collection
    {
        return $this->grade;
    }

    public function addGrade(Formation $grade): self
    {
        if (!$this->grade->contains($grade)) {
            $this->grade[] = $grade;
            $grade->setSchool($this);
        }

        return $this;
    }

    public function removeGrade(Formation $grade): self
    {
        if ($this->grade->removeElement($grade)) {
            // set the owning side to null (unless already changed)
            if ($grade->getSchool() === $this) {
                $grade->setSchool(null);
            }
        }

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }
}
