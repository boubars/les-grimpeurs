<?php

namespace App\Entity\University;

use App\Entity\Common\Country;
use App\Entity\Common\SocialNetwork;
use App\Entity\Organization\Organization;
use App\Repository\University\UniversityRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=UniversityRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="user_university")
 * @Serializer\ExclusionPolicy("all") 
 */
class University extends Organization
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /** 
    * @ORM\PrePersist
    */
    public function onPrePersist(){
        $this->setRoles(['ROLE_UNIVERSITY']);
    }
}
