<?php

namespace App\Entity\Application;

use App\Entity\Student\Student;
use App\Repository\Application\ApplicationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Scholarship\Scholarship;
use Exception;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=ApplicationRepository::class)
 * 
 */
class Application
{

    //Standard
    CONST EDIT = 1; //Formulaire icomplet
    CONST ENCOURS = 2; // Validate by student
    CONST VALIDATE = 3; // Validate by organisme
    CONST REFUSED = 4; // REFUSED by organisme

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"list", "details"})
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Student::class, inversedBy="applications")
     * @ORM\JoinColumn(nullable=false)
     * @Serializer\Groups({"student"})
     */
    private $student;

    /**
     * @ORM\ManyToOne(targetEntity=Scholarship::class, inversedBy="applications")
     * @Serializer\Groups({"scholarship"})
     */
    private $scholarship;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $dateApply;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"default" : 1})
     * @Serializer\Groups({"list", "details"})
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"list", "details"})
     */
    private $notificationSubject;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Groups({"list", "details"})
     */
    private $notification;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Serializer\Groups({"list", "details"})
     */
    private $average_note;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Serializer\Groups({"list", "details"})
     */
    private $date_note;

    /**
     * @ORM\OneToMany(targetEntity=ApplicationSheet::class, mappedBy="Application")
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $applicationSheets;

    public function __construct()
    {
        $this->applicationSheets = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStudent(): ?Student
    {
        return $this->student;
    }

    public function setStudent(?Student $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getScholarship(): ?Scholarship
    {
        try{
            return $this->scholarship;
        }catch(Exception $e){
            return null;
        }
    }

    public function setScholarship(?Scholarship $scholarship): self
    {
        $this->scholarship = $scholarship;

        return $this;
    }

    /**
     * @return Collection|ApplicationSheet[]
     */
    public function getApplicationSheets(): Collection
    {
        return $this->applicationSheets;
    }

    public function addApplicationSheet(ApplicationSheet $applicationSheet): self
    {
        if (!$this->applicationSheets->contains($applicationSheet)) {
            $this->applicationSheets[] = $applicationSheet;
            $applicationSheet->setApplication($this);
        }

        return $this;
    }

    public function removeApplicationSheet(ApplicationSheet $applicationSheet): self
    {
        if ($this->applicationSheets->removeElement($applicationSheet)) {
            // set the owning side to null (unless already changed)
            if ($applicationSheet->getApplication() === $this) {
                $applicationSheet->setApplication(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateApply()
    {
        return $this->dateApply;
    }

    /**
     * @param mixed $dateApply
     */
    public function setDateApply($dateApply): void
    {
        $this->dateApply = $dateApply;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param $status
     * @return Application
     */
    public function setStatus($status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNotificationSubject()
    {
        return $this->notificationSubject;
    }

    /**
     * @param mixed $notificationSubject
     */
    public function setNotificationSubject($notificationSubject): void
    {
        $this->notificationSubject = $notificationSubject;
    }

    /**
     * @return mixed
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * @param mixed $notification
     */
    public function setNotification($notification): void
    {
        $this->notification = $notification;
    }

    /**
     * @return mixed
     */
    public function getAverageNote()
    {
        return $this->average_note;
    }

    /**
     * @param $average_note
     * @return Application
     */
    public function setAverageNote($average_note): self
    {
        $this->average_note = $average_note;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateNote()
    {
        return $this->date_note;
    }

    /**
     * @param $date_note
     * @return Application
     */
    public function setDateNote($date_note): self
    {
        $this->date_note = $date_note;

        return $this;
    }


}
