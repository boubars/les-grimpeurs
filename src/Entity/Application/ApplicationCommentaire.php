<?php

namespace App\Entity\Application;

use App\Entity\User\User;
use App\Repository\Application\ApplicationCommentaireRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=ApplicationCommentaireRepository::class)
 */
class ApplicationCommentaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity=ApplicationSheet::class, inversedBy="applicationCommentaires")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $applicationSheet;

    /**
     * @ORM\Column(type="datetime")
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $dateCreation;

    /**
     * @ORM\Column(type="boolean")
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $constructive;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $content;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getApplicationSheet(): ?ApplicationSheet
    {
        return $this->applicationSheet;
    }

    public function setApplicationSheet(?ApplicationSheet $applicationSheet): self
    {
        $this->applicationSheet = $applicationSheet;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConstructive()
    {
        return $this->constructive;
    }

    /**
     * @param $constructive
     * @return ApplicationCommentaire
     */
    public function setConstructive($constructive): self
    {
        $this->constructive = $constructive;

        return $this;
    }

}
