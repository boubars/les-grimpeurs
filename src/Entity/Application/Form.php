<?php

namespace App\Entity\Application;

use App\Entity\Organization\Organization;
use App\Entity\Scholarship\Scholarship;
use App\Repository\Application\FormRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=FormRepository::class)
 */
class Form
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"list", "details"})
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Groups({"list", "details"})
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Organization::class, inversedBy="forms")
     * @ORM\JoinColumn(nullable=true)
     */
    private $organization;

    /**
     * @ORM\ManyToOne(targetEntity=Folder::class, inversedBy="forms")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $folder;

    /**
     * @ORM\Column(type="datetime")
     * @Serializer\Groups({"list", "details"})
     * @Serializer\Expose()
     */
    private $dateCreation;

    /**
     * @ORM\OneToMany(targetEntity=Scholarship::class, mappedBy="form")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $scholarship;

    /**
     * @ORM\OneToMany(targetEntity=Sheet::class, mappedBy="form", orphanRemoval=true, cascade={"PERSIST", "REMOVE"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $sheets;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"list", "details"})
     * @Serializer\Expose()
     */
    private $slug;

    public function __construct()
    {
        $this->scholarship = new ArrayCollection();
        $this->sheets = new ArrayCollection();
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }


    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    public function setOrganization(?Organization $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    public function getFolder(): ?Folder
    {
        return $this->folder;
    }

    public function setFolder(?Folder $folder): self
    {
        $this->folder = $folder;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * @return Collection|Scholarship[]
     */
    public function getScholarship(): Collection
    {
        return $this->scholarship;
    }

    public function addScholarship(Scholarship $scholarship): self
    {
        if (!$this->scholarship->contains($scholarship)) {
            $this->scholarship[] = $scholarship;
        }

        return $this;
    }

    public function removeScholarship(Scholarship $scholarship): self
    {
        $this->scholarship->removeElement($scholarship);

        return $this;
    }

    /**
     * @return Collection|Sheet[]
     */
    public function getSheets(): Collection
    {
        return $this->sheets;
    }

    /**
     * @return Collection|Sheet[]
     */
    public function setSheets($sheets)
    {
        $this->sheets = $sheets;
        return $this;
    }

    public function addSheet(Sheet $sheet): self
    {
        if (!$this->sheets->contains($sheet)) {
            $this->sheets[] = $sheet;
            $sheet->setForm($this);
        }

        return $this;
    }

    public function removeSheet(Sheet $sheet): self
    {
        if ($this->sheets->removeElement($sheet)) {
            // set the owning side to null (unless already changed)
            if ($sheet->getForm() === $this) {
                $sheet->setForm(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
