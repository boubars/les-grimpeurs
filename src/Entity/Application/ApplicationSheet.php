<?php

namespace App\Entity\Application;

use App\Entity\Student\Student;
use App\Repository\Application\ApplicationSheetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=ApplicationSheetRepository::class)
 */
class ApplicationSheet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"list", "details"})
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Student::class, inversedBy="applicationSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Student;

    /**
     * @ORM\ManyToOne(targetEntity=Application::class, inversedBy="applicationSheets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Application;

    /**
     * @ORM\ManyToOne(targetEntity=Sheet::class)
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $Sheet;

    /**
     * @ORM\Column(type="json")
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $content = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $note;


    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $percent;

    /**
     * @ORM\OneToMany(targetEntity=ApplicationCommentaire::class, mappedBy="applicationSheet", orphanRemoval=true, cascade={"PERSIST", "REMOVE"})
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $applicationCommentaires;

    public function __construct()
    {
        $this->applicationCommentaires = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStudent(): ?Student
    {
        return $this->Student;
    }

    public function setStudent(?Student $Student): self
    {
        $this->Student = $Student;

        return $this;
    }

    public function getApplication(): ?Application
    {
        return $this->Application;
    }

    public function setApplication(?Application $Application): self
    {
        $this->Application = $Application;

        return $this;
    }

    public function getSheet(): ?Sheet
    {
        return $this->Sheet;
    }

    public function setSheet(?Sheet $Sheet): self
    {
        $this->Sheet = $Sheet;

        return $this;
    }

    public function getContent(): ?array
    {
        return $this->content;
    }

    public function setContent(array $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(?int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getPercent(): ?int
    {
        return $this->percent;
    }

    public function setPercent(?int $percent): self
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * @return Collection|ApplicationCommentaire[]
     */
    public function getApplicationCommentaires(): Collection
    {
        return $this->applicationCommentaires;
    }

    public function addApplicationCommentaire(ApplicationCommentaire $applicationCommentaire): self
    {
        if (!$this->applicationCommentaires->contains($applicationCommentaire)) {
            $this->applicationCommentaires[] = $applicationCommentaire;
            $applicationCommentaire->setApplicationSheet($this);
        }

        return $this;
    }

    public function removeApplicationCommentaire(ApplicationCommentaire $applicationCommentaire): self
    {
        if ($this->applicationCommentaires->removeElement($applicationCommentaire)) {
            // set the owning side to null (unless already changed)
            if ($applicationCommentaire->getApplicationSheet() === $this) {
                $applicationCommentaire->setApplicationSheet(null);
            }
        }

        return $this;
    }
}
