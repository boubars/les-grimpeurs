<?php

namespace App\Entity\Application;

use App\Repository\Application\SheetRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=SheetRepository::class)
 */
class Sheet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"form"})
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Groups({"form"})
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     * @Serializer\Groups({"form"})
     * @Serializer\Expose()
     */
    private $dateCreation;

    /**
     * @ORM\ManyToOne(targetEntity=Form::class, inversedBy="sheets")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Serializer\Groups({"form"})
     * @Serializer\Expose()
     */
    private $form;

    /**
     * @ORM\Column(type="json")
     * @Serializer\Groups({"form"})
     * @Serializer\Expose()
     */
    private $content = [];


    /**
     * @ORM\Column(name="sheet_order", type="integer", nullable=true)
     * @Serializer\Groups({"form"})
     * @Serializer\Expose()
     */
    private $order;


    public function getId(): ?int
    {
        return $this->id;
    }
    public function setId($id) 
    {
        return $this->id = $id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getForm(): ?Form
    {
        return $this->form;
    }

    public function setForm(?Form $form): self
    {
        $this->form = $form;

        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order): void
    {
        $this->order = $order;
    }
}
