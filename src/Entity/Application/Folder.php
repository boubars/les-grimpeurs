<?php

namespace App\Entity\Application;

use App\Entity\Organization\Organization;
use App\Repository\Application\FolderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=FolderRepository::class)
 */
class Folder
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"list", "details"})
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Organization::class, inversedBy="folders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $organisation;


    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Groups({"list", "details"})
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     * @Serializer\Groups({"list", "details"})
     * @Serializer\Expose()
     */
    private $dateCreation;

    /**
     * @ORM\OneToMany(targetEntity=Form::class, mappedBy="folder")
     * @Serializer\Groups({"details","list"})
     * @Serializer\Expose()
     */
    private $forms;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"list", "details"})
     * @Serializer\Expose()
     */
    private $slug;

    public function __construct()
    {
        $this->forms = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrganisation(): ?Organization
    {
        return $this->organisation;
    }

    public function setOrganisation(?Organization $organisation): self
    {
        $this->organisation = $organisation;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * @return Collection|Form[]
     */
    public function getForms(): Collection
    {
        return $this->forms;
    }

    public function addForm(Form $form): self
    {
        if (!$this->forms->contains($form)) {
            $this->forms[] = $form;
            $form->setFolder($this);
        }

        return $this;
    }

    public function removeForm(Form $form): self
    {
        if ($this->forms->removeElement($form)) {
            // set the owning side to null (unless already changed)
            if ($form->getFolder() === $this) {
                $form->setFolder(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
