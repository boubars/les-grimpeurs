<?php

namespace App\Entity\Todo;

use App\Entity\User\User;
use App\Repository\Todo\TodoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TodoRepository::class)
 */
class Todo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fullDate;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $full_hour;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $name;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $additionalInfos = [];

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $alertOn;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $alertDuration;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="todoList")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $alertDate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $sent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullDate(): ?\DateTimeInterface
    {
        return $this->fullDate;
    }

    public function setFullDate(?\DateTimeInterface $fullDate): self
    {
        $this->fullDate = $fullDate;
       
        $this->updateAlertDate();

        return $this;
    }

    public function getFullHour(): ?string
    {
        return $this->full_hour;
    }

    public function setFullHour(?string $full_hour): self
    {
        $this->full_hour = $full_hour;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAdditionalInfos(): ?array
    {
        return $this->additionalInfos;
    }

    public function setAdditionalInfos(?array $additionalInfos): self
    {
        $this->additionalInfos = $additionalInfos;

        return $this;
    }

    public function getAlertOn(): ?bool
    {
        return $this->alertOn;
    }

    public function setAlertOn(?bool $alertOn): self
    {
        $this->alertOn = $alertOn;

        return $this;
    }

    public function getAlertDuration(): ?int
    {
        return $this->alertDuration;
    }

    public function setAlertDuration(?int $alertDuration): self
    {
        $this->alertDuration = $alertDuration;
        $this->updateAlertDate();
        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getAlertDate(): ?\DateTimeInterface
    {
        return $this->alertDate;
    }

    public function setAlertDate(?\DateTimeInterface $alertDate): self
    {
        $this->alertDate = $alertDate;

        return $this;
    }

    public function getSent(): ?bool
    {
        return $this->sent;
    }

    public function setSent(?bool $sent): self
    {
        $this->sent = $sent;

        return $this;
    }

    private function updateAlertDate(){



        $hasDuration =  ($this->getAlertDuration() > -1 );  
        if(!$hasDuration  || !$this->getFullDate())
            return $this;

        /** @var DateTime */
        $fullDate = clone $this->fullDate;
        $triggers = $this->getTodoTriggers();

        return $this->setAlertDate(
            $fullDate->sub(
                new \DateInterval($triggers[$this->alertDuration])
            )
        );
    } 

    /** base on student.todo.triggers data */
    private function  getTodoTriggers(){
        return [
            "P0D",
            "P1D",
            "P2D",
            "P3D",
            "P4D",
            "P5D",
            "P6D",
            "P7D",
            "P14D",
            "P21D",
            "P1M"
        ];
    }
}
