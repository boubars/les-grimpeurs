<?php

namespace App\Entity\Company;

use App\Entity\Organization\Organization;
use App\Repository\Company\CompanyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompanyRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="user_company")
 */
class Company extends Organization
{
    public function __construct()
    {
        parent::__construct();
    }

    /** 
    * @ORM\PrePersist
    */
    public function onPrePersist(){
        $this->setRoles(['ROLE_COMPANY']);
    }

}
