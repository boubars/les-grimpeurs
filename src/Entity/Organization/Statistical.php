<?php

namespace App\Entity\Organization;

use App\Repository\Organization\StatisticalRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=StatisticalRepository::class)
 * @ORM\Table(name="organization_statistical")
 * @Serializer\ExclusionPolicy("all") 
 */
class Statistical
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"})
     */
    private $scholarshipAwarded;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"})
     */
    private $acceptanceRate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"})
     */
    private $menCount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"})
     */
    private $womenCount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"})
     */
    private $scholarshipCount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScholarshipAwarded(): ?int
    {
        return $this->scholarshipAwarded;
    }

    public function setScholarshipAwarded(?int $scholarshipAwarded): self
    {
        $this->scholarshipAwarded = $scholarshipAwarded;

        return $this;
    }

    public function getAcceptanceRate(): ?string
    {
        return $this->acceptanceRate;
    }

    public function setAcceptanceRate(?string $acceptanceRate): self
    {
        $this->acceptanceRate = $acceptanceRate;

        return $this;
    }

    public function getMenCount(): ?int
    {
        return $this->menCount;
    }

    public function setMenCount(?int $menCount): self
    {
        $this->menCount = $menCount;

        return $this;
    }

    public function getWomenCount(): ?int
    {
        return $this->womenCount;
    }

    public function setWomenCount(?int $womenCount): self
    {
        $this->womenCount = $womenCount;

        return $this;
    }

    public function getScholarshipCount(): ?int
    {
        return $this->scholarshipCount;
    }

    public function setScholarshipCount(?int $scholarshipCount): self
    {
        $this->scholarshipCount = $scholarshipCount;

        return $this;
    }
}
