<?php

namespace App\Entity\Organization;

use App\Entity\Application\Folder;
use App\Entity\Application\Form;
use App\Entity\Common\Country;
use App\Entity\Common\Gallery;
use App\Entity\Common\Person;
use App\Entity\Common\SocialNetwork;
use App\Entity\Scholarship\Scholarship;
use App\Entity\Student\StudentTag;
use App\Entity\Traits\DateTimeEntityTrait;
use App\Repository\Organization\OrganizationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\User\User;
use JMS\Serializer\Annotation as Serializer;


/**
 * @ORM\Entity(repositoryClass=OrganizationRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="organization")
 * @Serializer\ExclusionPolicy("all") 
 */
class Organization
{


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"})
     */
    private $id;


    use DateTimeEntityTrait;


    private $sluger;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true, length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"})
     */
    private $logo;

    /**
     * @ORM\Column(type="string", nullable=true, length=4)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $yearOfCreation;

    /**
     * @ORM\Column(type="string", nullable=true, length=50)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $siretNumber;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $presidentName;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $website;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $accreditation;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", nullable=true, length=10)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $postalCode;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="organizations", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $country;

    /**
     * @ORM\ManyToMany(targetEntity=Country::class, inversedBy="organizationsAction", cascade={"persist"})
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $countryAction;

    /**
     * @ORM\Column(type="string", nullable=true, length=30)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $organizationEmail;

    /**
     * @ORM\OneToOne(targetEntity=Statistical::class, cascade={"persist", "remove"})
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $statistical;

    /**
     * @ORM\OneToOne(targetEntity=SocialNetwork::class, cascade={"persist", "remove"})
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $socialNetwork;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $coverPicture;

    /**
     * @ORM\OneToOne(targetEntity=Gallery::class, cascade={"persist", "remove"})
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $gallery;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $adresseMap;

    /**
     * @ORM\ManyToOne(targetEntity=OrganizationType::class, inversedBy="organizations")
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $institutionType;

    /**
     * @ORM\OneToMany(targetEntity=Scholarship::class, mappedBy="relatedOrganization")
     */
    private $myRelatedScholarships;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"})
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity=StudentTag::class, mappedBy="organization", cascade={"persist"})
     * @Serializer\Expose()
     */
    private $student_tag;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Serializer\Groups({"list","details"})
     * @Serializer\Expose()
     */
    private $adresseLocation = [];

    /**
     * @ORM\OneToMany(targetEntity=Folder::class, mappedBy="organisation")
     */
    private $folders;

    /**
     * @ORM\OneToMany(targetEntity=Form::class, mappedBy="organization", orphanRemoval=true)
     */
    private $forms;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $hiddenFields = [];

    /**
     * @ORM\ManyToMany(targetEntity=Contact::class, mappedBy="organization")
     */
    private $contacts;

    public function __construct()
    {
        $this->countryAction = new ArrayCollection();
        $this->myRelatedScholarships = new ArrayCollection();
        $this->forms = new ArrayCollection();
        $this->folders = new ArrayCollection();
        $this->contacts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getYearOfCreation(): ?string
    {
        return $this->yearOfCreation;
    }

    public function setYearOfCreation(string $yearOfCreation): self
    {
        $this->yearOfCreation = $yearOfCreation;

        return $this;
    }

    public function getSiretNumber(): ?string
    {
        return $this->siretNumber;
    }

    public function setSiretNumber(?string $siretNumber): self
    {
        $this->siretNumber = $siretNumber;

        return $this;
    }

    public function getPresidentName(): ?string
    {
        return $this->presidentName;
    }

    public function setPresidentName(string $presidentName): self
    {
        $this->presidentName = $presidentName;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getAccreditation(): ?string
    {
        return $this->accreditation;
    }

    public function setAccreditation(?string $accreditation): self
    {
        $this->accreditation = $accreditation;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection|Country[]
     */
    public function getCountryAction(): ?Collection
    {
        return $this->countryAction;
    }

    public function setCountryAction(?ArrayCollection $array): self
    {
        $this->countryAction = $array;
        return $this;
    }
    public function setCountryActionArray($array): self
    {
        foreach ($array as $key => $country) {
            $this->addCountryAction($country);
        }
        return $this;
    }

    public function addCountryAction(Country $countryAction): self
    {
        if (!$this->countryAction->contains($countryAction)) {
            $this->countryAction[] = $countryAction;
        }

        return $this;
    }

    public function removeCountryAction(Country $countryAction): self
    {
        $this->countryAction->removeElement($countryAction);

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getOrganizationEmail(): ?string
    {
        return $this->organizationEmail;
    }

    public function setOrganizationEmail(string $email): self
    {
        $this->organizationEmail = $email;

        return $this;
    }

    public function getStatistical(): ?Statistical
    {
        return $this->statistical;
    }

    public function setStatistical(?Statistical $statistical): self
    {
        $this->statistical = $statistical;

        return $this;
    }

    public function getSocialNetwork(): ?SocialNetwork
    {
        return $this->socialNetwork;
    }

    public function setSocialNetwork(?SocialNetwork $socialNetwork): self
    {
        $this->socialNetwork = $socialNetwork;

        return $this;
    }

    public function getCoverPicture(): ?string
    {
        return $this->coverPicture;
    }

    public function setCoverPicture(?string $coverPicture): self
    {
        $this->coverPicture = $coverPicture;

        return $this;
    }

    public function getGallery(): ?Gallery
    {
        return $this->gallery;
    }

    public function setGallery(?Gallery $gallery): self
    {
        $this->gallery = $gallery;
        return $this;
    }

    public function getAdresseMap(): ?string
    {
        return $this->adresseMap;
    }

    public function setAdresseMap(?string $adresseMap): self
    {
        $this->adresseMap = $adresseMap;

        return $this;
    }

    public function getInstitutionType(): ?OrganizationType
    {
        return $this->institutionType;
    }

    public function setInstitutionType(?OrganizationType $institutionType): self
    {
        $this->institutionType = $institutionType;

        return $this;
    }

    /**
     * @return Collection|Scholarship[]
     */
    public function getMyRelatedScholarships()
    {
        $aRelatedScholarships = [];
        foreach ($this->myRelatedScholarships as $relatedScholarship) {
            if (0 == $relatedScholarship->getDeleted()) {
                $aRelatedScholarships[] = $relatedScholarship;
            }
        }
        return $aRelatedScholarships;
    }

    public function addMyRelatedScholarship(Scholarship $myRelatedScholarship): self
    {
        if (!$this->myRelatedScholarships->contains($myRelatedScholarship)) {
            $this->myRelatedScholarships[] = $myRelatedScholarship;
            $myRelatedScholarship->setRelatedOrganization($this);
        }

        return $this;
    }

    public function removeMyRelatedScholarship(Scholarship $myRelatedScholarship): self
    {
        if ($this->myRelatedScholarships->removeElement($myRelatedScholarship)) {
            // set the owning side to null (unless already changed)
            if ($myRelatedScholarship->getRelatedOrganization() === $this) {
                $myRelatedScholarship->setRelatedOrganization(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }


    /**
     * @return Collection|StudentTag[]
     */
    public function getStudentTags(): Collection
    {
        return $this->student_tag;
    }

    public function addTag(StudentTag $student_tag): self
    {
        if (!$this->student_tag->contains($student_tag)) {
            $this->tag[] = $student_tag;
            $student_tag->setOrganization($this);
        }

        return $this;
    }

    public function removeStudentTag(StudentTag $studentTag): self
    {
        if ($this->student_tag->removeElement($studentTag)) {
            // set the owning side to null (unless already changed)
            if ($studentTag->getStudent() === $this) {
                $studentTag->setStudent(null);
            }
        }

        return $this;
    }

    public function getAdresseLocation(): ?array
    {
        return $this->adresseLocation;
    }

    public function setAdresseLocation(?array $adresseLocation): self
    {
        $this->adresseLocation = $adresseLocation;

        return $this;
    }

    /**
     * @return Collection|Folder[]
     */
    public function getFolders(): Collection
    {
        return $this->folders;
    }


    public function addFolder(Folder $folder): self
    {
        if (!$this->folders->contains($folder)) {
            $this->folders[] = $folder;
            $folder->setOrganisation($this);
        }

        return $this;
    }

    public function removeFolder(Folder $folder): self
    {
        if ($this->folders->removeElement($folder)) {
            // set the owning side to null (unless already changed)
            if ($folder->getOrganisation() === $this) {
                $folder->setOrganisation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Form[]
     */
    public function getForms(): Collection
    {
        return $this->forms;
    }

    public function addForm(Form $form): self
    {
        if (!$this->forms->contains($form)) {
            $this->forms[] = $form;
            $form->setOrganization($this);
        }

        return $this;
    }

    public function removeForm(Form $form): self
    {
        if ($this->forms->removeElement($form)) {
            // set the owning side to null (unless already changed)
            if ($form->getOrganization() === $this) {
                $form->setOrganization(null);
            }
        }

        return $this;
    }

    public function getHiddenFields(): ?array
    {
        return $this->hiddenFields;
    }

    public function setHiddenFields(?array $hiddenFields): self
    {
        $this->hiddenFields = $hiddenFields;

        return $this;
    }

    /**
     * @return Collection|Contact[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->addOrganization($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self
    {
        if ($this->contacts->removeElement($contact)) {
            $contact->removeOrganization($this);
        }

        return $this;
    }

}
