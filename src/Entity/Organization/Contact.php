<?php

namespace App\Entity\Organization;

use App\Entity\Traits\DateTimeEntityTrait;
use App\Entity\User\User;
use App\Entity\Common\Country;
use App\Repository\Organization\ContactRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=ContactRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="user_contact")
 * @Serializer\ExclusionPolicy("all")
 */
class Contact extends User
{

    /** Date created and date update */
    Use DateTimeEntityTrait;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="resideContactOrganizations")
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $code_postal;

    /**
     * @ORM\ManyToMany(targetEntity=Organization::class, inversedBy="contacts")
     */
    private $organization;

    public function __construct()
    {
        parent::__construct();
        $this->organization = new ArrayCollection();
    }


    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->code_postal;
    }

    public function setCodePostal(?string $code_postal): self
    {
        $this->code_postal = $code_postal;

        return $this;
    }

    /**
     * @return Collection|Organization[]
     */
    public function getOrganization(): Collection
    {
        return $this->organization;
    }

    public function addOrganization(Organization $organization): self
    {
        if (!$this->organization->contains($organization)) {
            $this->organization[] = $organization;
        }

        return $this;
    }

    public function removeOrganization(Organization $organization): self
    {
        $this->organization->removeElement($organization);

        return $this;
    }

}
