<?php

namespace App\Entity\Organization;

use App\Entity\Scholarship\Scholarship;
use App\Repository\Organization\OrganizationTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use App\Entity\Student\Organization\Organization as StudentOrganization;

/**
 * @ORM\Entity(repositoryClass=OrganizationTypeRepository::class)
 * @ORM\Table(name="organization_type")
 * @Serializer\ExclusionPolicy("all") 
 */
class OrganizationType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Organization::class, mappedBy="institutionType")
     */
    private $organizations;

    /**
     * @ORM\OneToMany(targetEntity=StudentOrganization::class, mappedBy="type")
     */
    private $studentOrganizations;

    /**
     * @ORM\OneToMany(targetEntity=Scholarship::class, mappedBy="awardOrganizationType")
     */
    private $awardedScholarships;

    public function __construct()
    {
        $this->organizations = new ArrayCollection();
        $this->studentOrganizations = new ArrayCollection();
        $this->awardedScholarships = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Organization[]
     */
    public function getOrganizations(): Collection
    {
        return $this->organizations;
    }

    public function addOrganization(Organization $organization): self
    {
        if (!$this->organizations->contains($organization)) {
            $this->organizations[] = $organization;
            $organization->setInstitutionType($this);
        }

        return $this;
    }

    public function removeOrganization(Organization $organization): self
    {
        if ($this->organizations->removeElement($organization)) {
            // set the owning side to null (unless already changed)
            if ($organization->getInstitutionType() === $this) {
                $organization->setInstitutionType(null);
            }
        }

        return $this;
    }
    /**
     * @return Collection|StudentOrganization[]
     */
    public function getStudentOrganizations(): Collection
    {
        return $this->studentOrganizations;
    }

    public function addStudentOrganization(StudentOrganization $organization): self
    {
        if (!$this->studentOrganizations->contains($organization)) {
            $this->studentOrganizations[] = $organization;
            $organization->setType($this);
        }

        return $this;
    }

    public function removeStudentOrganization(StudentOrganization $organization): self
    {
        if ($this->studentOrganizations->removeElement($organization)) {
            // set the owning side to null (unless already changed)
            if ($organization->getType() === $this) {
                $organization->getType(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection|Scholarship[]
     */
    public function getAwardedScholarships(): Collection
    {
        return $this->awardedScholarships;
    }

    public function addAwardedScholarship(Scholarship $awardedScholarship): self
    {
        if (!$this->awardedScholarships->contains($awardedScholarship)) {
            $this->awardedScholarships[] = $awardedScholarship;
            $awardedScholarship->setAwardOrganizationType($this);
        }

        return $this;
    }

    public function removeAwardedScholarship(Scholarship $awardedScholarship): self
    {
        if ($this->awardedScholarships->removeElement($awardedScholarship)) {
            // set the owning side to null (unless already changed)
            if ($awardedScholarship->getAwardOrganizationType() === $this) {
                $awardedScholarship->setAwardOrganizationType(null);
            }
        }

        return $this;
    }
}
