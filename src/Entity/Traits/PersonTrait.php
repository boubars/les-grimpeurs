<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

trait PersonTrait
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"list","details"})
     * @Serializer\Expose() 
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"list","details"})
     * @Serializer\Expose()
     */
    private $lastname;

    /**
     * @ORM\Column(type="date" ,nullable=true)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $dateOfBirth;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $adress;


    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getDateOfBirth(): ?\DateTimeInterface
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(?\DateTimeInterface $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(?string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }
}
