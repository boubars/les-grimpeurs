<?php

namespace App\Entity\Traits;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
/**
 * @Serializer\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks() 
 */
trait DateTimeEntityTrait
{
    /**
     * @ORM\Column(type="datetime",  nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Serializer\Groups({"details"})
     * @Serializer\Expose()
     */
    private $updatedAt;

    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime|null $createdAt
     */
    public function setCreatedAt(?DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime|null $updatedAt
     */
    public function setUpdatedAt(?DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }


    /** 
     * @ORM\PrePersist
    */
    public function onPrePersist()
    {
        $this->createdAt = new DateTime();
    }
    /** 
     * @ORM\PreUpdate  
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new DateTime();
    }
}