<?php

namespace App\Entity\Scholarship;

use App\Entity\Application\Form;
use App\Entity\Common\Continent;
use App\Entity\Common\Country;
use App\Entity\Common\Currency;
use App\Entity\Common\Gallery;
use App\Entity\Common\Level;
use App\Entity\Common\Nationality;
use App\Entity\Common\SocialNetwork;
use App\Entity\Discipline\Category;
use App\Entity\Discipline\Discipline;
use App\Entity\Organization\OrganizationType;
use App\Entity\Organization\Organization;
use App\Entity\Traits\DateTimeEntityTrait;
use App\Entity\User\User;
use App\Repository\Scholarship\ScholarshipRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Application\Application;
use JMS\Serializer\Annotation as Serializer;
use App\Entity\User\SeenScholarship;
use App\Entity\User\FavoriteScholarship;

/**
 * @ORM\Entity(repositoryClass=ScholarshipRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="scholarship_scholarship")
 * @Serializer\ExclusionPolicy("all")  
 */
class Scholarship
{
    use DateTimeEntityTrait;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})
     */
    private $logo;

    /**
     * @ORM\Column(type="string", length=50)
     * @var string , accepted value = "local, external"
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $cadidateOption;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string , if candidate option is external
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $cadidateOptionUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $contactName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $contactAvatar;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $contactEmail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $contactAdresse;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $contactPhone;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $candidatureLimit;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $infosEmail;

    /**
     * @ORM\Column(type="boolean")
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $faculty;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $amount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $count;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $duration;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $totalOffered;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $grantedSex;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $minAge;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $maxAge;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $otherInfos;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $story;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $attachments = [];


    /**
     * @ORM\Column(type="array", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $selectionCriteria = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $advantages = [];

    /**
     * @ORM\ManyToMany(targetEntity=Continent::class, inversedBy="scholarships")
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $grantedContinent;

    /**
     * @ORM\OneToOne(targetEntity=Gallery::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $gallery;

    /**
     * @ORM\ManyToMany(targetEntity=Nationality::class, inversedBy="scholarships")
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $grantedNationalities;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $slug;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class, inversedBy="scholarships")
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $studyField;

    /**
     * @ORM\ManyToMany(targetEntity=Discipline::class, cascade={"persist"})
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $studyFieldTags;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $durationUnit;

    /**
     * @ORM\ManyToMany(targetEntity=Country::class, inversedBy="scholarships")
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Expose() 
     * @Serializer\Groups({"details"})
     */
    private $grantedCountries;

    /**
     * @ORM\ManyToMany(targetEntity=Level::class, inversedBy="scholarships")
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Expose() 
     * @Serializer\Groups({"details"})
     */
    private $grantedLevel;

    /**
     * @ORM\OneToOne(targetEntity=SocialNetwork::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true) 
     * @Serializer\Expose() 
     * @Serializer\Groups({"details"})
     */
    private $socialNetwork;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose() 
     * @Serializer\Groups({"details"})
     */
    private $siteUrl;

    /**
     * @ORM\OneToMany(targetEntity=InformationRequest::class, mappedBy="scholarship")
     * @ORM\JoinColumn(nullable=true)
     */
    private $informationRequests;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $contactAdresseMap;

    /**
     * @ORM\ManyToOne(targetEntity=Currency::class)
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Expose() 
     * @Serializer\Groups({"details"})
     */
    private $currency;

    /**
     * @ORM\OneToMany(targetEntity=Application::class, mappedBy="scholarship")
     * @ORM\JoinColumn(nullable=true)
     */
    private $applications;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="scholarshipsAwarded")
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $originCountry;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="myScholarships")
     * @ORM\JoinColumn(nullable=true)
     */
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity=Organization::class, inversedBy="myRelatedScholarships")
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Expose() 
     * @Serializer\Groups({"details"})
     */
    private $relatedOrganization;

    /**
     * @ORM\ManyToOne(targetEntity=OrganizationType::class, inversedBy="awardedScholarships")
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details"})
     */
    private $awardOrganizationType;

    /**
     * @ORM\OneToMany(targetEntity=SeenScholarship::class, mappedBy="scholarship", orphanRemoval=true, cascade={"persist"})
     */
    private $seenUsers;

    /**
     * @ORM\OneToMany(targetEntity=FavoriteScholarship::class, mappedBy="scholarship", orphanRemoval=true, cascade={"persist"})
     */
    private $interestedUsers;

    /**
     * @ORM\ManyToOne(targetEntity=Form::class, inversedBy="scholarship")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     * @Serializer\Expose()
     * @Serializer\Groups({"details", "form"})
     */
    private $form;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : false})
     */
    private $deleted;

    public function __construct()
    {
        $this->grantedContinent = new ArrayCollection();
        $this->grantedNationalities = new ArrayCollection();
        $this->studyField = new ArrayCollection();
        $this->studyFieldTags = new ArrayCollection();
        $this->grantedCountries = new ArrayCollection();
        $this->grantedLevel = new ArrayCollection();
        $this->informationRequests = new ArrayCollection();
        $this->applications = new ArrayCollection();
        $this->seenUsers = new ArrayCollection();
        $this->interestedUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getCadidateOption(): ?string
    {
        return $this->cadidateOption;
    }

    public function setCadidateOption(?string $cadidateOption): self
    {
        $this->cadidateOption = $cadidateOption;

        return $this;
    }

    public function getCadidateOptionUrl(): ?string
    {
        return $this->cadidateOptionUrl;
    }

    public function setCadidateOptionUrl(?string $cadidateOptionUrl): self
    {
        $this->cadidateOptionUrl = $cadidateOptionUrl;

        return $this;
    }

    public function getContactName(): ?string
    {
        return $this->contactName;
    }

    public function setContactName(?string $contactName): self
    {
        $this->contactName = $contactName;

        return $this;
    }

    public function getContactAvatar(): ?string
    {
        return $this->contactAvatar;
    }

    public function setContactAvatar(?string $contactAvatar): self
    {
        $this->contactAvatar = $contactAvatar;

        return $this;
    }

    public function getContactEmail(): ?string
    {
        return $this->contactEmail;
    }

    public function setContactEmail(?string $contactEmail): self
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    public function getContactAdresse(): ?string
    {
        return $this->contactAdresse;
    }

    public function setContactAdresse(?string $contactAdresse): self
    {
        $this->contactAdresse = $contactAdresse;

        return $this;
    }

    public function getContactPhone(): ?string
    {
        return $this->contactPhone;
    }

    public function setContactPhone(?string $contactPhone): self
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    public function getCandidatureLimit(): ?\DateTimeInterface
    {
        return $this->candidatureLimit;
    }

    public function setCandidatureLimit(?\DateTimeInterface $candidatureLimit): self
    {
        $this->candidatureLimit = $candidatureLimit;
        
        return $this;
    }

    public function getInfosEmail(): ?string
    {
        return $this->infosEmail;
    }

    public function setInfosEmail(?string $infosEmail): self
    {
        $this->infosEmail = $infosEmail;

        return $this;
    }


    public function getFaculty(): ?bool
    {
        return $this->faculty;
    }

    public function setFaculty(bool $faculty): self
    {
        $this->faculty = $faculty;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(?int $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(?string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getTotalOffered(): ?int
    {
        return $this->totalOffered;
    }

    public function setTotalOffered(?int $totalOffered): self
    {
        $this->totalOffered = $totalOffered;

        return $this;
    }

    public function getGrantedSex(): ?string
    {
        return $this->grantedSex;
    }

    public function setGrantedSex(?string $grantedSex): self
    {
        $this->grantedSex = $grantedSex;

        return $this;
    }

    public function getMinAge(): ?int
    {
        return $this->minAge;
    }

    public function setMinAge(?int $minAge): self
    {
        $this->minAge = $minAge;

        return $this;
    }

    public function getMaxAge(): ?int
    {
        return $this->maxAge;
    }

    public function setMaxAge(?int $maxAge): self
    {
        $this->maxAge = $maxAge;

        return $this;
    }

    public function getOtherInfos(): ?string
    {
        return $this->otherInfos;
    }

    public function setOtherInfos(?string $otherInfos): self
    {
        $this->otherInfos = $otherInfos;

        return $this;
    }

    public function getStory(): ?string
    {
        return $this->story;
    }

    public function setStory(?string $story): self
    {
        $this->story = $story;

        return $this;
    }

    public function getAttachments(): ?array
    {
        return $this->attachments;
    }

    public function setAttachments(array $attachments): self
    {
        $this->attachments = $attachments;

        return $this;
    }

    public function getSelectionCriteria(): ?array
    {
        return $this->selectionCriteria;
    }

    public function setSelectionCriteria(?array $selectionCriteria): self
    {
        $this->selectionCriteria = $selectionCriteria;

        return $this;
    }

    public function getAdvantages(): ?array
    {
        return $this->advantages;
    }

    public function setAdvantages(?array $advantages): self
    {
        $this->advantages = $advantages;

        return $this;
    }

    /**
     * @return Collection|Continent[]
     */
    public function getGrantedContinent(): Collection
    {
        return $this->grantedContinent;
    }

    public function setGrantedContinent(?ArrayCollection $array): self
    {
        $this->grantedContinent = $array;
        return $this;
    }

    public function addGrantedContinent(Continent $grantedContinent): self
    {
        if (!$this->grantedContinent->contains($grantedContinent)) {
            $this->grantedContinent[] = $grantedContinent;
        }

        return $this;
    }

    public function setGrantedContinentArray($array): self
    {
       foreach ($array as $continent) {
          $this->addGrantedContinent($continent);
       }
        
        return $this;
    }

    public function removeGrantedContinent(Continent $grantedContinent): self
    {
        $this->grantedContinent->removeElement($grantedContinent);

        return $this;
    }

    public function getGallery(): ?Gallery
    {
        return $this->gallery;
    }

    public function setGallery(?Gallery $gallery): self
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * @return Collection|Nationality[]
     */
    public function getGrantedNationalities(): Collection
    {
        return $this->grantedNationalities;
    }

    public function setGrantedNationalities(?ArrayCollection $array): self
    {
        $this->grantedNationalities = $array;
        return $this;
    }
    public function addGrantedNationality(Nationality $grantedNationality): self
    {
        if (!$this->grantedNationalities->contains($grantedNationality)) {
            $this->grantedNationalities[] = $grantedNationality;
        }

        return $this;
    }

    public function removeGrantedNationality(Nationality $grantedNationality): self
    {
        $this->grantedNationalities->removeElement($grantedNationality);

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getStudyField(): Collection
    {
        return $this->studyField;
    }
    public function setStudyField(?ArrayCollection $array): self
    {
        $this->studyField = $array;
        return $this;
    }

    public function setStudyFieldArray($array): self
    {
        foreach ($array as $value) {
            $this->addStudyField($value);
        }
        return $this;
    }
    
    public function addStudyField(Category $studyField): self
    {
        if (!$this->studyField->contains($studyField)) {
            $this->studyField[] = $studyField;
        }

        return $this;
    }

    public function removeStudyField(Category $studyField): self
    {
        $this->studyField->removeElement($studyField);

        return $this;
    }

    /**
     * @return Collection|Discipline[]
     */
    public function getStudyFieldTags(): Collection
    {
        return $this->studyFieldTags;
    }

    public function setStudyFieldTagArray($array)
    {
        foreach ($array as $key => $value) {
            $this->addStudyFieldTag($value);
        }
        return $this;
    }

    public function addStudyFieldTag(Discipline $studyFieldTag): self
    {
        if (!$this->studyFieldTags->contains($studyFieldTag)) {
            $this->studyFieldTags[] = $studyFieldTag;
        }

        return $this;
    }

    public function removeStudyFieldTag(Discipline $studyFieldTag): self
    {
        $this->studyFieldTags->removeElement($studyFieldTag);

        return $this;
    }

    public function getDurationUnit(): ?string
    {
        return $this->durationUnit;
    }

    public function setDurationUnit(?string $durationUnit): self
    {
        $this->durationUnit = $durationUnit;

        return $this;
    }

    /**
     * @return Collection|Country[]
     */
    public function getGrantedCountries(): Collection
    {
        return $this->grantedCountries;
    }

    public function setGrantedCountries(?ArrayCollection $array): self
    {
        $this->grantedCountries = $array;
        return $this;
    }

    public function addGrantedCountry(Country $grantedCountry): self
    {
        if (!$this->grantedCountries->contains($grantedCountry)) {
            $this->grantedCountries[] = $grantedCountry;
        }

        return $this;
    }

    public function removeGrantedCountry(Country $grantedCountry): self
    {
        $this->grantedCountries->removeElement($grantedCountry);

        return $this;
    }

    /**
     * @return Collection|Level[]
     */
    public function getGrantedLevel(): Collection
    {
        return $this->grantedLevel;
    }
    
    public function setGrantedLevel(?ArrayCollection $array):self
    {
        $this->grantedLevel = $array;
        return $this;
    }

    public function setGrantedLevelArray($array):self
    {
        foreach ($array as $key => $value) {
            $this->addGrantedLevel($value);
        }
        return $this;
    }

    public function addGrantedLevel(Level $grantedLevel): self
    {
        if (!$this->grantedLevel->contains($grantedLevel)) {
            $this->grantedLevel[] = $grantedLevel;
        }

        return $this;
    }

    public function removeGrantedLevel(Level $grantedLevel): self
    {
        $this->grantedLevel->removeElement($grantedLevel);

        return $this;
    }

    public function getSocialNetwork(): ?SocialNetwork
    {
        return $this->socialNetwork;
    }

    public function setSocialNetwork(?SocialNetwork $socialNetwork): self
    {
        $this->socialNetwork = $socialNetwork;

        return $this;
    }

    public function getSiteUrl(): ?string
    {
        return $this->siteUrl;
    }

    public function setSiteUrl(?string $siteUrl): self
    {
        $this->siteUrl = $siteUrl;

        return $this;
    }

    /**
     * @return Collection|InformationRequest[]
     */
    public function getInformationRequests(): Collection
    {
        return $this->informationRequests;
    }

    public function addInformationRequest(InformationRequest $informationRequest): self
    {
        if (!$this->informationRequests->contains($informationRequest)) {
            $this->informationRequests[] = $informationRequest;
            $informationRequest->setScholarship($this);
        }

        return $this;
    }

    public function removeInformationRequest(InformationRequest $informationRequest): self
    {
        if ($this->informationRequests->removeElement($informationRequest)) {
            // set the owning side to null (unless already changed)
            if ($informationRequest->getScholarship() === $this) {
                $informationRequest->setScholarship(null);
            }
        }

        return $this;
    }
    public function setInformationRequests(?ArrayCollection $collection): self
    {
        $this->informationRequests = $collection;
        return $this;
    }
    
    public function getContactAdresseMap(): ?string
    {
        return $this->contactAdresseMap;
    }

    public function setContactAdresseMap(?string $contactAdresseMap): self
    {
        $this->contactAdresseMap = $contactAdresseMap;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }
    
    /**
     * @return Collection|Application[]
     */
    public function getApplications(): Collection
    {
        return $this->applications;
    }
    public function setApplications(?ArrayCollection $collection): self
    {
        $this->applications = $collection;
        return $this;
    }

    public function addApplication(Application $application): self
    {
        if (!$this->applications->contains($application)) {
            $this->applications[] = $application;
            $application->setScholarship($this);
        }

        return $this;
    }

    public function removeApplication(Application $application): self
    {
        if ($this->applications->removeElement($application)) {
            // set the owning side to null (unless already changed)
            if ($application->getScholarship() === $this) {
                $application->setScholarship(null);
            }
        }

        return $this;
    }

    public function removeAllApplications(): self
    {
        $applications = $this->getApplications();
        foreach ($applications as $application) {
            $this->removeApplication($application);
        }
        return $this;
    }

    public function getOriginCountry(): ?Country
    {
        return $this->originCountry;
    }

    public function setOriginCountry(?Country $originCountry): self
    {
        $this->originCountry = $originCountry;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getRelatedOrganization(): ?Organization
    {
        return $this->relatedOrganization;
    }

    public function setRelatedOrganization(?Organization $relatedOrganization): self
    {
        $this->relatedOrganization = $relatedOrganization;

        return $this;
    }

    public function getAwardOrganizationType(): ?OrganizationType
    {
        return $this->awardOrganizationType;
    }

    public function setAwardOrganizationType(?OrganizationType $awardOrganizationType): self
    {
        $this->awardOrganizationType = $awardOrganizationType;

        return $this;
    }

   
    /**
     * @return Collection|SeenScholarship[]
     */
    public function getseenUsers(): Collection
    {
        return $this->seenUsers;
    }

    /**
     * @return Collection|FavoriteScholarship[]
     */
    public function getInterestedUsers(): Collection
    {
        return $this->interestedUsers;
    }

    /**
     * @return Form
     */
    public function getForm(): ?Form
    {
        return $this->form;
    }

    public function setForm(?Form $form): self
    {
       $this->form = $form;
       return $this;
    }

    /**
     * @return mixed
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param mixed $deleted
     */
    public function setDeleted($deleted): void
    {
        $this->deleted = $deleted;
    }

}
