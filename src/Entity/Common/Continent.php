<?php

namespace App\Entity\Common;

use App\Entity\Scholarship\Scholarship;
use App\Repository\Common\ContinentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=ContinentRepository::class)
 * @ORM\Table(name="common_continent")
 * @Serializer\ExclusionPolicy("all")  
 */
class Continent
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"details","list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     * @Serializer\Expose()
     * @Serializer\Groups({"details","list"})
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=Scholarship::class, mappedBy="grantedContinent")
     */
    private $scholarships;

    /**
     * @ORM\OneToMany(targetEntity=Country::class, mappedBy="continent")
     */
    private $countries;

    public function __construct()
    {
        $this->scholarships = new ArrayCollection();
        $this->countries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Scholarship[]
     */
    public function getScholarships(): Collection
    {
        return $this->scholarships;
    }

    public function addScholarship(Scholarship $scholarship): self
    {
        if (!$this->scholarships->contains($scholarship)) {
            $this->scholarships[] = $scholarship;
            $scholarship->addGrantedContinent($this);
        }

        return $this;
    }

    public function removeScholarship(Scholarship $scholarship): self
    {
        if ($this->scholarships->removeElement($scholarship)) {
            $scholarship->removeGrantedContinent($this);
        }

        return $this;
    }

    /**
     * @return Collection|Country[]
     */
    public function getCountries(): Collection
    {
        return $this->countries;
    }

    public function addCountry(Country $country): self
    {
        if (!$this->countries->contains($country)) {
            $this->countries[] = $country;
            $country->setContinent($this);
        }

        return $this;
    }

    public function removeCountry(Country $country): self
    {
        if ($this->countries->removeElement($country)) {
            // set the owning side to null (unless already changed)
            if ($country->getContinent() === $this) {
                $country->setContinent(null);
            }
        }

        return $this;
    }
}
