<?php

namespace App\Entity\Common;

use App\Entity\Student\Formation;
use App\Repository\Common\GradeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=GradeRepository::class)
 * @ORM\Table(name="common_grade")
 * @Serializer\ExclusionPolicy("all") 
 */
class Grade
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose() 
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=15)
     * @Serializer\Expose() 
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Formation::class, mappedBy="grade")
     */
    private $formations;

    public function __construct()
    {
        $this->formations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Formation[]
     */
    public function getFormations(): Collection
    {
        return $this->formations;
    }

    public function addFormation(Formation $formation): self
    {
        if (!$this->formations->contains($formation)) {
            $this->formations[] = $formation;
            $formation->setGrade($this);
        }

        return $this;
    }

    public function removeFormation(Formation $formation): self
    {
        if ($this->formations->removeElement($formation)) {
            // set the owning side to null (unless already changed)
            if ($formation->getGrade() === $this) {
                $formation->setGrade(null);
            }
        }

        return $this;
    }
}
