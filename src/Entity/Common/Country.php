<?php

namespace App\Entity\Common;

use App\Entity\Organization\Organization;
use App\Entity\Scholarship\Scholarship;
use App\Entity\Student\School;
use App\Entity\Student\Student;
use App\Entity\University\University;
use App\Repository\Common\CountryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;


/**
 * @ORM\Entity(repositoryClass=CountryRepository::class)
 * @Serializer\ExclusionPolicy("all") 
 */
class Country
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Organization::class, mappedBy="country")
     */
    private $organizations;

    /**
     * @ORM\ManyToMany(targetEntity=Organization::class, mappedBy="countryAction")
     */
    private $organizationsAction;

    /**
     * @ORM\Column(type="string", length=3)
     * @Serializer\Expose()
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity=Continent::class, inversedBy="countries")
     */
    private $continent;

    /**
     * @ORM\ManyToMany(targetEntity=Scholarship::class, mappedBy="grantedCountries")
     */
    private $scholarships;

    /**
     * @ORM\ManyToOne(targetEntity=Currency::class, inversedBy="country")
     */
    private $currency;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $symbol;

    /**
     * @ORM\OneToMany(targetEntity=University::class, mappedBy="country")
     */
    private $universities;

    /**
     * @ORM\ManyToMany(targetEntity=Student::class, mappedBy="desiredCountries")
     */
    private $students;

    /**
     * @ORM\OneToMany(targetEntity=Scholarship::class, mappedBy="originCountry")
     */
    private $scholarshipsAwarded;

    /**
     * @ORM\OneToMany(targetEntity=Student::class, mappedBy="country")
     */
    private $originStudents;

    /**
     * @ORM\OneToMany(targetEntity=Organization::class, mappedBy="contactCountry")
     */
    private $resideContactOrganizations;

    /**
     * @ORM\OneToMany(targetEntity=School::class, mappedBy="country")
     */
    private $schools;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"})
     */
    private $location = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nameFr;

    public function __construct()
    {
        $this->organizations = new ArrayCollection();
        $this->organizationsAction = new ArrayCollection();
        $this->scholarships = new ArrayCollection();
        $this->universities = new ArrayCollection();
        $this->students = new ArrayCollection();
        $this->scholarshipsAwarded = new ArrayCollection();
        $this->originStudents = new ArrayCollection();
        $this->resideContactOrganizations = new ArrayCollection();
        $this->schools = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Organization[]
     */
    public function getOrganizations(): Collection
    {
        return $this->organizations;
    }

    public function addOrganization(Organization $organization): self
    {
        if (!$this->organizations->contains($organization)) {
            $this->organizations[] = $organization;
            $organization->setCountry($this);
        }

        return $this;
    }

    public function removeOrganization(Organization $organization): self
    {
        if ($this->organizations->removeElement($organization)) {
            // set the owning side to null (unless already changed)
            if ($organization->getCountry() === $this) {
                $organization->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Organization[]
     */
    public function getOrganizationsAction(): Collection
    {
        return $this->organizationsAction;
    }

    public function addOrganizationsAction(Organization $organizationsAction): self
    {
        if (!$this->organizationsAction->contains($organizationsAction)) {
            $this->organizationsAction[] = $organizationsAction;
            $organizationsAction->addCountryAction($this);
        }

        return $this;
    }

    public function removeOrganizationsAction(Organization $organizationsAction): self
    {
        if ($this->organizationsAction->removeElement($organizationsAction)) {
            $organizationsAction->removeCountryAction($this);
        }

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

   
    public function getContinent(): ?Continent
    {
        return $this->continent;
    }

    public function setContinent(?Continent $continent): self
    {
        $this->continent = $continent;

        return $this;
    }

    /**
     * @return Collection|Scholarship[]
     */
    public function getScholarships(): Collection
    {
        return $this->scholarships;
    }

    public function addScholarship(Scholarship $scholarship): self
    {
        if (!$this->scholarships->contains($scholarship)) {
            $this->scholarships[] = $scholarship;
            $scholarship->addGrantedCountry($this);
        }

        return $this;
    }

    public function removeScholarship(Scholarship $scholarship): self
    {
        if ($this->scholarships->removeElement($scholarship)) {
            $scholarship->removeGrantedCountry($this);
        }

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    public function setSymbol(?string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * @return Collection|University[]
     */
    public function getUniversities(): Collection
    {
        return $this->universities;
    }

    public function addUniversity(University $university): self
    {
        if (!$this->universities->contains($university)) {
            $this->universities[] = $university;
            $university->setCountry($this);
        }

        return $this;
    }

    public function removeUniversity(University $university): self
    {
        if ($this->universities->removeElement($university)) {
            // set the owning side to null (unless already changed)
            if ($university->getCountry() === $this) {
                $university->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Student[]
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function addStudent(Student $student): self
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
            $student->addDesiredCountry($this);
        }

        return $this;
    }

    public function removeStudent(Student $student): self
    {
        if ($this->students->removeElement($student)) {
            $student->removeDesiredCountry($this);
        }

        return $this;
    }

    /**
     * @return Collection|Scholarship[]
     */
    public function getScholarshipsAwarded(): Collection
    {
        return $this->scholarshipsAwarded;
    }

    public function addScholarshipsAwarded(Scholarship $scholarshipsAwarded): self
    {
        if (!$this->scholarshipsAwarded->contains($scholarshipsAwarded)) {
            $this->scholarshipsAwarded[] = $scholarshipsAwarded;
            $scholarshipsAwarded->setOriginCountry($this);
        }

        return $this;
    }

    public function removeScholarshipsAwarded(Scholarship $scholarshipsAwarded): self
    {
        if ($this->scholarshipsAwarded->removeElement($scholarshipsAwarded)) {
            // set the owning side to null (unless already changed)
            if ($scholarshipsAwarded->getOriginCountry() === $this) {
                $scholarshipsAwarded->setOriginCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Student[]
     */
    public function getOriginStudents(): Collection
    {
        return $this->originStudents;
    }

    public function addOriginStudent(Student $originStudent): self
    {
        if (!$this->originStudents->contains($originStudent)) {
            $this->originStudents[] = $originStudent;
            $originStudent->setCountry($this);
        }

        return $this;
    }

    public function removeOriginStudent(Student $originStudent): self
    {
        if ($this->originStudents->removeElement($originStudent)) {
            // set the owning side to null (unless already changed)
            if ($originStudent->getCountry() === $this) {
                $originStudent->setCountry(null);
            }
        }

        return $this;
    }

    public function __toString() : string
    {
        return (string) $this->getId();
    }

    /**
     * @return Collection|Organization[]
     */
    public function getResideContactOrganizations(): Collection
    {
        return $this->resideContactOrganizations;
    }

    public function addResideContactOrganization(Organization $resideContactOrganization): self
    {
        if (!$this->resideContactOrganizations->contains($resideContactOrganization)) {
            $this->resideContactOrganizations[] = $resideContactOrganization;
            $resideContactOrganization->setContactCountry($this);
        }

        return $this;
    }

    public function removeResideContactOrganization(Organization $resideContactOrganization): self
    {
        if ($this->resideContactOrganizations->removeElement($resideContactOrganization)) {
            // set the owning side to null (unless already changed)
            if ($resideContactOrganization->getContactCountry() === $this) {
                $resideContactOrganization->setContactCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|School[]
     */
    public function getSchools(): Collection
    {
        return $this->schools;
    }

    public function addSchool(School $school): self
    {
        if (!$this->schools->contains($school)) {
            $this->schools[] = $school;
            $school->setCountry($this);
        }

        return $this;
    }

    public function removeSchool(School $school): self
    {
        if ($this->schools->removeElement($school)) {
            // set the owning side to null (unless already changed)
            if ($school->getCountry() === $this) {
                $school->setCountry(null);
            }
        }

        return $this;
    }

    public function getLocation(): ?array
    {
        return $this->location;
    }

    public function setLocation(?array $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getNameFr(): ?string
    {
        return $this->nameFr;
    }

    public function setNameFr(?string $nameFr): self
    {
        $this->nameFr = $nameFr;

        return $this;
    }

}
