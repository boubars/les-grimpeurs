<?php

namespace App\Entity\Common;

use App\Entity\Scholarship\Scholarship;
use App\Entity\Student\Student;
use App\Repository\Common\LevelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=LevelRepository::class)
 * @Serializer\ExclusionPolicy("all") 
 */
class Level
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose() 
     * @Serializer\Groups({"list","details"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Serializer\Expose()
     * @Serializer\Groups({"list","details"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Student::class, mappedBy="currentLevel")
     */
    private $students;

    /**
     * @ORM\ManyToMany(targetEntity=Scholarship::class, mappedBy="grantedLevel")
     */
    private $scholarships;

    public function __construct()
    {
        $this->students = new ArrayCollection();
        $this->scholarships = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Student[]
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function addStudent(Student $student): self
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
            $student->setCurrentLevel($this);
        }

        return $this;
    }

    public function removeStudent(Student $student): self
    {
        if ($this->students->removeElement($student)) {
            // set the owning side to null (unless already changed)
            if ($student->getCurrentLevel() === $this) {
                $student->setCurrentLevel(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Scholarship[]
     */
    public function getScholarships(): Collection
    {
        return $this->scholarships;
    }

    public function addScholarship(Scholarship $scholarship): self
    {
        if (!$this->scholarships->contains($scholarship)) {
            $this->scholarships[] = $scholarship;
            $scholarship->addGrantedLevel($this);
        }

        return $this;
    }

    public function removeScholarship(Scholarship $scholarship): self
    {
        if ($this->scholarships->removeElement($scholarship)) {
            $scholarship->removeGrantedLevel($this);
        }

        return $this;
    }

    public function __toString() : string
    {
        return (string) $this->getId();
    }
}
