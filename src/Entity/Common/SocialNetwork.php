<?php

namespace App\Entity\Common;

use App\Repository\Common\SocialNetworkRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=SocialNetworkRepository::class)
 * @Serializer\ExclusionPolicy("all") 
 */
class SocialNetwork
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"details","list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true, length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"details","list"})
     */
    private $facebook;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true, length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"details","list"})
     */
    private $twitter;

    /**
     * @ORM\Column(type="string", length=255,  nullable=true, length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"details","list"})
     */
    private $youtube;

    /**
     * @ORM\Column(type="string", length=255 ,  nullable=true, length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"details","list"})
     */
    private $instagram;

    /**
     * @ORM\Column(type="string", length=255 ,  nullable=true, length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"details","list"})
     */
    private $linkedin;

    public function getId(): ?int
    {
        return $this->id;
    }

    /** need for clone social network */
    public function setId(?int $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(?string $facebook): self
    {
        $this->facebook = $facebook;

        return $this;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function setTwitter(?string $twitter): self
    {
        $this->twitter = $twitter;

        return $this;
    }

    public function getYoutube(): ?string
    {
        return $this->youtube;
    }

    public function setYoutube(?string $youtube): self
    {
        $this->youtube = $youtube;

        return $this;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function setInstagram(?string $instagram): self
    {
        $this->instagram = $instagram;

        return $this;
    }

    public function getLinkedin(): ?string
    {
        return $this->linkedin;
    }

    public function setLinkedin(?string $linkedin): self
    {
        $this->linkedin = $linkedin;

        return $this;
    }
}
