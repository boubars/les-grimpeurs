<?php

namespace App\Entity\Common;

use App\Repository\Common\CurrencyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=CurrencyRepository::class)
 */
class Currency
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose() 
     * @Serializer\Groups({"list","details"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose() 
     * @Serializer\Groups({"list","details"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=4)
     * @Serializer\Expose() 
     * @Serializer\Groups({"list","details"})
     */
    private $code;

    /**
     * @ORM\Column(type="float")
     * @Serializer\Expose() 
     * @Serializer\Groups({"details"})
     */
    private $usdRate;

    /**
     * @ORM\OneToMany(targetEntity=Country::class, mappedBy="currency")
     */
    private $country;

    public function __construct()
    {
        $this->country = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getUsdRate(): ?float
    {
        return $this->usdRate;
    }

    public function setUsdRate(float $usdRate): self
    {
        $this->usdRate = $usdRate;

        return $this;
    }

    /**
     * @return Collection|Country[]
     */
    public function getCountry(): Collection
    {
        return $this->country;
    }

    public function addCountry(Country $country): self
    {
        if (!$this->country->contains($country)) {
            $this->country[] = $country;
            $country->setCurrency($this);
        }

        return $this;
    }

    public function removeCountry(Country $country): self
    {
        if ($this->country->removeElement($country)) {
            // set the owning side to null (unless already changed)
            if ($country->getCurrency() === $this) {
                $country->setCurrency(null);
            }
        }

        return $this;
    }
}
