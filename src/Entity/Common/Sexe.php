<?php
namespace App\Entity\Common;

class Sexe {
    
    const MAN   = 'man';
    const WOMAN = 'woman';
    const TRANSGENRE = 'transgenre';

    public static function getOptions(){
        return [ self::MAN, self::WOMAN, self::TRANSGENRE ];
    }
}