<?php

namespace App\Entity\Common;

use App\Repository\Common\GalleryRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=GalleryRepository::class)
 * @Serializer\ExclusionPolicy("all") 
 */
class Gallery
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"details","list"})
     */
    private $id;

    /**
     * @ORM\Column(type="array")
     * @Serializer\Expose()
     * @Serializer\Groups({"details","list"})
     */
    private $pictures = [];

    /**
     * @ORM\Column(type="array")
     * @Serializer\Expose()
     * @Serializer\Groups({"details","list"})
     */
    private $videos = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"details","list"})
     */
    private $documents = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    /** need for clone orgnaization gallery */
    public function setId(?int $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getPictures(): ?array
    {
        return $this->pictures;
    }

    public function setPictures(array $pictures): self
    {
        $this->pictures = $pictures;

        return $this;
    }
    public function addPicture(string $picture): self
    {
        $this->pictures[] = $picture;
        return $this;
    }

    public function getVideos(): ?array
    {
        return $this->videos;
    }

    public function setVideos(array $videos): self
    {
        $this->videos = $videos;

        return $this;
    }
    public function addVideo(string $video): self
    {
        $this->videos[] = $video;

        return $this;
    }

    public function getDocuments(): ?array
    {
        return $this->documents;
    }

    public function setDocuments(?array $documents): self
    {
        $this->documents = $documents;

        return $this;
    }
    public function addDocument(string $document): self
    {
        $this->documents[] = $document;

        return $this;
    }
}
