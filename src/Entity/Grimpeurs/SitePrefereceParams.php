<?php

namespace App\Entity\Grimpeurs;

use Symfony\Contracts\Translation\TranslatorInterface;

class SitePrefereceParams {

    public static function params(TranslatorInterface $translator)
    {
        return  [
            [
                'key'   => 'cgu_url',
                'label' => $translator->trans("Condition génerale d'utilisation"),
                'desc'  => $translator->trans("URL de la page condition génerale d'utilisation"),
                'value' => false
            ],
            [
                'key'   => 'policy_url',
                'label' => $translator->trans("Politique de confidentialité"),
                'desc'  => $translator->trans("URL de la page Politique de confidentialité"),
                'value' => false
            ],
            [
                'key'   => 'social_url_facebook',
                'label' => $translator->trans("Page facebook"),
                'desc'  => $translator->trans("Lien vers la page facebook de grimpeurs"),
                'value' => false
            ],
            [
                'key'   => 'social_url_twitter',
                'label' => $translator->trans("Profil twitter"),
                'desc'  => $translator->trans("Lien vers la profil twitter de grimpeurs"),
                'value' => false
            ],
            [
                'key'   => 'social_url_youtube',
                'label' => $translator->trans("Chaine youtube"),
                'desc'  => $translator->trans("Lien vers le chaine youtube de grimpeurs"),
                'value' => false
            ],
            [
                'key'   => 'social_url_linkedin',
                'label' => $translator->trans("Page linkedin"),
                'desc'  => $translator->trans("Lien vers le page linkedin de grimpeurs"),
                'value' => false
            ],
            [
                'key'   => 'social_url_instagram',
                'label' => $translator->trans("Profil instagram"),
                'desc'  => $translator->trans("Lien vers le profil de grimpeurs"),
                'value' => false
            ],
        ];
    }
}