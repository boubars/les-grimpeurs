<?php

namespace App\Entity\Grimpeurs;

use App\Repository\Grimpeurs\PostRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\String\Slugger\SluggerInterface;
use App\Entity\Traits\DateTimeEntityTrait;
use DateTime;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=PostRepository::class)
 * @Serializer\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks() 
 */
class Post
{

    const POST_TYPE = ['POST','PAGE','PRESS'];

    /** Date created and date update */
      Use DateTimeEntityTrait;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose() 
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     * @Serializer\Expose() 
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose() 
     */
    private $name;

        /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose() 
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Serializer\Expose() 
     */
    private $lang;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Serializer\Expose() 
     */
    private $type;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Expose() 
     */
    private $content;

    /**
     * @ORM\Column(type="array")
     * @Serializer\Expose() 
     */

    private $files = [];
      /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Serializer\Expose() 
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose() 
     */
    private $magazine;

    /**
     * @ORM\Column(type="array")
     */
    private $data = [];

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->name .'-'.($this->lang);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function computeSlug(SluggerInterface $slugger)
    {
        /*
        if(!$this->slug || '-' === $this->slug) {
            $this->slug = (string) $slugger->slug((string) $this)->lower();
        }*/
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLang(): ?string
    {
        return $this->lang;
    }

    public function setLang(string $lang): self
    {
        $this->lang = $lang;

        return $this;
    }

    
    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }


    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }


    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getDate(): ?Datetime
    {
        return $this->date;
    }

    public function setDate(Datetime $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getFiles(): ?array
    {
        return $this->files;
    }

    public function setFiles(array $files): self
    {
        $this->files = $files;

        return $this;
    }
    public function addFiles(string $file): self
    {
        $this->files[] = $file;
        return $this;
    }

    public function getMagazine(): ?string
    {
        return $this->magazine;
    }

    public function setMagazine(?string $magazine): self
    {
        $this->magazine = $magazine;

        return $this;
    }

    public function getData(): ?array
    {
        return $this->data;
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }
}
