<?php

namespace App\Entity\Discipline;

use App\Entity\Scholarship\Scholarship;
use App\Entity\Student\Student;
use App\Repository\Discipline\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 * @ORM\Table(name="discipline_category")
 * @Serializer\ExclusionPolicy("all") 
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose() 
     * @Serializer\Groups({"list", "details"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Discipline::class, mappedBy="category")
     */
    private $disciplines;

    /**
     * @ORM\ManyToMany(targetEntity=Scholarship::class, mappedBy="studyField")
     */
    private $scholarships;

        /**
     * @ORM\ManyToMany(targetEntity=Student::class, mappedBy="disciplines")
     */
    private $students;

    /**
     * @ORM\ManyToMany(targetEntity=Student::class, mappedBy="desiredDisciplines")
     */
    private $desiredStudents;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})
     */
    private $name_fr;



    public function __construct()
    {
        $this->disciplines =  new ArrayCollection();
        $this->scholarships = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Discipline[]
     */
    public function getDisciplines(): Collection
    {
        return $this->disciplines;
    }

    public function addDiscipline(Discipline $discipline): self
    {
        if (!$this->disciplines->contains($discipline)) {
            $this->disciplines[] = $discipline;
            $discipline->setCategory($this);
        }

        return $this;
    }

    public function removeDiscipline(Discipline $discipline): self
    {
        if ($this->disciplines->removeElement($discipline)) {
            // set the owning side to null (unless already changed)
            if ($discipline->getCategory() === $this) {
                $discipline->setCategory(null);
            }
        }

        return $this;
    }
    /**
     * @return Collection|Scholarship[]
     */
    public function getScholarships(): Collection
    {
        return $this->scholarships;
    }

    public function addScholarship(Scholarship $scholarship): self
    {
        if (!$this->scholarships->contains($scholarship)) {
            $this->scholarships[] = $scholarship;
            $scholarship->addStudyField($this);
        }

        return $this;
    }

    public function removeScholarship(Scholarship $scholarship): self
    {
        if ($this->scholarships->removeElement($scholarship)) {
            $scholarship->removeStudyField($this);
        }

        return $this;
    }

    /**
     * @return Collection|Student[]
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function addStudent(Student $student): self
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
            $student->addDiscipline($this);
        }

        return $this;
    }

    public function removeStudent(Student $student): self
    {
        if ($this->students->removeElement($student)) {
            $student->removeDiscipline($this);
        }

        return $this;
    }

    /**
     * @return Collection|Student[]
     */
    public function getDesiredStudents(): Collection
    {
        return $this->desiredStudents;
    }

    public function addDesiredStudents(Student $student): self
    {
        if (!$this->desiredStudents->contains($student)) {
            $this->desiredStudents[] = $student;
            $student->addDesiredDiscipline($this);
        }

        return $this;
    }

    public function removeDesiredStudents(Student $student): self
    {
        if ($this->desiredStudents->removeElement($student)) {
            $student->removeDesiredDiscipline($this);
        }

        return $this;
    }

    public function getNameFr(): ?string
    {
        return $this->name_fr;
    }

    public function setNameFr(?string $name_fr): self
    {
        $this->name_fr = $name_fr;

        return $this;
    }
}
