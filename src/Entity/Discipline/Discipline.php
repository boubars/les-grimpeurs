<?php

namespace App\Entity\Discipline;

use App\Entity\Student\Student;
use App\Repository\Discipline\DisciplineRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use App\Entity\Student\Formation;

/**
 * @ORM\Entity(repositoryClass=DisciplineRepository::class)
 * @Serializer\ExclusionPolicy("all") 
 */

class Discipline
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose()
     * @Serializer\Groups({"list", "details"})
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="disciplines")
     * @Serializer\Expose()
     */
    private $category;

    /**
    * @ORM\OneToMany(targetEntity=Formation::class, mappedBy="studyField")
    */
    private $formations;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameFr;

    public function __construct()
    {
        $this->students = new ArrayCollection();
        $this->desiredStudents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }
    /**
     * @return Collection|Formation[]
     */
    public function getFormations(): Collection
    {
        return $this->formations;
    }

    public function addFormation(Formation $formation): self
    {
        if (!$this->formations->contains($formation)) {
            $this->formations[] = $formation;
            $formation->setStudyField($this);
        }

        return $this;
    }

    public function removeFormation(Formation $formation): self
    {
        if ($this->formations->removeElement($formation)) {
            // set the owning side to null (unless already changed)
            if ($formation->getStudyField() === $this) {
                $formation->setStudyField(null);
            }
        }

        return $this;
    }

    public function __toString() : string
    {
        return (string) $this->getId();
    }

    public function getNameFr(): ?string
    {
        return $this->nameFr;
    }

    public function setNameFr(string $nameFr): self
    {
        $this->nameFr = $nameFr;

        return $this;
    }

}
