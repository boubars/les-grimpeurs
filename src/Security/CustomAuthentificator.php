<?php

namespace App\Security;

use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use App\Service\Mailer\Mailer;
use App\Manager\User\ActivationAccountManager;

class CustomAuthentificator extends AbstractGuardAuthenticator 
{
    use TargetPathTrait;


    private $entityManager;
    private $passwordEncoder;
    private $send_mail;
    private $activation_account;
    private $mailExist = false;
    private $passwordValid = false;
    private $mailer;
    private $manager;
    private $serializer;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder, Mailer $mailer , ActivationAccountManager $manager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->mailer = $mailer;
        $this->manager = $manager;
        $this->serializer = $serializer;
        
    }

    public function supports(Request $request)
    {
        if($request->get("_route") == "login"){
            return $request->isMethod('POST');
        }
        return false;

    }

    public function getCredentials(Request $request)
    {
       
        $credentials = [
            'username' => $request->request->get('username'),
            'password' => $request->request->get('password'),
            'typeAuth' => $request->request->get('typeAuth')
        ];
        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {

      
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $credentials['username']]);
        if (!$user) {
            throw new CustomUserMessageAuthenticationException('mail_not_exist');
        }else {
            $this->mailExist = true ;
        }
        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        
        $reponse = false;
        $checkPassword = $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
        $enable = $user->isEnable();
    
        if($checkPassword && $enable) {
            $reponse = true;
        }else if(!$user->isEnable() && $checkPassword ){
            $this->passwordValid = true;
            if(in_array('ROLE_STUDENT',$user->getRoles())){
                if($credentials['typeAuth']){
                    $reponse = true;
                    $this->manager->sendCodeActivate($user);
                }else{
                    throw new CustomUserMessageAccountStatusException('page_authentification');
                }
           
            }else{
                $this->manager->sendLinkActivate($user);
                throw new CustomUserMessageAccountStatusException('account_disabled');
            }
        }else{
            throw new CustomUserMessageAccountStatusException('password_not_exist');
        }
        return $reponse;
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function getPassword($credentials): ?string
    {
        return $credentials['password'];
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {

        /** @var User */
        $user  = $token->getUser();
        $user->setLastLogin(new \DateTime());
        $user->setIp($request->getClientIp());
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return new JsonResponse(["user" => json_decode($this->serializer->serialize($token->getUser(), 'json'))]);

    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
        ];
        $response = null;
        if(!($this->passwordValid &&  $this->mailExist)) {
            $response = new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
        }else{
            if($data['message']) {
                $response =  new JsonResponse($data, Response::HTTP_NOT_ACCEPTABLE);
            }else{
                $response =  new JsonResponse($data, Response::HTTP_CREATED);
            }
            
        }
        return $response ;
    }

    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        if ($request->isXmlHttpRequest()) {
            $data = [
                // you might translate this message
                'message' => 'authentication_required'
            ];

            return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
        } else {
            return new RedirectResponse("/");
        }

    }

    public function supportsRememberMe()
    {
        return true;
    }

}
