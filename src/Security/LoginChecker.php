<?php
namespace App\Security;


use App\Entity\User\User;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class LoginChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof User) {
            return;
        }
        if (!$user->isEnable()) {
            // the message passed to this exception is meant to be displayed to the user
            // second authetification
            if($user->getRoles('ROLE_STUDENT')){
                throw new CustomUserMessageAccountStatusException('page_authentification');
            }else{
                throw new CustomUserMessageAccountStatusException('Compte desactivé');
            }
            
        }
    }

    public function checkPostAuth(UserInterface $user)
    {
        if (!$user instanceof User) {
            return;
        }

        // user account is expired, the user may be notified
        if (! $user->isEnable()) {
            throw new AccountExpiredException('...');
        }
    }
}
