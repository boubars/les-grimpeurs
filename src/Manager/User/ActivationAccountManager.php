<?php namespace App\Manager\User;

use App\Entity\User\User;
use App\Entity\Student\Student;
use App\Manager\BaseManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use DateTime;
use DateInterval;
use App\Service\Mailer\Mailer;
use App\Service\Utilis\TokenGenerator;
use Exception;

class ActivationAccountManager extends BaseManager
{
    private $mailer;
    public function __construct(EntityManagerInterface $em ,  Mailer $mailer)
    {
        parent::__construct($em, User::class, null);
        $this->mailer = $mailer;
    }
    public function handle($token)
    {
        $user = $this->repository->findOneBy(['token' => $token]);

        if (!$user) {
            return false ;
        
        }
        return $this->enableUser($user);
    }

    public function activationByCode($request) {
        $data =json_decode($request->getContent());
        $user = $this->entityManager->getRepository(Student::class)->findOneBy(['codeActivate' => $data->code , 'email' => $data->email]);
        if(!$user || ($user->getDateExpired() == null || $user->getDateExpired() < new DateTime())){
            return false;
        }
        
        return $this->enableUser($user);
    } 
    
    private function enableUser($user) {
        $user->setEnable(true)
        ->setToken("");
        $this->entityManager->persist($user);
        $this->entityManager->flush();
        return $user;
    
    }
    public function sendCodeActivate(UserInterface $user) {
        $reponse = false;
        $user->setCodeActivate(strval(rand(111111,999999)));
        $now = new DateTime() ; 
        $now->add(new DateInterval('PT20M'));
        $user->setDateExpired($now);
        try {
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $this->mailer->sendUserActivationCode($user);
            $reponse = true;
        } catch (Exception $e) {
           
        }

        return $reponse;
    }

    public function sendLinkActivate( $user) {
        $reponse = false;
        $user->setToken(TokenGenerator::generate(15));
        try {
            $this->mailer->sendUserActivationLink($user);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $reponse = true;
        } catch (Exception $e) {
           
        }

        return $reponse;

    }





 
}
