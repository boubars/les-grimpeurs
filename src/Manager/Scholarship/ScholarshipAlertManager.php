<?php


namespace App\Manager\Scholarship;


use App\Entity\Scholarship\Scholarship;
use App\Entity\Scholarship\ScholarshipAlert;
use App\Entity\User\User;
use App\Manager\BaseManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class ScholarshipAlertManager extends BaseManager
{

    public function __construct(EntityManagerInterface $em , ValidatorInterface $validator )
    {
        parent::__construct($em, ScholarshipAlert::class,$validator);
    }

    /**
     * @param Scholarship $scholarship
     * @return ScholarshipAlert[]
     */
    public function getMatchedAlerts(Scholarship $scholarship): array
    {
        $grantedLevels = $scholarship->getGrantedLevel();
        $aLevels = [];
        foreach ($grantedLevels as $grantedLevel) {
            $aLevels[] = $grantedLevel->getId();
        }

        $grantedDisciplines = $scholarship->getStudyField();
        $aCategorie = [];
        foreach ($grantedDisciplines as $grantedDiscipline) {
            $aCategorie[] = $grantedDiscipline->getId();
        }

        $grantedCountries = $scholarship->getGrantedCountries();
        $aCountries = [];
        foreach ($grantedCountries as $grantedCountry) {
            $aCountries[] = $grantedCountry->getId();
        }
        /** @var ScholarshipAlert[] $alerts */
       return $this->entityManager->getRepository(ScholarshipAlert::class)->findWithCriteria($aLevels, $aCategorie, $aCountries );

    }

}