<?php

namespace App\Manager\Student;

use App\Entity\Student\Student;
use App\Entity\Student\Publication;
use App\Entity\User\User;
use App\Manager\BaseManager;
use App\Repository\Student\PublicationRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PublicationManager extends BaseManager
{
    

    /**
     * Method __construct
     *
     * @param EntityManagerInterface $em 
     * @param ValidatorInterface $validator 
     *
     * @return void
     */

    private $publicationRepository;

    public function __construct(EntityManagerInterface $em , ValidatorInterface $validator , PublicationRepository $publicationRepository) 
    {
        parent::__construct($em, User::class,$validator);
        $this->publicationRepository =  $publicationRepository;
        
    }



    public function savePublication(Request $request , Student $student  )
    {
        $response =array('status' => false , 'message' => '' ) ;
 
        $data =json_decode($request->getContent());
        $publication = ($data->id == null )? new Publication() : $this->publicationRepository->findOneBy(['id' => $data->id]);
        $publication->setTitle($data->title ?? '');
        $publication->setEditor($data->editor ?? '');
        $publication->setAuthor($data->author ?? '');
        $publication->setUrl($data->url ?? '');
        $publication->setDescription($data->description ?? '');
        $publication->setPublicationDate(new \DateTime($data->publicationDate));
        $errors = $this->validator->validate($publication) ;
        if(count($errors) == 0 ) {
            try {
                $student->addPublication($publication);
                $this->save($student);
                $response['status'] = true;
                $response['message'] = 'update_pub_OK';
            } catch (Exception $e) {
                $response['message'] = "Internal_error" ;
            }
        }else {
            $response['message'] = $errors->get(0)->getMessage();
        }

        return $response;
      
    }
}


