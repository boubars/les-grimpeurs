<?php

namespace App\Manager\Student;

use App\Entity\Student\Student;
use App\Entity\User\User;
use App\Manager\BaseManager;
use App\Service\Utilis\TokenGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegistrationManager extends BaseManager
{
    /**
     * passwordEncoder
     *
     * @var mixed
     */
    private $passwordEncoder;

    /**
     * Method __construct
     *
     * @param UserPasswordEncoderInterface $encoder [explicite description]
     * @param EntityManagerInterface $em [explicite description]
     *
     * @return void
     */
    public function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $em , ValidatorInterface $validator) 
    {
        parent::__construct($em, User::class,$validator);
        $this->passwordEncoder = $encoder;
    }



    /**
     * Method handle
     *
     * @param Request $request [explicite description]
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $student = new Student();
        $student->setRoles(["ROLE_STUDENT"]);
        $data =json_decode($request->getContent());
        
        $email = $data->email;
        /** Check if email exist */
        if($this->checkEmail($email)){
            return "mail_exist";
        }

        if(!$this->checkFileRequire($data)){
            return "field_required";
        }

     
        if(!$this->validPassword($data->password)){
            return "password_invalid";
        }

        
        $student->setEmail($email);
        $student->setToken(TokenGenerator::generate(15));

        $student->setCodeActivate(strval(rand(111111,999999)));
        $now = new \DateTime() ; 
        $now->add(new \DateInterval('PT20M'));
        $student->setDateExpired($now);


        $student->setFirstname($data->firstname);
        $student->setLastName($data->lastname);
        $student->setPhone(str_replace(' ', '' , $data->phone));
        $student->setPassword($this->passwordEncoder->encodePassword($student, $data->password));
        
        /**
         * Throw error JSON_ARRAY
         * $errors = $this->validator->validate($student);
         * 
         */

        //if(count($errors) == 0 ) {
        try {
            $this->save($student);
            return true;
        } catch (Exception $e) {
            echo $e;
            return "Internal_error" ;
        }
        //}
        /*
        else {
            return $errors->get(0)->getMessage();
        }*/
      
    }

    public function checkEmail($email){
        $exist = $this->repository->findOneByEmail($email);
        return $exist ? true : false ; 
    }

    public function checkFileRequire($data) {
        return ($data->phone !== '' && $data->lastname !== '' && $data->firstname !== '' );
    }

    public function validPassword($password) {
        $lenght  = strlen($password) >= 8;
        $number = preg_match("#[0-9]+#", $password); 
        $lowercase = preg_match("#[a-z]+#" , $password);
        $uppercase = preg_match("#[a-z]+#" , $password);
        return ($lenght && $number  && $lowercase && $uppercase);
    }
}
