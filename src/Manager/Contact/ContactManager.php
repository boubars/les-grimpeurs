<?php


namespace App\Manager\Contact;


use App\Entity\Common\Contact;
use App\Manager\BaseManager;
use App\Service\Mailer\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ContactManager
 * @package App\Controller
 */
class ContactManager extends BaseManager
{
    /**
     * @var Request
     */
    private $parameters;
    private $mailer;

    /**
     * ContactManager constructor.
     * @param EntityManagerInterface $entityManager
     * @param Mailer $mail
     */
    public function __construct(EntityManagerInterface $entityManager , Mailer $mail)
    {
        parent::__construct($entityManager, Contact::class, null);
        $this->mailer = $mail;

    }


    /**
     * @param $request
     * @return array
     */
    public function sendMessage($request):array
    {

        $this->parameters  = json_decode($request->getContent());
        $checkParameter = $this->checkParameter();

        $response = ['state' => false , 'message' =>  $checkParameter['message']];

        if($checkParameter['state']){     //check state parameter
            $contact = $checkParameter['data'] ;
            //save in database
            $this->entityManager->persist($contact);
            $this->entityManager->flush();
            $response['state'] = true;
            $response['message'] = 'message send succeed';

        }
        return $response;
    }


    /**
     * @param Request $request
     * @return array
     */
    private function checkParameter():array
    {
        //get parameter by client
        $fullname = $this->parameters->fullname;
        $phone = $this->parameters->phone;
        $subject = $this->parameters->subject;
        $email = $this->parameters->email;
        $message = $this->parameters->message;

        $response = ["state" =>  true , "message" => "parameter is complete" , 'data' => [] ];

        if($fullname === null || $fullname === '') {       //check full name
            $response['state'] = false ;
            $response['message'] = 'parameter fullname missing'  ;

        }elseif ($email === null || $email === '' || $this->validateEmail($email) == false) {     //check email
            $response['state'] = false ;
            $response['message'] = "parameter email missing or invalid" ;

        }elseif ($message === null || $message === '' ) { //check message
            $response['state'] = false;
            $response['message'] = "parameter message missing";
        } elseif  ($phone !== '' && $this->validatePhoneNumber($phone) == '') {
            $response['state'] = false;
            $response['message'] = "parameter phone invalid";
        }else{
            $contact = new Contact();
            $contact->setEmail($email);
            $contact->setFullname($fullname);
            $contact->setSubject($subject);
            $contact->setPhone(str_replace("-", "", $this->validatePhoneNumber($phone)));
            $contact->setMessage($message);
            $response['data'] = $contact;
        }
        return $response;
    }

    /**
     * @param $email
     * @return mixed
     */
    private function validateEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL) ;
    }

    /**
     * @param $phone
     * @return mixed
     */
    private function validatePhoneNumber($phone)
    {
        return  filter_var($phone, FILTER_SANITIZE_NUMBER_INT);

    }
}

