<?php

namespace App\Manager\Organization;

use App\Entity\Organization\Contact;
use App\Entity\Organization\Organization;
use App\Manager\BaseManager;
use App\Service\Utilis\TokenGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationManager extends BaseManager
{
    /**
     * passwordEncoder
     *
     * @var mixed
     */
    private $passwordEncoder;
    /**
     * Method __construct
     *
     * @param UserPasswordEncoderInterface $encoder [explicite description]
     * @param EntityManagerInterface $em [explicite description]
     *
     * @return void
     */
    public function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $em)
    {
        parent::__construct($em, Contact::class, null);
        $this->passwordEncoder = $encoder;
    }



    /**
     * Method handle
     *
     * @param Request $request [explicite description]
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $organization = new Contact();
        $organization->setRoles(["ROLE_ORGANIZATION"]);
        $data = json_decode($request->getContent());
        
        $email = $data->email;
        /** Check if email exist */
        if($this->checkEmail($email)){
            return "mail_exist";
        }

        $organization->setEmail($email);
        $organization->setToken(TokenGenerator::generate(15));

        $organization->setFirstname($data->firstname);
        $organization->setLastName($data->lastname);

        $organization->setPhone($data->phone);
        $organization->setPassword($this->passwordEncoder->encodePassword($organization,  $data->password));

        try {
            $this->save($organization);
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function checkEmail($email){
        $exist = $this->repository->findOneByEmail($email);
        return $exist ? true : false ; 
    }
}
