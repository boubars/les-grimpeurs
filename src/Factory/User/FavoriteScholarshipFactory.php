<?php
namespace App\Factory\User;

use App\Entity\Scholarship\Scholarship;
use App\Entity\User\FavoriteScholarship;
use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;

class FavoriteScholarshipFactory
{
    public static $entityManager,
             $userManager,
             $scholarshipManager;
             
    public function __construct(EntityManagerInterface $entityManager)
    {
        self::$entityManager       = $entityManager;
        self::$userManager         = self::$entityManager->getRepository(User::class);
        self::$scholarshipManager   = self::$entityManager->getRepository(Scholarship::class);

    }


    public static function create($scholarship, $user = false){

        if($user && !($user instanceof User)){
            $user = self::$userManager->findOneById($user);
        }

        if(!($scholarship instanceof Scholarship)){
            $scholarship = self::$scholarshipManager->findOneById($scholarship);
        }

        return (new FavoriteScholarship())
        ->setScholarship($scholarship)
        ->setUser($user ? $user : null);
    }
}