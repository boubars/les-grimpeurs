<?php
namespace App\Twig;

use App\Service\Utilis\Base64Converter;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;


class FormMakingExtension extends AbstractExtension
{

    
    public function __construct()
    {
       
    }


    public function getFunctions()
    {
        
        return [
            new TwigFunction(
                'FmGetInputConfig' , 
                [$this , 'FmGetInputConfig']
            ),
        ];
    }

    public function FmGetInputConfig($datakey, $list)
    { 
       $aDataKey = explode("_", $datakey);

       $key      = $aDataKey[1].'_'.$aDataKey[2];
       for($i=0; $i < count($list) ; $i++) { 
            $config = $list[$i];
            if($config['key'] == $key){
                return $config;
            } 
       }

       return null;
       



    }

   
}