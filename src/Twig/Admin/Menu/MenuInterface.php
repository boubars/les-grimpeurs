<?php
namespace App\Twig\Admin\Menu;

interface MenuInterface
{
    const ADMIN_MENU = [
        array(
            "label" => [
                "en"=> "Dashboard",
                "fr"=> "Tableau de board",
            ],
            "route" => "admin_index",
            "icon"  => "fa fa-dashboard"
        ),   
        
        array(
            "label" => [
                "en" => "Organization",
                "fr" => "Organisme"
            ],
            "route" => "admin_organization_index",
            "icon"  => "fa fa-home"
        ),
        array(
            "label" => [
                "en" => "Student",
                "fr" => "Etudiant"
            ],
            "route" => "admin_student_index",
            "icon"  => "fa fa-users"
        ),
        array(
            "label" => [
                "en" => "Scholarship",
                "fr" => "Bourse"
            ],
            "route" => "admin_scholarship_index",
            "icon"  => "fa fa-graduation-cap"
        ),
        array(
            "label" => [
                "en" => "Posts & Press",
                "fr" => "Articles & Presse"
            ],
            "route"  => "admin_post_index",
            "params" => [
                'type' => 'post'
            ],
            "icon"  => "fa fa-newspaper-o",
            "children" => [
                array(
                    "label" => [
                        "en" => "Post",
                        "fr" => "Articles"
                    ],
                    "route"  => "admin_post_index",
                    "params" => [
                        'type' => 'post'
                    ],
                    "icon"  => "fa fa-newspaper-o",
                ),
                array(
                    "label" => [
                        "en" => "Press",
                        "fr" => "Presse"
                    ],
                    "route"  => "admin_post_index",
                    "params" => [
                        'type' => 'press'
                    ],
                    "icon"  => "fa fa-newspaper-o",
                ),
            ]

        ),
        array(
            "label" => [
                "en" => "Pages",
                "fr" => "Pages"
            ],
            "route"  => "admin_post_index",
            "params" => [
                'type' => 'page'
            ],
            "icon"  => "fa fa-file-text",
            "children" => [
                array(
                    "label" => [
                        "en" => "UGC",
                        "fr" => "CGU"
                    ],
                    "route" => false,
                    "url" => "/page/cgu",
                    "icon"  => "fa fa-file-text"
                ),
                array(
                    "label" => [
                        "en" => "Concept",
                        "fr" => "Concept"
                    ],
                    'route'  => 'admin_concept_index',
                    "icon"  => "fa fa-lightbulb-o"
                ),
                array(
                    "label" => [
                        "en" => "Team",
                        "fr" => "Equipe"
                    ],
                    "route" => "admin_team_index",
                    "icon"  => "fa fa-users"
                ),
            ]
        ),
        array(
            "label" => [
                "en" => "Testimonial",
                "fr" => "Témoignage"
            ],
            "route" => "admin_student_testimonial_index",
            "icon"  => "fa fa-comments"
        ),
        array(
            "label" => [
                "en" => "Data",
                "fr" => "Data"
            ],
            "route" => "#",
            "icon"  => "fa fa-database",
            "children" => [
                array(
                    "label" => [
                        "en" => "Disciplines",
                        "fr" => "Disciplines"
                    ],
                    "route" => "admin_discipline_index",
                    "icon"  => "fa fa-check"
                ),
                array(
                    "label" => [
                        "en" => "Countries",
                        "fr" => "Pays"
                    ],
                    "route" => "admin_country_index",
                    "icon"  => "fa fa-globe"
                ),
                array(
                    "label" => [
                        "en" => "Languages",
                        "fr" => "Langues"
                    ],
                    "route" => "admin_language_index",
                    "icon"  => "fa fa-flag"
                )
            ]
        ),
        array(
            "label"  => [
                'en' => 'Site preference',
                'fr' => 'Site préference' 
            ],
            'route'  => 'admin_preference_index',
            'icon'   => 'fa fa-cogs'
        ),
        array(
            "label"  => [
                'en' => 'Messaging',
                'fr' => 'Messagerie' 
            ],
            'route'  => '#',
            'icon'   => 'fa fa-comment-o',
            "children" => [
                array(
                    "label"  => [
                        'en' => 'Livechat',
                        'fr' => 'Tchat' 
                    ],
                    'route'  => 'admin_message_live_chat',
                    'icon'   => 'fa fa-comments',
                ),
                array(
                    "label"  => [
                        'en' => 'Contact',
                        'fr' => 'Contact' 
                    ],
                    'route'  => 'admin_message_contact',
                    'icon'   => 'fa fa-envelope',
                ),
            ]
        ),
        array(
            "label"  => [
                'en' => 'FAQ',
                'fr' => 'FAQ' 
            ],
            'route'  => 'admin_faq_index',
            'icon'   => 'fa fa-question-circle'
        ),


    ];
}