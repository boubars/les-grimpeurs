<?php
namespace App\Twig\Admin\Menu;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AdminMenuBuilder extends AbstractExtension{
    
    public function getFunctions()
    {
        
        return [
            new TwigFunction(
                'adminSidebarMenu' , 
                [$this , 'getRender'], 
                array(
                    "needs_environment" => true,
                    "is_safe"           => ["html"]
                ) 
            )
        ];
    }

    public function getRender(\Twig\Environment $environment)
    {
        return $environment->render("admin/components/sidebar.html.twig",array(
            'menus' => MenuInterface::ADMIN_MENU 
        ));
    }
}