<?php
namespace App\Twig;

use App\Service\Utilis\Base64Converter;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;


class ImagesExtension extends AbstractExtension
{

    private $params;
    
    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }


    public function getFunctions()
    {
        
        return [
            new TwigFunction(
                'grimpeursLogo' , 
                [$this , 'grimpeursLogo']
            ),
            new TwigFunction(
                'getImage',
                [$this , 'getImage']
            ),
            new TwigFunction(
                'getUploadedImage',
                [$this , 'getUploadedImage']
            )
        ];
    }

    public function grimpeursLogo()
    { 
        return Base64Converter::toBase64(
            sprintf( "%s/assets/vue/assets/img/logo_menu.png", $this->params->get('project_dir')),'png'
        );   
    }
    public function getImage($name, $extension = 'png')
    { 
        return Base64Converter::toBase64(
            sprintf( "%s/assets/vue/assets/img/%s", $this->params->get('project_dir'), $name), $extension );   
    }
    public function getUploadedImage($name, $folder)
    { 
        return Base64Converter::toBase64(
            sprintf( "%s/public/uploads/%s/%s", $this->params->get('project_dir'), $folder, $name), 'png');   
    }
   
}