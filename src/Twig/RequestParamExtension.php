<?php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;


class RequestParamExtension extends AbstractExtension
{
    private $request;
    private $router;
    
    public function __construct(RequestStack $request, RouterInterface $router)
    {
    $this->request = $request;
    $this->router  = $router;
    }
    public function getFunctions()
    {
        
        return [
            new TwigFunction(
                'appendParam' , 
                [$this , 'appendParam']
            ),
            new TwigFunction(
                'removeParam' , 
                [$this , 'removeParam']
            )
        ];
    }

    public function appendParam($param, $value, $routeName = false)
    {
        $request = $this->request->getCurrentRequest();
        $params  = $request->query->all();
        $routeName = $routeName ? $routeName : $request->get("_route");

        $params[$param] = $value;
        return $this->router->generate($routeName,$params);
        
    }
    public function removeParam($param, $routeName = false)
    {
        $request = $this->request->getCurrentRequest();
        $params  = $request->query->all();
        $routeName = $routeName ? $routeName : $request->get("_route");

        if(isset($params[$param])){
            unset($params[$param]);
        }
        return $this->router->generate($routeName,$params);
        
    }
}