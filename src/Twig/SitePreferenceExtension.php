<?php
namespace App\Twig;

use App\Repository\Grimpeurs\SitePreferenceRepository;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;


class SitePreferenceExtension extends AbstractExtension
{

    private $preferences;
    
    public function __construct(SitePreferenceRepository $preferences)
    {
        $this->preferences = $preferences;
    }


    public function getFunctions()
    {
        
        return [
            new TwigFunction(
                'getPreference' , 
                [$this , 'getPreference']
            )
        ];
    }

    public function getPreference($key, $local = "en")
    { 
        $this->preferences->findOneByName($key);
    }
   
}