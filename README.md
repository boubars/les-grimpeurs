LesGrimpeurs Docker Orchestrator
==========================

Docker global deployment
------------------------
Build and Run:

***Build and Run Production env
```bash
$ docker-compose up -d
```

***Build and Run Dev env 
Create a local .env.local file from .env file
```bash
$ cp .env .env.local 
```
Use docker-compose.dev.yaml for developement env by specifying the docker compose file via -f params
```bash
$ docker-compose -f docker-compose.dev.yaml  up -d
```

Backend configuration
---------------------
Install symfony vendors:

```bash
$ docker-compose exec engine composer install
```

Then, update db schema:
```bash
$ docker-compose exec engine bin/console doctrine:schema:update --force
```

Compile email MJML Templace
```bash
$ docker-compose exec engine bin/console grimp:compile:emails
```


Frontend configuration
----------------------
Launch install :
```bash
$ docker-compose exec engine yarn install
```

Build encore webpack app, execute:

```bash
$ docker-compose exec engine yarn encore dev
```

For prod env :
```bash
$ docker-compose exec engine yarn encore production
```
Chmod upload :
```bash
sudo chmod 777 -R uploads/
```

Create default admin account 
---------------------------

```bash
php bin/console grimp:create:admin-user
```

RAZ command 
---------------------------
Resetting database data
```bash
  php bin/console grimp:raz:data
```

Resetting database structure and data
```bash
  php bin/console grimp:init:database
```

Notification Email System
---------------------------
command for launch alert email:
 ```bash
  php bin/console grimp:todo:alert
```


Generate default form application
```bash
$ docker-compose exec engine bin/console grimp:import:default-form
```


Cron tab config
 ```bash
  crontab -e
```

Add this line :
 ```bash
*/1 * * * * php /absolute-project-dir-path/bin/console grimp:todo:alert > /absolute-project-dir-path/var/log/job.log
```
Install DOM PDF fonts (necessary for NUNITO PDF fonts)
------------------------------------------------------
 ```bash
  php bin/console grimp:load:fonts
```

Aditional information
---------------------------
Wireframe : 
https://invis.io/RFU0GZO785M

Preprod : 
https://rec.les-grimpeurs.org




