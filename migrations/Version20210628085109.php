<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210628085109 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE student_discipline_category (student_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_A6321296CB944F1A (student_id), INDEX IDX_A632129612469DE2 (category_id), PRIMARY KEY(student_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE student_discipline_category ADD CONSTRAINT FK_A6321296CB944F1A FOREIGN KEY (student_id) REFERENCES user_student (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE student_discipline_category ADD CONSTRAINT FK_A632129612469DE2 FOREIGN KEY (category_id) REFERENCES discipline_category (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE student_category');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE student_category (student_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_B5C7555A12469DE2 (category_id), INDEX IDX_B5C7555ACB944F1A (student_id), PRIMARY KEY(student_id, category_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE student_category ADD CONSTRAINT FK_B5C7555A12469DE2 FOREIGN KEY (category_id) REFERENCES discipline_category (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE student_category ADD CONSTRAINT FK_B5C7555ACB944F1A FOREIGN KEY (student_id) REFERENCES user_student (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('DROP TABLE student_discipline_category');
    }
}
