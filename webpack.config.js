const Encore = require('@symfony/webpack-encore');
var dotenv = require('dotenv');
var path   = require('path')
Encore
// directory where compiled assets will be stored
  .setOutputPath('public/build/')
  // public path used by the web server to access the output path
  .setPublicPath('/build')
  // only needed for CDN's or sub-directory deploy
  // .setManifestKeyPrefix('build/')

  /*
   * ENTRY CONFIG
   *
   * Add 1 entry for each "page" of your app
   * (including one that's included on every page - e.g. "app")
   *
   * Each entry will result in one JavaScript file (e.g. app.js)
   * and one CSS file (e.g. app.css) if you JavaScript imports CSS.
   */
  .addEntry('app', './assets/vue/index.js')
  // .addEntry('page1', './assets/js/page1.js')
  // .addEntry('page2', './assets/js/page2.js')

  .addEntry('app_admin', './assets/admin/js/admin.js')
  .addEntry('app_admin_testimonial', './assets/admin/js/admin_testimonial.js')
  .addEntry('app_admin_organization', './assets/admin/js/organization/index.js')
  .addEntry('app_admin_student', './assets/admin/js/student/index.js')
  .addEntry('app_admin_scholarship', './assets/admin/js/scholarship/index.js')
  .addEntry('app_admin_post', './assets/admin/js/post/index.js')
  .addEntry('app_admin_concept', './assets/admin/js/concept/index.js')
  .addEntry('app_admin_message', './assets/admin/js/message/index.js')


  // will require an extra script tag for runtime.js
  // but, you probably want this, unless you're building a single-page app
  // .enableSingleRuntimeChunk()
  .disableSingleRuntimeChunk()

  /*
   * FEATURE CONFIG
   *
   * Enable & configure other features below. For a full
   * list of features, see:
   * https://symfony.com/doc/current/frontend.html#adding-more-features
   */
  .cleanupOutputBeforeBuild()
  .enableBuildNotifications()
  .enableSourceMaps(!Encore.isProduction())
  // enables hashed filenames (e.g. app.abc123.css)
  .enableVersioning(Encore.isProduction())
  .enableVueLoader()
  .configureBabel((config) => {
    config.plugins.push('@babel/plugin-transform-runtime');
  })
// enables Sass/SCSS support
 .enableSassLoader()

// admin assets image
.copyFiles([
  { from: './assets/admin/images',          to: 'admin/images/[path][name].[ext]' },
  { from: './node_modules/tinymce/themes' , to: 'themes/[path]/[name].[ext]'       },
  { from: './node_modules/tinymce/skins'  , to: 'skins/[path]/[name].[ext]'       },
  { from: './node_modules/tinymce/plugins', to: 'plugins/[path]/[name].[ext]'      },
  { from: './node_modules/tinymce/icons'  , to: 'icons/[path]/[name].[ext]'      },
])

 .configureDefinePlugin(options => {
    const env = dotenv.config();

    if (env.error) {
        throw env.error;
    }

    options['process.env'].VUE_APP_FRONT_URL  = JSON.stringify(env.parsed.VUE_APP_FRONT_URL);
    options['process.env'].GMAP_KEY           = JSON.stringify(env.parsed.GMAP_KEY);
    options['process.env'].VUE_APP_BOT_SERVER = JSON.stringify(env.parsed.VUE_APP_BOT_SERVER);
    options['process.env'].MERCURE_PUBLIC_URL = JSON.stringify(env.parsed.MERCURE_PUBLIC_URL);
  }).addAliases({
      '@'   : path.resolve(__dirname, './assets/vue/'),
  })
  
  .addLoader({
    test: /\.pug$/,
    loader: 'pug-plain-loader',
    oneOf: [
      {
        resourceQuery: /^\?vue/,
        use: ['pug-plain-loader']
      },
    ]
  }
  )
  ;

// uncomment if you use TypeScript
// .enableTypeScriptLoader()

// uncomment if you're having problems with a jQuery plugin
// .autoProvidejQuery()

// uncomment if you use API Platform Admin (composer req api-admin)
// .enableReactPreset()
// .addEntry('admin', './assets/js/admin.js')


module.exports = Encore.getWebpackConfig();
