import Vue from 'vue'
import GalleryVideoInput from './GalleryVideoInput.vue'

/** Vue video url  */
import VueVideo from "@tryangled/vue-video"


const VueGalleryVideoInput = {
  init : function(){
    
    Vue.use(VueVideo)

    new Vue({ 
      template: '<GalleryVideoInput/>',
      components: { GalleryVideoInput },
    }).$mount('#vue-gallery-video-input')
  }
}

export default VueGalleryVideoInput