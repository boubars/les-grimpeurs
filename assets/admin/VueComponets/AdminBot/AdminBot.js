import Vue from 'vue'
import AdminBot from './AdminBot.vue'

const VueAdminBot  = {
  init : function(){
    new Vue({ 
      template: '<AdminBot/>',
      components: { AdminBot },
    }).$mount('#vue-admin-bot')
  }
}

export default VueAdminBot