import StringHelpers from "@/services/helpers/StringHelpers.js" 

function loadScript(){

    let postForm = document.getElementById('admin-post-form')
    if(postForm){
      bindOnTitleChange()
    }
  }
  
function bindOnTitleChange(){
    let titleInput = document.getElementById('post_title')
    let slugInput  = document.getElementById('post_slug')
    titleInput.addEventListener('keyup', function(){
        slugInput.value = StringHelpers.slugify(titleInput.value)
    })
}





const PostForm = {
    init : function(){
      loadScript()
    }
}
  
export default PostForm