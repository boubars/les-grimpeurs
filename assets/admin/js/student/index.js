import StudentForm from "./StudentForm.js"

document.addEventListener("DOMContentLoaded", ()=>{
    StudentForm.init()
    binOnProfilLinkClick()
})

/**
 * Encode student id and redirect to the student profil
 * 
 * @returns  void
 */
function binOnProfilLinkClick(){
    let links = document.querySelectorAll('.admin-student-profil--link')
    if(!links){
        return 
    }
    var encoder = require('int-encoder')
    
    links.forEach(link => {
        link.addEventListener('click', function(){
            let student = link.getAttribute('data-id')
            let id = encoder.encode(student)
            document.location.href =  `/student/${id}`
        })
    })
}
