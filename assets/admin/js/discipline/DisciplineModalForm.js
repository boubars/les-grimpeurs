import DisciplineService from "./../../../vue/services/common/DisciplineService.js"
const $ = require('jquery'); 

function loadScript(){
  $(document).ready(function(){
    let form = document.querySelector('.scolarship-form-modals #discipline-form')

    if(!form){
      return
    }

    let submitBtn  = form.querySelector('#discipline--submit-btn') 
    
    submitBtn.addEventListener('click', function(e){
      e.preventDefault()
      form.validity
      if(form.checkValidity()){
        let data = $(form).serializeArray()
        console.log("submit service", data)
        DisciplineService.addDiscipline({
          category : data[0].value,
          name     : data[1].value,
          name_fr  : data[1].value
        }).then(
          (response) => {
            if(response.data.success){
              let event = new Event('discipline:added:success')
              let eventData = {
                ...response.data.discipline,
                name_fr: data[2].value 
              }
              eventData.id = 192 // for teste
              event.data = eventData
              form.dispatchEvent(event)
            }else{
              let event = new Event('discipline:added:failed')
              form.dispatchEvent(event)
            }
          }
        )
      
      }else{
        form.reportValidity()
      }
    })
  })
}

/*** Export module */
const DisciplineModalForm = {
  init : function(){
    loadScript()
  }
}

export default DisciplineModalForm







