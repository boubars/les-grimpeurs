/** GLOBAL variable locale delared in admin/base.html.twig */


const $ = jQuery = require('jquery')

var   $categoryFormModal   = false
var   $disciplineFormModal = false


document.addEventListener("DOMContentLoaded", ()=>{
    initScript()
})

function initScript(){
    $categoryFormModal = $("#discipline-category-form-modal")
    handleOnEditCategory()
    handleOnNewCategory()

    $disciplineFormModal = $("#discipline-form-modal")
    handleOnNewDiscipline()
    handleOnEditDiscipline()


}

function handleOnNewCategory(){
    $('body').on('click','.discipline-category-btn--new', function($e){
        $categoryFormModal.find("#category_name").val("")
        $categoryFormModal.find("#category_name_fr").val("")
        $categoryFormModal.find("#discipline-category-form").removeAttr("action")
        $categoryFormModal.modal('show')
    })
}

function handleOnEditCategory(){
    $('body').on('click','.category-btn--edit', function($e){
        let thematic = {
            id     : $(this).closest(".category-data").data('id'),
            name   : $(this).closest(".category-data").data('name'),
            nameFr : $(this).closest(".category-data").data('namefr'),
        }

        $categoryFormModal.find("#category_name").val(thematic.name)
        $categoryFormModal.find("#category_name_fr").val(thematic.nameFr)
        
        $categoryFormModal.find("#discipline-category-form").attr("action", `/${locale}/admin/discipline/category/${thematic.id}/edit`)

        $categoryFormModal.modal('show')
    })
}



function handleOnNewDiscipline(){
    $('body').on('click','.discipline-btn--new', function($e){
        let categID = getSelectedCategory()
        $disciplineFormModal.find("#discipline_category").val(categID)
        $disciplineFormModal.find("#discipline_name").val("")
        $disciplineFormModal.find("#discipline_nameFr").val("")
        $disciplineFormModal.find("#discipline-form").removeAttr("action")
        $disciplineFormModal.modal('show')
    })
}

function handleOnEditDiscipline(){
    $('body').on('click','.discipline-btn--edit', function($e){
        
        let discipline = {
            id       : $(this).closest(".discipline-data").data('id'),
            name     : $(this).closest(".discipline-data").data('name'),
            namefr   : $(this).closest(".discipline-data").data('namefr'),
            category : $(this).closest(".discipline-data").data('category'),
        }
        $disciplineFormModal.find("#discipline_category").val(discipline.category)
        $disciplineFormModal.find("#discipline_name").val(discipline.name)
        $disciplineFormModal.find("#discipline_nameFr").val(discipline.namefr)
      
        $disciplineFormModal.find("#discipline-form").attr("action", `/${locale}/admin/discipline/${discipline.id}/edit`)
        $disciplineFormModal.modal('show')
    })
}

function getSelectedCategory(){
    return $('.discipline-category-nav-tabs').find('.nav-link.active').closest(".category-data").data('id')
}