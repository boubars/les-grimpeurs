import './../../css/organization.css'
import OrganizationForm from "./OrganizationForm.js"
import 'chosen-js'

document.addEventListener("DOMContentLoaded", ()=>{
  OrganizationForm.init()
})