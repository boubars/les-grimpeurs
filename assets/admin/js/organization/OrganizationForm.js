import ImageHelper from "./../common/ImageHelper.js"
import intlTelInput from 'intl-tel-input'
import axios from 'axios'
import CountryService from "./../../../vue/services/common/CountryService"

var oldCountryChosen = true  

/*** import vue js components */
import VueGalleryVideoInput from "./../../VueComponets/GalleryVideoInput/GalleryVideoInput.js"

import ImageCropper from "../common/ImageCropper"

const OrganizationForm = {

    /**
     * Organization form entry script
     */
    init : function(){
        this.bindGalleryPictureEditBtn()

        this.initMapAutocompletFields()

        /*** bind tel input field */
        this.initTelInputFields()

        this.syncContactUserData()

        this.binBeforeSubmit()

        this.bindOnEditAllSelect()

        this.bindOnChosenDrop()
        
        this.bindMailContactChange();
        /** init vue component loder */
        VueGalleryVideoInput.init()

        ImageCropper.init()
    },

    /*** bind chosen on drop */
    bindOnChosenDrop: function(){
        $('#organization_country_chosen .chosen-drop').on('click', function(){
            let newVal = $('#organization_country.chosen').val()     
            $('#organization_contactCountry.chosen').val(newVal)
            $('#organization_contactCountry.chosen').trigger('chosen:updated')  
        })
    },

    /** bind gallery edit btn click */
    bindGalleryPictureEditBtn: function(){
        let btns = document.querySelectorAll('.gallery-picture-wrapper--btn-edit')
        if(btns){
            btns.forEach((element) => {
                element.addEventListener('click', function(e){
                    e.preventDefault()
                    let inputFile = element.closest('.gallery-picture-wrapper--container').querySelector("input[type='file']")
                    if(inputFile){
                        inputFile.click()
                        element.closest('.input-file-preview').classList.add('d-none')
                        inputFile.classList.remove('d-none')
                    }
                })

            })
        }

        
    },


    initMapAutocompletFields: function(){
        let that = this
        let adresseOrg = new google.maps.places.Autocomplete(
            document.getElementById("organization_adresse")
        )
        adresseOrg.addListener('place_changed', function() {
            var place = adresseOrg.getPlace()
            for (var i = 0; i < place.address_components.length; i++) {
                for (var j = 0; j < place.address_components[i].types.length; j++) {

                    /*** update postal code */
                    if (place.address_components[i].types[j] == "postal_code") {
                        document.getElementById('organization_postalCode').value = place.address_components[i].long_name
                        document.getElementById('organization_contactPostalCode').value = place.address_components[i].long_name
                    }
                    
                    /** update organization_contactCountry select country  */
                    if (place.address_components[i].types[j] == "country") {
                        let countryName = place.address_components[i].long_name
                        let countryId = CountryService.getCountryId(countryName)
                        if(countryId){
                            that.updateContactCountry(countryId)
                        }
                    }

                }
            }

            /** update contact adresse  */
            document.getElementById('organization_adress').value = place.formatted_address
            this.updateMapImg(place, 'organization_adresseMap', 'organization-adressemap-preview')

            /*** update adresse location */
            let location = {
                lat: place.geometry.location.lat(),
                lng: place.geometry.location.lng()
            }
            document.getElementById('organization_adresseLocation').value = `[${location.lat},${location.lng}]`

        })
        /*** */
        let contactAdresseOrg = new google.maps.places.Autocomplete(
            document.getElementById("organization_adress")
        )
        contactAdresseOrg.addListener('place_changed', function() {
            var place = contactAdresseOrg.getPlace()
            for (var i = 0; i < place.address_components.length; i++) {
                for (var j = 0; j < place.address_components[i].types.length; j++) {
                    if (place.address_components[i].types[j] == "postal_code") {
                        document.getElementById('organization_contactPostalCode').value = place.address_components[i].long_name
                    }
                }
            }
        })

    },

    updateContactCountry: function(countryId){
        $('#organization_contactCountry').val(countryId)
        let oldChosen = document.getElementById("organization_contactCountry_chosen")
        if(oldCountryChosen){
            oldChosen.remove()
            oldCountryChosen = false
        }
        $('#organization_contactCountry').chosen().trigger('chosen:updated');
    },

    initTelInputFields: function(){
        const inputs = document.querySelectorAll(".tel-input")
        if(inputs){
            inputs.forEach((input)=>{
                var iti = intlTelInput(input, {
                    separateDialCode : false
                });
                input.addEventListener("countrychange", function(e){
                    let dialCode   = iti.getSelectedCountryData().dialCode
                    let dialNumber = input.value
                    

                    if(dialCode){
                        /** prefix the input */
                        if(!(dialNumber.startsWith("+"+dialCode))){
                            input.value = `+${dialCode}${dialNumber}`
                        }
                    }

                    /*** update organization_phone contry if organization_phoneNumber change */
                    
                    if(input.getAttribute('id') == 'organization_phoneNumber'){
                        let targetInput = document.getElementById('organization_phone')
                        let iso2 = iti.getSelectedCountryData().iso2
                        if(iso2){
                            intlTelInput(targetInput, {
                                separateDialCode : false,
                                initialCountry   : iso2
                            }).setCountry(iso2)
                        }
                    }
                })

            })
        }


    },

    syncContactUserData: function(){
        let contactSelector = {
            name      : "organization_contactName",
            lastName  : "organization_contactLastName",
            phone     : "organization_phoneNumber",
            email     : "organization_organizationEmail",   
        }
        
        let userSelector = {
            name      : "organization_firstname",
            lastName  : "organization_lastname",
            phone     : "organization_phone",
            email     : "organization_email",   
        }
    
        this.watchContact(contactSelector, userSelector)
    },

    watchContact: function(data, user){
        Object.entries(data).forEach((value)=>{

            let key = value[0];
            let selector = value[1];
            let input = document.getElementById(selector)        

            if(input){
                /*** check if relative user field is already filled */
                let userKeyInput = document.getElementById(user[key])
                if(!((userKeyInput.value).length > 0)){
                    input.addEventListener('keyup', function(){
                        /*** update user input */
                        userKeyInput.value = input.value
                    })  
                }
            }
        })
    },

    updateMapImg: function(adresse, inputId, previewImg=false){
        let selectedAdresse = adresse.formatted_address
        let gmapKey = process.env.GMAP_KEY


        let apiImageAdrs = `https://maps.googleapis.com/maps/api/staticmap?center=${selectedAdresse}&&markers=size:mid|color:red|${selectedAdresse}&zoom=14&size=400x400&key=${gmapKey}`
        ImageHelper.ImgToBase64URL(apiImageAdrs, function(base64Img){ 
            base64Img
            document.getElementById(inputId).value = base64Img 
            if(previewImg != false )
                document.getElementById(previewImg).setAttribute('src',base64Img)  
        })
    },

    binBeforeSubmit: async function(){
        let form      = document.getElementById('admin-organization-form')
        let submitBtn = document.getElementById('admin-organization-form--validate')

        form.addEventListener('submit', async function(e){
            e.preventDefault()
            
            /*** check duplicate user email only form add */
            if(form.classList.contains('admin-organization-form-add')){
                let email = document.getElementById("organization_email")
                let duplicatedError = document.getElementById('duplicated-message-error')
                let emailExist = await this.checkUserEmail(email.value).then((response)=>{ return response.data })
                if(emailExist.exist){
                    email.classList.add('invalid')
                    duplicatedError.classList.remove('d-none')
                    email.scrollIntoView()      
                    return false
                }
                email.classList.remove('invalid')
                duplicatedError.classList.add('d-none')
            }    

            
            /*** clean country action contains all */
            let selects = document.querySelectorAll("select[all='true']")
            selects.forEach((select)=> {
                let selectedOptions = select.selectedOptions
                let selectedValue   = []
                for (let index = 0; index < selectedOptions.length; index++) {
                    const option = selectedOptions[index]
                    if(option.value == 0 ){
                        selectedValue = []
                        $(select).find('option').removeAttr("selected");
                        this.cleanSelect(select)
                        break
                    }
                    selectedValue = [...selectedValue, option.value]
                }
            })
        /*** submit form if valid */
        form.validity
        if(form.checkValidity()){
            form.submit()
        }
        else{
            form.reportValidity()
        }
        })
    },


    cleanSelect:function(select){
        let selectedOptions = select.selectedOptions
        for (let index = 0; index < selectedOptions.length; index++) {
            const option = selectedOptions[index]
            option.removeAttribute('selected')
        }
        select.value = []
    },

    checkUserEmail: function(email){
        return axios.post('/api/user/email/check', {email}) 
    },

    /*** bind all select edit */
    bindOnEditAllSelect: function(){
        let editForm = document.querySelector('.admin-organization-form-edit')
        if(!editForm){
            return false
        }
        let selects = editForm.querySelectorAll("select[all='true']")
        selects.forEach(select => {
            select.addEventListener("append:all:option", function(e){
                if(!select.value){
                    $(select).val(["0"]).trigger('chosen:updated');
                }
            })
            
        })
    },

    bindMailContactChange: function(){
        let email = $("#organization_organizationEmail");
        email.on('focusout', async  function () {
            let emailExist = await this.checkUserEmail(email.val()).then((response)=>{ return response.data })
            if (true == emailExist.exist) {
                email.addClass('invalid');
                email.after('<span class="error text-danger small float-right">Adresse mail déjà prise</span>')
            } else {
                $("#organization_email").val(email.val());
                email.removeClass('invalid');
            }

        }).on('focusin', function () {
            email.next('.error').remove();
            email.removeClass('invalid');
        })

    }

}

export default OrganizationForm