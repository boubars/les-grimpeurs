import ImageCropper from '../common/ImageCropper';

const ConceptForm = {
    init : function(){
        this.bindOndEditCover()

        this.bindOnNewValue()

        this.bindOnRemoveValue()

        ImageCropper.init()

    },

    bindOndEditCover: function(){
        $(document).on('click', '.edit-image-concept', function () {
            let inputFile = $(this).siblings('[type="file"]')
            inputFile.trigger('click')
        })
    },

    bindOnNewValue: function(){
        $(document).on('click', '#add_values', function(e) {
            e.preventDefault();
            let $wrapper = $('#concept-value-wrapper');
                let prototype = $wrapper.data('prototype');
                let index = $wrapper.data('index');
                //change image to default image
                let imageIndex = (index) % 4;
                let defaultImg = $wrapper.data('defaults-images').split(',');
                let newForm = prototype.replace(/__name__/g, index);
                newForm = $(newForm).addClass('col-4');
                $wrapper.data('index', index + 1);
                $("#concept-value-wrapper").append(newForm);
                $(newForm).find('img.default-image').attr('src', defaultImg[imageIndex]);
                ImageCropper.initConceptValueUploaderHandler()
                return;
        });
    },
    
    bindOnRemoveValue: function(){
        $(document).on('click', '.remove-value', function () {
           $(this).parent().remove();
        })
    }
}

export default ConceptForm







