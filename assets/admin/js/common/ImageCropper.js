/**
 * dependencies lib and helpers
 */
import Cropper     from "cropperjs"
import ImageHelper from "../../../vue/services/helpers/ImageHelper"

//import 'bootstrap'

/**
 * Dependance global variable
 */
var context, $modal, image, crop, canvas, reader, file, url, valueTarget
var isBase64 = false

const ImageCropper = {
    /**
     *  Image cropper modal entry point
     */
    init: function(){
        context = null
        $modal = $('#image-cropper-modal')
        image = document.getElementById('imageCropper')
        
        if(!image || $modal){
            return 
        }
        
        this.initAvatarUploaderHandler()
        this.initLogoUploaderHandler()
        this.initCoverUploaderHandler()

        this.initConceptUploaderHandler()
        this.initConceptValueUploaderHandler()

        this.bindValidateCropper()
    },

    /*** handle avatar uploader */
    initAvatarUploaderHandler: function(){
        let inputFile = document.querySelector(".avatar-input-file")
        if(!inputFile){
            return 
        }
        /*** handle edit thubnail click btn */
        let editBtn = document.querySelector('button.avatar-preview--edit')
        editBtn.addEventListener('click', function(e){
            e.preventDefault()
            inputFile.click()
        })

        /** handle iput file change */
        if(inputFile){
            inputFile.addEventListener("change", function(e) {
                let legende = document.getElementById("avatar-legende-file")
                legende.classList.remove('error')
                var files = e.target.files
                ImageHelper.getImageSize(files[0], function(size){
                    /** check if file size 250x250px */
                    console.log(size)
                    if(size.w < 250 || size.h < 250){
                        legende.classList.add('error')
                    }else{
                        var done = function(url) {
                            image.src = url
                            $modal.modal('show')
                            context = "avatar"
                        }
                        if (files && files.length > 0) {
                            file = files[0]
                            if (URL) {
                                done(URL.createObjectURL(file));
                            } else if (FileReader) {
                                reader = new FileReader()
                                reader.onload = function(e) {
                                    done(reader.result)
                                };
                                reader.readAsDataURL(file)
                            }
                        }
                        if(typeof crop !== "undefined")[
                            crop.destroy()
                        ]
                        crop = new Cropper(image, {
                            aspectRatio: 1,
                            viewMode: 1,
                            minCropBoxWidth: 250,
                            minCropBoxHeight: 250,
                        });
                    }
                })          
            });
        }
    },

    /*** handle logo uploader */
    initLogoUploaderHandler: function(){
        let inputFile = document.querySelector(".logo-input-file")
        if(!inputFile){
            return 
        }
    
        /*** handle edit thubnail click btn */
        let editBtn = document.querySelector('button.logo-preview--edit')
        editBtn.addEventListener('click', function(e){
            e.preventDefault()
            inputFile.click()
        })
    
        /** handle iput file change */
        if(inputFile){
            inputFile.addEventListener("change", function(e) {
                let legende = document.getElementById("legende-file")
                legende.classList.remove('error')
                var files = e.target.files
                ImageHelper.getImageSize(files[0], function(size){
                    /** check if file size 250x250px */
                    console.log(size)
                    if(size.w < 250 || size.h < 250){
                        legende.classList.add('error')
                    }else{
                        var done = function(url) {
                            image.src = url
                            $modal.modal('show')
                            context = "logo"
                        }
                        if (files && files.length > 0) {
                            file = files[0]
                            if (URL) {
                                done(URL.createObjectURL(file));
                            } else if (FileReader) {
                                reader = new FileReader()
                                reader.onload = function(e) {
                                    done(reader.result)
                                };
                                reader.readAsDataURL(file)
                            }
                        }
                        if(typeof crop !== "undefined")[
                            crop.destroy()
                        ]
                        crop = new Cropper(image, {
                            aspectRatio: 1,
                            viewMode: 1,
                            minCropBoxWidth: 250,
                            minCropBoxHeight: 250,
                        });
                    }
                })          
            });
        }
    },
    
    /*** handle cover uploader */
    initCoverUploaderHandler:function(){
        let inputFile = document.querySelector(".cover-picture-input-file")
        if(!inputFile){
            return 
        }
        /** handle iput file change */
        if(inputFile){
            inputFile.addEventListener("change", function(e) {
                let legende = document.getElementById("cover-legende-file")
                //legende.classList.remove('error')
                var files = e.target.files
                ImageHelper.getImageSize(files[0], function(size){
                    /** check if file size 250x250px */
                    if(size.w < 300 || size.h < 250){
                        legende.classList.add('error')
                    }else{
                        var done = function(url) {
                            image.src = url
                            $modal.modal('show')
                            context = "cover"
                        }
                        if (files && files.length > 0) {
                            file = files[0]
                            if (URL) {
                                done(URL.createObjectURL(file));
                            } else if (FileReader) {
                                reader = new FileReader()
                                reader.onload = function(e) {
                                    done(reader.result)
                                };
                                reader.readAsDataURL(file)
                            }
                        }
                        if(typeof crop !== "undefined")[
                            crop.destroy()
                        ]
                        crop = new Cropper(image, {
                            aspectRatio: 3/2,
                            viewMode: 1,
                            minCropBoxWidth: 300,
                            minCropBoxHeight: 250,
                        });
                    }
                })          
            });
        }
    },
    /*** handle cover uploader */
    initConceptUploaderHandler:function(){
        let inputFile = document.querySelector(".concept-image-input-file")
        if(!inputFile){
            return 
        }
        /** handle iput file change */
        if(inputFile){
            inputFile.addEventListener("change", function(e) {
                let legende = document.getElementById("concept-legende-file")
                //legende.classList.remove('error')
                var files = e.target.files
                ImageHelper.getImageSize(files[0], function(size){
                    /** check if file size 250x250px */
                    if(size.w < 300 || size.h < 250){
                        legende.classList.add('error')
                    }else{
                        var done = function(url) {
                            image.src = url
                            $modal.modal('show')
                            context = "concept"
                        }
                        if (files && files.length > 0) {
                            file = files[0]
                            if (URL) {
                                done(URL.createObjectURL(file));
                            } else if (FileReader) {
                                reader = new FileReader()
                                reader.onload = function(e) {
                                    done(reader.result)
                                };
                                reader.readAsDataURL(file)
                            }
                        }
                        if(typeof crop !== "undefined")[
                            crop.destroy()
                        ]
                        crop = new Cropper(image, {
                            aspectRatio: 3/2,
                            viewMode: 1,
                            minCropBoxWidth: 300,
                            minCropBoxHeight: 250,
                        });
                    }
                })          
            });
        }
    },

    /*** handle concept value uploader */
    initConceptValueUploaderHandler:function(){
        let inputFiles = document.querySelectorAll(".concept-value-input-file")
        if(!inputFiles){
            return 
        }
        /** handle iput file change */
        inputFiles.forEach(inputFile => {
            inputFile.addEventListener("change", function(e) {
                let legende = inputFile.closest('.concept-value--item').querySelector('.concept-value-legende-file')
                legende.classList.remove('error')
                var files = e.target.files
                ImageHelper.getImageSize(files[0], function(size){
                    /** check if file size 250x250px */
                    if(size.w < 250  || size.h < 250){
                        legende.classList.add('error')
                    }else{
                        var done = function(url) {
                            image.src = url
                            $modal.modal('show')
                            context     = "concept-value"
                            valueTarget = inputFile
                        }
                        if (files && files.length > 0) {
                            file = files[0]
                            if (URL) {
                                done(URL.createObjectURL(file));
                            } else if (FileReader) {
                                reader = new FileReader()
                                reader.onload = function(e) {
                                    done(reader.result)
                                };
                                reader.readAsDataURL(file)
                            }
                        }
                        if(typeof crop !== "undefined")[
                            crop.destroy()
                        ]
                        crop = new Cropper(image, {
                            aspectRatio: 1,
                            viewMode: 1,
                            minCropBoxWidth: 250,
                            minCropBoxHeight: 250,
                        });
                    }
                })          
            }); 
        });
        
    },

    /** handle validate cropper btn click */
    bindValidateCropper: function(){
        let validateBtn = document.getElementById("validate-cropper")
        let that = this
        if(validateBtn){
          validateBtn.addEventListener("click", function(e) {
              e.preventDefault()
              canvas = crop.getCroppedCanvas();
              canvas.toBlob(function(blob) {
                  var url = URL.createObjectURL(blob);
                  var reader = new FileReader();
                  reader.readAsDataURL(blob);
                  reader.onloadend = function() {
                        var base64data = reader.result
                        $modal.modal('hide')
                        crop.destroy()
                        //crop = null
                        isBase64 = true
                        switch (context) {
                            case 'logo':
                                that.validateCropperLogo(base64data)
                                break;
                        
                            case 'cover':
                                that.validateCropperCover(base64data)
                                break;
                        
                            case 'avatar':
                                that.validateCropperAvatar(base64data)
                                break;
                        
                            case 'concept':
                                that.validateCropperConcept(base64data)
                                break;
                        
                            case 'concept-value':
                                that.validateCropperConceptValue(base64data)
                                break;
                        }
                  }
              });
          })
        }
    },

    /*** if context == avatar validate */
    validateCropperAvatar: function(base64data){
        let previewImage   = document.querySelector('.avatar-image-preview img')
        let avatar64Input  = document.querySelector('#avatar64')
        avatar64Input  = avatar64Input ? avatar64Input : document.querySelector('#contactAvatar64')
        
        avatar64Input.value = base64data
        previewImage.removeAttribute('width')
        previewImage.src = base64data
    },

    /*** if context == logo validate */
    validateCropperLogo: function(base64data){
        let previewImage     = document.querySelector('.thumbnail-preview__image img')
        let logo64Input = document.querySelector('#logo64')
        logo64Input.value = base64data
        previewImage.removeAttribute('width')
        previewImage.src = base64data
    },

    /*** if context == cover validate */
    validateCropperCover: function(base64data){
        let previewImage = document.querySelector('.cover-image-preview img')
        let logo64Input  = document.querySelector('#coverPicture64')
        logo64Input.value = base64data
        previewImage.removeAttribute('width')
        previewImage.src = base64data
    },

    /*** if context == concept validate */
    validateCropperConcept: function(base64data){
        let previewImage = document.querySelector('#concept_image_preview')
        let logo64Input  = document.querySelector('#concept_image64')
        logo64Input.value = base64data
        previewImage.removeAttribute('width')
        previewImage.src = base64data
    },

    /*** if context == concept-value validate */
    validateCropperConceptValue: function(base64data){
        let previewImage = valueTarget.closest('.concept-value--item').querySelector('.thumbnail-preview img')
        let logo64Input  = valueTarget.closest('.concept-value--item').querySelector('.concept-value-input-file64')
        logo64Input.value = base64data
        previewImage.src = base64data
    }
}

export default ImageCropper