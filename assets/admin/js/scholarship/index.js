import './../../css/scholarship.css';
import ScholarshipForm from "./ScholarshipForm.js"
import 'chosen-js'

document.addEventListener("DOMContentLoaded", ()=>{
    ScholarshipForm.init() 
    /*** diseable summbit on key up */
})





/*** module duplicate input on enter key */
var duplicateData = []
$(document).ready(function(event){
    let duplicateInputs = document.querySelectorAll('.duplicate-input')
    duplicateInputs.forEach((item, key)=>{
        let fields = item.querySelectorAll('.duplicate-input--field')
        duplicateData[key] = duplicateData[key] ?? []
        fields.forEach((input, index) => {
            /*** init array if null and push if not */
            duplicateData[key][index] = input.value
            
            /*** init duplicate on enter key on last input */
            input.addEventListener('input', function(e) {
                duplicateData[key][index] = e.target.value
            })
            input.addEventListener('keydown', function(e) {
                if(e.keyCode == 13){
                    e.preventDefault()
                    if((index == fields.length - 1)){
                        duplicateData[key] = [...duplicateData[key], ""]
                        reloadDuplicateInputData(key, e.target.closest('.duplicate-input'))
                    }
                }
            })

        })
    })
});

function reloadDuplicateInputData(key, canvas){
    canvas.innerHTML = ""
    for(let i = 0; i < duplicateData[key].length ; i++ ){
        let newInput = document.createElement('input')
        newInput.setAttribute('name', canvas.getAttribute('input-name'))

        /*newInput.setAttribute('type','text')*/
        if(i >= 1){  
            newInput.setAttribute('class','form-control mt-2')
        }else{
            newInput.setAttribute('class','form-control')
        }
         /*** init duplicate on enter key on last input */
        newInput.addEventListener('input', function(e) {
            duplicateData[key][i] = e.target.value
        })
        newInput.addEventListener('keydown', function(e) {
            duplicateData[key][i] = newInput.value           
            console.log("updated", duplicateData[key][i])
            if(e.keyCode == 13){
                e.preventDefault();
                /** if last, add a new input */
                if( i == (duplicateData[key].length - 1)){
                    duplicateData[key] = [...duplicateData[key], ""]
                }
                reloadDuplicateInputData(key, canvas)
            }

        })
        newInput.value = duplicateData[key][i]
        canvas.appendChild(newInput)
    }
}

/*** end duplicate input on key enter */