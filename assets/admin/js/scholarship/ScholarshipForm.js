import ImageHelper from "./../common/ImageHelper.js"
import intlTelInput from 'intl-tel-input'
import DisciplineModalForm from "../discipline/DisciplineModalForm.js"

import ProfilService        from "./../../../vue/services/organization/ProfilService.js";
import SocialNetworkService from "./../../../vue/services/common/SocialNetworkService.js" 


let removeOldChosen = 0 // for teste 
let removeOldDisciplineChosen = 0 // for teste 

import ImageCropper from "../common/ImageCropper"

/*** Export module */
const ScholarshipForm = {
    init : function(){
        this.bindGalleryPictureEditBtn()

        this.initMapAutocompletFields()

        this.bindOnChangeField('scholarship_awardOrganizationType', 'scholarship_relatedOrganization');

        this.bindOnChangeField('scholarship_grantedContinent', 'scholarship_grantedCountries')

        this.bindOnChangeField('scholarship_studyField', 'scholarship_studyFieldTags');

        this.bindOnChangeOrganization()

        this.bindOnChangeGrantedCountries()

        this.binBeforeSubmit()

        this.bindOnAgeCbChecked()

        this.bindOnEditAllSelect()

        this.updateCountriesValue()

        this.initTelInputFields()

        DisciplineModalForm.init()

        this.bindOnDisciplineAdded()

        this.bindDateLimitCbChange()

        this.bindOnChangeStudyFieldTags()
    
        this.bindInitRelatedOrganizationVal() 

        ImageCropper.init()
    },

    /** bind gallery edit btn click */
    bindGalleryPictureEditBtn: function(){
        let btns = document.querySelectorAll('.gallery-picture-wrapper--btn-edit')
        if(btns){
            btns.forEach((element) => {
                element.addEventListener('click', function(e){
                    e.preventDefault()
                    let inputFile = element.closest('.gallery-picture-wrapper--container').querySelector("input[type='file']")
                    if(inputFile){
                        inputFile.click()
                        element.closest('.input-file-preview').classList.add('d-none')
                        inputFile.classList.remove('d-none')
                    }
                })

            })
        }
    },

    initMapAutocompletFields: function(){
        let ac = new google.maps.places.Autocomplete(
            document.getElementById("scholarship_contactAdresse")
        )
        let that = this
        ac.addListener('place_changed', function() {
            var place = ac.getPlace()
            that.updateMapImg(place, 'scholarship_contactAdresseMap', 'contact-adressemap-preview')
        })
    },

    updateMapImg: function(adresse, inputId, previewImg = false){
        let that = this
        let selectedAdresse = adresse.formatted_address
        let gmapKey = process.env.GMAP_KEY
        let apiImageAdrs = `https://maps.googleapis.com/maps/api/staticmap?center=${selectedAdresse}&&markers=size:mid|color:red|${selectedAdresse}&zoom=14&size=400x400&key=${gmapKey}`
        ImageHelper.ImgToBase64URL(apiImageAdrs, function(base64Img){ 
            base64Img
            document.getElementById(inputId).value = base64Img 
            if(previewImg != false )
                document.getElementById(previewImg).setAttribute('src',base64Img)  
        })
    },

    bindOnChangeField: function(field, target){
        $(`#${field}`).on('change chosen-change', function(e){
            let $field = $(this)
            let $form = $field.closest('form')
            let data = $form.serializeArray()
            
            let $target = `#${target}`;
            if ('scholarship_relatedOrganization' == target) {
                let imgSrc = $('#scholarship-logo .thumbnail-preview__image').data('default');
                document.querySelector('#scholarship-logo .thumbnail-preview__image img').src = imgSrc;
            }
            let that = this 
            $.post($form.attr('action'), data).then(function(data) {     
                let $input = $(data).find($target).closest('.form-group');
                $($target).closest('.form-group').replaceWith($input);
                $($target).val('');
                /*** refresh chosen select */
                if(e.type == 'chosen-change'){
                    let element = $($input).find($target)
                    if(element.attr('all') == 'true'){
                        element = that.addAllOption(element)
                    }
                    element.chosen({
                        no_results_text: "aucun résultat correspondant"
                    }).change(function(e){
                        e.target.dispatchEvent(new Event('chosen-change'))
                    })
                }
                
            })
        });
    },

    binBeforeSubmit: function(){
        let form      = document.getElementById('admin-scholarship-form')
        let submitBtn = document.getElementById('admin-scholarship-form--validate')
        let that = this
        submitBtn.addEventListener('click', function(e){
            e.preventDefault()
            /*** clean country action contains all */
            let selects = document.querySelectorAll("select[all='true']")
            selects.forEach((select)=> {
                let selectedOptions = select.selectedOptions
                let selectedValue   = []
                for (let index = 0; index < selectedOptions.length; index++) {
                    const option = selectedOptions[index]
                    if(option.value == 0 ){
                        selectedValue = []
                        $(select).find('option').removeAttr("selected");
                        that.cleanSelect(select)
                        break
                    }
                    selectedValue = [...selectedValue, option.value]
                }
            })

            
            /*** submit form */
            form.validity
            if(form.checkValidity()){
                form.submit()
            }
            else{
                form.reportValidity()
            }
        })
    },

    cleanSelect: function(select){
        let selectedOptions = select.selectedOptions
        for (let index = 0; index < selectedOptions.length; index++) {
            const option = selectedOptions[index]
            option.removeAttribute('selected')
        }
        select.value = []
    },

    addAllOption: function(element){
        // all option
        let option = document.createElement('option')
        option.value = "0"
        option.innerText = 'Tous'
        element.append(option)

        return element
    },

    bindOnChangeOrganization: function(){
        let previewImage = document.querySelector('#scholarship-logo .thumbnail-preview__image img')
        let defaultSrc   = previewImage.src
        $('body').on('change','#scholarship_relatedOrganization', function(e){
            let organizationId = e.target.value
            /*** update social network field */
            SocialNetworkService.getSocialNetwork(organizationId).then(
                (response) => {
                    let data = response.data.socialNetwork
                    document.getElementById('scholarship_socialNetwork_facebook').value  = data.facebook
                    document.getElementById('scholarship_socialNetwork_twitter').value   = data.twitter
                    document.getElementById('scholarship_socialNetwork_instagram').value = data.instagram
                    document.getElementById('scholarship_socialNetwork_linkedin').value  = data.linkedin
                }
            )
            /** update default scholarship logo */
            ProfilService.getLogo(organizationId).then(
                (response) => {
                    previewImage = document.querySelector('#scholarship-logo .thumbnail-preview__image img')
                    let fileInput    = document.getElementById("scholarship_logo")
                    
                    if(response.data.logo.url){
                        previewImage.removeAttribute('width')
                        fileInput.removeAttribute('required')
                        previewImage.src = response.data.logo.url
                        console.log(response.data.logo.url)
                    }else{
                        previewImage.src = defaultSrc
                        previewImage.setAttribute('width', '50')
                    }
                }
            )

            /** update default scholarship contact infos */
            ProfilService.getContact(organizationId).then(
                (response) => {
                    let data = response.data.contact
                    document.getElementById('scholarship_contactName').value    = data.firstname
                    document.getElementById('scholarship_contactEmail').value   = data.email
                    document.getElementById('scholarship_contactAdresse').value = data.adresse
                    document.getElementById('scholarship_contactPhone').value   = data.phone
                    
                    previewImage = document.querySelector('#contact-avatar  .thumbnail-preview__image img')
                    let fileInput = document.getElementById("scholarship_contactAvatar")
                    
                    if(data.avatar.url){
                        previewImage.removeAttribute('width')
                        fileInput.removeAttribute('required')
                        previewImage.src = data.avatar.url
                    }else{
                        previewImage.src = defaultSrc
                        previewImage.setAttribute('width', '50')
                    }
                }
            )

            /*** update default gallery photo */
            ProfilService.getGallery(organizationId).then(
                (response) => {
                    let data     = response.data.gallery
                    let pictures = (data && data.pictures) ? data.pictures : false
                    
                    if(pictures){
                        let canvas = document.querySelector("#gallery-picture-uploader .file-preview-image")
                        canvas.innerHTML = ""
                        for (let index = 0; index < pictures.length; index++) {
                            const picture = pictures[index];
                            let imgItem   = document.createElement('img')
                            imgItem.src   = `/uploads/picture/${picture}`
                            canvas.appendChild(imgItem)
                        }
                    }
                }
            )
        })
    },

    bindOnChangeGrantedCountries: function(){
        
        $("#scholarship_grantedCountries").on('change chosen-change', function(e){
            $("#scholarship_grantedNationalities").val($(this).val()).trigger("change")
        }) 
        $("body").on('change chosen-change', '#scholarship_grantedCountries', function(e){
            $("#scholarship_grantedNationalities").val($(this).val()).trigger("change")
        }) 
    },

    updateCountriesValue: function(){
    
    $("#scholarship_grantedNationalities").on("change", function(){
            let oldChosen = document.getElementById("scholarship_grantedNationalities_chosen")
            if(removeOldChosen == 0){
                oldChosen.remove()
                removeOldChosen = 1
            }
            $chosen = $('#scholarship_grantedNationalities').chosen().trigger('chosen:updated');
            let chosen = $chosen.data("chosen");
            let _fn = chosen.result_select;
            chosen.result_select = function(evt) {    
                evt["metaKey"] = true;
                evt["ctrlKey"] = true;
                chosen.result_highlight.addClass("result-selected");
                return _fn.call(chosen, evt);
            }
        })
    
    },

    bindDateLimitCbChange: function(){
        let cb = document.getElementById("candidatureLimitCb")
        let input = document.getElementById("scholarship_candidatureLimit")
        cb.addEventListener('click', function(e){
            input.value    = cb.checked ? null : input.value
            //input.required =  !cb.checked
            input.disabled =  cb.checked
        })
    },

    bindOnAgeCbChecked: function(){
        let cb = document.getElementById("all-age-cb")
        let maxAge = document.getElementById("scholarship_maxAge")
        let minAge = document.getElementById("scholarship_minAge")
        
        cb.addEventListener('click', function(e){
            maxAge.value    = cb.checked ? "" : minAge.maxAge
            minAge.value    = cb.checked ? "" : minAge.value 
            minAge.disabled = maxAge.disabled = cb.checked
        })

        /*** auto check all on edit if min or max are null */
        let editForm = document.querySelector('.admin-scholarship-form-edit')
        if(editForm && (minAge.value =='' ) && (maxAge.value == '')){ 
            minAge.disabled = maxAge.disabled = true
            cb.setAttribute('checked','checked')
        }
    },

    /*** bind all select edit */
    bindOnEditAllSelect: function(){
        let editForm = document.querySelector('.admin-scholarship-form-edit')
        if(!editForm){
            return false
        }

        let selects = editForm.querySelectorAll("select[all='true']")
        selects.forEach(select => {
            //option.value = "0"
            select.addEventListener("append:all:option", function(e){
                if(!select.value){
                    $(select).val(["0"]).trigger('chosen:updated');
                }
            })
            
        })
    },


    initTelInputFields: function(){
        const inputs = document.querySelectorAll(".tel-input")
        if(inputs){
            inputs.forEach((input)=>{
                var iti = intlTelInput(input, {
                    separateDialCode : false
                });
                
                input.addEventListener("countrychange", function(e){
                    let dialCode   = iti.getSelectedCountryData().dialCode
                    let dialNumber = input.value
                    if(dialCode){
                        /** prefix the input */
                        if(!(dialNumber.startsWith("+"+dialCode))){
                            input.value = `+${dialCode}${dialNumber}`
                        }
                    }
                })

            })
        }
    },

    bindOnDisciplineAdded: function(){
        let form = document.getElementById('discipline-form')
        let that = this 
        form.addEventListener("discipline:added:success", function(e){
            /** update discipline liste and discipline select */
            let select = document.getElementById("scholarship_studyFieldTags");
            let option = document.createElement('option')
            option.value = e.data.id
            option.innerText = e.data.name_fr
            select.appendChild(option)
            /*** add new discipline in selected value */
            let value = $(select).val() 
            $(select).val( [...value, e.data.id.toString()])
            $(select).trigger('chosen:updated')
            that.refreshDisciplineSelect()

            /*** close modal */
            $('.modal').modal('hide')
            $('.modal-backdrop').remove()
        })
    },

    refreshDisciplineSelect: function(){
        let oldChosen = document.getElementById("scholarship_studyFieldTags_chosen")
        if(removeOldDisciplineChosen == 0){
            oldChosen.remove()
            removeOldDisciplineChosen = 1
        }
        $chosen = $('#scholarship_studyFieldTags').chosen().trigger('chosen:updated');
        let chosen = $chosen.data("chosen");
        let _fn = chosen.result_select;
        chosen.result_select = function(evt) {    
            evt["metaKey"] = true;
            evt["ctrlKey"] = true;
            chosen.result_highlight.addClass("result-selected");
            return _fn.call(chosen, evt);
        }
    },

    bindOnChangeStudyFieldTags: function(){
        $('#scholarship_studyFieldTags').on('change chosen-change', (evt, params) => {
            $('#scholarship_studyFieldTags').find('option:selected').each(function(){
                let idCateg =$(this).attr('data-categ');
                $("#scholarship_studyField option[value='" + idCateg + "']").prop("selected", true);
            });
            $("#scholarship_studyField").chosen().trigger('chosen:updated');
            $("#scholarship_studyField_chosen").next().remove();
        })
    },

    bindInitRelatedOrganizationVal: function(){
        let editForm = document.querySelector('.admin-scholarship-form-edit')
        if(editForm){
            return false
        }

        $('#scholarship_relatedOrganization').val(null);
    }
}

export default ScholarshipForm