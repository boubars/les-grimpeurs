import Cropper from "cropperjs"
const $ = require('jquery')
import 'bootstrap'

$(document).ready(function(){

  var $modal = $('#image-cropper-modal');
  var image = document.getElementById('imageCropper')
  var inputFile = document.querySelector(".thumbnail-input-file")
  let isBase64  = false
  let crop, canvas, reader, file, url

  let thumbnail64Input = document.querySelector('#thumbnail64')

  if(inputFile){
      inputFile.addEventListener("change", function(e) {
            let legende = document.getElementById("legende-file")
            legende.classList.remove('error')
            var files = e.target.files
            getImageSize(files[0], function(size){
                /** check if file size 250x250px */
                console.log(size)
                if(size.w < 250 || size.h < 250){
                    legende.classList.add('error')
                }else{
                    var done = function(url) {
                        image.src = url
                        $modal.modal('show')
                    }
                    if (files && files.length > 0) {
                        file = files[0]
                        if (URL) {
                            done(URL.createObjectURL(file));
                        } else if (FileReader) {
                            reader = new FileReader()
                            reader.onload = function(e) {
                                done(reader.result)
                            };
                            reader.readAsDataURL(file)
                        }
                    }
                    if(typeof crop !== "undefined")[
                        crop.destroy()
                    ]
                    crop = new Cropper(image, {
                        aspectRatio: 1,
                        viewMode: 1,
                        minCropBoxWidth: 250,
                        minCropBoxHeight: 250,
                    });
                }
            })



            
          
      });
  }

  let validateBtn = document.getElementById("validate-cropper")
  if(validateBtn){
    validateBtn.addEventListener("click", function(e) {
        let previewContainer = document.querySelector('.thumbnail-preview')
        let previewImage     = document.querySelector('.thumbnail-preview__image img')
        canvas = crop.getCroppedCanvas({
            width: 300,
            height: 300,
        });
        canvas.toBlob(function(blob) {
            var url = URL.createObjectURL(blob);
            var reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = function() {
                var base64data = reader.result
                isBase64 = true
                thumbnail64Input.value = base64data
                $modal.modal('hide')
                crop.destroy()
                //crop = null

                previewContainer.classList.remove('d-none')
                previewImage.src = base64data

            }
        });
    })
  }

  /*** handle edit thubnail click btn */
  let editBtn = document.querySelector('.thumbnail-preview__btn button')
  editBtn.addEventListener('click', function(e){
      e.preventDefault()
      inputFile.click()
  })

})


function getImageSize(file, callback) {
    const img = new Image()
    let s = 12
    img.onload = function() {
       let w = this.width
       let h = this.height
       callback({w,h})
    }
    let URL = window.URL || window.webkitURL
    if (URL && URL.createObjectURL){
        img.src =  URL.createObjectURL(file)
    } 
}