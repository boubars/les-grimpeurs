import './../css/admin.css';
import './../css/theme.css';
import './../css/dashboard.css';
// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
const $ = require('jquery'); 
global.$ = global.jQuery = $;

import 'bootstrap';
import './admin_main.js'
import 'chosen-js'
import './grimpeurs/team.js'
import './grimpeurs/faq.js'
import './discipline/index.js'