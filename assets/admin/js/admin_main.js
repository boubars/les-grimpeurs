const $ = jQuery = require('jquery');

/*** import tinymce module dependancies  */
//var tinymce = require('tinymce/tinymce.js')
var tinymce = require('tinymce/tinymce')
var Cropper = require("cropperjs");
// A theme is also required
//import 'tinymce/themes/silver'
var crop;
// Any plugins you want to use has to be imported
/*
import 'tinymce/plugins/paste';
import 'tinymce/plugins/link';
*/

var confirm_event = null;
$("form .need-confirmation").on('click', function(e) {
    e.preventDefault();
    confirm_event = e;
    var form = $(this).parents('form');
    $("#confirm-dialog .message").text($(this).attr("confirmation-message"));
    $("#confirm-dialog").modal("show");
});

$(document).on('click', "#confirm-dialog #yesbtn", function() {
    $(confirm_event.currentTarget).parents('form').submit();
});


$(document).ready(function(){
    /*** add all choice to all select with attr all */
    let selects = document.querySelectorAll('select')
    selects.forEach(element => {
        if(element.hasAttribute('all') && element.getAttribute('all') == 'true'){
            // all option
            let option = document.createElement('option')
            option.value = "0"
            option.innerText = 'Tous'
            element.appendChild(option)
            element.dispatchEvent(new Event('append:all:option'))
        }

    })
    

    /*** Map flag */
    let $codeInput = $(document).find('#language_code')
    if($codeInput){
        let $flagLabel = $(document).find('#language_code_flag')
        $codeInput.on('keyup',function(){
            $flagLabel.attr('class',`flag-icon flag-icon-${$(this).val()}`)
        })
    }

    /** init all select picker (chosen_js) */
    $('.chosen').each(function() {
        $chosen = $(this).chosen({
                no_results_text: "aucun résultat correspondant",
                allow_single_deselect: true
            }).change(function(e){
                let input = $(e.currentTarget).attr('id')+'_chosen';
                $('#'+input).find('.chosen-search-input').val('').trigger('change');
                e.target.dispatchEvent(new Event('chosen-change'))
            }
        )
       
        let chosen = $chosen.data("chosen");
        let _fn = chosen.result_select;
        chosen.result_select = function(evt) {    
            evt["metaKey"] = true;
            evt["ctrlKey"] = true;
            chosen.result_highlight.addClass("result-selected");
            return _fn.call(chosen, evt);
        }
    
       
    })




    /** trig click file to drag-area */
    $('.drag-area').on('click', function(e){
        $(this).children("input[type='file']").trigger("click")
    })

    $('.drag-area').on("click","input[type='file']", function(e){
        e.stopPropagation()
    })

    $('.drag-area--multiple').on("change", "input[type='file']", function(e){
        /** refresh image preview */
        let files = e.target.files
        let preview = e.target.closest('.drag-area--multiple').querySelector('.file-preview-image')
        preview.innerHTML = ""
        for (let index = 0; index < files.length; index++) {
            const file = files[index];
            let img = document.createElement('img')
            img.src = URL.createObjectURL(file)
            preview.appendChild(img)
        }

    })

    /*** add patter for all input url */
    $("input[inputmode='url']").each(function(e) {
        $(this).attr('pattern','https?://.+')
    })


    /*** handle sidebar treeview is-expanded hover */

    $('.app-sidebar .treeview').on('mouseenter', function(e){
        $(this).addClass('is-expanded')

        /** calcul position */
        /** position en pixel */
        let defauPosition       = -100
        let sideBar             = document.getElementById('admin-app-sidebar')
        let sideBarScroolTop    = sideBar.scrollTop
        //let sideBarScroolHeight = sideBar.scrollHeight
        //let sideBarClientHeight = sideBar.clientHeight
        let $subMenu            = $(this).find('.treeview-menu')
        //let submenuHeight       = $subMenu.innerHeight()
        let submenuPosition     = (defauPosition - sideBarScroolTop)
        /*
        let overflow            = ( (submenuHeight + defauPosition ) + sideBarClientHeight) + sideBarScroolTop
        
        console.log(sideBarScroolHeight, sideBarClientHeight , sideBarScroolTop, submenuPosition, submenuHeight, overflow)
        */

        $subMenu.css('margin-top',`${submenuPosition}px`)
        $subMenu.addClass('show')
    })
    
    $('.app-sidebar .treeview').on('mouseleave', function(e){
        $(this).removeClass('is-expanded')
        $(this).find('.treeview-menu').removeClass('show')
    })
    
    /*** initialize tinymce field */
    tinymce.init({
        selector: '.tinymce',
        height : "500",
        plugins: ['paste', 'link']
    })
    tinymce.init({
        selector: '.tinymce400',
        height : "400",
        plugins: ['paste', 'link']
    })

    /*** active bootstrap tooltips */
    $('[data-toggle="tooltip"]').tooltip()


    /*** sidebar toogler */
    $(document).on('click', "#admin-app-sidebar__toggle , #admin-app-sidebar__closer", function() {
        $("#admin-app-sidebar").toggleClass('open')
    });

});
