/** GLOBAL variable locale delared in admin/base.html.twig */


const $ = jQuery = require('jquery')

var   $thematicFormModal = false
var   $questionFormModal = false


document.addEventListener("DOMContentLoaded", ()=>{
    initScript()
})

function initScript(){
    $thematicFormModal = $("#thematic-form-modal")
    handleOnNewThematic()
    handleOnEditThematic()

    /** */
    $questionFormModal = $("#question-form-modal")
    handleOnNewQuestion()
    handleOnEditQuestion()
}


function handleOnNewThematic(){
    $('body').on('click','.thematic-btn--new', function($e){
        $thematicFormModal.find("#thematic_name").val("")
        $thematicFormModal.find("#thematic-form").removeAttr("action")
        $thematicFormModal.modal('show')
    })
}


function handleOnEditThematic(){
    $('body').on('click','.thematic-btn--edit', function($e){
        let thematic = {
            id   : $(this).closest(".thematic-data").data('id'),
            name : $(this).closest(".thematic-data").data('name'),
        }

        $thematicFormModal.find("#thematic_name").val(thematic.name)
        
        $thematicFormModal.find("#thematic-form").attr("action", `/${locale}/admin/faq/thematic/${thematic.id}/edit`)

        $thematicFormModal.modal('show')
    })
}


function handleOnNewQuestion(){
    $('body').on('click','.question-btn--new', function($e){
        $questionFormModal.find("#question_title").val("")
        $questionFormModal.find("#question_answer").val("")
        $questionFormModal.find("#question-form").removeAttr("action")
        $questionFormModal.modal('show')
    })
}

function handleOnEditQuestion(){
    $('body').on('click','.question-btn--edit', function($e){
        let question = {
            id        : $(this).closest(".question-data").data('id'),
            title     : $(this).closest(".question-data").data('title'),
            answer    : $(this).closest(".question-data").data('answer'),
            thematics : $(this).closest(".question-data").data('thematics')
        }

        console.log('edit', question)
        $questionFormModal.find("#question_title").val(question.title)
        $questionFormModal.find("#question_answer").val(question.answer)

        /** update chose thematic choices */
        $questionFormModal.find('#question_thematic').val(question.thematics)
        $questionFormModal.find('#question_thematic').trigger('chosen:updated')  

        
        $questionFormModal.find("#question-form").attr("action", `/${locale}/admin/faq/${question.id}/edit`)

        $questionFormModal.modal('show')
    })
}

