import Cropper from "cropperjs"
import ImageHelper from "@/services/helpers/ImageHelper.js"
const $ = jQuery = require('jquery')


var context, $modal, image, crop, canvas, reader, file

document.addEventListener("DOMContentLoaded", ()=>{
    initAvatarUploaderHandler()
})


/*** handle avatar uploader */
function initAvatarUploaderHandler(){
    let teamForm = document.getElementById("admin-team-form")
    
    if(!teamForm){
        return
    }

    bindValidateCropper()

    context = null
    $modal = $('#image-cropper-modal')
    image = document.getElementById('imageCropper')
    
    let inputFile = document.querySelector(".avatar-input-file")

    /*** handle edit thubnail click btn */
    let editBtn = document.querySelector('button.avatar-preview--edit')
    editBtn.addEventListener('click', function(e){
        e.preventDefault()
        inputFile.click()
    })

    /** handle iput file change */
    if(inputFile){
        inputFile.addEventListener("change", function(e) {
            let legende = document.getElementById("avatar-legende-file")
            legende.classList.remove('error')
            var files = e.target.files
            ImageHelper.getImageSize(files[0], function(size){
                /** check if file size 250x250px */
                console.log(size)
                if(size.w < 250 || size.h < 250){
                    legende.classList.add('error')
                }else{
                    var done = function(url) {
                        image.src = url
                        $modal.modal('show')
                        context = "avatar"
                    }
                    if (files && files.length > 0) {
                        file = files[0]
                        if (URL) {
                            done(URL.createObjectURL(file));
                        } else if (FileReader) {
                            reader = new FileReader()
                            reader.onload = function(e) {
                                done(reader.result)
                            };
                            reader.readAsDataURL(file)
                        }
                    }
                    if(typeof crop !== "undefined")[
                        crop.destroy()
                    ]
                    crop = new Cropper(image, {
                        aspectRatio: 1,
                        viewMode: 1,
                        minCropBoxWidth: 250,
                        minCropBoxHeight: 250,
                    });
                }
            })          
        });
    }
}

function bindValidateCropper(){
    /** handle validate cropper btn click */
    let validateBtn = document.getElementById("validate-cropper")
    if(validateBtn){
      validateBtn.addEventListener("click", function(e) {
          e.preventDefault()
          canvas = crop.getCroppedCanvas();
          canvas.toBlob(function(blob) {
              var url = URL.createObjectURL(blob);
              var reader = new FileReader();
              reader.readAsDataURL(blob);
              reader.onloadend = function() {
                  var base64data = reader.result
                  $modal.modal('hide')
                  crop.destroy()
                  //crop = null
                  let previewImage = document.querySelector('.avatar-image-preview img')
                  let logo64Input  = document.querySelector('#avatar64')
                  logo64Input.value = base64data
                  previewImage.removeAttribute('width')
                  previewImage.src = base64data
                 
              }
          });
      })
    }
  }