/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 17:57:56
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-14 14:33:27
 * @ Description:
 */

const select = {
    type: 'select',
    icon: 'icon-select',
    options: {
      defaultValue: '',
      multiple: false,
      disabled: false,
      clearable: false,
      placeholder: '',
      required: false,
      showLabel: false,
      width: '',
      options: [
        {
          value: 'Option 1'
        },
        {
          value: 'Option 2'
        },{
          value: 'Option 3'
        }
      ],
      remote: false,
      filterable: false,
      remoteOptions: [],
      props: {
        value: 'value',
        label: 'label'
      },
      remoteFunc: ''
    }
}

export default select