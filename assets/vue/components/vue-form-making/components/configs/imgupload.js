/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 17:44:37
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:13:47
 * @ Description:
 */

const imgupload = {
    type: 'imgupload',
    icon: 'icon-tupian',
    options: {
      defaultValue: [],
      size: {
        width: 100,
        height: 100,
      },
      width: '',
      tokenFunc: 'funcGetToken',
      token: '',
      domain: 'http://pfp81ptt6.bkt.clouddn.com/',
      disabled: false,
      length: 8,
      multiple: false,
      isQiniu: false,
      isDelete: false,
      min: 0,
      isEdit: false,
      action: '/api/media/upload'
    }
}

export default imgupload