/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 17:56:45
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:14:49
 * @ Description:
 */

const rate = {
    type: 'rate',
    icon: 'icon-pingfen1',
    options: {
      defaultValue: null,
      max: 5,
      disabled: false,
      allowHalf: false,
      required: false
    }
}

export default rate