/*
 * Copyright (c) 2022.
 * Boba <boubarisoa@gmail.com>
 *
 */

/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 17:56:10
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:12:44
 * @ Description:
 */

export const date_of_birth = {
    type: 'date_of_birth',
    icon: 'icon-date',
    name: 'date_of_birth',
    id: 'date_of_birth',
    options: {
      defaultValue: '',
      readonly: false,
      disabled: false,
      editable: true,
      clearable: true,
      placeholder: '',
      startPlaceholder: '',
      endPlaceholder: '',
      type: 'date',
      format: 'yyyy-MM-dd',
      timestamp: false,
      required: false,
      width: '',
    }
}

export default date_of_birth