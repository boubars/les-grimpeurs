/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 17:41:29
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:13:22
 * @ Description:
 */

const fileupload = {
    type: 'fileupload',
    icon: 'fa fa-file-o',
    options: {
        defaultValue: [],
        size: {
        width: 100,
        height: 100,
        },
        width: '',
        tokenFunc: 'funcGetToken',
        token: '',
        domain: 'http://pfp81ptt6.bkt.clouddn.com/',
        disabled: false,
        length: 8,
        multiple: false,
        isQiniu: false,
        isDelete: false,
        min: 0,
        isEdit: false,
        action: '/api/media/upload'
    }
}

export default fileupload