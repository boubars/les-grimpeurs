/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 17:53:02
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:14:34
 * @ Description:
 */

const radio = {
    type: 'radio',
    icon: 'icon-radio-active',
    options: {
      inline: false,
      defaultValue: '',
      showLabel: false,
      options: [
        {
          value: 'Option 1',
          label: 'Option 1'
        },
        {
          value: 'Option 2',
          label: 'Option 2'
        },
        {
          value: 'Option 3',
          label: 'Option 3'
        }
      ],
      required: false,
      width: '',
      remote: false,
      remoteOptions: [],
      props: {
        value: 'value',
        label: 'label'
      },
      remoteFunc: '',
      disabled: false,
    }
}

export default radio