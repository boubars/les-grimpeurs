/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-14 18:39:55
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-14 18:50:29
 * @ Description:
 */

 const language = {
    type: 'language',
    icon: 'fa fa-globe',
    options: {
      defaultValue: null,
      multiple: false,
      disabled: false,
      clearable: false,
      placeholder: 'Chose option',
      required: false,
      showLabel: false,
      width: '',
      options: [],
      remote: false,
      filterable: false,
      props: {
        value: 'value',
        label: 'label'
      },
    }
}

export default language