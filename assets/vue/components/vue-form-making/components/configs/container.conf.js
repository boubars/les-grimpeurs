/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-15 12:22:57
 * @ Modified by: Boba
 * @ Modified time: 2022-01-10 16:33:18
 * @ Description:
 */

 export const basicFields = [
    'input',
    'textarea',
    'text',
    'select',
    'fileupload',
    'radio',
    'checkbox',
    'number',
    'date',
    'time', 
    'rate', 
    'color' , 
    'switch', 
    'slider'
]

export  const advanceFields = [
    'title',
    'lastname',
    'firstname',
    'sexe',
    'email',
    'date_of_birth',
    'adress',
    'phone',
    'country',
    'language', 
    'recommendationAcademic',
    'recommendationProfessional', 
    'qualification',
    'experiences',
    'languageLevel',
    'interests',
    'imgupload' , 
    'editor'
]


