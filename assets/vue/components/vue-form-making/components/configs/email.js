/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-13 13:07:50
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-13 14:02:50
 * @ Description:
 */

const email = {
    type: 'email',
    id: 'email',
    icon: 'fa fa-envelope',
    options: {
      width: '100%',
      defaultValue: '',
      required: false,
      dataType: 'email',
      pattern: '',
      placeholder: '',
      disabled: false,
    }
}

export default email