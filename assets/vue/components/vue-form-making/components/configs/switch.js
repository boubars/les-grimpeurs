/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 17:58:31
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:15:28
 * @ Description:
 */

const Switch = {
    type: 'switch',
    icon: 'icon-switch',
    options: {
      defaultValue: false,
      required: false,
      disabled: false,
    }
}

export default Switch