/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 17:36:07
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:14:03
 * @ Description:
 */

const input = {
    type: 'input',
    icon: 'icon-input',
    options: {
      width: '100%',
      defaultValue: '',
      required: false,
      dataType: 'string',
      pattern: '',
      placeholder: '',
      disabled: false,
    }
}

export default input