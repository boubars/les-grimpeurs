/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 17:54:45
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:12:23
 * @ Description:
 */

const checkbox = {
    type: 'checkbox',
    icon: 'icon-check-box',
    options: {
      inline: false,
      defaultValue: [],
      showLabel: false,
      options: [
        {
          value: 'Option 1'
        },
        {
          value: 'Option 2'
        },
        {
          value: 'Option 3'
        }
      ],
      required: false,
      width: '',
      remote: false,
      remoteOptions: [],
      props: {
        value: 'value',
        label: 'label'
      },
      remoteFunc: '',
      disabled: false,
    }
}

export default checkbox