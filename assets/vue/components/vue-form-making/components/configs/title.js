/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 17:38:15
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:11:11
 * @ Description:
 */

const title = {
    type: 'title',
    icon: 'icon-wenzishezhi-',
    options: {
        defaultValue: 'Title 1', 
        customClass: '',
    }
}

export default title