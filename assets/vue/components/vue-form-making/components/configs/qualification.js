/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-16 17:39:17
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-16 18:34:21
 * @ Description:
 */

const qualification = {
    type: 'qualification',
    icon: 'icon-zidingyishuju',
    options: {
        defaultValue: [
            {
                institution: '',
                date: null,
                gradeName: '',
                mention: '',
                country: null,
                departement: null,
                city: ''
            }
        ]
        
    }
}

export default qualification