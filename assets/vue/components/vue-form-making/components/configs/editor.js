/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 17:47:49
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:13:06
 * @ Description:
 */

const editor = {
    type: 'editor',
    icon: 'icon-fuwenbenkuang',
    options: {
      defaultValue: '',
      width: ''
    }
}

export default editor