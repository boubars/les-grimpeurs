/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 17:57:13
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:12:35
 * @ Description:
 */

const color = {
    type: 'color',
    icon: 'icon-color',
    options: {
      defaultValue: '',
      disabled: false,
      showAlpha: false,
      required: false
    }
}

export default color