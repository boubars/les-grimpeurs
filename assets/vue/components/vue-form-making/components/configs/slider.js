/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 18:00:00
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:15:16
 * @ Description:
 */

const slider = {
    type: 'slider',
    icon: 'icon-slider',
    options: {
      defaultValue: 0,
      disabled: false,
      required: false,
      min: 0,
      max: 100,
      step: 1,
      showInput: false,
      range: false,
      width: ''
    }
}

export default slider