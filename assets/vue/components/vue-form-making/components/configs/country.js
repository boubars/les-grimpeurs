/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 17:57:56
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-14 14:49:21
 * @ Description:
 */

 const select = {
    type: 'country',
    icon: 'fa fa-flag',
    options: {
      defaultValue: null,
      multiple: false,
      all: false,
      disabled: false,
      clearable: false,
      placeholder: 'Chose option',
      required: false,
      showLabel: false,
      width: '',
      options: [],
      remote: false,
      filterable: false,
      props: {
        value: 'value',
        label: 'label'
      },
    }
}

export default select