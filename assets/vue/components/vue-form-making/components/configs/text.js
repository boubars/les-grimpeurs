/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 18:00:32
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:15:42
 * @ Description:
 */

const text = {
    type: 'text',
    icon: 'icon-wenzishezhi-',
    options: {
      defaultValue: 'This is a text',
      customClass: '',
    }
}

export default text