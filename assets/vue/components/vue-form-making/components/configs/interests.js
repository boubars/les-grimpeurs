/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-20 20:12:59
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-20 21:36:10
 * @ Description:
 */

const interests = {
    type: 'interests',
    icon: 'icon-zidingyishuju',
    options: {
        defaultValue: [
            {
                title : null,
                description  : null
            }
        ]
        
    }
}
export default interests