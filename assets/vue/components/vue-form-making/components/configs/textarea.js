/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 17:49:00
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:15:51
 * @ Description:
 */

export const textarea = {
    type: 'textarea',
    icon: 'icon-diy-com-textarea',
    options: {
      width: '100%',
      defaultValue: '',
      required: false,
      disabled: false,
      pattern: '',
      placeholder: ''
    }
}

export default textarea