/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-15 12:16:47
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-15 18:54:24
 * @ Description:
 */

 export const recommendationAcademic = {
    type: 'recommendationAcademic',
    icon: 'icon-zidingyishuju',
    options: {
        defaultValue: [
            {
                name: '',
                institution: '',
                title: '',
                city: '',
                country: null,
                phone: null,
                email: ''
            }
        ]
        
    }
}

export const recommendationProfessional = {
    type: 'recommendationProfessional',
    icon: 'icon-zidingyishuju',
    options: {
        defaultValue: [
            {
                name: '',
                lastname: '',
                title: '',
                institution: '',
                occupation: null,
                city: null,
                country: '',
                email: ''
            }
        ]
        
    }
}