/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-20 20:12:59
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-20 20:25:25
 * @ Description:
 */

const languageLevel = {
    type: 'languageLevel',
    icon: 'icon-zidingyishuju',
    options: {
        defaultValue: [
            {
                language : null,
                level    : null,
                comment  : null
            }
        ]
        
    }
}

export default languageLevel