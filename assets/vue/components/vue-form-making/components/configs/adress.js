/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 17:39:08
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:11:36
 * @ Description:
 */

const adress = {
    type: 'adress',
    icon: 'fa fa-map-marker',
    options: {
        width: '100%',
        defaultValue: '',
        required: false,
        dataType: 'string',
        pattern: '',
        placeholder: '',
        disabled: false,
    }
}
export default adress