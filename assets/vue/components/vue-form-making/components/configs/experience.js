/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-16 17:39:17
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-16 19:07:31
 * @ Description:
 */

const experience = {
    type: 'experiences',
    name: 'experiences',
    id: 'experiences',
    icon: 'icon-zidingyishuju',
    options: {
        defaultValue: [
            {
                title: null,
                company: null,
                place: null,
                startDate: null,
                endDate: null,
                description:''
            }
        ]
        
    }
}

export default experience