/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 17:45:54
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:11:47
 * @ Description:
 */

const blank = {
    type: 'blank',
    icon: 'icon-zidingyishuju',
    options: {
        defaultType: 'String'
    }
}
export default blank