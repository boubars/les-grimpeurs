/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 17:55:37
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:16:06
 * @ Description:
 */

const time = {
    type: 'time',
    icon: 'icon-time',
    options: {
      defaultValue: '21:19:56',
      readonly: false,
      disabled: false,
      editable: true,
      clearable: true,
      placeholder: '',
      startPlaceholder: '',
      endPlaceholder: '',
      isRange: false,
      arrowControl: true,
      format: 'HH:mm:ss',
      required: false,
      width: '',
    }
}

export default time