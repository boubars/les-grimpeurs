/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-13 13:07:50
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-13 13:19:34
 * @ Description:
 */

const phone = {
    type: 'phone',
    icon: 'fa fa-phone',
    options: {
      width: '100%',
      defaultValue: '',
      required: false,
      dataType: 'string',
      pattern: '',
      placeholder: '',
      disabled: false,
    }
}

export default phone