/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 17:50:59
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:14:19
 * @ Description:
 */

const number = {
    type: 'number',
    icon: 'icon-number',
    options: {
      width: '',
      required: false,
      defaultValue: 0,
      min: '',
      max: '',
      step: 1,
      disabled: false,
      controlsPosition: ''
    }
}

export default number