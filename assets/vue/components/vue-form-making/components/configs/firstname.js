/**
 * @ Author: Boba
 * @ Create Time: 2022-01-07 17:36:07
 * @ Modified by: Boba
 * @ Modified time: Boba 18:14:03
 * @ Description:
 */

const firstname = {
    type: 'firstname',
    icon: 'icon-input',
    options: {
      width: '100%',
      defaultValue: '',
      required: false,
      dataType: 'string',
      pattern: '',
      placeholder: '',
      disabled: false,
        id: "firstname",
    }
}

export default firstname