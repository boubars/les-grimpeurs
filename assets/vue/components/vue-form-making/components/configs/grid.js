/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 18:03:57
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:13:35
 * @ Description:
 */


const grid = {
    type: 'grid',
    icon: 'icon-grid-',
    columns: [
      {
        span: 12,
        list: []
      },
      {
        span: 12,
        list: []
      }
    ],
    options: {
      gutter: 0,
      justify: 'start',
      align: 'top'
    }
  }

  export default grid