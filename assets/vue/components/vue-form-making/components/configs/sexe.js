/**
 * @ Author: Boba
 * @ Create Time: 2022-01-10 17:39:17
 * @ Modified by: Boba
 * @ Modified time: 2022-01-10 17:39:17
 * @ Description:
 */

const sexe = {
    type: 'sexe',
    icon: 'fa fa-user',
    name: 'sexe',
    id: 'sexe',
    options: {
        defaultValue:'',
        inline: true,
        showLabel: false,
        options: [
            {
                value: 'man',
                label: 'man',
            },
            {
                value: 'woman',
                label: 'woman',
            },
            {
                value: 'transgenre',
                label: 'transgenre'
            }
        ],
    }
}

export default sexe