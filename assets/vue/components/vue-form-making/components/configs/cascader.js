/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-10 18:02:50
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-10 18:12:00
 * @ Description:
 */

const cascader = {
    type: 'cascader',
    icon: 'icon-jilianxuanze',
    options: {
      defaultValue: [],
      width: '',
      placeholder: '',
      disabled: false,
      clearable: false,
      remote: true,
      remoteOptions: [],
      props: {
        value: 'value',
        label: 'label',
        children: 'children'
      },
      remoteFunc: ''
    }
}
  
export default cascader