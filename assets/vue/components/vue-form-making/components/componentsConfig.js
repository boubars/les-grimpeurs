import title         from "./configs/title"
import input         from "./configs/input"
import firstname     from "./configs/firstname"
import name          from "./configs/name"
import adress        from "./configs/adress"
import textarea      from "./configs/textarea"
import number        from "./configs/number"
import radio         from "./configs/radio"
import blank         from "./configs/blank"
import imgupload     from "./configs/imgupload"
import fileupload    from "./configs/fileupload"
import editor        from "./configs/editor"
import checkbox      from './configs/checkbox';
import time          from './configs/time';
import date          from './configs/date';
import rate          from './configs/rate';
import color         from './configs/color';
import select        from './configs/select';
import Switch        from './configs/switch';
import slider        from './configs/slider';
import text          from './configs/text';
import cascader      from './configs/cascader';
import grid          from './configs/grid';
import country       from './configs/country';
import phone         from './configs/phone';
import email         from './configs/email';
import language      from './configs/language';
import qualification from './configs/qualification';
import experience    from './configs/experience';
import languageLevel from './configs/languageLevel';
import interests     from './configs/interests';
import sexe          from './configs/sexe';
import date_of_birth from './configs/date_of_birth';

import {recommendationAcademic, recommendationProfessional} from './configs/recommendation';


export const basicComponents = [
  input,
  textarea,
  select,
  radio,
  checkbox,
  fileupload,
  number,
  time,
  date,
  rate,
  color,
  Switch,
  slider,
  text
]

export const advanceComponents = [
  blank,
  title,
  name,
  firstname,
  sexe,
  date_of_birth,
  email,
  adress,
  phone,
  country,
  language,
  recommendationAcademic,
  recommendationProfessional,
  qualification,
  experience,
  languageLevel,
  interests,
  imgupload,
  editor,
  cascader
]

export const layoutComponents = [
  grid
]


