import VueI18n from 'vue-i18n'
import 'normalize.css/normalize.css'

import MakingForm from './components/Container.vue'
import GenerateForm from './components/GenerateForm.vue'

import en from './lang/en'
import fr from './lang/fr'

import './iconfont/iconfont.css'
import './styles/cover.scss'
import './styles/index.scss'

const loadLang = function (Vue, lang, locale, i18n) {
  if (locale) {
    locale('en', {...locale('en'), ...en})
    locale('fr', {...locale('fr'), ...fr})
    Vue.config.lang = lang
  } else if (i18n) {
    i18n.setLocaleMessage('en', {...i18n.messages['en'], ...en})
    i18n.setLocaleMessage('fr', {...i18n.messages['fr'], ...fr})
    i18n.locale = lang
  } else {
    Vue.use(VueI18n)
    Vue.locale('en', {...Vue.locale('en'), ...en})
    Vue.locale('fr', {...Vue.locale('fr'), ...fr})
    Vue.config.lang = lang
  }
}

MakingForm.install = function (Vue, opts = {
  lang: 'fr',
  locale: null,
  i18n: null
}) {
  loadLang(Vue, opts.lang, opts.locale, opts.i18n)
  Vue.component(MakingForm.name, MakingForm)
}

GenerateForm.install = function (Vue, opts = {
  lang: 'fr',
  locale: null,
  i18n: null
}) {
  loadLang(Vue, opts.lang, opts.locale, opts.i18n)
  Vue.component(GenerateForm.name, GenerateForm)
}

const components = [
  MakingForm,
  GenerateForm
]

const install = function (Vue, opts = {
  lang: 'fr',
  locale: null,
  i18n: null
}) {
  loadLang(Vue, opts.lang, opts.locale, opts.i18n)
  components.forEach(component => {
    Vue.component(component.name, component)
  })
}

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}

export {
  install,
  MakingForm,
  GenerateForm
}

export default {
  install,
  MakingForm,
  GenerateForm
}
