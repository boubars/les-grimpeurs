import Modal from "@/components/Modal/Modal.vue";
import LoginForm from "@/view/login/LoginForm.vue";

const SidebarMixin = {
  name: "sidebar",
  components: {
    Modal,
    LoginForm,
  },
  data: () => {
    return {
      /*** logo_menu.png is used in email template so dont rename or replace this file by the svg */
      logo_grippeur: require("@/assets/img/logo_menu.png").default,
      logo_grimpeur_small: require("@/assets/img/nav-brand-logo.png").default,
      img_favoris: require("@/assets/img/icon-heart.svg").default,
      img_agenda: require("@/assets/img/icon-calendar-green.svg").default,
      img_candidat: require("@/assets/img/icon-candidate.svg").default,
      img_message: require("@/assets/img/icon-envelope.svg").default,
      img_forum: require("@/assets/img/icon-forum.svg").default,
      img_blog: require("@/assets/img/icon-blog.svg").default,
      img_search: require("@/assets/img/icon-search-grey.svg").default,
      img_notif: require("@/assets/img/notif.png").default,
      img_scholarship: require("@/assets/img/icon-dollar.svg").default,
      /*** the user.png file is used in template email so dont replace this by an otrher svg file  */
      img_user: require("@/assets/img/user.png").default,
      img_dropdown: require("@/assets/img/dropdown.png").default,
      isOpen: false
    };
  },
  computed: {
    navbrandLogo(){
      return this.isOpen ? this.logo_grippeur : this.logo_grimpeur_small
    },
    sidebarClass(){
        return { 
          'side-conecter': this.logged,  
          'open' :  this.isOpen,
          'close':  !this.isOpen
        }
    },
  },
  methods: {
    closeMenuMobile() {
      var menu_interne = document.getElementsByClassName("sidebar")[0];
      var bouton_ouverture_interne = document.getElementsByClassName(
        "humbergur-interne"
      )[0];
      bouton_ouverture_interne.style.display = "block";
      menu_interne.style.display = "none";
    },
    
    logOut() {
      this.$store.dispatch("user/logout");
    },
    
    switchSidebar(){
      this.isOpen = !this.isOpen
      this.switchSidebarStatus()
    },

    goToApplicationList(){
        let router = 'student_application_list'
        if(this.profil.roles.indexOf("ROLE_ORGANIZATION") != -1){
          router = 'organization_candidat_list'
        }
        this.$router.push({name: router})
    }
  },

  mounted: function(){
    let storage = localStorage.getItem('sidebaropen')
    this.isOpen = storage == 1
  },

 
}
export default SidebarMixin