import axios from 'axios'
import Api from '../api/api'

const PostService = {
    get: function(params){
        return axios.get(Api.post.getPost, {
            params : params
        })
    },
    download: function(id){
        document.location.href = Api.post.downloadPost(id)
    }
}
export default PostService