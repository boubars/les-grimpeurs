import axios from 'axios'
import Api from '../api/api.js'

const StudentService = {

    getSearchResult: function (params) {
        return axios.get(Api.student.find, {
            params: {
              ...params
            }
        })

        /*if (context != 'pagination') {
            this.getCountAndAmount(criteria, this.onlyFavorited);
        }*/
    },
    getApplications: function(params = {} ){
        return axios.get(Api.student.applicationList, { params })
    },
    getStudentPosition(sid){
        return axios.get(Api.student.profil.getCurrentPosition, {
            params: {
                student: sid
            }
        })
    },
    submitDocument(document){
        return axios.post(Api.student.ducument_add, document, {headers: {'Content-Type': 'multipart/form-data' }} )
    },
    getDocuments: function(){
        return axios.get(Api.student.ducument_list)
    },
    removeDocument: function(doc){
        return axios.get(Api.student.ducument_remove, {
            params: {
                id: doc.id
            }
        })
    },
}


export default StudentService
