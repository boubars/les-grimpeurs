import axios from 'axios'
import Api from '../api/api.js'

const RegistrationService = { 
    /**
     * @param  data :{ firstname ,lastname,phone , emailpassword } 
     */
    register : function (data) {
        return axios.post(Api.student.registration, data)
    }
}


export default RegistrationService
