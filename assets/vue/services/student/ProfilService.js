import axios from 'axios'
import Api from '../api/api.js'

const ProfilService = { 
    updateAvatar : function(formData){
        return axios.post(
            Api.student.profil.updateStudentAvatar,
                formData, 
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            )
    },
    updateStudentInfos : function(data){
        return axios.post(
            Api.student.profil.updateStudentInfos, data)
    },
    updateStudentLangue:function(data){
        return axios.post(Api.student.profil.updateStudentLangue, data)
    },
    updateStudentFormation(data){
        return axios.post(Api.student.profil.updateStudentFormation, data)
    },
    updateStudentExperience(data){
        return axios.post(Api.student.profil.updateStudentExperience, data)
    },
    updateStudentPublication(data){
        return axios.post(Api.student.profil.updateStudentPublication, data)
    },
    updateStudentActivity(data){
        return axios.post(Api.student.profil.updateStudentActivity, data)
    },
    updateStudentAssociation(data){
        return axios.post(Api.student.profil.updateStudentAssociation, data)
    },
    updateStudentDistinction(data){
        return axios.post(Api.student.profil.updateStudentDistinction, data)
    },
    updateStudentExam(data){
        return axios.post(Api.student.profil.updateStudentExam, data)
    },
    removeStudentLanguage: (id) => {
        return axios.delete(
            Api.student.profil.removeStudentLanguage, {data : {id : id}}
        )
    },
    removeStudentFormation:(id) => {
        return axios.delete(
            Api.student.profil.removeStudentFormation, {data : {id : id}}
        )
    },
    removeStudentExperience: (id) => {
        return axios.delete(
            Api.student.profil.removeStudentExperience, {data : {id : id}}
        )
    },
    removeStudentPublication :(id) => {
        return axios.delete(
            Api.student.profil.removeStudentPublication, {data : {id : id}}
        )
    },
    removeStudentActivity:(id) => {
        return axios.delete(
            Api.student.profil.removeStudentActivity, {data : {id : id}}
        )
    },
    removeStudentDistinction: (id) => {
        return axios.delete(
            Api.student.profil.removeStudentDistinction, {data : {id : id}}
        )
    },
    removeStudentExam: (id) => {
        return axios.delete(
            Api.student.profil.removeStudentExam, {data : {id : id}}
        )
    },
    removeStudentAssociation: (id) => {
        return axios.delete(
            Api.student.profil.removeStudentAssociation, {data : {id : id}}
        )
    },
    updateSearchPreference(data){
        return axios.post(Api.student.profil.updateSearchPreference, data)
    },
    downloadStudentCv(studentId, locale){
        document.location.href = Api.student.profil.downloadStudentCv(studentId, locale)
    }
}


export default ProfilService
