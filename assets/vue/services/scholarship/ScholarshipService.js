import axios from 'axios'
import Api from '../api/api.js'

const ScholarshipService = {
    saveScholarship(data){
        return axios.post(Api.scholarship.save, data , 
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        )
    },
    findOneBySlug(slug){
        return axios.get(Api.scholarship.find, 
            { 
                params:{
                    criteria  :  { slug: slug },
                    maxResult :  1
                }
            }
        ) 
    },
    findByCriteria(criteria = [], order = null, limit = null, offset = null, onlyFavorited = false){
       
        return axios.get(
            Api.scholarship.find,
            {
                params: {
                    criteria,
                    order,
                    limit,
                    offset,
                    onlyFavorited
                }
            })
    },
    countAll(criteria, onlyFavorited = false){
        return axios.get(
            Api.scholarship.total,
            {
                params: {
                    criteria: criteria,
                    onlyFavorited: onlyFavorited
                }
            }
        )
    },
    /**
     * 
     * @param {integer} scholarship 
     */
    requestInfos(data){
        return axios.post(Api.scholarship.requestInfos, { ...data } )
    },
    submitApplication(scholarship){
        return axios.post(Api.scholarship.submitApplication, { scholarship } )
    },
    /**
     * @param {integer} scholarship 
     */
    addFavoriteScholarship(scholarship){
        return axios.post(Api.scholarship.addFavoriteScholarship, { scholarship} )
    },
    removeFavoriteScholarship(scholarship){
        return axios.post(Api.scholarship.removeFavoriteScholarship, { scholarship} )
    },
    getMyFavorite(){
        return axios.get(Api.scholarship.getFavoriteScholarship)
    },
    isOnMyFavorite(scholarship){
        return axios.post(Api.scholarship.isFavoriteScholarship, {scholarship} )
    },
    getSuggestionList(){
        return axios.get(Api.scholarship.getStudentSuggestionScholarship)
    },
    getStudentStat(){
        return axios.get(Api.scholarship.getStudentStat)
    },
    getDistributionStat(){
        return axios.get(Api.scholarship.getDistributionStat)
    },
    saveScholarshipAlert(scholarshipAlert){
        return axios.get(Api.scholarship.saveScholarshipAlert, {params:scholarshipAlert})
    },
    getScholarshipAlert(){
        return axios.get(Api.scholarship.getScholarshipAlert)
    },
    findByCountry(country, order, limit, offset, onlyFavorited){
        let params = {country:country, order:order, limit: limit, offset:offset, onlyFavorited:onlyFavorited}
        return axios.get(Api.scholarship.findSpecific,
            {params:params})
    },
    findByUniversity(university, country, order, limit, offset, onlyFavorited){
        let params = {university:university, country:country, order:order, limit: limit, offset:offset, onlyFavorited:onlyFavorited}
        return axios.get(Api.scholarship.findSpecific, {params:params })
    },
    totalByCountry(country, onlyFavorited){
        let params = {country:country, onlyFavorited:onlyFavorited}
        return axios.get(Api.scholarship.totalSpecific,
            {params:params})
    },
    /**
     * @param {integer} scholarship 
     */
    addBlacklistcholarship(scholarship){
        return axios.post(Api.scholarship.addBlacklistcholarship, { scholarship} )
    },

    /*** scholarship view count
     * @var Integer scholarship [scholarship ID]
     */
    seenByUser(scholarship){
        return axios.get(Api.scholarship.seenByUser(scholarship) )
    },
    /**** 
    * Get scholarship view count, favorited by user stat 
    */
    getStatEvolutionStat : function(params){
        return axios.get(Api.scholarship.getStat, {
            params : {
                ...params,
                context: 'evolution'
            }
        })
        
    },
    getRangeLevelStat : function(params){
        return axios.get(Api.scholarship.getStat, {
            params : {
                ...params,
                context: 'range_level'
            }
        })
        
    },
    getRangeSexeStat : function(params){
        return axios.get(Api.scholarship.getStat, {
            params : {
                ...params,
                context: 'range_sexe'
            }
        })
        
    },
    getRangeAgeStat : function(params){
        return axios.get(Api.scholarship.getStat, {
            params : {
                ...params,
                context: 'range_age'
            }
        })
        
    },
    getRangeCountryStat : function(params){
        return axios.get(Api.scholarship.getStat, {
            params : {
                ...params,
                context: 'range_country'
            }
        })
        
    },
    getRangeDisciplineStat : function(params){
        return axios.get(Api.scholarship.getStat, {
            params : {
                ...params,
                context: 'range_discipline'
            }
        })
        
    },
    getScholarshipStory : function(params){
        return axios.get(Api.scholarship.getStory, { params })
        
    },
        
    getApplicationForm: function(scholarship){
        return axios.get(Api.scholarship.applicationForm(scholarship.id))
    },

    deleteScholarship: function(scholarship){
        return axios.delete(Api.scholarship.delete, { data: { id: scholarship.id }})
    }
}


export default ScholarshipService
