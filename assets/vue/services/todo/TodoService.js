import axios from 'axios'
import Api from '../api/api.js'

const TodoService = { 
    getList : function(){
        return axios.get(Api.todo.list)
    },
    addTodo: function(todo){
      return axios.post(Api.todo.add, {...todo} )
    },
    editTodo: function(todo){
      return axios.post(Api.todo.edit, {...todo} )
    },
    removeTodo: function(todo){
      return axios.post(Api.todo.remove, {...todo} )
    }
}


export default TodoService
