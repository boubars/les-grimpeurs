import axios from 'axios'
import Api from '../api/api.js'

const ApplicationService = {
   getFolderList: function(){
       return axios.get(Api.application.folderList)
   },

   addFolder: function(folder){
    return axios.post(Api.application.folderAdd, folder)
   },
   
   editFolder: function(folder){
       return axios.post(Api.application.folderEdit(folder.id), folder)
   },

   deleteFolder: function(folder){
       return axios.get(Api.application.folderDelete(folder.id))
   },

   findOneBySlug: function(slug){
        return axios.get(Api.application.findFolder, {
            params: { 
                slug : slug,
                max  : 1 
            }
        })
   },

   addForm: function(params){
        return axios.post(Api.application.formAdd, {
            folder   : params.folder.id,
            name     : params.name ?? '',
            template : params.template ?? false
        })
   },
   

   editForm: function(form){
        return axios.post(Api.application.formEdit(form.id), form)
   },
   

   publishForm: function(form, scholarhsips){
        return false
        return axios.post(Api.application.formEdit(form.id), form)
   },
   
   findFormBySlug: function(slug){
       return axios.get(Api.application.findForm, {
           params: { 
               slug : slug,
               max  : 1 
            }
        })
    },
    
    addSheet: function(form, sheet){
        return axios.post(Api.application.sheetAdd, {
            form: form.id,
            ...sheet
        })
    },

    updateSheet: function(sheet){
        return axios.post(Api.application.sheetEdit(sheet.id), sheet)
    },

    deleteSheet: function(sid){
        return axios.delete(Api.application.sheetDelete,{params :{ id :sid }})
    },

    getFormSheets: function(form){
        return axios.get(Api.application.formSheets(form.id))
    },

    getFormScholarships: function(fid){
        return axios.get(Api.application.formScholarships(fid))
    },

    deleteForm: function(form){
        return axios.get(Api.application.formDelete, {
            params: {
                forms: [form.id]
            }
        })
    },

    /**
     * Delete form service
     * @param {Array|integer} ids 
     * @returns 
     */
    deleteForms: function(ids, force = false){
        ids = Array.isArray(ids) ? ids : [ids]
        return axios.get(Api.application.formDelete, {
            params: {
                ids: ids,
                ...(force && {force: force})
            }
        })
    },
    
    publishForm: function(form, scholarships){
        return axios.post(Api.application.formAffect(form.id), {  scholarships })
    },

    saveFormData: function(formData, application){
        return axios.post( Api.application.saveFormData(application.id), {
            formdata: formData
        })
    },

    publishApplication: function(application){
        return axios.get(Api.application.publishApplication, {
            params : {
                application: application.id
            }
        })
    },

    showApplication: function(appid){
        return axios.get(Api.application.applicationShow, {
            params : {
                id : appid
            }
        })
        
    },
    updateApplicationStatus: function(data){
        return axios.post(Api.application.updateStatus, data)
    },

    addSheetNote: function(data){
        return axios.post(Api.application.addSheetNote, data)
    },

    addSheetComment: function(data){
        return axios.post(Api.application.addSheetComment, data)
    },

    downloadApplication: function(appid){
        document.location.href = Api.application.downloadApplication(appid)
    },
    getApplicationNote: function(id){
        return axios.get(Api.application.getApplicationNote, { params : {  id } })
    },
    updateApplicationLastAverage: function(id){
        return axios.post(Api.application.updateLastAverage, {id})
    },
    cloneForm: function(ids){
        return axios.post(Api.application.cloneForm, {
            id   : ids
        })
   },

}

export default ApplicationService