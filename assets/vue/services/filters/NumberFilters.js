export default {
  toCurrency:function (value, locale, currency) {

          if(locale === null || locale === undefined){
              locale = 'en-US'
          }
          if(currency === null || currency === undefined){
              currency = 'USD'
          }
          if (typeof value !== "number") {
              return value;
          }
          const formatter = new Intl.NumberFormat(locale, {
              style: 'currency',
              currency: currency,
              currencyDisplay: 'symbol',
              minimumFractionDigits: 0
          });
          return formatter.format(value);
      }
}
