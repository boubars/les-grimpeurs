export default {
    isVideoBaseUrl(url){
        let baseUrl = [null,"", "https://www.linkedin.com", "https://www.facebook.com", "https://www.twitter.com", "https://www.youtube.com"]

        /*** remove last / on url */
        url = url && url.length > 1 && url[url.length-1] == "/" ? url.substring(0, url.length-1) : url
        return (baseUrl.indexOf(url) > -1)
   },
   isValidURL(str) {
        let url
        try {
            url = new URL(str)
        } catch (_) { 
            return false 
        }
        return url.protocol === "http:" || url.protocol === "https:"
    },
    camelToSnake(str) {
        return str.replace( /([A-Z])/g, "_$1" ).toLowerCase();
    },

    slugify(str) {
        str = str.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
        const map = {
          '-' : ' ',
          '-' : '_',
          'a' : 'á|à|ã|â|ä|À|Á|Ã|Â|Ä',
          'e' : 'é|è|ê|ë|É|È|Ê|Ë',
          'i' : 'í|ì|î|ï|Í|Ì|Î|Ï',
          'o' : 'ó|ò|ô|õ|ö|Ó|Ò|Ô|Õ|Ö',
          'u' : 'ú|ù|û|ü|Ú|Ù|Û|Ü',
          'c' : 'ç|Ç',
          'n' : 'ñ|Ñ'
        };
      
        for (var pattern in map) {
          str = str.replace(new RegExp(map[pattern], 'g'), pattern);
        }
        return str;
      
    },

    removeAccents: function(text){
      const accentsMap = new Map([
        ["A", "Á|À|Ã|Â|Ä"],
        ["a", "á|à|ã|â|ä"],
        ["E", "É|È|Ê|Ë"],
        ["e", "é|è|ê|ë"],
        ["I", "Í|Ì|Î|Ï"],
        ["i", "í|ì|î|ï"],
        ["O", "Ó|Ò|Ô|Õ|Ö"],
        ["o", "ó|ò|ô|õ|ö"],
        ["U", "Ú|Ù|Û|Ü"],
        ["u", "ú|ù|û|ü"],
        ["C", "Ç"],
        ["c", "ç"],
        ["N", "Ñ"],
        ["n", "ñ"]
      ]);

      const reducer = (acc, [key]) => acc.replace(new RegExp(accentsMap.get(key), "g"), key);
      return  [...accentsMap].reduce(reducer, text)

    }

}