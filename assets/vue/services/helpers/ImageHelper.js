import * as htmlToImage from 'html-to-image'
import { toPng, toJpeg, toBlob, toPixelData, toSvg } from 'html-to-image'

export default {
  ImgToBase64URL:function(url, callback, outputFormat){
      var img = new Image();
      img.crossOrigin = 'Anonymous'
      img.onload = function(){
          var canvas = document.createElement('CANVAS'),
          ctx = canvas.getContext('2d'), dataURL
          canvas.height = img.height
          canvas.width = img.width
          ctx.drawImage(img, 0, 0)
          dataURL = canvas.toDataURL(outputFormat)
          callback(dataURL)
          canvas = null 
      }
      img.src = url
  },
  DataURIToBlob: function(dataURI) {
    const splitDataURI = dataURI.split(',')
    const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1])
    const mimeString = splitDataURI[0].split(':')[1].split(';')[0]

    const ia = new Uint8Array(byteString.length)
    for (let i = 0; i < byteString.length; i++)
        ia[i] = byteString.charCodeAt(i)

    return new Blob([ia], { type: mimeString })
  },
  
  getImageSize: function(file, callback) {
      const img = new Image()
      let s = 12
      img.onload = function() {
        let w = this.width
        let h = this.height
        callback({w,h})
      }
      let URL = window.URL || window.webkitURL
      if (URL && URL.createObjectURL){
          if(file.file){
            img.src =  URL.createObjectURL(file.file)
          }else{
            img.src =  URL.createObjectURL(file)
          }

      } 
  },

  getNodeImage: function(node, callback){
    htmlToImage.toJpeg(node, { quality: 1, backgroundColor:"white" })
    .then(function(dataUrl) {
      callback(dataUrl)
    })
    .catch(function (error) {
        console.dispatchError(error);
    });

  }
}