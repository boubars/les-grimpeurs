import moment from "moment"

export default {
    formatDateRender: function(dte, local, short = false ){
        dte = moment(dte).format('YYYY-MM-DD')
        let dateArray = dte.split('-') 
        let mounth = this.$t(`word.months.${parseInt(dateArray[1])}`)

        if(short)
          mounth = mounth.substring(0,3)

        if(local == 'fr'){
          return `${dateArray[2]} ${mounth} ${dateArray[0]} `
        }else{
          return `${dateArray[0]} ${mounth} ${dateArray[2]}`
        }
    },
    
    getLocalDate(dte, local){
        if(local == 'fr')
          return moment(dte).format('DD-MM-YYYY')
          
        return moment(dte).format('YYYY-MM-DD')
    },

    formatDate: function (date, locale) {
        moment.locale(locale);
        let timestamps = Date.parse(date);
        return moment(timestamps).format('LL');   
    },

    isToday:(someDate) => {
        const today = new Date()
        someDate    = new Date(someDate)
        return someDate.getDate() == today.getDate() &&
          someDate.getMonth() == today.getMonth() &&
          someDate.getFullYear() == today.getFullYear()
    }

}