import StringHelpers from '@/services/helpers/StringHelpers.js';

const Helpers = {
    removeExtraSpace : function (str)
    {
        str = str.replace(/[\s]{1,}/g,""); // Enlève les espaces doubles, triples, etc.
        str = str.replace(/^[\s]/, ""); // Enlève les espaces au début
        str = str.replace(/[\s]$/,""); // Enlève les espaces à la fin
        return str;    
    },
    TraitementListdiscipline : function(data , that)
    {
        
        let key = ['home', 'category'].includes(that.context) ? "discipline.category." : "discipline."
        
        let sortData = data.map(function(x) { 
            return  {
                "id" : x.id , 
                "category" : x.category ,  
                "name" : that.$t(key+x.id)
            }
        }).sort(function(a, b) {

              var nameA = StringHelpers.removeAccents(a.name.toUpperCase()); // ignore upper and lowercase and accent
              var nameB = StringHelpers.removeAccents(b.name.toUpperCase()); // ignore upper and lowercase
              
              if (nameA < nameB) {
                return -1;
              }
              if (nameA > nameB) {
                return 1;
              }
              return 0;
        })
        /** move all at the first list */
        if(that.all){
            sortData =[ {id:0, name: that.$t('word.all') },...sortData]
        }
            
        return sortData  
    }

}

export default Helpers;