import axios from 'axios'
import Api from '../api/api.js'

const ProfilService = { 
    
    updateOrganizationName : function (data, id) {
        return axios.put(Api.organization.profil.updateOrganizationName(id), data)
    },
    updateOrganizationInfos : function(data, id){
        return axios.put(Api.organization.profil.updateOrganizationInfos(data.organization.id), data)
    },
    updateGalleryPhoto : function(formData, id){
        return axios.post(
            Api.organization.profil.updateGalleryPhoto(id),
                formData, 
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            )
    },
    updateGalleryVideo : function(data, id){
        return axios.post( Api.organization.profil.updateGalleryVideo(id), data )
    },
    updateAvatar : function(formData, id){
        return axios.post(
            Api.organization.profil.updateOrganizationAvatar(id),
                formData, 
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            )
    },
    getLogo: function(organizationId){
        return axios.get(
            Api.organization.profil.getOrganizationLogo,{
                params : {
                    organization : organizationId
                }
            }
        )
    },
    getContact:function(organizationId){
        return axios.get(
            Api.organization.profil.getOrganizationContact,{
                params : {
                    organization : organizationId
                }
            }
        )
    },
    updateLogo : function(formData, organizationId){
        return axios.post(
            Api.organization.profil.updateOrganizationLogo(organizationId),
            formData, 
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        )
    },
    updateCover : function(formData, organizationId){
        return axios.post(
            Api.organization.profil.updateOrganizationCover(organizationId),
            formData, 
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        )
    },
    getOrganizationList : function(institutionType = false){
        return axios.get(Api.organization.profil.getList, {
            params : {
                institutionType 
            }
        })
    },
    getGallery:function(organizationId){
        return axios.get(
            Api.organization.profil.getOrganizationGallery,{
                params : {
                    organization : organizationId
                }
            }
        )
    },
    findOneBySlug(slug){
        return axios.get(Api.organization.profil.getOneBySlug(slug)) 
    },
    getRelatedScholarship: function(context = 'list', order = []){
        return axios.get(Api.organization.profil.getRelatedScholarship, {
            params: {
                context,
                order
            }
        })
    },
    downloadScholarshipStat: function(series){
        return axios({
            url : Api.organization.profil.downloadScholarshipStat,
            method: 'POST',
            data: { series },
            responseType: 'blob', // important
        })
    },
    getRelatedDisciplines: function(){
        return axios.get(Api.common.disciplineFind, {
            params: {
                context: "organization"
            }
        })
    },
    getApplications: function(params){
        return axios.get(Api.organization.applicationList, {
            params
        })
    },

    updateHiddenFields: function(fields){
        return axios.post(Api.organization.profil.updateHiddenFields, {
            fields
        })
    },

    getMyOrganisation: function(){
        return axios.get(Api.organization.profil.myOrganization)
    }

}


export default ProfilService
