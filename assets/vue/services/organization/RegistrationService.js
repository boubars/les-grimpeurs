import axios from 'axios'
import Api from '../api/api.js'

const RegistrationService = { 
    /**
     * @param  data :{ firstname ,lastname, dateOfBirth, phone , emailpassword } 
     */
    register : function (data) {
        return axios.post(Api.organization.registration, data)
    },
    requestActivationLink: function(email){
        return axios.post(Api.user.requestActivationLink, {
            email
        })
    }
}


export default RegistrationService
