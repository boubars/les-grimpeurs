import axios from 'axios'
import Api from '../api/api'

const PageService = {

    /**
     * @param  {name,lang} data
     */
    getPage : (data) => {
        return axios.get(
            Api.page.getPage, {params : data }

        )
    },
    /**
     * @param  Object params
     */
    getConcept : (params) => {
        return axios.get(
            Api.page.getConcept, {params}

        )
    }
}

export default PageService;