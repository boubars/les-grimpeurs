import axios  from 'axios'
import Api    from '../api/api'
import uniqid from 'uniqid'

const botServer = process.env.VUE_APP_BOT_SERVER

const BotService = {
    initConversation: function(oUser = false, message = false){
        let participant = {
            uid: uniqid('grimpeurs-bot-user--')
        }
        if(oUser){
            participant = {
                ...participant, 
                ...oUser
            }
        }
        return axios.post(Api.bot.createConversation, { 
            participant,
            message
        })
    },

    updateBotUser: function(nUser){
        localStorage.setItem('les-grimpeurs-bot-user', JSON.stringify(nUser))
    },

    addMessage: function(params){
        return axios.post(Api.bot.addMessage, params)   
    },

    getConversationData: function(params){
        return axios.get(Api.bot.getConversation, { params })   
    },
    /** granted for admin only */
    getAllConversation: function(params){
        return axios.get(Api.bot.allConversation, { params })   
    },

    getAdminUserData: function(){
        return axios.get(Api.bot.adminUserData)
    },

    /** granted for admin only */
    deleteConversation: function(cid){
        return axios.delete(Api.bot.deleteConversation(cid))   
    }
}

export default BotService