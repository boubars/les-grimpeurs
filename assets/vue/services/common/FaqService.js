import axios from 'axios'
import Api from '../api/api'

const FaqService = {
 getThematicList : (data) => {
        return axios.get( Api.common.faqThematicList)
    }
}

export default FaqService
