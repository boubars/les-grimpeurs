import axios from 'axios'
import Api from '../api/api'

const AssociationCauseService = {
 getList : (data) => {
        return axios.get( Api.common.AssociationCauseList)
    }
}

export default AssociationCauseService
