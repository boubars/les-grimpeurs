import axios from 'axios'
import Api from '../api/api'

const GradeService = {
 getList : (data) => {
        return axios.get( Api.common.gradeList)
    }
}

export default GradeService
