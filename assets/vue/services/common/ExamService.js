import axios from 'axios'
import Api from '../api/api'

const ExamService = {
 getList : (data) => {
        return axios.get( Api.common.ExamList)
    }
}

export default ExamService
