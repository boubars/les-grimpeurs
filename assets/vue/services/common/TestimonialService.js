import axios from 'axios'
import Api from '../api/api'

const TestimonialService = {
    TestimonialStudent : () => {
        return axios.get( Api.common.testimonialStudent)
    },
    TestimonialUniversity : () => {
        return axios.get( Api.common.testimonialUniversity)
    },
    TestimonialOrganization : () => {
        return axios.get( Api.common.testimonialOrganization)
    }
}

export default TestimonialService