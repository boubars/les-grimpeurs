import axios from 'axios'
import Api from '../api/api'

const LanguageLevelService = {
 getList : (data) => {
        return axios.get( Api.common.languageLevel)
    }
}

export default LanguageLevelService