import axios from 'axios'
import Api from '../api/api'

const ContactService = {

    /**
     * @param  {fullname,phone,eamil,subject,messsage} data
     */
    SubmitContact : (data) => {
        return axios.post(
            Api.common.SubmitContact, data

        )
    }
}

export default ContactService;