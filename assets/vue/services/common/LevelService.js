import axios from 'axios'
import Api from '../api/api'

const LevelService = {
 getList : (data) => {
        return axios.get( Api.common.levelList)
    }
}

export default LevelService