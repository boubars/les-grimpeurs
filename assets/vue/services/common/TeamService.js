
import axios from 'axios'
import Api from '../api/api'

const TeamService = {
    getList : () => {
        return axios.get( Api.common.teamList)
    },
    
}

export default TeamService