import axios from 'axios'
import Api from '../api/api'

const PreferenceService = {
    getParam : (data) => {
        return axios.get(Api.common.getPreference, {
            params: {
                ...data
            }
        })
    },
    getAdminMenu: function(locale = 'en'){
        return axios.get(Api.common.menuAdmin, {
            params: {
                locale
            }
        })
    }
}

export default PreferenceService