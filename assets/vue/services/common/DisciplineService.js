import axios from 'axios'
import Api from '../api/api'

const DisciplineService = {
    getList : (data) => {
        return axios.get( Api.common.disciplineList)
    },
    addDiscipline:(data) => {
        return axios.post(Api.common.disciplineAdd, {data})
    }


}

export default DisciplineService