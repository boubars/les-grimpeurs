import axios from 'axios'
import Api from '../api/api'

const StatisticalService = {
    statisticalHome : () => {
        return axios.get( Api.common.statistical)
    }
}

export default StatisticalService