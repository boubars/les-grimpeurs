import axios from 'axios'
import Api from '../api/api'

export default {
/**
 * 
 * @param {String} type - Organization type 
 * @param {Integer} organization  - organization id
 */
 getSocialNetwork : (organization) => {
        return axios.get( Api.common.getSocialNetwork , {
          params : {
            organization
          }
        })
    }
}