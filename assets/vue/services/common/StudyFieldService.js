import axios from 'axios'
import Api from '../api/api'

const StudyFieldService = {
 getList : (context = 'category', category = false ) => {
        category = category === "false" ? 0 : category
        return axios.get( Api.common.studyFieldList, {
            params : { context , category }
        })
    }
}

export default StudyFieldService