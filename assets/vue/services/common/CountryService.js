import axios from 'axios'
import Api from '../api/api'

const CountryService = {
    getList : (continents = [] ) => {
        return axios.get( Api.common.countryList, {
          params : {
            continents
          }
        })
    },
    getStat: function(id){
      return axios.get(Api.common.countryStat(id))
    },
    getUserList : function() {
        return axios.get( Api.common.userCountryList)
    },
    getCountryId(countryName){

      let countries = {
        en :  Object.entries( require('@/lang/translations/country/en.json') ),
        fr :  Object.entries( require('@/lang/translations/country/fr.json') ) 
      }
      
      /** translate countryName to english */
      for (let index = 0; index < countries.en.length; index++){
        const countryEn = countries.en[index]
        const countryFr = countries.fr[index]
        if(countryName == countryEn[1] || countryName == countryFr[1]){
          return countryEn[0] // retun id 
          break
        } 
      }

      return false
    }
}

export default CountryService