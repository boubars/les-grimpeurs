import axios from 'axios'
import Api from '../api/api'

const OrganizationTypeService = {
 getList : (data) => {
        return axios.get( Api.common.organizationTypeList)
    }
}

export default OrganizationTypeService
