import axios from 'axios'
import Api from '../api/api'

const DistinctionService = {
 getList : (data) => {
        return axios.get( Api.common.DistinctionList)
    }
}

export default DistinctionService
