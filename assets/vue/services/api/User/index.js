const base_url = "/api"
export default {
    profil                : `${base_url}/user/me`,
    recoverPassword       : `${base_url}/recover/password`,
    updatePassword        : `${base_url}/update/password`,
    confirmbycode         : `${base_url}/confirmationbycode`,
    resendCodeActivation  : `${base_url}/activation/code/resend`, 
    requestActivationLink : `${base_url}/activation/link/resend` 
}