const base_url = "/api/bot"

const Bot = {
    createConversation : `${base_url}/conversation/new`,
    getConversation    : `${base_url}/conversation`,
    deleteConversation : function(cid){ return `${base_url}/conversation/${cid}/delete` },
    allConversation    : `${base_url}/conversation/all`,
    addMessage         : `${base_url}/message/new`,
    adminUserData      : `${base_url}/participant/admin`,
}
export default Bot