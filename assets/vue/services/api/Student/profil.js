const base_url = "/api"
export default {
    updateStudentAvatar       : `${base_url}/student/profil/avatar`,
    updateStudentInfos        : `${base_url}/student/profil/infos`,
    updateStudentLangue       : `${base_url}/student/profil/language/update`,
    updateStudentFormation    : `${base_url}/student/profil/formation/update`,
    updateStudentExperience   : `${base_url}/student/profil/experience/update`,
    updateStudentPublication  : `${base_url}/student/profil/publication/update`,
    updateStudentActivity     : `${base_url}/student/profil/extra-activity/update`,
    updateStudentAssociation  : `${base_url}/student/profil/association/update`,
    updateStudentDistinction  : `${base_url}/student/profil/distinction/update`,
    updateStudentExam         : `${base_url}/student/profil/exam/update`,
    removeStudentFormation    : `${base_url}/student/profil/formation/remove`,
    removeStudentExperience   : `${base_url}/student/profil/experience/remove`,
    removeStudentPublication  : `${base_url}/student/profil/publication/remove`,
    removeStudentActivity     : `${base_url}/student/profil/extra-activity/remove`,
    removeStudentAssociation  : `${base_url}/student/profil/association/remove`,
    removeStudentDistinction  : `${base_url}/student/profil/distinction/remove`,
    removeStudentExam         : `${base_url}/student/profil/exam/remove`,
    removeStudentLanguage     : `${base_url}/student/profil/language/remove`,
    updateSearchPreference    : `${base_url}/student/profil/preference/edit`,
    downloadStudentCv         : function(id, locale='fr'){
        return `/${locale}/student/${id}/download/cv`
    },
    getCurrentPosition        : `${base_url}/student/profil/position`
}