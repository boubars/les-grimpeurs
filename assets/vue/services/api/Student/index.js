const base_url = "/api"
export default {
    profil             : require('./profil.js').default,
    registration       : `${base_url}/student/register`,
    find               : `${base_url}/student/find`,
    tag                : `${base_url}/student/`,
    applicationList    : `${base_url}/student/application/list`,
    ducument_add       : `${base_url}/student/document/add/new/`,
    ducument_list      : `${base_url}/student/document/list`,
    ducument_remove    : `${base_url}/student/document/delete/`,
    ducument_download    : `${base_url}/student/document/download`,
}