const base_url = "/api"
export default {
    getPage     : `${base_url}/page`,
    getAllPage  : `${base_url}/page/all`,
    updatePage  : `${base_url}/page/update`,
    deletePage  : `${base_url}/page/delete`,
    getConcept  : `${base_url}/page/concept`,
}