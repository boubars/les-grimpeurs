const base_url = "/api"
export default {
    getFondationInfos    : `${base_url}/organization/profil/infos`,
    updateFondationName  : `${base_url}/organization/profil/name`,
    updateFondationLogo  : `${base_url}/organization/profil/logo`,
    updateFondationAvatar  : `${base_url}/organization/profil/avatar`,
    updateFondationCover  : `${base_url}/organization/profil/cover`,
    updateFondationInfos : `${base_url}/organization/profil/infos`,
    updateGalleryPhoto   : function(id){
        return`${base_url}/organization/profil/gallery/photo/${id}`
    },
    updateGalleryVideo   : function(id){
        return `${base_url}/organization/profil/gallery/video/${id}`
    },
    getList              : `${base_url}/organization/list`,
}
