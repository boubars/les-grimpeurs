const Api = {
    common       : require('./Common/index').default ,
    organization : require("./Organization/index").default,
    page         : require("./Page/index").default,
    user         : require("./User/index").default,
    student      : require("./Student/index").default,
    scholarship  : require("./Scholarship/index").default,
    university   : require("./University/index").default,
    todo         : require("./Todo/index").default,
    post         : require("./Post/index").default,
    application  : require("./Application/index").default,
    bot          : require("./Bot/index").default,

}
export default Api