const base_url = "/api/user/todo"
export default {
    add    : `${base_url}/add`,
    edit   : `${base_url}/edit`,
    remove : `${base_url}/remove`,
    list   : `${base_url}/list`
}