const base_url = "/api"
export default {
    getPost       : `${base_url}/post`,
    downloadPost  : function(id){
        return `${base_url}/post/${id}/download`
    }
}