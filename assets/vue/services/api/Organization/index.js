const base_url = "/api"
export default {
    profil            : require('./profil.js').default,
    registration      : `${base_url}/organization/register`,
    applicationList   : `${base_url}/organization/application/list`,
}