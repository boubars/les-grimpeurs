const base_url = "/api"
export default {
    getOrganizationInfos      :  function(id){
        return `${base_url}/organization/profil/${id}`
    },
    updateOrganizationName    : function(id) {
        if (id) {
            return `${base_url}/organization/profil/name/${id}`
        }
        return `${base_url}/organization/profil/name`
    },
    updateOrganizationLogo    : function(id) {
        if (id) {
            return `${base_url}/organization/profil/logo/${id}`
        }
        return `${base_url}/organization/profil/logo`
    },
    getOrganizationLogo       : function(id) {
        return `${base_url}/organization/profil/logo/${id}`
    },
    getOrganizationContact    : function(id) {
        return `${base_url}/organization/profil/contact/${id}`
    },
    updateOrganizationAvatar  : function(id) {
        return `${base_url}/organization/profil/avatar/${id}`
    },
    updateOrganizationCover   : function(id){
        return`${base_url}/organization/profil/cover/${id}`
    },
    updateOrganizationInfos   : function(id){
        return `${base_url}/organization/profil/infos/${id}`
    },
    updateHiddenFields        : function(id){
        return `${base_url}/organization/profil/hidden-fields/${id}`
    },
    updateGalleryPhoto        : function(id){
        return `${base_url}/organization/profil/gallery/photo/${id}`
    },
    getOrganizationGallery    : function(id){
        return `${base_url}/organization/profil/gallery/${id}`
    },
    updateGalleryVideo        : function(id){
        return `${base_url}/organization/profil/gallery/video/${id}`
    },
    getList                   : `${base_url}/organization`,
    getOneBySlug              :  function(slug){
        return `${base_url}/organization/${slug}` 
    },
    getRelatedScholarship     : `${base_url}/organization/myscholarships`,
    myOrganization     : `${base_url}/contact/my-organization`,
    downloadScholarshipStat   : `${base_url}/organization/download/stat`
}