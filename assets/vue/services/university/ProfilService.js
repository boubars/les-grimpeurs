import axios from 'axios'
import Api from '../api/api.js'

const ProfilService = { 
    getList : function(){
        return axios.get(Api.university.profil.getList)
    }
}


export default ProfilService
