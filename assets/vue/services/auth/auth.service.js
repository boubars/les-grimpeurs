import axios from 'axios'
import Api from '../api/api'

class AuthService {

  login(data) {
    return axios
      .post( Api.common.login_check, data)
      .then(response => {
        if (response.data.user) {
          localStorage.setItem('les-grimpeurs-user',   JSON.stringify(response.data.user));
        }
        return response.data;
      });
  }

  logout() {
    return axios.get(Api.common.logout)
    .then(response => {
        localStorage.removeItem('les-grimpeurs-user');
        document.location.href = "/"
    })
  }

  getCodeByEmail(data) {
    return axios.post(
         Api.common.login_check, data

      )
  }

  activateAccount(data) {
    return axios.post(
         Api.common.login_check, data
      )
  }

  checkUserToken(){
    return axios.get(
        Api.common.token_check
    )
  }

}

export default new AuthService();