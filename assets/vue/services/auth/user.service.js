import axios from 'axios'
import Api from '../api/api'

const UserService = {
    recoverPassword: function(email){
        return axios.post(Api.user.recoverPassword , {
            email
        })
    },
    updatePassword: function(password){
        return axios.post(Api.user.updatePassword , {
            password
        })
    },
    getProfil : function(){
        return axios.get(Api.user.profil)
    },
    confirmbycode : function(data) {
        return axios.post(Api.user.confirmbycode , data)
    },
    resendCodeActivation : function(data){
        return axios.post(Api.user.resendCodeActivation , data)  
    }
}

export default UserService;