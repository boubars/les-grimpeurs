import Vue from 'vue'
import App from '@/App.vue'

/**
 * Import styles
 */
import "@/assets/css/app.css"

/** Import global style css */
import "@/assets/css/app.css"
import "@/assets/css/interne.css"
import "@/assets/css/responsive.css"

/** Import Routes data */
import Routes from '@/routes/routes.js'

/** Config Internationalization */
import VueI18n from 'vue-i18n'
import messages from '@/lang'

/** import axios */
import axios from 'axios'
import VueMaterial from 'vue-material'
//import 'vue-material/dist/vue-material.min.css'
//import 'vue-material/dist/theme/default.css'
/*** Import global layout */
import Default         from '@/layouts/Default.vue'
import NoSidebar       from '@/layouts/NoSidebar.vue'
import Splash          from '@/components/Splash.vue'
import NewFeatureModal from "@/components/common/NewFeatureModal.vue"

/** store global */
import store from "@/store"

/*** Global components */
import DefaultButton from "@/components/common/DefaultButton.vue"
import DateSelect from "@/components/common/DateSelect.vue"
import Avatar from "@/components/common/Avatar.vue"

/**  */
//import vueCountryRegionSelect from 'vue-country-region-select'

/** Vue video url  */
import VueVideo from "@tryangled/vue-video"


import {Checkbox, Radio} from 'vue-checkbox-radio'
 
/*** Modal component */
import Modal from "@/components/Modal/Modal.vue"

/** vue share btn */
import VueSocialSharing from 'vue-social-sharing'


Vue.use(VueSocialSharing)

Vue.use(VueVideo)


//Vue.use(vueCountryRegionSelect)

Vue.use(VueMaterial)
Vue.use(VueI18n)

Vue.component('checkbox', Checkbox)
Vue.component('radio', Radio)

Vue.component('checkbox', Checkbox)
Vue.component('radio', Radio)

Vue.component('Modal', Modal)
Vue.component('NewFeatureModal', NewFeatureModal)

//register jw pagination component globally
import JwPagination from 'jw-vue-pagination'
Vue.component('jw-pagination', JwPagination)



/**
 * import vuejsDialoge 
 * use for confirm action with button
 */

import VuejsDialog from 'vuejs-dialog'
 
// include the default style
import 'vuejs-dialog/dist/vuejs-dialog.min.css'

// Tell Vue to install the plugin.
Vue.use(VuejsDialog)

import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';

Vue.use(VueToast);

//import filter date
import VueFilterDateFormat from '@vuejs-community/vue-filter-date-format'
import VueFilterDateParse from 'vue-filter-date-parse'
// import vue-ellipse-progress
import VueEllipseProgress from 'vue-ellipse-progress';
Vue.use(VueEllipseProgress);


import VueCompositionAPI from '@vue/composition-api'

Vue.use(VueCompositionAPI)


import NumberFilters from "@/services/filters/NumberFilters.js"
import TextFilters from "@/services/filters/TextFilters.js"


Vue.use(VueFilterDateParse)
Vue.use(VueFilterDateFormat)


import LangFlag from "@/components/common/LangFlag/LangFLag.vue"
Vue.component('lang-flag', LangFlag)

/*** tooltip component */
import VTooltip from 'v-tooltip'
Vue.use(VTooltip)

export const i18n = new VueI18n({
    locale: store.state.lang,
    fallbackLocale: 'en',
    messages
})

/*** register global filter */
Vue.filter('toCurrency', NumberFilters.toCurrency)
Vue.filter('capitalize', TextFilters.capitalize)
Vue.filter('truncate', function (text, length, clamp){
  clamp = clamp || '...';
  var node = document.createElement('div');
  node.innerHTML = text;
  var content = node.textContent;
  return content.length > length ? content.slice(0, length) + clamp : content;
})

/***
 * Config global layouts
 */
Vue.component('default-layout', Default)
Vue.component('no-sidebar-layout', NoSidebar)
Vue.component('DefaultButton', DefaultButton)
Vue.component('DateSelect', DateSelect)
Vue.component('Avatar', Avatar)
Vue.component('Splash', Splash)

/*** masonry */
import VueMasonry from 'vue-masonry-css'
Vue.use(VueMasonry);

/*** Global function */
import AppMixin from "@/mixins/AppMixin.js"
Vue.mixin(AppMixin)

/*** Gmaps config */
import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
  load: {
    key: process.env.GMAP_KEY,
    libraries: 'places',
  }
});

/*** progress loading bar */
import NProgress from 'vue-nprogress'

const nprogress = new NProgress()
const options = {
  latencyThreshold: 100, 
  router: true, 
  http: true 
};

Vue.use(NProgress, options)

// axios request interceptor
axios.interceptors.request.use(
  config => {
    nprogress.start() // Set the loading progress bar (start...)
    return config
  },
  error => {
    return Promise.reject(error)
  }
)
// axios response interceptor
axios.interceptors.response.use(
  function(response) {
    nprogress.done() 
    // Set the loading progress bar (end...)
    return response
  },
  function(error) {
    return Promise.reject(error)
  }
)

/*** end nprogressbar loading */
/*** form making config */
import Element from 'element-ui'
Vue.use(Element, {
  i18n: (key, value) => i18n.t(key, value)
})
import '@/components/vue-form-making/styles/index.scss'
import FormMaking from '@/components/vue-form-making'

Vue.use(FormMaking, { lang: store.state.lang, i18n})


new Vue({
  nprogress, 
  template: '<App/>',
  components: { App },
  i18n,
  router : Routes ,
  store
}).$mount('#app')

//click outside popup

Vue.directive('click-outside', {
  bind: function (el, binding, vnode) {
    el.event = function (event) {
      if (!(el == event.target || el.contains(event.target) )) {
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener('click', el.event)
  },
  unbind: function (el) {
    document.body.removeEventListener('click', el.event)
  },
});