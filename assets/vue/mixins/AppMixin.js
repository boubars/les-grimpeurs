/** Global app mixins */

import DateHelper    from "@/services/helpers/DateHelper.js"
import StringHelpers from "@/services/helpers/StringHelpers.js"
import ObjectHelpers from "@/services/helpers/ObjectHelpers.js"

/** click outside directive */
import ClickOutside from 'vue-click-outside'

const AppMixin = {

    data: function(){
        return {
            newFeatureModal : false
        }
    },

    methods: {
        ...DateHelper,
        ...StringHelpers,
        ...ObjectHelpers,
        dispatchError(message){
            this.$toast.open({
                message,
                duration: 5000,
                type: 'error'
            });
        },
        
        openNewFeatureModal: function(){
            this.newFeatureModal = true
        },
        
        
        closeNewFeatureModal: function(){
            this.newFeatureModal = false
        },


        switchSidebarStatus: function(){
            this.$store.dispatch('sidebarstatus/switch')
        },

        getSitePreference: function(name, local = false){
            local = local ? local : this.local 
            return this.$store.getters['site/getParams'](name, local)
        }
    },

    computed: {
        local            : function(){ return this.$i18n.locale ?? 'fr' },
        currentUser      : function(){ return this.$store.state.user },
        profil           : function(){ return this.currentUser.profil },
        logged           : function(){ return this.currentUser.status.loggedIn }
    },

    directives: {
        ClickOutside
    }
}

export default AppMixin