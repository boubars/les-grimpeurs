import IconImages         from '@/services/helpers/IconImages.js'
import SocialNetwokLinks  from '@/view/organization/profil/parts/InfosCard/SocialNetwokLinks.vue'
const OrganizaitonInfosCardMixin = {
    props: {
        organization: {
           require: true
        }
    },

    components: {
        SocialNetwokLinks
    },


    data: function(){
        return {
            image         : { ...IconImages },
            hiddenFields  : []
        }
    },

    methods: {
        /**
         * Update default hidden fields state according to current profil
         * 
         */
         initHiddenFields: function(){
            this.hiddenFields = this.organization.hidden_fields ? [...this.organization.hidden_fields] : []
        }
    },

    watch: {
        organization: {
            deep: true,
            handler: function(){
                console.log('organizaiton, updated')
                this.initHiddenFields()
            }
        }
    },

    mounted: function(){
        this.initHiddenFields()
    },
}

export default OrganizaitonInfosCardMixin