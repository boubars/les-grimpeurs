import ScholarshipService from "@/services/scholarship/ScholarshipService"

export const ScholarshipMixin ={
    data: function() {
        return {
            scholarshipCount: 0,
            totalAmount: 0,
            totalUnit: 0,
            scholarships: [],
            organisations:[],
            waitResult: true,
            nb: 0,
            orderOptions: [
                {'sort': 'id', 'order': 'DESC', 'text':'Du plus récent au moins récent'},
                {'sort': 'id', 'order': 'ASC', 'text':'Du moins récent au plus récent'},
                {'sort': 'amount', 'order': 'DESC', 'text':'Montant le plus élevé au moins élevé'},
                {'sort': 'amount', 'order': 'ASC', 'text':'Montant le moins élevé au plus élévé'},
                {'sort': 'candidatureLimit', 'order': 'ASC', 'text':'Date candidature la moins proche à la plus proche'},
                {'sort': 'candidatureLimit', 'order': 'DESC', 'text':'Date de candidature la plus proche à la moins proche'}
            ],
            orderOption: {'sort': 'id', 'order': 'DESC', 'text':'Du plus récent au moins récent'},
            specificCountry: null,
            desiredCountries: []
        }
    },
    methods: {
        getSearchResult: function (context) {
            let criteria = {};
            if (this.level) {
                criteria.grantedLevels = this.level
            }
            if (this.disciplines) {
                criteria.disciplines = this.disciplines
            }
            if (this.country) {
                criteria.grantedCountries = this.country
            }
            let vi = this
            let searchContext = ['scholarship', 'pagination', 'student'];
            if (searchContext.includes(context)) {
                ScholarshipService.findByCriteria(
                    criteria,
                    {'sort': this.orderOption.sort, 'order': this.orderOption.order},
                    this.maxResult,
                    this.offset,
                    this.onlyFavorited,
                )
                    .then(function (response) {
                        vi.scholarships = response.data.scholarship;
                    })
                    .catch(function (error) {
                        console.log(error.message)
                    });
            }
            if (context != 'pagination') {
                this.getCountAndAmount(criteria, this.onlyFavorited);
            }
            console.log(this.orderOption)
        },
        refreshResult: function (searchParam) {

            this.level = searchParam.level;
            this.country = searchParam.country;
            this.disciplines = searchParam.disciplines;
            this.getSearchResult(searchParam.context);
        },
        getCountAndAmount: function (criteria, onlyFavorited) {
            let vi = this;
            this.waitResult = true
            ScholarshipService.countAll(criteria, onlyFavorited)
                .then(response => this.refreshCountAndAmount(response))
                .catch(function (error) {
                    console.log(error.message)
                });
        },
        refreshCountAndAmount: function (response) {
            this.waitResult = false
            if (!response.data.scholarships.count) {
                this.scholarshipCount = 0
            } else {
                this.scholarshipCount = parseInt(response.data.scholarships.count)
            }
            if (!response.data.scholarships.total) {
                this.totalAmount = 0
            } else {
                this.totalAmount = parseInt(response.data.scholarships.total)
            }
            
            if (!response.data.scholarships.totalUnit) {
                this.totalUnit = 0
            } else {
                this.totalUnit = parseInt(response.data.scholarships.totalUnit)
            }

            if (!response.data.scholarships.nb) {
                this.nb = 0
            } else {
                this.nb = parseInt(response.data.scholarships.nb)
            }
            this.paginationKey += 1

        },
        reorderResult: function () {
            this.getSearchResult()
        },
        getSpecificResult: function () {
            return new Promise((resolve, reject) => {
                ScholarshipService.findByCountry(
                    this.specificCountry,
                    {'sort': this.orderOption.sort, 'order': this.orderOption.order},
                    this.maxResult,
                    this.offset,
                    this.onlyFavorited
                )
                    .then(function (response) {
                        resolve(response)
                    })
                    .catch(function (error) {
                        reject(error)
                    });
            });
        },
        findByUniversity: function (university) {
            return new Promise((resolve, reject) => {
                ScholarshipService.findByUniversity(
                    university,
                    this.specificCountry,
                    {'sort': this.orderOption.sort, 'order': this.orderOption.order},
                    this.maxResult,
                    this.offset,
                    this.onlyFavorited
                ).then(function (response) {
                    resolve(response)
                })
                .catch(function (error) {
                    reject(error)
                });
            });
        },
        onResponseSuccess: function (response) {
            this.scholarships = response.data.scholarships;
            this.desiredCountries = response.data.desiredCountries;
            this.specificCountry = response.data.specificCountry;
            this.organisations = response.data.organisations;
        },
        onResponseFailure: function (error) {
            console.log(error.message)
        },

        getSpecificCount: function () {
            return new Promise((resolve, reject) => {
                let vi = this;
                ScholarshipService.totalByCountry(
                    this.specificCountry,
                    this.onlyFavorited
                )
                    .then(function (response) {
                        resolve(response)
                    })
                    .catch(function (error) {
                        reject(error)
                    });
            });
        }
    },
    watch: {
        orderOption: {
            deep: true,
            immediate: false,
            handler: function(val){
                if(val){
                    this.reorderResult()
                }
            }
        },
    }
}