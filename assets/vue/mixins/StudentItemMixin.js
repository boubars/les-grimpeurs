import ActionBtn from "@/components/Student/buttons/ActionBtn.vue"
import CircularProgressChart from "@/components/Charts/CircularProgress.vue"

const StudentItemMixin = {
    components : {
        ActionBtn,
        CircularProgressChart
    },
    props  : {
        student: {
            type : Object
        },
        context: {
            // accepted value ['candidat', 'student']
            default: 'student'
        }
    },
    data: () => {
        return {
            img_search: require("@/assets/img/icon-search-grey.svg").default,
            value: [20, 40],
            icon_favorite: require('@/assets/img/Ajout-favoris.svg').default,
            load: {'remove': false, 'favorite': false},
        };
    },
    methods: {
        /**
         * Calculate student age 
         * 
         * @param {String} dateString 
         * @returns integer
         */
        getAge: function (dateString)
        {
            var today = new Date();
            var birthDate = new Date(dateString);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate()))
            {
                age--;
            }
            return age;
        },

        /**
         * Calculate student profil completion
         * 
         * @param {object} student 
         * @returns {Number}
         */
        getCompletion(student) {
            /** calculate profile completion rate */
            let completedInfos = 0
            let rapport = -1

            let ignoreKeys = ['medias','profil_completion','date_of_birth','adress', 'search_preferences']
            Object.keys(student).map(function(key, index) {
                let value = student[key]
                if( !ignoreKeys.includes(key)){
                    rapport++
                    if((value !== null)){
                        if(Object.keys(value).length){
                            completedInfos++
                        }
                    }
                }
            });

            return Math.floor((completedInfos / rapport) * 100)
        },

        /**
         * Emit go-search event 
         * 
         * @returns void
         */
        reloadResult: function () {
            this.$emit('go-search');
        },

        /**
         * Emit openApplicaion detail event
         * 
         * @returns void
         */
        viewApplicationDetail: function(){
           this.$emit('openApplicationDetail')
        },

        /**
         * Encode student id and switch to profil public page
         * 
         * @returns void
         */
        viewProfil: function(){
            var encoder = require('int-encoder')
            let id = encoder.encode(this.student.id)
            this.$router.push({name: 'student_profil_public', params:{id}})
        }
    }
}

export default StudentItemMixin