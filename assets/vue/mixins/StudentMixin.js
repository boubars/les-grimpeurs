import StudentService from "@/services/student/StudentService.js"

const StudentMixin = {
    methods: {
        /**
         * Get Student last or current position 
         * @param {Integer} sid [student id] 
         * @param {Function} cb [function callback]
         * @return promise
         */
        getStudentPosition(sid, cb){
            let app = this
            StudentService.getStudentPosition(sid).then(
                (response) => {
                    if(response.data && response.data.post){
                        cb(response.data)
                    }
                }
            )
        }
    },
}

export default StudentMixin