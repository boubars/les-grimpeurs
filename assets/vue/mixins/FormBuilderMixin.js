

const FormBuilderMixin = {

    methods: {
        /**
         * Generated default form data with title only
         * @param {string|null} title 
         * @returns Array
         */
        generateNewSheetContent: function(title = false){
            return {
                "list": [ 
                    { 
                        "type": "title", 
                        "icon": "icon-wenzishezhi-", 
                        "options": { 
                            "defaultValue": title ? title : "Title 1", 
                            "customClass": "", 
                            "remoteFunc": "func_1629355095000_42077" 
                        }, 
                        "name": "Titre", "key": "1629355095000_42077", 
                        "model": "title_1629355095000_42077", 
                        "rules": [] 
                    } 
                ], 
                "config": { 
                    "labelWidth": 100, 
                    "labelPosition": "right", 
                    "size": "small" 
                } 
            }
        },

        /**
         * Get form title from content
         * @param {Object} data 
         * @return String|Boolean
         */
        getFormTitle: function(data){
            let array = data.content ? data.content.list : (data.list ? data.list : [])
          
            for (let index = 0; index < array.length; index++) {
                const value = array[index];
                if(value.type == 'title'){
                    return value.options.defaultValue
                }
            }
            return false
            
        }
    }

}

export default FormBuilderMixin