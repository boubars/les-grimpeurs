import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'

/**
 * Import styles
 */
import "./assets/css/app.css"

/** Import global style css */
import "./assets/css/app.css"
import "./assets/css/interne.css"
import "./assets/css/responsive.css"

/** Import Routes data */
import Routes from './routes/routes.js';

/** Config Internationalization */
import VueI18n from 'vue-i18n';
import messages from './lang';

/** import axios */
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
/*** Import global layout */
import Default from './layouts/Default.vue'
import NoSidebar from './layouts/NoSidebar.vue'
import Navbar from './components/Navbar/NavbarUser.vue'

/** store global */
import store from "./store"

/*** Global components */
import DefaultButton from "./components/common/DefaultButton.vue"
import DateSelect from "./components/common/DateSelect.vue"

/**  */
import vueCountryRegionSelect from 'vue-country-region-select'

/** Vue video url  */
import VueVideo from "@tryangled/vue-video"


import {Checkbox, Radio} from 'vue-checkbox-radio';
 
/*** Modal component */
import Modal from "./components/Modal/Modal.vue"

import VuePlaceAutocomplete from './components/vue-place-autocomplete/src/index.js';


Vue.use(VuePlaceAutocomplete)

Vue.use(VueVideo);


Vue.use(vueCountryRegionSelect)

Vue.use(VueMaterial)
Vue.use(VueI18n, axios, VueAxios)

Vue.component('checkbox', Checkbox);
Vue.component('radio', Radio);

Vue.component('checkbox', Checkbox);
Vue.component('radio', Radio);


Vue.component('Modal', Modal);



/**
 * import vuejsDialoge 
 * use for confirm action with button
 */

import VuejsDialog from 'vuejs-dialog'
 
// include the default style
import 'vuejs-dialog/dist/vuejs-dialog.min.css';

// Tell Vue to install the plugin.
Vue.use(VuejsDialog);

//import filter date
import VueFilterDateFormat from '@vuejs-community/vue-filter-date-format';
import VueFilterDateParse from 'vue-filter-date-parse'

Vue.use(VueFilterDateParse)
Vue.use(VueFilterDateFormat);

export const i18n = new VueI18n({
  locale: store.state.lang,
  fallbackLocale: 'en',
  messages
});
/***
 * Config global layouts
 */
Vue.component('default-layout', Default)
Vue.component('no-sidebar-layout', NoSidebar)
Vue.component('DefaultButton', DefaultButton)
Vue.component('DateSelect', DateSelect)
Vue.component('Navbar', Navbar)
Vue.component('SideBar', SideBar)



new Vue({ 
  template: '<App/>',
  components: { App },
  i18n,
  router : Routes ,
  store
}).$mount('#app');
