import './StudentRegistration.css'
import RegistrationService from '../../../services/student/RegistrationService.js'
import PhoneInput from '../../../components/common/PhoneInput.vue'
import facebookLogin from 'facebook-login-vuejs'
import LinkedinAuth  from '../../../services/LinkedIn/LinkedInAuth.js'

const redirect_uri = "http://localhost:8888/student"

export default {
    name : 'student_registration',
    components: {
      PhoneInput, facebookLogin
    },
    data : function() {
        return { 
            /**
             * FB Config
             */
            isConnected: false,
            personalID: '',
            FB        : false,
            LinkedIn  :false,
            passwordType : "password",
            
            user : {
                firstname   : "",
                lastname    : "",
                phone       : "",
                email       : "",
                password    : ""
            },
            waiting      : false
        }
    },
    methods: {
        checkChamp(user) {
          let firstname = (user.firstname !== '' && user.firstname !== null );
          let lastname = (user.lastname !== '' && user.lastname !== null );
          let phone = (user.phone !== '' && user.phone !== null && user.phone.length > 10 );
          let email = (user.email !== '' && user.email !== null );
          let password = (user.password !== '' && user.password !== null );
          return firstname && lastname && phone && email && password
        },
        registerSubmit(e) {
          e.preventDefault()
          if(this.checkChamp(this.user)) {

         
                if(!this.waiting) {
                let that = this 
                that.waiting = true 
               
                
                RegistrationService.register(this.user)
                .then(function (response) {
                    if(response.data.success){
                        //redirect login
                        window.location.href = '/authentification'
                    }else{
                      that.waiting        = false
                      that.dispatchError(that.$t("word.form." + response.data.error)) 
                    }
                })
                .catch(function (error) {
                  console.log(error)
                        that.waiting = false   
                        that.dispatchError(that.$t("word.form." + ((error.response.data.code == 500 ) ? 'internal_error' : error.message))) 

                })
            }
          }else {
            that.dispatchError(that.$t("word.form.field_incomplet")) 
          }
        },   
        /**FB Functions */
        onBtnFbClick(){
          let btn = this.$refs.btnFb
          btn.$el.querySelector('button').click()
        },
        getUserData() {
            let that = this 
            this.FB.api('/me', 'GET', { fields: 'id,name,email,birthday,first_name,last_name' },
              user => {
                that.user.firstname =  user.first_name
                that.user.lastname = user.last_name
                that.user.email = user.email
              }
            )
        },
        sdkLoaded(payload) {
          this.isConnected = payload.isConnected
          this.FB = payload.FB
          if (this.isConnected) this.getUserData()
        },
        onLogin() {
          this.isConnected = true
          this.getUserData()
        },
        onLogout() {
          this.isConnected = false;
        },
        signLinkedIn(){
          LinkedinAuth.onLoadSdk(redirect_uri)
        },
        openLogin() {
          document.getElementById('connexion').click();
          document.getElementsByClassName('close')[0].click();
        },
        switchPasswordType(){
          this.passwordType = (this.passwordType == "password") ? 'text' : 'password'
        }
    },
    mounted : function(){
      /** linkedin code fallback */
      let linkedInCode = this.$route.query.code ?? false
      if(linkedInCode !== false){
        let that = this
        LinkedinAuth.getUserInfos(linkedInCode, redirect_uri).then( (response) =>{
            let user = response.data ?? false
            if(user && user.id){
              that.user.firstname =  user.localizedFirstName
              that.user.lastname = user.localizedLastName
              that.user.email = user.email
            }

        })
       
    }
  }
}