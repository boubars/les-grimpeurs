import "./style.css"
import "./style-mobile.css"

import StudentAvatar from "./steeps/StudentAvatar.vue"
import StudentInfos from "./steeps/StudentInfos.vue"
import StudentInfosBase from "./steeps/StudentInfosBase.vue"
import StudentDesired from "./steeps/StudentDesired.vue"
import StudentParcours from "./steeps/StudentParcours.vue"
import StudentSocialNetwork from "./steeps/StudentSocialNetwork.vue"

export default {
  name       : 'ProfilForm',
  components : {
      StudentAvatar, 
      StudentInfos, 
      StudentInfosBase, 
      StudentDesired, 
      StudentParcours,
      StudentSocialNetwork
  },
  data : function(){
    return {
      currentSteep : 'none'
    }
  },
  props :  ['open'] ,
  watch: { 
    open: function(newVal, oldVal) { // watch it
      if(newVal)
        this.currentSteep = 'StudentAvatar'
    }
  },
  methods : {
    goTo(steepName){
      this.currentSteep = steepName 
      if(steepName == "None"){
        this.$emit('closeProfilEditor')
      }
    }
  },
}