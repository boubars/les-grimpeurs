import ScholarshipSearchForm from "./../../scholarship/search/form/ScholarshipSearchForm.vue"
import ScholarshipItemGrid from "./../../scholarship/item/ItemGrid.vue"
import { Splide, SplideSlide } from '@splidejs/vue-splide';
import '@splidejs/splide/dist/css/themes/splide-default.min.css';
import TodoList from "./../Todo/List.vue"
import Stats from "./parts/Stats.vue"
import CountryItem from "./parts/CountryItem.vue"
import CountryItemMobile from "./parts/CountryItemMobile.vue"
import FavorisSlide from "./parts/FavorisSlide.vue"
import SuggestionSlide from "./parts/SuggestionSlide.vue"
import DefaultButton from "@/components/common/DefaultButton.vue"

import VueSlickCarousel from 'vue-slick-carousel'
import 'vue-slick-carousel/dist/vue-slick-carousel.css'
import 'vue-slick-carousel/dist/vue-slick-carousel-theme.css'

export default {
    name : "StudentDashboard",
    components : {
        ScholarshipSearchForm , 
        Splide,
        SplideSlide,
        ScholarshipItemGrid,
        TodoList,
        Stats,
        CountryItem,
        CountryItemMobile,
        FavorisSlide,
        SuggestionSlide, 
        DefaultButton,
        VueSlickCarousel
    },
    data : function() {
        return {
          sliderOptionsMob: {
             /*type     : 'loop',
             focus    : 'center',
             pagination : true,
              */
              rewind : true,
              perPage: 1,
              gap    : '1rem',
            },
          image : {
              "logo_company" :  require('@/assets/img/Mybiosource.jpg').default,
          },
          favoriteCountries : false
        }
    },
    mounted : function(){

      if(this.profil.desired_countries)
        this.favoriteCountries = this.profil.desired_countries
    }
}