import './style.css'
import RegistrationService from '../../../services/organization/RegistrationService.js'
import PhoneInput  from '../../../components/common/PhoneInput.vue'
import PasswordInput  from '../../../components/common/PasswordInput.vue'
import facebookLogin from 'facebook-login-vuejs'
import LinkedinAuth  from '../../../services/LinkedIn/LinkedInAuth.js'

const redirect_uri = "http://localhost:8888/organization"

export default {
    name : 'organization_registration',
    components: {
      PhoneInput, facebookLogin , PasswordInput
    },
    data : function() {
        return { 
            /**
             * FB Config
             */
            isConnected: false,
            personalID: '',
            FB        : false,
            LinkedIn  :false,
            user : {
                firstname  : "",
                lastname    : "",
                phone       : "",
                email       : "",
                password    : ""
            },
            waiting      : false,
            registrationSuccess: false,
            params: false
        }
    },
    methods: {
        
        registerSubmit(e) {
            let that = this 
            that.waiting = true 
            e.preventDefault()
            RegistrationService.register(this.user)
            .then(function (response) {
                if(response.data.success){
                    that.registrationSuccess = true
                }else{
                  that.waiting        = false   
                  if(response.data.error){
                    that.dispatchError(that.$t("word.form." + response.data.error))
                  }else{
                    that.dispatchError(that.$t("word.form." + response.data))
                  }
                }
                that.waiting = false
            })
            .catch(function (error) {
                  that.waiting = false   
                  that.dispatchError(that.$t("word.form.internal_error"))
            })
        },
        openLogin() {
          this.$emit('login')
        },   
        /**FB Functions */
        onBtnFbClick(){
          let btn = this.$refs.btnFb
          btn.$el.querySelector('button').click()
        },
        getUserData() {
            let that = this 
            this.FB.api('/me', 'GET', { fields: 'id,name,email,birthday,first_name,last_name' },
              user => {
                that.user.firstname =  user.first_name
                that.user.lastname = user.last_name
                that.user.email = user.email
              }
            )
        },
        sdkLoaded(payload) {
          this.isConnected = payload.isConnected
          this.FB = payload.FB
          if (this.isConnected) this.getUserData()
        },
        onLogin() {
          this.isConnected = true
          this.getUserData()
        },
        onLogout() {
          this.isConnected = false;
        },
        signLinkedIn(){
          LinkedinAuth.onLoadSdk(redirect_uri)
        },
        requestActivationLink(){
          let that = this
          if(!that.user.email){
            return
          }
          that.waiting = true
          RegistrationService.requestActivationLink(that.user.email).then(
            (response) => {
              if(response.data.success){
                that.$toast.open({
                    message  : that.$t('modal.registration.success.message6'),
                    duration : 5000,
                    type     : 'info'
                });
              }
              that.waiting = false
            }
          )

        }
    },
    mounted : function(){
      this.params = {
        cgu_url: this.getSitePreference('cgu_url')
      }
      
      
      /** linkedin code fallback */
      let linkedInCode = this.$route.query.code ?? false
      if(linkedInCode !== false){
        let that = this
        try {
          LinkedinAuth.getUserInfos(linkedInCode, redirect_uri).then( (response) =>{
              let user = response.data ?? false
              if(user && user.id){
                that.user.firstname =  user.localizedFirstName
                that.user.lastname = user.localizedLastName
                that.user.email = user.email
              }
  
          })
          
        } catch (error) {
            console.log('error Linkedin API', error)
        }
    }
  }
}