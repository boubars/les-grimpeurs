import OrganizationEvolutionStat from "@/view/organization/dashboard/parts/OrganizationEvolutionStat/OrganizationEvolutionStat.vue"
import StudentLevelStat from '@/view/organization/dashboard/parts/StudentLevelStat.vue'
import StudentSexeStat from '@/view/organization/dashboard/parts/StudentSexeStat.vue'
import StudentApplicationRateStat from '@/view/organization/dashboard/parts/StudentApplicationRateStat.vue'
import StudentAgeStat from '@/view/organization/dashboard/parts/StudentAgeStat.vue'

import OrganizationCountryStat from '@/view/organization/dashboard/parts/OrganizationCountryStat/OrganizationCountryStat.vue'
import StudentDisciplineStat from '@/view/organization/dashboard/parts/StudentDisciplineStat.vue'
import StudentConnexionStat from '@/view/organization/dashboard/parts/StudentConnexionStat.vue'

import ImageHelper from "@/services/helpers/ImageHelper"
import ProfilService from '@/services/organization/ProfilService'
import DownloadBtn from "./DownloadBtn.vue"

import download from 'downloadjs'

export default {
    name: "OrganizationStatPreview",
    components:{
        OrganizationEvolutionStat,
        StudentLevelStat,
        StudentSexeStat,
        StudentApplicationRateStat,
        StudentAgeStat ,   

        OrganizationCountryStat,
        StudentDisciplineStat,
        DownloadBtn
    },
    
    data: function(){
        return {
            "logo"          : require("@/assets/img/logo_grimp.png").default,
            "downloadIcon"  : require("@/assets/img/Download.svg").default,
            scholarships    : [],
            scholarship     : false,
            downloaded      : false,
            period          : false,
            mobileDevice    : true
        }
    },

    methods: {
        downloadPDF: function(){
            let charts = document.querySelectorAll('.print-chart')
            let series = [] 
            let that   = this

            charts.forEach((element, index) => {
               
                let title = element.querySelector('.stat-title').innerText
                let size  = element.getAttribute("size")
                
                let chart = element.querySelector(".printable")
                chart     = chart ? chart : element.querySelector(".stat-container")
                chart     = chart ? chart : element.querySelector(".stat-content")

                ImageHelper.getNodeImage(chart, function(image){
                    series = [...series, {
                        title: title,
                        chart: image,
                        order: index,
                        size : size
                    }]
                    /*
                    var img = new Image();
                    img.src = image;
                    document.body.appendChild(img)
                    */
                    /*** submit on the last loop if last */
                    
                    if((charts.length) == series.length){
                        ProfilService.downloadScholarshipStat(series).then((response) => {
                            const content = response.headers['content-type']
                            download(response.data, "dashboard-organization.pdf", content)
                            that.downloaded = true
                        })
                         
                    }


                })
                
            });
        },
        getArrayIDS(array){
            let IDS = []
            for (let index = 0; index < array.length; index++) {
                const element = array[index];
                IDS = [...IDS, element.id]
                
            }
            return IDS
        }
    },

    watch: {
        downloaded : {
            handler: function(success){
                let that = this
                if(success){
                    setTimeout(function(){
                        that.$router.go(-1)
                    }, 1500)
                }
            }
        }
    },
    mounted:function(){
        this.mobileDevice = window.screen.width < 768
        
        let fakeScho      = {
            id: 17,
            name: "Bourse fake"
        }
        this.scholarships = this.$route.params.selectedScholarships ?? [fakeScho]
        this.period       = this.$route.params.period ?? "year"
        this.scholarship  = this.scholarships.length ? this.scholarships[0] : false

    },

    beforeCreate: function(){
        let meta = document.querySelector("meta[name='viewport']")
        meta.setAttribute('content', "width=device-width, initial-scale=0")
    },
    beforeDestroy:function(){
        let meta = document.querySelector("meta[name='viewport']")
        meta.setAttribute('content', "width=device-width, initial-scale=1.0")
    }

}