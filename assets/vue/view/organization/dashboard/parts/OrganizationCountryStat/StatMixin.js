export const StatMixin ={

    data: function () {
      return {
        legendes : [
            {color : "#16355F"},
            {color : "#3662BB"},
            {color : "#4B7EE3"},
            {color : "#6196FF"},
            {color : "#88AEF7"},
            {color : "#B2CCFF"},
            {color : "#CCDDFF"},
        ],
        
      }
    },
    methods: {
        getLegendeKey: function(index, data){
            return data.length < 7 ? index : round((index * 7) / data.length)
        },
        getLegende: function(index, data){
            return this.legendes[this.getLegendeKey(index, data)]
        },
        getLegendeColor: function(index, data){
            return (this.getLegende(index, data)).color
        },
    }
  }