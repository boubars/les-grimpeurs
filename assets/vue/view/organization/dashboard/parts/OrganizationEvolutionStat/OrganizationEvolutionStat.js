import SerieCategorySwitcher  from './SerieCategorySwitcher.vue'
import SerieDateSwitcher      from './SerieDateSwitcher.vue'
import SerieChart             from './SerieChart.vue'
import ScholarshipSelect      from '@/components/Scholarship/ScholarshipSelect.vue'
import ScholarshipService     from '@/services/scholarship/ScholarshipService.js'



export default {
    name: 'OrganizationEvolutionStat',
    components: {
        SerieChart,
        SerieCategorySwitcher,
        SerieDateSwitcher,
        ScholarshipSelect
    },
    props: {
        scholarships : {
            default: []
        },
        /*** for dahsboard preview pdf */
        selectedScholarships: {
            default: false
        }, 
        period : {
            default : false
        }
    },
    data: function(){
        return {
            selectedScholarship  : null,
            selectedCategory     : "interested",
            selectedDateFilter   : "years",

            evolutionSeriesData:{
                series:[{
                    name: "etudiants",
                    data: [] // dynamic content
                }],
                labels:[], //dynamic content
                pic   : {
                    today: 0,
                    year : 0
                }
            },
        
        }
    },
    methods : {
        openExportModal(){
            this.$emit("openExportModal");
        },
        
        updateSeriesData(e){
            let scholarships = this.selectedScholarships ? this.selectedScholarships : [this.selectedScholarship.id] 
            
            ScholarshipService.getStatEvolutionStat({
                scholarship: scholarships,
                serie: this.selectedCategory,
                date : this.selectedDateFilter
            }).then(
                (response) => {
                    let stat = response.data.stat
                    
                    this.evolutionSeriesData.series.data = Object.values(stat)
                    this.evolutionSeriesData.labels      = this.getFormatedLabel([...Object.keys(stat)], this.selectedDateFilter)

                    this.evolutionSeriesData.pic         = response.data.pic

                }
            )

        },
        getFormatedLabel(array, date){
            let labels = []
            if(date == 'years'){
                let res =  require(`@/lang/translations/word/${this.local}.json`)
                for (let index = 0; index < array.length; index++) {
                    const element = array[index];
                    labels = [...labels, res.months[element]] 
                }
                return labels
            }

            for (let index = 0; index < array.length; index++) {
                const element = array[index];
                labels = [...labels,  element < 10 ? "0"+element : element] 
            }
            return labels 
        }
    },
    watch: {
        selectedScholarship: {
            handler: function(val){
                this.$emit("scholarshipchange", val)
            }
        },
        scholarships: {
            deep: true,
            handler: function(val){
                if(val.length){
                    this.selectedScholarship = val[0]
                }
            }
        },
        period: function(period){
            if(period){
                this.selectedDateFilter = this.period
            }
        }
    }
}