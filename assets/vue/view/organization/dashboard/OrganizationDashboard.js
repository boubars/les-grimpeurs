import './OrganizationDashboard.css'
import DashboardHeader            from './parts/DashboardHeader.vue'
import StudentSexeStat            from './parts/StudentSexeStat.vue'
import StudentLevelStat           from './parts/StudentLevelStat.vue'
import OrganizationEvolutionStat  from './parts/OrganizationEvolutionStat/OrganizationEvolutionStat.vue'
import StudentApplicationRateStat from './parts/StudentApplicationRateStat.vue'
import StudentAgeStat             from './parts/StudentAgeStat.vue'
import OrganizationCountryStat    from './parts/OrganizationCountryStat/OrganizationCountryStat.vue'
import StudentDisciplineStat      from './parts/StudentDisciplineStat.vue'
import StudentConnexionStat       from './parts/StudentConnexionStat.vue'

import StudentSearchForm          from "./../../student/search/form/StudentSearchForm.vue"
import ProfilService              from '@/services/organization/ProfilService.js'
import ExportModal                from "./parts/ExportModal.vue"

export default {
    name : 'OrganizationDashboard',
    components : {
        DashboardHeader,
        StudentSexeStat,
        StudentLevelStat,
        StudentSearchForm,     
        OrganizationEvolutionStat,
        StudentApplicationRateStat,
        OrganizationCountryStat,
        StudentDisciplineStat,
        StudentConnexionStat,
        StudentAgeStat,
        ExportModal
    },
    data: function(){
        return {
            scholarship  : false,
            scholarships : [],
            showExportModal : false,
            fetching        : false,
        }
    },
    methods: {

        /**
         * Change global scholarship data state on select change
         * 
         * @param {Object} scholarship 
         */
        onScholarshipChange: function(scholarship){
           this.scholarship = scholarship
        },

        /**
         * Refresh scholarship list 
         * 
         * @return void
         */
        updateScholarshipList: function(){
            let that = this
            ProfilService.getRelatedScholarship().then(
                (response)=> {
                    that.scholarships = response.data.scholarships
                    that.fetching     = true
                }) 
        },

        /**
         * Open export modal form
         * 
         * @return void 
         */
        openExportModal: function(){
            this.showExportModal = true;
        },

        /**
         * Close export modal
         * 
         * @return void
         */
        closeExportModal: function(){
            this.showExportModal = false;
        },
        
        /**
         * Hanlde and search click redirect to student search page with selected parms
         * 
         * @param {Object} params
         * @return void 
         */
        handleStudentSearch: function(params){
            this.$router.push({'name':'student_search',  params: {
                searchParams : params }
            })
        }
    },
    mounted: function(){
        this.updateScholarshipList()
    }
}