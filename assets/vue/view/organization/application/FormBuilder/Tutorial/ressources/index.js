const data = [

    {
        title       : 'Ajouter un nouvelle page sur à formulaire',
        description : 'Pour ajouter un nouvelle page à votre formulaire , blabalabala',
        slider      : {
            type : 'gif',
            src  : require('./add_sheet.gif').default
        }
    },
    {
        title       : 'Grille de formulaire',
        description : 'Pour pourvoir ajouter des champs sur la meme ligne, On utilise le tableau et les grilles avec un diviseur de 24 (Ex: 2 colonne (12 - 12), 3 colonne (8 - 8 - 8)) ',
        slider      : {
            type : 'gif',
            src  : require('./grid.gif').default
        }
    },
    {
        title       : 'Ajouter un nouveau element basique',
        description : 'Pour ajouter un nouveau champs concernant les informations de l\'etudiant tel que nom, prenom, etc ...',
        slider      : {
            type : 'gif',
            src  : require('./basic.gif').default
        }
    },
    {
        title       : 'Ajouter un nouveau champ specifique',
        description : 'Pour ajouter un nouveau champ specifique pour votre bourse qui n\'est pas dans la liste des elements basic, il faut reduire la liste des elements basique ',
        slider      : {
            type : 'gif',
            src  : require('./advanced.gif').default
        }
    },


]


const Tutorials = {
    getList: function(){
        return data
    }
}
export default Tutorials