import ProfilService       from "@/services/organization/ProfilService.js" 
import ItemLarge           from "@/view/scholarship/item/ItemLarge.vue"
import ScholarshipItemGrid from "@/view/scholarship/item/ItemGrid.vue"

import "./style.css"

export default{
    name : 'MyScholarships',
    components : {
        ItemLarge, ScholarshipItemGrid
    },
    data: function(){
        return {
            presentationGrid : false,
            icon             : require("@/assets/img/find-icon.svg").default,
            searchFav        : null,
            scholarships     : [],
            orderOptions: [
                {'sort': 'id', 'order': 'DESC', 'text':'Date le plus reçent au plus ancien'},
                {'sort': 'id', 'order': 'ASC', 'text':'Date le plus ancien au plus recent'},
                {'sort': 'amount', 'order': 'DESC', 'text':'Montant le plus élevé au plus bas'},
                {'sort': 'amount', 'order': 'ASC', 'text':'Montant le plus bas au plus élévé'},
                {'sort': 'candidatureLimit', 'order': 'ASC', 'text':'Date candidature le plus proche au plus éloigné'},
                {'sort': 'candidatureLimit', 'order': 'DESC', 'text':'Date candidature le plus éloigné au plus élévé'}
            ],
            orderOption: {'sort': 'id', 'order': 'DESC', 'text':'Date le plus reçent au plus ancien'},
        }
    },
    methods: {
        launchSearch: function(){

        },
        refreshList: function(){
            let that   = this 
            ProfilService.getRelatedScholarship('details', this.orderOption).then(
                (response) => {
                    if(response.data.scholarships){
                        that.scholarships = response.data.scholarships
                    }
                }
            )
        },
        togglePresentation: function(){
            this.presentationGrid = !this.presentationGrid
        }
    },
    watch: {
        orderOption: {
            deep: true,
            handler: function(){
                this.refreshList()
            }
        }
    },
    mounted: function () {
        if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
         || window.screen.width <= 768){
            this.presentationGrid = true
        }
        
        this.refreshList()
    }
}