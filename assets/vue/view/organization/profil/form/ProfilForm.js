import "./style.css"
import OrganizationName from "./steeps/OrganizationName.vue"
import OrganizationLogo from "./steeps/OrganizationLogo.vue"
import OrganizationInfos from "./steeps/OrganizationInfos.vue"
import OrganizationAdresse from "./steeps/OrganizationAdresse.vue"
import OrganizationStat from "./steeps/OrganizationStat.vue"
import OrganizationSocialNetwork from "./steeps/OrganizationSocialNetwork.vue"
import OrganizationCover from "./steeps/OrganizationCover.vue"
import OrganizationGalleryPhoto from "./steeps/OrganizationGalleryPhoto.vue"
import OrganizationGalleryVideo from "./steeps/OrganizationGalleryVideo.vue"
import CongratuationModal from "./steeps/CongratuationModal.vue"

export default {
  name       : 'ProfilForm',
  components : {
      OrganizationName, 
      OrganizationLogo, 
      OrganizationInfos, 
      OrganizationAdresse, 
      OrganizationStat,
      OrganizationSocialNetwork,
      OrganizationCover,
      OrganizationGalleryPhoto,
      OrganizationGalleryVideo,
      CongratuationModal
  },
  data : function(){
    return {
       currentSteep : 'None',
       editing_organization: null
    }
  },
  props :  ['open', 'organization'] ,
  watch: { 
    open:function(newVal, oldVal) { // watch it
       this.editing_organization = this.organization;
      if(newVal){
        this.currentSteep = 'OrganizationName'
      }else{
        this.$emit('closeProfilEditor')
      }
    }
  },
  methods : {
    goTo(steepName){
        this.currentSteep = steepName
        if(steepName == "None"){
            this.$emit('closeProfilEditor')
        }
    },
      updateOrganization(organization) {
          this.$emit('update:organization', organization)
      }
  }
}