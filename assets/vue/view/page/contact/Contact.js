import ContactService      from '@/services/common/ContactService.js'
import { validationMixin } from 'vuelidate'
import { required, email } from 'vuelidate/lib/validators'

export default {
    name: 'contact',
    mixins: [validationMixin],
    data: () => ({
        form: {
            fullname: '',
            phone: '',
            email: '',
            subject: '',
            message: '',
            response: '',
        },
        sending: false,
        showSnackbar: false,
        position: 'center',
        duration: 4000,
        isInfinity: false,
        afterClick: false,
        MenuMobile: true,
        isShowMenuMobile: false,
     



    }),
    validations: {
        form: {
            fullname: {
                required,
            },
            email: {
                required,
                email
            },
            message: {
                required
            }
        }
    },
    methods: {
        hideMenuMobile() {
            this.isShowMenuMobile = false
        },
        getValidationClass(fieldName) {
            const field = this.$v.form[fieldName]

            if (field) {
                return {
                    'has-error': field.$invalid && field.$dirty
                }
            }
        },
        clearForm() {
            this.$v.$reset()
            this.form.fullname = null
            this.form.phone = null
            this.form.subject = null
            this.form.message = null
            this.form.email = null
            afterClick : false
        },
        submitContact() {
            let that = this
            that.sending = true

            ContactService.SubmitContact(this.form)
                .then(function (response) {

                    if (response.data.state) {
                        that.sending = false
                        that.form.response = "Votre message a étè envoyer avec succès"
                        that.showSnackbar = true
                        that.clearForm()
                    } else {
                        that.sending = false
                        that.form.response = "échec de l'envoi du message de raison : " + response.data.message
                        that.showSnackbar = true
                    }

                })
                .catch(function (error) {
                    that.sending = false
                    that.form.response = "échec de l'envoi du message de raison : " + error.message
                    that.showSnackbar = true
                })
            that.afterClick = false
        },
        validateForm() {
            this.afterClick = true

            this.$v.$touch()

            if (!this.$v.$invalid) {
                this.submitContact()
            }
        }
    },
    mounted : function(){
        console.log(this.currentUser == null)
    }


}



