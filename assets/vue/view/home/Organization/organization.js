import Testimonial              from '@/components/Home/Testimonial.vue'
import OrganizationRegistration from "@/view/organization/registration/OrganizationRegistration.vue"
import NoSidebar                from "@/layouts/NoSidebar.vue"
import StatisticalService       from "@/services/common/StatisticalService";

export default {
    name: 'home',
    components: {
        OrganizationRegistration, NoSidebar, Testimonial
    },      
     data : () => {
        return {
            linkedInCode     : false,
            statistical : {},
            stopAnnimated : false
        }
    } ,
    methods: {
        inscription() {
            document.getElementById('inscrire').click()
        },
        getStatistical() {
            let that = this
            StatisticalService.statisticalHome()
            .then(function(response) {
                that.statistical = response.data
               
            })
            .catch(function() {

            })
        }
    },
    mounted : function(){
        let appBody = document.querySelector('.app-body')
        appBody.classList.add('home-page')
        appBody.classList.add('home-page-organization')

           
        document.getElementById('footer').classList.add('up-footer')
        document.getElementById('haut-footer').classList.add('up-footer')
        document.getElementById('bas-footer').classList.add('down-footer')
        document.querySelector('#choix-langue-footer select').classList.add('down-footer')
         /** linkedin code fallback */
        this.linkedInCode = this.$route.query.code ?? false
        if(this.linkedInCode !== false){
            this.inscription()
        }

        this.getStatistical()
    },
    beforeDestroy:function(){
        let appBody = document.querySelector('.app-body')
        appBody.classList.remove('home-page')
        appBody.classList.remove('home-page-organization')
    }
}