import Testimonial              from '@/components/Home/Testimonial.vue'
import NoSidebar                from "@/layouts/NoSidebar.vue"
import StudentRegistration      from "@/view/student/registration/StudentRegistration.vue"
import OrganizationRegistration from "@/view/organization/registration/OrganizationRegistration.vue"
import ScholarshipSearchForm    from "@/view/scholarship/search/form/ScholarshipSearchForm.vue"
import ScholarshipResultForm    from "@/view/scholarship/search/form/ScholarshipResultForm.vue"
import {ScholarshipMixin}       from "@/mixins/ScholarshipMixin"
import StatisticalService       from "@/services/common/StatisticalService"
import Stats                    from "@/view/home/parts/Stats.vue"
import ChatBot                  from "@/view/home/parts/ChatBot.vue"
import './home.css'
export default {
    name: 'home',
    mixins:[ScholarshipMixin],
    components: {
        NoSidebar,
        StudentRegistration, 
        OrganizationRegistration,
        ScholarshipSearchForm,
        Testimonial,
        ScholarshipResultForm,
        Stats,
        ChatBot
    },
    data : () => {
        return {
            linkedInCode     : false,
            scholarshipForm : 'search',
            statistical : {}
        }
    },
    methods: {

        refreshResult(searchParam){
            this.level = searchParam.level;
            this.country = searchParam.country;
            this.disciplines = searchParam.disciplines;
            this.getSearchResult(true);
            this.scholarshipForm = 'result';
        },
        getStatistical() {
            let that = this
            StatisticalService.statisticalHome()
            .then(function(response) {
               that.statistical = response.data
            })
            .catch(function() {
      
            })
        },
        inscription() {
            document.getElementById('inscrire').click()
        }
    },
    mounted() {
        this.getStatistical()
        document.getElementsByClassName('app-body')[0].classList.add('home-page')
    },
    beforeDestroy() {
        document.getElementsByClassName('app-body')[0].classList.remove('home-page')
    }
}
