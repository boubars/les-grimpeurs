import GMap from "@/components/Gmaps/GMap.vue"
import ScholarshipMapsFilter from "./ScholarshipMapsFilter.vue"
import ScholarshipService from '../../../services/scholarship/ScholarshipService';

export default 
{
    name: 'ScholarshipMapsModal',
    
    components: {
        GMap,
        ScholarshipMapsFilter
    },
    data:function(){
        return {
            filter:{
                discipline     : null,
                allDiscipline  : [],
                nationality    : null,
                allNationality : [],
                country        : null,
                allCountry     : [], 
                name           : null,
            },
            scholarships: [],
            allScholarships   : false,
            selectedLocation  : false,
            worldAgregate     : false,
            criteriaAgregate  : false
        }
    },
    methods : {

        updateWorldAgregate(){
            let that = this 
            ScholarshipService.countAll({}, false).then(
                (response) => {
                    that.worldAgregate =  {
                        amount : response.data.scholarships.totalUnit,
                        count  : response.data.scholarships.nb
                    }    
                }
            )
        },
        refreshScholarshipList: function(){
            let that = this
            let criteria = {}
            
            /*** Mapped discipline criteria */
            criteria.disciplines = this.filter.allDiscipline
            if (this.filter.discipline && this.filter.discipline.id > 0 ) {
                criteria.disciplines = [this.filter.discipline]
            }

            /*** Mapped countries criteria */
            criteria.grantedCountries = this.filter.allCountry
            if (this.filter.country && this.filter.country.id > 0) {
                criteria.grantedCountries = [this.filter.country]
            }

            ScholarshipService.findByCriteria(criteria, null, '', null, false).then(
              (response) => {
                that.scholarships = response.data.scholarship
                if(!that.allScholarships){
                  that.allScholarships = response.data.scholarship
                }
              }
            )

            ScholarshipService.countAll(criteria, false).then(
                (response) => {
                    that.criteriaAgregate = {
                        amount : response.data.scholarships.totalUnit,
                        count  : response.data.scholarships.nb
                    }
                }
            )
        },

        onSelectedSuggestion: function(scholarship){
            this.selectedLocation   = scholarship.related_organization.adresse_location ? scholarship.related_organization.adresse_location : scholarship.origin_country.location
        }

    },
    watch: {
        filter: {
            deep: true,
            handler: function(val){
              this.refreshScholarshipList()
            }
        }
    },
    mounted: function(){
        this.updateWorldAgregate()

        this.refreshScholarshipList()
    }
}