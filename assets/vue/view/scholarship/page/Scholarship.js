
import ScholarshipService from "@/services/scholarship/ScholarshipService.js"
import Breadcrumb         from "@/components/common/Breadcrumb.vue"
import ScholarshipContent from "./ScholarshipContent.vue"

export default {
  name : 'Scholarship',
  components : {
    Breadcrumb,
    ScholarshipContent
  },
  data : function(){
    return {
      scholarship : {},
      fetching : false,
      breadcrumbs: [
        { 
          text: this.$t('word.scholarship'), 
          link: "#"
        },
        { 
          text  : this.$t('scholarship.search_scholarship'),
          route : {'name' : 'scholarship_search' }, 
          link  : "/user/scholarship"
        },
        { 
          text: this.$t('word.detail'), 
          link: "#"
        }
      ]
    }
  },
  methods : {

    getRequestedScholarship(){
      let that = this
      let slug = that.$route.params.slug
      ScholarshipService.findOneBySlug(slug).then(
        (response) => {
          if(response.data.scholarship && response.data.scholarship.id){
            that.scholarship = response.data.scholarship
            that.fetching = true
          }else{
            that.$router.push({name:"error_404"})
          }
      }).catch(
          (error) => {
            that.$router.push("/internal-error")
      })
    },

    emitSeenBy(){
      ScholarshipService.seenByUser(this.scholarship.id)
    }

  },

  watch:{
    fetching:{
      handler: function(yes){
        if(yes && this.logged){
          this.emitSeenBy()
        }
      }
    }
  },

  mounted: function(){
    this.getRequestedScholarship()

  }
}