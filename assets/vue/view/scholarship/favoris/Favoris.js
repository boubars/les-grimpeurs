import ItemLarge           from "@/view/scholarship/item/ItemLarge.vue"
import ScholarshipItemGrid from "@/view/scholarship/item/ItemGrid.vue"
import {ScholarshipMixin}  from "@/mixins/ScholarshipMixin.js"
import AutoCompleteInput   from '@/components/Scholarship/AutoCompleteInput.vue'

import "./style.css"
const customLabels = {
    first: '',
    last: '',
    previous: '',
    next: ''
};
export default{
    name : 'ScholarshipSearch',
    
    mixins:[ScholarshipMixin],
    
    components : {
        ItemLarge, 
        ScholarshipItemGrid, 
        AutoCompleteInput
    },

    data: function(){
        return {
            maxResult: 10,
            offset:0,
            paginationKey : 0,
            presentationGrid: false,
            firstItemShown:0,
            lastItemShown: 0,
            onlyFavorited: true,
            customLabels,
            searchFavoris:[],
            icon       : require("@/assets/img/find-icon.svg").default,
            searchFav: null,
            cloneScholarships : false,
        }
    },
    
    mounted: function () {
        if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
         || window.screen.width <= 768){
            this.presentationGrid = true
        }
        this.getSearchResult('scholarship')
    },

    methods: {
        onChangePage: function(pageOfItems){
            if(pageOfItems.length > 0) {
                this.offset = pageOfItems[0];
                this.firstItemShown = pageOfItems[0] + 1;
                this.lastItemShown =  pageOfItems[pageOfItems.length - 1] + 1;
                this.getSearchResult('pagination');
            }
        },
        togglePresentation: function () {
            this.presentationGrid = !Boolean(this.presentationGrid)
        },

        launchSearch(scholarship){
            if(!this.cloneScholarships){
                this.cloneScholarships =  [...this.scholarships]
            }

            this.scholarships = [scholarship]   
        }
    },
    computed: {
        paginationItems : function(){
            //fake array for pagination
            return [...Array(this.nb).keys()]
        },
        pageSize : function(){
            return parseInt(this.maxResult)
        },
        
        searchData: function(){
            return this.cloneScholarships ? this.cloneScholarships : this.scholarships
        }
    },
}