import ScholarshipSearchForm from "./form/ScholarshipSearchForm.vue"
import ScholarshipSearchFormMobile from "./form/ScholarshipSearchFormMobile.vue"
import ItemLarge from "./../item/ItemLarge.vue"
import ScholarshipItemGrid from "./../item/ItemGrid.vue"
import RadioMobile from "@/components/common/RadioMobile.vue"
import Modal from "@/components/Modal/Modal.vue"

import {ScholarshipMixin} from "@/mixins/ScholarshipMixin.js";
import ProfilService from "@/services/university/ProfilService.js"
import NumberHelpers from "@/services/helpers/NumberHelpers"
import "./style-mobile.css"
const customLabels = {
    first: '',
    last: '',
    previous: '',
    next: ''
};
export default{
    name : 'ScholarshipSearch',
    mixins:[ScholarshipMixin],
    components : {
        ScholarshipSearchForm, ItemLarge, ScholarshipItemGrid, RadioMobile, ScholarshipSearchFormMobile, Modal
    },
    data: function(){
        return {
            mobileSearchInput: false,
            maxResult: 12,
            offset:0,
            disciplines:[],
            level:null,
            country:null,
            paginationKey : 0,
            presentationGrid: false,
            firstItemShown:0,
            lastItemShown: 0,
            onlyFavorited: false,
            isSpecific: false,
            activeClass: 'active',
            customLabels,
            defaultSearchParams:false,
            concernedUniversities : [],
            selectedUniversity: { id:0 , name:'Tous' },
            mobileSearchPopup: false,
        }
    },
    mounted: function () {
        let that = this
        if(this.isMobile()){
            this.presentationGrid = true
        }

        if(this.$route.params && this.$route.params.searchParams){
            /*** populate filter item */
            let params =  this.$route.params.searchParams
            this.disciplines = params.disciplines !== undefined ? params.disciplines : []
            this.country     = params.country     !== undefined ? params.country : null
            this.level       = params.level       !== undefined ? params.level : null

            this.defaultSearchParams = params
        }

        /** refresh concerned university */
        ProfilService.getList().then(
            (response) => {
                that.concernedUniversities = response.data.list
                that.concernedUniversities = [
                    { id:0 , name:'Tous' },
                    ...that.concernedUniversities
                ]
            }

        )

        this.getSearchResult('scholarship')
    },
    methods: {
        ...NumberHelpers, 
        isMobile: function(){
            return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || window.screen.width <= 768)      
        },
        showInput: function (showSearch){
            this.inputSearch = showSearch
        },
        onChangePage: function(pageOfItems){
            if(pageOfItems.length > 0) {
                this.offset = pageOfItems[0];
                this.firstItemShown = pageOfItems[0] + 1;
                this.lastItemShown =  pageOfItems[pageOfItems.length - 1] + 1;
                if(!this.isSpecific) {
                    this.getSearchResult('pagination');
                }
            }
        },
        togglePresentation: function () {
            this.presentationGrid = !Boolean(this.presentationGrid)
        },
        refreshResultOnToggleFavorite: function () {
            this.onlyFavorited = !this.onlyFavorited;
            if(!this.isSpecific){
                this.getSearchResult();
            }
            else {
                this.getSpecificResult()
                    .then(response => this.onResponseSuccess(response))
                    .catch(error => console.log(error))
                this.getSpecificCount()
                    .then(response => this.refreshCountAndAmount(response))
                    .catch(error => console.log(error))
            }
        },
        onToggleIsSpecific: function(){
            this.isSpecific = !this.isSpecific;
            if(this.isSpecific){
                this.getSpecificResult()
                    .then(response => this.onResponseSuccess(response))
                    .catch(error => console.log(error))
                this.getSpecificCount()
                     .then(response => this.refreshCountAndAmount(response))
                     .catch(error => console.log(error))
            }
            else {
                this.getSearchResult('scholarship')
            }
        },
        changeSpecificCountry:function (selectedCountry){
           if(this.specificCountry != selectedCountry) {
               this.specificCountry = selectedCountry;
               this.getSpecificResult()
                   .then(response => this.onResponseSuccess(response))
                   .catch(error => this.onResponseFailure(error))
                this.getSpecificCount()
                    .then(response => this.refreshCountAndAmount(response))
                    .catch(error => this.onResponseFailure(error))
           }
        },
        handleMobileSearchClick(){
            let btnSearch = document.getElementById("")
        },
        changeSelectedUniversity:function (){
            this.findByUniversity(this.selectedUniversity)
            .then(response => this.onResponseSuccess(response))
            .catch(error => this.onResponseFailure(error))
        },
        reorderResult: function () {
            this.getSearchResult()
        },
        reloadResult(data){
            this.refreshResult(data)
            this.defaultSearchParams = data
            if(this.isMobile()){
                this.mobileSearchInput = true
                this.mobileSearchPopup = false
            }
        },
        onMobileToggleIsSpecific(data){
            this.onToggleIsSpecific(data)
            this.mobileSearchInput = false
            this.mobileSearchPopup = false
        },
        remove(index){
            this.scholarships.splice(index, 1)
        },
    },
    computed: {
        paginationItems : function(){
            //fake array for pagination
            return [...Array(this.nb).keys()]
        },
        pageSize : function(){
            return parseInt(this.maxResult)
        },
    },
    filters: {
        'toCurrency': function (value, locale, currency) {
            if(locale === null || locale === undefined){
                locale = 'en-US'
            }
            if(currency === null || currency === undefined){
                currency = 'USD'
            }
            if (typeof value !== "number") {
                return value;
            }
            const formatter = new Intl.NumberFormat(locale, {
                style: 'currency',
                currency: currency,
                currencyDisplay: 'symbol',
                minimumFractionDigits: 0
            });
            return formatter.format(value);
        },
        'toFrenchDate': function (value) {
            const date = new Date(value)
            //const formatter = new Intl.DateTimeFormat('fr-FR');
            return date.toLocaleDateString("fr-FR", {day:"2-digit",month:"short", year:"numeric"});
        }
    },
    watch: {
        specificCountry: {
            deep: true,
            handler: function (val) {
                for(let i in this.desiredCountries){
                    if(this.desiredCountries[i].id == val.id){
                        this.desiredCountries[i].active = true;
                    }
                    else {
                        this.desiredCountries[i].active = false
                    }
                }
            }
        }
    }
}