import FileUpload  from 'vue-upload-component'
import ImageHelper from '@/services/helpers/ImageHelper'
import Cropper     from 'cropperjs'

export default {
    name: 'ScholarshipLogo',
    components : {
        FileUpload
    },
    props : {
        scholarship: {
            require : true
        }
    },
    data : function() {
        return {
            files: [],
            edit : false,
            cropper: false
        }
    },
    computed: {
        logoPreview: function(){
            
            if(this.scholarship.logo && this.scholarship.logo.base64){
                return this.scholarship.logo.base64
            }
            
            if(this.scholarship.logo && this.scholarship.logo.file){
                return this.scholarship.logo.file.url
            }

            if(typeof this.scholarship.logo == 'string'){
                return '/uploads/logo/'+this.scholarship.logo
            }

            return require('@/assets/img/image.svg').default
        }
    },
    methods: {
        /**
         * Validate scholarship logo file 
         * 
         * @returns void
         */
        validateLogo: function(){
            if(this.hasLogo()){
                this.$emit('goTo','ScholarshipCandidate')
                return
            }
            this.dispatchError(this.$t("scholarship.form.field_empty_error"))
        },

        /**
         * Handle logo file change
         * 
         * @param {Object} newFile 
         * @param {Object} oldFile 
         * @param {Callback} prevent 
         * @returns void
         */
        inputFile: function(newFile, oldFile, prevent){
            if (newFile && !oldFile) {
                this.$nextTick(function () {
                this.edit = true
                })
            }
            if (!newFile && oldFile) {
                this.edit = false
            }
        },

        /**
         * Check shcolarship input uploaded file
         * 
         * @param {Object} newFile 
         * @param {Object} oldFile 
         * @param {*} prevent 
         * @returns Mixed 
         */
        inputFilter: function(newFile, oldFile, prevent) {
            let that = this;
            if (newFile && !oldFile) {
                if (!/\.(jpg|jpeg|png)$/i.test(newFile.name)) {
                    this.dispatchError(that.$t("media.error.invalid_format"))
                    return prevent()
                }
            }
            let prevents = false
            let filetoCheck = newFile
            ImageHelper.getImageSize(filetoCheck, function(size){
                if(size.w < 250 || size.h < 250){
                    that.edit = false
                    that.dispatchError(that.$t("media.error.invalid_size"))  
                }
            })

            if (newFile && (!oldFile || newFile.file !== oldFile.file)) {
                newFile.url = ''
                let URL = window.URL || window.webkitURL
                if (URL && URL.createObjectURL) {
                    newFile.url = URL.createObjectURL(newFile.file)
                }
            }
            
        },

        /**
         * Save croped scholarship logo
         * 
         * @return void
         */
        editSave: function() {
            let that  = this 
            this.edit = false
            let oldFile = this.files[0]
            let binStr = atob(this.cropper.getCroppedCanvas().toDataURL(oldFile.type).split(',')[1])
            let arr = new Uint8Array(binStr.length)
            for (let i = 0; i < binStr.length; i++) {
                arr[i] = binStr.charCodeAt(i)
            }
            let file = new File([arr], oldFile.name, { type: oldFile.type })
            file.url = ''
            let URL = window.URL || window.webkitURL
            if (URL && URL.createObjectURL) {
                file.url = URL.createObjectURL(file)
            }

            let fileData = {
                file,
                type: file.type,
                size: file.size,
                active: true,
                base64: false

            }

            ImageHelper.ImgToBase64URL(file.url, function(base64Img){
                fileData.base64 = base64Img
                that.$refs.upload.update(oldFile.id, fileData)
                that.scholarship.logo = fileData
            })
            
        },
        
        /**
         * Check if current scholarhsip has a logo
         * @returns boolean
         */
        hasLogo: function(){
            return this.scholarship.logo ?? false
        },
    },

    watch: {
        edit(value) {
            if (value) {
                this.$nextTick(function () {
                if (!this.$refs.editImage) {
                    return
                }
                let cropper = new Cropper(this.$refs.editImage, {
                    aspectRatio: 1 / 1,
                    viewMode: 1,
                    minCropBoxWidth: 250,
                    minCropBoxHeight: 250,
                })
                this.cropper = cropper
                })
            } else {
                if (this.cropper) {
                this.cropper.destroy()
                this.cropper = false
                }
            }
        }
    }
}