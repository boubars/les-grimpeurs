import FileUpload from 'vue-upload-component'
    export default {
        name: 'ScholarshipGallery',
        components : {
            FileUpload
        },
        props : {
            scholarship: {
                require : true
            }
        },
        data : function() {
            return {
                message   : false,
                uploaded  : false,
                invalid   : false,
                defaultPdfIcon : require("@/assets/img/icon-pdf.png").default
            }
        },
        methods: {
            /**
             * Hanlde preview clicked button
             * 
             * @returs void 
             */
            preview: function(){
                if(this.scholarship.gallery.length){
                    this.$emit('goTo','ScholarshipPreview')
                }else{
                    invalid = true
                }
            },

            /**
             * check user uploaded file
             * 
             * @param {Object} newFile 
             * @param {Object} oldFile 
             * @param {Callback} prevent 
             * @returns 
             */
            inputFilter: function(newFile, oldFile, prevent) {
                if (newFile && !oldFile) {
                    if (!/\.(gif|jpg|jpeg|png|pdf)$/i.test(newFile.name)) {
                    let that = this;
                    this.$dialog.confirm(that.$t("media.error.invalid_format"), {
                            okText: that.$t('student.profil.steep1_info8'),
                            cancelText: that.$t("word.cancel"),
                        }).then(function (dialog) {
                            that.$refs.galeryPhotoLabel.click()
                            dialog.close()
                        }).catch(function (dialog) {
                            dialog.close()
                        });
                        return prevent()
                    }
                }
                if (newFile && (!oldFile || newFile.file !== oldFile.file)) {
                    newFile.url = ''
                    let URL = window.URL || window.webkitURL
                    if (URL && URL.createObjectURL) {
                        newFile.url = URL.createObjectURL(newFile.file)
                    }
                }   
            },

            /**
             * Initialize gallery data state
             * 
             * @returns void 
             */
            initGallery: function(){
                let gallery = this.scholarship.gallery
                if(!gallery){
                    return
                }

                /** On edit scholarship, rebuild gallery object */
                if(gallery.id && gallery.pictures && gallery.documents){
                    let pictures   = gallery.pictures
                    let documments = gallery.documents
                    this.scholarship.gallery = []

                    pictures.forEach(pic => {
                        let nPic = {
                            type :  'image/*',
                            url  :  '/uploads/picture/'+ pic
                        }
                        this.scholarship.gallery = [...this.scholarship.gallery, nPic]
                    })

                    documments.forEach(doc => {
                        let nDoc = {
                            type :  'application/pdf',
                            url  :  '/uploads/document/'+ nDoc
                        }
                        this.scholarship.gallery = [...this.scholarship.gallery, nDoc]
                    });
                }

            },

            /**
             * trig open file chooser event
             * 
             * @return void
             */
            openFileChooser: function(){
                this.$refs.galeryPhotoLabel.click()
            }
        },
        mounted: function(){
            this.initGallery()
        }
    }