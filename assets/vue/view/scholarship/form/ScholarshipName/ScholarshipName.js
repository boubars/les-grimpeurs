export default {
    name :'ScholarshipName',
    props:['scholarship'],
    data : function(){
        return {
            message : false
        }
    },
    methods : {
        validateScholarshipName(){
            if(!this.scholarship.name){
               this.dispatchError(this.$t("scholarship.form.field_empty_error"))
                return false
            }
            if(this.scholarship.name.length < 3){
                this.dispatchError(this.$t("scholarship.form.field_short_error"))
                return false
            }
            this.message = false
            this.$emit('goTo','ScholarshipLogo')

        }
    }

}