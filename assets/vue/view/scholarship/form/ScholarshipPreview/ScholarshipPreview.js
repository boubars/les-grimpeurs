import ScholarshipService from "@/services/scholarship/ScholarshipService.js"
import ImageHelper        from "@/services/helpers/ImageHelper.js"
import IconImages         from '@/services/helpers/IconImages'
import ScholarshipContent from '@/view/scholarship/page/ScholarshipContent.vue'
import Breadcrumb         from "@/components/common/Breadcrumb.vue"
export default {
    name  : "ScholarshipPreview",
    props : ["scholarship"],
    components:{
        ScholarshipContent,
        Breadcrumb
    },
    data : function(){
      return {
        loading : false,
        image : {
         ...IconImages
        },
        previewScholarship : false,
        breadcrumbs: [
            { 
              text: this.$t('word.scholarship'), 
              link: "#"
            },
            { 
              text: this.$t('scholarship.search_scholarship'), 
              link: "#"
            },
            { 
              text: this.$t('word.detail'), 
              link: "#"
            }
        ]
      }
    },
    methods : {
      saveScholarship(){
          let that = this 
          that.loading = true

          let formData = this.convertJsonToFormData(this.scholarship)

          this.scholarship.gallery.map(input => {
              formData.append('galery[]', input.file)
          })

          /** append adresse map */
          /** convert maps to blob */
          if(this.scholarship.contactAdresseMap){
            if(this.scholarship.contactAdresseMap instanceof Blob){
                let mapFile = ImageHelper.DataURIToBlob(this.scholarship.contactAdresseMap)
                formData.append('contactAdresseMap', mapFile,'map.png')
            }
          }
          
          ScholarshipService.saveScholarship(formData).then(
              (response) => {
                  if(response.data.success){
                    this.$store.dispatch("newscholarship/purge")
                      this.$router.push( {name : 'scholarship_page', params : {slug:response.data.slug}})
                  }
              }
          ).catch(
              (error) => {
                  that.$dialog.confirm("Une erreur s'est produite lors de l'ajout d'une bourse", {
                  okText: "Réessayer",
                  cancelText: that.$t("word.cancel"),
                  }).then(function (dialog) {
                      that.saveScholarship()
                      dialog.close()
                      that.loading = false
                  }).catch(function (dialog) {
                      dialog.close()
                      that.loading = false
                  });
              }
          )
                  
      },
      convertJsonToFormData(data) {
            /** remove array last item for advantage and  */
            
            if(data.attachments[data.attachments.length -1 ] == "")
              data.attachments.pop()
            
            if(data.advantages[data.advantages.length -1 ] == "")
              data.advantages.pop()
            
            if(data.selectionCriteria[data.selectionCriteria.length -1 ] == "")
              data.selectionCriteria.pop()


            const formData = new FormData()
            const entries = Object.entries(data) 

            for (let i = 0; i < entries.length; i++) {
                const arKey = entries[i][0]
                let arVal = entries[i][1]

                if (typeof arVal === 'boolean') {
                    arVal = arVal === true ? 1 : 0
                }

                if (Array.isArray(arVal)) {
                    if (arVal[0] instanceof Object) {
                        for (let j = 0; j < arVal.length; j++) {
                            if (arVal[j] instanceof Object) {
                            // if first element is not file, we know its not files array
                            for (const prop in arVal[j]) {
                                if (Object.prototype.hasOwnProperty.call(arVal[j], prop)) {
                                    formData.append(`${arKey}[${j}][${prop}]`, arVal[j][prop])
                                }
                            }
                            }
                        }
                        continue // we don't need to append current element now, as its elements already appended
                    } else {
                        arVal = JSON.stringify(arVal)
                    }
                }else if (arVal instanceof Object) {
                    Object.keys(arVal).map(function(key, index) {
                        let value = arVal[key] 
                        formData.append(`${arKey}[${key}]`, value)  
                    })
                    continue
                }

                if (arVal === null) {
                    continue
                }
                formData.append(arKey, arVal)
            }
            return formData
      },
    },
    mounted : function(){
        this.previewScholarship = this.camelCaseKeysToUnderscore({...this.scholarship})
    }
}