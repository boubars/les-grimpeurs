import FileUpload             from 'vue-upload-component'
import StudyFieldSelect       from "@/components/common/StudyFieldSelect.vue"
import DateSelect             from "@/components/common/DateSelect.vue"
import NationalityFieldSelect from "@/components/common/NationalitySelect.vue"
import ContinentSelect        from "@/components/common/ContinentSelect.vue"
import CountrySelect          from "@/components/common/CountrySelect.vue"
import DuplicateInput         from "@/components/common/DuplicateInput.vue"
import PhoneInput             from "@/components/common/PhoneInput.vue"
import { VueEditor }          from "vue2-editor";
import Multiselect            from "vue-multiselect";
import MoneyInput             from "@/components/common/MoneyInput.vue"
import BooleanInput           from "@/components/common/BooleanInput.vue"
import Helpers                from "@/services/helpers/Helpers.js"
import LevelSelect            from "@/components/common/LevelSelect.vue";
import ImageHelper            from "@/services/helpers/ImageHelper.js"
import SocialNetworkService   from "@/services/common/SocialNetworkService.js" 
import ProfilService          from "@/services/organization/ProfilService.js";
import { VueTelInput }        from 'vue-tel-input'
import AdresseInput           from "@/components/common/AdresseInput.vue"
import Cropper                from 'cropperjs'

import "vue-multiselect/dist/vue-multiselect.min.css";

export default {
    name: 'ScholarshipInfos',
    components : {
        PhoneInput, 
        FileUpload, 
        StudyFieldSelect , 
        DateSelect,
        NationalityFieldSelect,
        ContinentSelect,
        DuplicateInput,
        VueEditor,
        Multiselect,
        MoneyInput,
        CountrySelect,
        BooleanInput,
        LevelSelect,
        AdresseInput,
        VueTelInput
        
    },
    props : {
            scholarship: {
                require : true
            }
        },
    data : function() {
        return {
            files             : [],
            edit              : false,
            cropper           : false,
            uploaded          : false,
            loading           : false,
            organizationsList : [],
            InstitutionTypes  : [],
            selectedContients : [],
            customToolbar     :  [
                ["bold", "italic", "underline"], [{ list: "ordered" }, { list: "bullet" }]
            ],
            // data for amout field
            amount: {
                value : null,
                currency : null
            },
            levels            : [],
            categoryCriteria  : [],
            unlimitedCandidature: false,
            oldCandidatureLimit : null,
            allAge: false
        }
    },
    computed: {
        getAdresseMapSrc(){
            if(!this.scholarship.contactAdresseMap){
                return require('@/assets/img/map.png').default 
            }
            if(this.scholarship.contactAdresseMap instanceof Blob){
                return this.scholarship.contactAdresseMap
            } 

            return '/uploads/maps/'+this.scholarship.contactAdresseMap 
        },
    },
    methods: {
        /**
         * Handle file choosen by user
         * 
         * @param {Object} newFile 
         * @param {Object} oldFile 
         * @param {Object} prevent 
         * @return void
         */
        inputFile(newFile, oldFile, prevent) {
            if(newFile && !oldFile) {
                this.$nextTick(function () {
                    this.edit = true
                })
            }
            if (!newFile && oldFile) {
                this.edit = false
            }
        },

        /**
         * Handle and filtre user uploaded file
         * 
         * @param {Object} newFile 
         * @param {Object} oldFile 
         * @param {Object} prevent 
         * @returns void
         */
        inputFilter(newFile, oldFile, prevent) {
            let that = this
            if (newFile && !oldFile) {
                if (!/\.(jpg|jpeg|png)$/i.test(newFile.name)) {
                    this.dispatchError(that.$t("media.error.invalid_format"))
                   return prevent()
                }
            }

            let filetoCheck = newFile
            ImageHelper.getImageSize(filetoCheck, function(size){
                if(size.w < 250 || size.h < 250){
                    that.edit  = false
                    that.showImageUploader  = true
                    that.files = []
                    that.dispatchError(that.$t("media.error.invalid_size"))
                   
                }
            })

            if (newFile && (!oldFile || newFile.file !== oldFile.file)) {
                newFile.url = ''
                let URL = window.URL || window.webkitURL
                if (URL && URL.createObjectURL) {
                    newFile.url = URL.createObjectURL(newFile.file)
                    this.change = true
                }
            }
        },

        /**
         * Handle next steep clicked button
         * 
         * @return void
         */
        nextSteep(){
            if(this.validForm()){
                this.$emit("goTo", "ScholarshipGallery")
            }
        },

        /**
         * Check scholarship form infos validation 
         * 
         * @returns {Boolean}
         */
        validForm(){
            let form     = document.querySelector("#form-infos")
            
            const entries = Object.entries(this.scholarship) 

            let ignoreKeys = ["gallery","faculty",'cadidateOptionUrl','otherStudyFields','socialNetwork','contactAdresseMap','infosEmail']
            
            if(this.allAge){
                ignoreKeys = [...ignoreKeys, 'minAge', 'maxAge']
            }

            if(!form.otherStudyFields){
                ignoreKeys = [...ignoreKeys, 'otherStudyFieldTags']
            }

            if(this.unlimitedCandidature){
                ignoreKeys = [...ignoreKeys, "candidatureLimit"];
            }

            
            for (let i = 0; i < entries.length; i++) {
                const key = entries[i][0]
                let value = entries[i][1]
                let input = document.getElementById(key)
                
                if(ignoreKeys.includes(key)){
                    if(input){
                        input.classList.remove('invalid')
                    }

                    continue
                }

                if(key == 'count' && isNaN(value)){
                    input.scrollIntoView()
                    input.classList.add("invalid")
                    return false
                }
                
                if (value instanceof Object) {
                    if(!Object.keys(value).length){
                        input.scrollIntoView()
                        input.classList.add("invalid")
                        return false
                        break
                    }
                }
                
                if(value  == null ||  value == "" ){ 
                    input.scrollIntoView()
                    input.classList.add("invalid")
                    return false
                    break
                }
                
                if(typeof input !== "undefined" && input !== null )
                    input.classList.remove("invalid")

            }
            return true

        },

        /**
         * Handle on oraganization type change
         * 
         * @param {Object} value 
         */
        updateRelatedOrganizationList(value){
            let that = this
            ProfilService.getOrganizationList(value).then(
                (response) => {
                    if(response.data.organizations){
                        that.organizationsList = response.data.organizations
                    }
                } 
            ).catch(
                (error) => {
                    that.organizationsList = []
                }
            )
            
        },

        
        onContientChange(){
            let that = this
            that.selectedContients = []
            that.scholarship.grantedContinent.map((contient) => {
                that.selectedContients = [...that.selectedContients, contient.id]
            })
        },
        label_level(level) {
            return this.$t("word."+Helpers.removeExtraSpace(level.name).toLowerCase());
        },
        getLabel({id}){
            return this.$t(`organizationType.${id}`)
        },          
        /** input validation  **/
        validateNumberField(e){
            let value = e.target.value
            if(value.length && isNaN(value)){
                e.target.classList.add('invalid')
            }else{
                e.target.classList.remove('invalid')
            }
        },
        calculateAccount(){
            if((this.scholarship.amount > 0) && (this.scholarship.count > 0)){
            this.scholarship.totalOffered = this.scholarship.count * this.scholarship.amount 
            }else{
                this.scholarship.totalOffered = null
            }
        },
        updateMap(adresse){
            let that = this
            let selectedAdresse = adresse.formatted_address
            let apiImageAdrs = `https://maps.googleapis.com/maps/api/staticmap?center=${selectedAdresse}&&markers=size:mid|color:red|${selectedAdresse}&zoom=14&size=400x400&key=${process.env.GMAP_KEY}`
            ImageHelper.ImgToBase64URL(apiImageAdrs, function(base64Img){
                that.scholarship.contactAdresseMap = base64Img
            })
        },
        autoCompleteSocial(){
            let that = this 
            SocialNetworkService.getSocialNetwork(
                that.scholarship.relatedOrganization.id
            ).then(
                (response) => {
                    if(response.data.socialNetwork){
                        that.scholarship.socialNetwork = response.data.socialNetwork
                    }    
                }
            )
        },
        updateDisciplineCateg(val){
            let that = this
            
            let categs = []
            that.categoryCriteria = []
            that.scholarship.studyField.map((item)=>{
                categs = [...categs, item.id ]
            })
            that.categoryCriteria = categs
            
        },
        editSave() {
            this.edit = false
            let oldFile = this.files[0]
            let binStr = atob(this.cropper.getCroppedCanvas().toDataURL(oldFile.type).split(',')[1])
            let arr = new Uint8Array(binStr.length)
            for (let i = 0; i < binStr.length; i++) {
                arr[i] = binStr.charCodeAt(i)
            }
            let file = new File([arr], oldFile.name, { type: oldFile.type })
            file.url = ''
            let URL = window.URL || window.webkitURL
            if (URL && URL.createObjectURL) {
                file.url = URL.createObjectURL(file)
            }
            
            let fileData = {
                file,
                type: file.type,
                size: file.size,
                active: true,
            }
            
            this.$refs.upload.update(oldFile.id, fileData)
            this.scholarship.contactAvatar = fileData
            
        },
        closeCropper: function(){
            this.edit = false
        }
    },
    watch: {
        amount: {
            handler: function(val){
                this.scholarship.amount         = val.value
                this.scholarship.currency = val.currency
                this.calculateAccount()
            },
            deep: true
        },
        unlimitedCandidature: function(val){
            if(val){
                this.oldCandidatureLimit          = this.scholarship.candidatureLimit 
                this.scholarship.candidatureLimit = null
            }else{
                this.scholarship.candidatureLimit = this.oldCandidatureLimit
            }
        },
        edit(value) {
            if (value) {
                this.$nextTick(function () {
                if (!this.$refs.editImage) {
                    return
                }
                this.showImageUploader = false
                let cropper = new Cropper(this.$refs.editImage, {
                    aspectRatio: 1 / 1,
                    viewMode: 1,
                    minCropBoxWidth: 250,
                    minCropBoxHeight: 250,
                })
                this.cropper = cropper
                })
            } else {
                if (this.cropper) {
                this.cropper.destroy()
                this.cropper = false
                }
            }
        }
    },
    mounted: function(){
        console.log("mounted scholarship infos", this.scholarship)
        let that = this
        let local = that.$store.state.lang
        let data = Object.entries(
            require(`./../../../../lang/translations/organizationType/en.json`)
        );
        data.map((item) => {
            that.InstitutionTypes = [
            ...that.InstitutionTypes,
            { id: item[0], label: item[1] },
            ];
        });

    }
}