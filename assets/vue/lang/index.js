/*** Veillez à respecter l'indentation pour la lisiblité du code XD */

const fr = { 
    'word'             : require('./translations/word/fr.json') , 
    'menu'             : require('./translations/menu/fr.json') , 
    'organization'     : require('./translations/organization/fr.json'), 
    'page'             : require('./translations/page/fr.json'),
    'student'          : require('./translations/student/fr.json'),
    'nationality'      : require('./translations/nationality/fr.json'),
    'media'            : require('./translations/media/fr.json'),
    'continent'        : require('./translations/continent/fr.json'),
    'country'          : require('./translations/country/fr.json'),
    'modal'            : require('./translations/modal/fr.json'),
    'scholarship'      : require('./translations/scholarship/fr.json'),
    'country'          : require('./translations/country/fr.json'),
    'error'            : require('./translations/error/fr.json'),
    'todo'             : require('./translations/todo/fr.json'),
    'bot'              : require('./translations/bot/fr.json'),
    'el'               : require('./translations/element/fr.json'),
    
    /** dynamic translation file */
    'discipline'       : require('./../../../public/translations/discipline/fr.json'),
    'language'         : require('./translations/language/fr.json'),
    'institutionType'  : require('./translations/InstitutionType/fr.json'),
    'organizationType' : require('./translations/organizationType/fr.json'),
    'application'      : require('./translations/application/fr.json'),

}
const en = { 
    'word'             : require('./translations/word/en.json') , 
    'menu'             : require('./translations/menu/en.json') ,
    'organization'     : require('./translations/organization/en.json'),
    'page'             : require('./translations/page/en.json'),
    'student'          : require('./translations/student/en.json'),
    'nationality'      : require('./translations/nationality/en.json'),
    'media'            : require('./translations/media/en.json'),
    'continent'        : require('./translations/continent/en.json'),
    'country'          : require('./translations/country/en.json'),
    'modal'            : require('./translations/modal/en.json'),
    'scholarship'      : require('./translations/scholarship/en.json'),
    'country'          : require('./translations/country/en.json'),
    'error'            : require('./translations/error/en.json'),
    'todo'             : require('./translations/todo/en.json'),
    'bot'              : require('./translations/bot/en.json'),
    'el'               : require('./translations/element/en.json'),

    /** dynamic translation file */
    'discipline'       : require('./../../../public/translations/discipline/en.json'),
    'language'         : require('./translations/language/en.json'),
    'institutionType'  : require('./translations/InstitutionType/en.json'),
    'organizationType' : require('./translations/organizationType/en.json'),
    'application'      : require('./translations/application/en.json'),
}

export default {
    fr,
    en
}
/*** Veillez à respecter l'indentation pour la lisiblité du code XD */