
import Security from '../security';

const home = [
    {
        name : "home",
        path: "/",
        meta : { layout : 'no-sidebar'},
        components: require('@/view/home/Home.vue'),
        ...Security.isGranted('ANONYMOUS_ONLY')
    }
]

export default home