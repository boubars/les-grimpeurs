import Security from '../security';
const    routes = [
    {
        name : "scholarship_search",
        path: "/user/scholarship",
        components: require("@/view/scholarship/search/ScholarshipSearch.vue"),
        params: {
            searchParams : false
        }
    },
    {
        name : "scholarship_new",
        path: "/user/scholarship/new",
        components: require("@/view/scholarship/form/NewScholarship.vue"),
    },
    {
        name : "scholarship_edit",
        path: "/user/scholarship/:slug/edit",
        components: require("@/view/scholarship/form/EditScholarship.vue"),
    },
    {
        name : "scholarship_page",
        path: "/scholarship/:slug",
        components: require("@/view/scholarship/page/Scholarship.vue"),
    },
    {
        name: "scholarship_application_form",
        path: "/scholarship/:slug/application",
        meta : {layout : 'default'},
        components: require('@/view/scholarship/application/DefaultForm/DefaultForm.vue'),
        ...Security.isGranted('ROLE_STUDENT')
    },
    {
        name: "user-favoris",
        path: "/user/favoris",
        meta : {layout : 'default'},
        components: require('@/view/scholarship/favoris/Favoris.vue')
    },

]
export default routes