import   Security from '../security'
const StudentsRoute = [
    {
        name : "student_home",
        path: "/student",
        meta : { layout : 'no-sidebar'},
        components: require('@/view/home/Student/Student.vue'),
        ...Security.isGranted('ANONYMOUS_ONLY')  
    },
    {
        path: "/student/registration",
        name: "student_registration",
        meta : {layout : 'no-sidebar'},
        components: require("@/view/student/registration/StudentRegistration.vue")
    },
    {
        name : "student_profil",
        path: "/user/student/profil",
        meta : {layout : 'default'},
        components: require("@/view/student/profil/Profil.vue"),
        ...Security.isGranted('ROLE_STUDENT')  
    },
    {
        name : "student_profil_public",
        path: "/student/:id",
        meta : {layout : 'default'},
        components: require("@/view/student/profil/ProfilPublic.vue")
    },

    {
        name : "student_dashboard",
        path: "/user/student/dashboard",
        meta : {layout : 'default'},
        components: require("@/view/student/dashboard/StudentDashboard.vue"),
        ...Security.isGranted('ROLE_STUDENT')  
    },
    {
        name : "student_search",
        path: "/user/student",
        components: require("@/view/student/search/StudentSearch.vue"),
        params: {
            searchParams : false
        },
        ...Security.isGranted('ROLE_ORGANIZATION')  
    },
    {
        name : "student_application_list",
        path: "/user/student/application",
        components: require("@/view/student/application/ApplicationList.vue"),
        ...Security.isGranted('ROLE_STUDENT')  
    },
    {
        name : "student_application_detail",
        path: "/user/student/application/:id",
        components: require("@/view/student/application/ApplicationDetail.vue"),
        ...Security.isGranted('ROLE_STUDENT')  
    },

]
export default StudentsRoute