import Security from '../security';

const    routes = [
    {
        name:"login",
        path: "/login",
        components: require('@/view/login/Login.vue'),
        meta : {layout : 'default'},
        ...Security.isGranted('ANONYMOUS_ONLY')

    },
    {
        name:"reset_password",
        path: "/reset/password",
        components: require('@/view/login/ResetPassword.vue'),
        meta : {layout : 'default'}
    },
    {
        name:"change_password",
        path: "/user/change/password",
        components: require('@/view/login/ChangePassword.vue'),
        meta : {layout : 'default'}
    },
    {
        name:"2factorlogin",
        path: "/authentification",
        components: require('@/view/login/Authentification.vue'),
        meta : {layout : 'default'},
        ...Security.isGranted('ANONYMOUS_ONLY')
    },
    {
        name : "user_activation_successfull",
        path : "/user/activation/successfull",
        components : require('@/view/user/SuccessActivation.vue')

    },
    {
        name : "error_404",
        path : "*",
        components : require('@/view/page/error/404.vue')

    },
    {
        name : "error_403",
        path : "/access-denied",
        components : require('@/view/page/error/403.vue')

    }
]
export default routes