/**
 * @ Author: DADAY Andry
 * @ Create Time: 2021-12-02 14:43:50
 * @ Modified by: DADAY Andry
 * @ Modified time: 2021-12-02 15:53:47
 * @ Description: custom vue Security route guard 
 */
import AuthService from "@/services/auth/auth.service.js"
const Security = {
    /**
     *  The Security method handles the route before enter.
     *  
     * @param {MIXED} roles 
     */
    isGranted: function(roles){
        if(roles === 'ANONYMOUS_ONLY'){
            return this.anonymousOnly
        }

        if(typeof roles === 'string' || roles instanceof String){
            return this.authenticatedRole(roles)
        }
    },

    /**
     * Check if the current user is authenticated, then redirect to the fallback url
     * 
     * @return void
     */
    anonymousOnly : {
        beforeEnter:function(to, from, next){
            let that = Security
            AuthService.checkUserToken().then(
                (response) => {
                    if(response.data.token){
                        let roles = response.data.roles
                        next(that.getFallbackRoute(roles[0]))
                    }else{
                        next()
                    }  
                }
            ).catch(
                (error) => {
                    next()
                }
            )
        }
    },

    /**
     * Check if current user is authenticated, ele redirect to login
     * 
     * @return void
     */
    authenticatedUser: {
        beforeEnter:function(to, from, next){
            AuthService.checkUserToken().then(
                (response) => {
                    if(response.data.token){
                        next()
                    }else{
                        next({name: 'login'})
                    }  
                }
            ).catch(
                (error) => {
                    next({name: 'login'})
                }
            )
        }
    },

    /**
     * Check if current user is authenticated and have a role "r", else redirect to login
     * 
     * @param {String} r 
     * @returns void
     */
    authenticatedRole: function(r){
        return {
            beforeEnter: function(to, from, next, role = r){
                AuthService.checkUserToken().then(
                    (response) => {
                        if(response.data.token){
                            let roles = response.data.roles
                            if(roles[0] == r){
                                next()
                            }else{
                                next({name: 'error_403'})
                            }
                        }else{
                            next({name: 'login'})
                        }  
                    }
                ).catch(
                    (error) => {
                        next({name: 'login'})
                    }
                )
            }
        }
    },
    
    /**
     * Get fallback url according user role
     * 
     * @param   {String} role 
     * @returns {Object|String}
     */
    getFallbackRoute: function(role){
        switch (role) {
            case 'ROLE_ORGANIZATION':
                return { name: 'organization_profil'}
            break;
            case 'ROLE_STUDENT':
                return { name: 'student_dashboard'}
            break;
            case 'ROLE_ADMIN':
                return "/fr/admin/index"
            break;
        
            default:
                return {name: 'login'}
            break;
        }
    },
}

export default Security