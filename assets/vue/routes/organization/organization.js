import   Security from '../security'
const    routes = [
    {
        name      : "organization_home",
        path      : "/organization",
        meta      : { layout : 'no-sidebar'},
        components: require("@/view/home/Organization/Organization.vue"),
        ...Security.isGranted('ANONYMOUS_ONLY')
    },
    {
        path      : "/user/organization/profil",
        name      : "organization_profil",
        meta      : {layout : 'default'},
        components: require("@/view/organization/profil/PagePrivate.vue"),
        ...Security.isGranted('ROLE_ORGANIZATION')  
    },
    {
        name      : "organization_page",
        path      : "/organization/:slug",
        components: require("@/view/organization/profil/PagePublic.vue")
    },
    {
        name      : "organization_dashboard",
        path      : "/user/organization/dashboard",
        meta      : {layout : 'default'},
        components: require("@/view/organization/dashboard/OrganizationDashboard.vue"),
        ...Security.isGranted('ROLE_ORGANIZATION'),   
    },
    {
        name : "organization_scholarships",
        path: "/user/organization/scholarships",
        meta : {layout : 'default'},
        components: require("@/view/organization/myscholarships/MyScholarships.vue"),
        ...Security.isGranted('ROLE_ORGANIZATION')  
    },
    {
        name : "organization_stat_preview",
        path: "/user/organization/stat-preview",
        meta : {layout : 'default'},
        components: require("@/view/organization/dashboard/statPreview/OrganizationStatPreview.vue"),
        ...Security.isGranted('ROLE_ORGANIZATION')  
    },
    {
        name : "organization_folder_list",
        path: "/user/organization/application-folders",
        components: require("@/view/organization/application/FormList/FolderList.vue"),
        ...Security.isGranted('ROLE_ORGANIZATION')  
    },
    {
        name : "organizaiton_application_folder_open",
        path: "/user/organization/folder/:slug",
        components: require("@/view/organization/application/FormList/FolderOpen.vue"),
        ...Security.isGranted('ROLE_ORGANIZATION')  
    },
    {
        name : "organization_folder_form_new",
        /**
         * @param {String} slug  [folder slug]
         */
        path: "/user/organization/folder/:slug/new-form",
        components: require("@/view/organization/application/FormBuilder/NewForm.vue"),
        ...Security.isGranted('ROLE_ORGANIZATION')  
    },
    {
        name : "organization_folder_form_edit",
        /**
         * @param {String} slug  [folder slug]
         * @param {String} form  [form   slug] 
         */
        path: "/user/organization/folder/:slug/:form/edit",
        components: require("@/view/organization/application/FormBuilder/EditForm.vue")
    },
    {
        name : "organization_folder_form_preview",
        /**
         * @param {String} slug  [folder slug]
         * @param {String} form  [form   slug] 
         */
        path: "/user/organization/folder/:slug/:form/preview",
        components: require("@/view/organization/application/FormBuilder/PreviewForm.vue")
    },
    {
        name : "organization_candidat_list",
        path: "/user/organization/candidat-list",
        components: require("@/view/organization/application/CandidatList/CandidatList.vue"),
        ...Security.isGranted('ROLE_ORGANIZATION')  
    },
    {
        name : "organization_candidat_detail",
        path: "/user/organization/application/:id",
        components: require("@/view/organization/application/ApplicationDetail/ApplicationDetail.vue"),
        ...Security.isGranted('ROLE_ORGANIZATION')  
    },
    {
        name : "organization_student_applications",
        path: "/user/organization/application/student/:id",
        components: require("@/view/organization/application/StudentApplication/StudentApplicationDetail.vue"),
        ...Security.isGranted('ROLE_ORGANIZATION')  

    },

]
export default routes