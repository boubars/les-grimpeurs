const    routes = [
    {
        name: "page-index",
        path: "/page/:slug",
        components: require('@/view/page/default/DefaultPage.vue')
    },
    {
        name: "page-contact",
        path: "/contact",
        meta : {layout : 'default'},
        components: require('@/view/page/contact/Contact.vue')
    },
    {
        name: "page-press",
        path: "/press",
        meta : {layout : 'default'},
        components: require('@/view/page/press/Press.vue')
    },
    {
        name: "grimpeurs-teams",
        path: "/team",
        components: require('@/view/page/team/Team.vue')
    },
    {
        name: "grimpeurs-concept",
        path: "/concept",
        components: require('@/view/page/concept/Concept.vue')
    },
    {
        name: "grimpeurs-faq",
        path: "/faq",
        components: require('@/view/page/faq/Faq.vue')
    }
 ]
export default routes