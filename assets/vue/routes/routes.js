import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter)

import Organization from "./organization/organization"
import Home from "./home/home"
import Student from "./student/student"
import Common from './common/common.js'
import Page from "./page/page"
import Scholarship from "./scholarship/scholarship"
const routes = new VueRouter({
    mode: "history",
    routes: [
        ...Home ,
        ...Organization,
        ...Common,
        ...Page,
        ...Student,
        ...Scholarship
    ]
  })
export default  routes;