import Vue            from 'vue'
import Vuex           from 'vuex'
import { user }       from './modules/user.js'
import lang           from './modules/lang.js'
import newscholarship from './modules/newscholarship.js'
import sidebarstatus  from './modules/sidebarstatus.js'
import site           from './modules/site.js'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user, 
    lang,
    newscholarship,
    sidebarstatus,
    site
  }
})