import PreferenceService from "@/services/common/PreferenceService.js"

const current = JSON.parse(localStorage.getItem('grimpeurs-site'))

const initialState = current ? current : {
    params: {}
}

const site = {
    namespaced: true,
    state: initialState,

    getters: {
        getParams:(state) => (name = false , local = false) => {
            let params = state.params
            if(name && local){
                return params.filter(param => {
                   return param.name == name && param.lang == local
                })
            }
            if(name){
                return params.filter(param => {
                   return param.name == name
                })
            }
            if(local){
                return params.filter(param => {
                   return param.lang == local
                })
            }

            return params
        }
    },
    actions: {
        updateInfos({commit}){
            return new Promise((resolve, reject) => {
                PreferenceService.getParam().then((response) => {
                    commit('updateInfos', response.data.params)
                    resolve(response)
                })
            })
        }
    },
    mutations: {
        updateInfos(state, newParams) {
            state.params = newParams
            localStorage.setItem('grimpeurs-site', JSON.stringify(newParams))
      }
    }
  }
export default site 