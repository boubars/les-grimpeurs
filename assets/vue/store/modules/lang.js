const curentLang = localStorage.getItem('lang');

const initialState = curentLang ? curentLang : (navigator.language.split('-')[0] || 'en')

const lang = {
    namespaced: true,
    state: initialState,
    getters: {
      getState: function(state){
        return state
      }
    },
    actions: {
      switch({ commit },newLang) {
        localStorage.setItem('lang', newLang);
        commit("switch", newLang)
      }
    },
    mutations: {
        switch(state, newlang) {
            state = newlang
      }
    }
  }
export default lang 