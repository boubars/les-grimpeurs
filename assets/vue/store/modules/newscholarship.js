const scholarship = JSON.parse(localStorage.getItem('newscholarship'));
const initialState = { object: scholarship ? scholarship : false  }

const newscholarship = {
    namespaced: true,
    state: initialState,
    mutations: {
        update(state, scholarship) {
            state.object = scholarship
            localStorage.setItem('newscholarship', JSON.stringify(scholarship))
        },
    },
    actions: {
        update({ commit }, scholarship) {
            commit('update', scholarship)
        },
        purge({commit}){
            commit('update', false)
        }
    }
}
export default newscholarship