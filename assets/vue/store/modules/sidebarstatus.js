const initialState = localStorage.getItem('sidebaropen') ?? 0


const sidebarstatus = {
    namespaced: true,
    state: initialState,
    mutations: {
        switchStatus(state) {
            let currentState = localStorage.getItem('sidebaropen') ?? 0
            let newState = currentState == 0 ? 1 : 0
            localStorage.setItem('sidebaropen', newState)
        },
    },
    actions: {
        switch({ commit }) {
            commit('switchStatus')
        }
    }
}

export default sidebarstatus