import AuthService from '@/services/auth/auth.service';
import UserService from '@/services/auth/user.service.js';

const auth = JSON.parse(localStorage.getItem('les-grimpeurs-user'));
const initialState = auth
  ? { status: { loggedIn: true }, profil : auth }
  : { status: { loggedIn: false }, profil : null };

export const user = {
  namespaced: true,
  state: initialState,
  actions: {
    login({ commit }, data) {
      return AuthService.login(data).then(
        data => {
          return Promise.resolve(data);
        },
        error => {
          commit('loginFailure');
          return Promise.reject(error);
        }
      );
    },
    logout({ commit }) {
        AuthService.logout()
    },
    updateInfos({commit}){
      UserService.getProfil().then((response) => {
        if(response.data.user && response.data.user != null && response.data.user != 'null' ){
          commit('updateInfos', response.data.user)
        }else{
          /*** force logout  */
          commit('logout')
          document.location.href = "/login"
          return 
        }
      })
    }
  },
  mutations: {
    loginSuccess(state, data) {
      state.status.loggedIn = true;
      state.profil = user;
    },
    loginFailure(state) {
      state.status.loggedIn = false;
      state.profil = null;
    },
    logout(state) {
      state.status.loggedIn = false;
      state.profil = null;
    },
    updateInfos(state, user){
      /** calculate profile completion rate */
      let completedInfos = 0  
      let rapport = -1 

      let ignoreKeys = [
          'medias',
          'profil_completion',
          'date_of_birth',
          'adress','updated_at', 
          'hidden_fields', 
          'search_preferences',
          'siret_number',
          'accreditation'
        ]
      /** ignore student social networks */
      if(user.roles[0] == "ROLE_STUDENT"){
        ignoreKeys = [...ignoreKeys, 'site_url', 'social_network', 'tag']
      }

      
      Object.keys(user).map(function(key, index) {
        let value = user[key] 
        if( !ignoreKeys.includes(key)){
          rapport++
          if((value !== null)){
            if(Object.keys(value).length){
              completedInfos++
            }
          }
        }
      });

      let profil_completion = Math.floor((completedInfos / rapport) * 100)
      
      if(user.profil_completion){
        user.profil_completion = profil_completion
      }else{
        user = {...user, profil_completion }
      }
      state.profil = user
      localStorage.setItem('les-grimpeurs-user', JSON.stringify(user));
    }
  }
};